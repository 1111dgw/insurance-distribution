import { RequestUrl } from 'src/common/url';
import { RouteUrl, Mutations, SaveName, WE_CHAT_ENTERPRISE } from 'src/common/const';
export default {
    weChatOauth(_this, parameters) {
        let ua = window.navigator.userAgent.toLowerCase();
        if (ua.match(/MicroMessenger/i) != 'micromessenger') {
            return;
        }
        let url = window.location.href;
        if (url.indexOf("userid") > 0) {
            //userid有效
            let userid = _this.$common.getShareUrlKey(url, "userid");
            //根据userId查询数据库的企业微信账户
            let initData = { TYPE: "FORM", userId: userid, };
            _this.$http.post(RequestUrl.GET_USER_BY_USER_ID, initData).then(function (res) {
                if (res.state == 0) {
                    //保存USER_INFO信息
                    localStorage[SaveName.LOCAL_STORAGE_USER_ENTERPRISE] = JSON.stringify(res.result);

                    _this.sportsDto.departmentId = res.result.departmentId;// 部门ID
                    _this.sportsDto.departmentName = res.result.departmentName;// 部门名称
                    _this.sportsDto.userId = res.result.userid;// 账号ID
                    _this.sportsDto.userName = res.result.name;// 账号名称
                    _this.isDisabled = false;//可提交
                    return;
                }
            });
        } else {
            if (url.indexOf("msg") > 0) {
                let msg = _this.$common.getShareUrlKey(url, "msg");
                _this.isDisabled = true;//不可提交表单
                _this.$common.showMsg(_this, msg, "red;", true); //显示错误信息
                return;
            }
            //发起授权
            this.sendRequest(parameters);
        }
    },
    //授权请求
    sendRequest(parameters) {
        let REDIRECT_CONTROLLER = encodeURIComponent(RequestUrl.WECHAT_OAUTH2_ENTERPRISE_REDIRECT_URI);//回调接口
        let REDIRECT_URI = encodeURIComponent(RequestUrl.WECHAT_OAUTH2_ENTERPRISE_AFTER_URL);//接口参数
        let SCOPE = "snsapi_userinfo";
        let url = "https://open.weixin.qq.com/connect/oauth2/authorize"
            + "?appid=" + WE_CHAT_ENTERPRISE.CORPID
            + "&redirect_uri=" + REDIRECT_CONTROLLER + "?url=" + REDIRECT_URI
            + "&response_type=code&scope=" + SCOPE
            + "&agentid=" + WE_CHAT_ENTERPRISE.AGENTID_AYD
            + "&state=" + parameters + "#wechat_redirect";
        window.location.href = url;
    }
}