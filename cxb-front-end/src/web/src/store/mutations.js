import { Mutations, RouteUrl, Url_Key, SaveName, TO_TYPE, SERVICE_CODE } from 'src/common/const';
import DateUtil from "src/common/util/dateUtil";
import { RequestUrl } from 'src/common/url';
import { setTimeout } from 'timers';

export default {
  [Mutations.SET_VAL_DATA](state, _this) {
    _this.$common.getJwtVal();//执行jwt第一次获取token的方法
  },
  [Mutations.GET_IP_BAIDU_REGION](state, _this) {
    if (!state.isQuery) {
      _this.$common.storeCommit(_this, Mutations.INIT, _this);
      state.isQuery = true;
      sessionStorage["ACC_LOG"] = "";
      state.initIndexData.region = "恒华保险代理有限公司";
      // sessionStorage["ACC_LOG"] += "访问ip接口-";
      // //本地token
      // let localToken = localStorage[SaveName.JWT_TOKEN_NAME];
      // $.ajax({
      //   type: "GET",
      //   url: RequestUrl.IP_BAIDU_REGION,
      //   data: "",
      //   timeout: 1000,
      //   async: false,
      //   dataType: 'jsonp',
      //   headers: {
      //     "authorization": localToken,
      //     // "Accept": "application/json; charset=utf-8",
      //     // "Content-Type": "application/x-www-form-urlencoded",
      //   },
      //   success: function (res, status, xhr) {
      //     let addArray = res.address.split("|");
      //     state.initIndexData.region = addArray[1];
      //     sessionStorage["ACC_LOG"] += "成功:" + addArray[1];
      //     state.initIndexData.pointX = res.content.point.x;
      //     state.initIndexData.pointY = res.content.point.y;
      //     _this.$common.storeCommit(_this, Mutations.INIT, _this);
      //   },
      //   error: function (XMLHttpRequest, textStatus, errorThrown) {
      //     sessionStorage["ACC_LOG"] += "失败";
      //     state.initIndexData.region = "重庆";
      //     state.initIndexData.pointX = "";
      //     state.initIndexData.pointY = "";
      //     _this.$common.storeCommit(_this, Mutations.INIT, _this);
      //   },
      // });
    }
  },
  [Mutations.INIT](state, _this) {
    sessionStorage["ACC_LOG"] += "<br>准备初始化数据-";
    if (!sessionStorage["IS_JWT_QUERY"] == "T") {
      _this.$common.storeCommit(_this, Mutations.SET_VAL_DATA, _this);
    }
    sessionStorage["SHOP_INDEX_FORM_PATH"] = "";
    sessionStorage["INSURANCE_FORM_URL"] = "";
    let id = "";
    if (_this.user.isLogin) {
      id = _this.user.userDto.userCode;
    }
    state.initIndexData.userId = id;
    let curUrl = "";
    let url = window.location.href;

    //记录当前进入访问地址和时间 
    let curUrlLog = "ACC:" + url + "    ACC_TIME:" + DateUtil.getNowDateStr_Ymdhm(new Date());

    //记录历史最近一次带shareid的访问地址和id
    if (url.indexOf("?") > -1 && (url.indexOf("shareid") > -1 || url.indexOf("shareId") > -1)) {
      localStorage["BASE_URL_ACC_LOG"] = "  HI:" + url + "--HI_TIME:" + DateUtil.getNowDateStr_Ymdhm(new Date());
      localStorage["BASE_URL_ACC"] = url;
    }

    //LCB路由保护机制，如果当前不访问地址不存在shareid 就取历史最近一次带有shareid的地址
    if (url.indexOf("shareid") < 0 && url.indexOf("shareId") < 0) {
      let baseUrl = localStorage["BASE_URL_ACC"];
      if (_this.$common.isNotEmpty(baseUrl) && (baseUrl.indexOf("LCB") > -1 || baseUrl.indexOf("lcb") > -1)) {
        curUrl = url;
        url = baseUrl;
      }
    }


    // let urlLowCase = url.toLowerCase();

    if (_this.$route.path == RouteUrl.DETAILS) {
      //用于判断支付成功之后的跳转地址  如果是直接点开的详情页面则设置为F 支付成功后跳转首页
      localStorage["IS_SHOP_DETAIL"] = "F";
    }

    //判断是否支付成功页面进入
    if (_this.$route.path == RouteUrl.INSURESUCCESS) {
      //支付成功需要把地址赋值为支付前进入的地址
      url = localStorage["HOME_RUL_SAVE"];

      //排除地址中不包含注册页面
    } else if (url.indexOf("register") < 0 && _this.$route.path != RouteUrl.REGISTER_ACCEPT) {
      localStorage["HOME_RUL_SAVE"] = url;
    }

    //最近一次带shareid访问的日志
    let hiLog = "没有历史记录"
    if (typeof (localStorage["BASE_URL_ACC_LOG"]) != "undefined") {
      hiLog = localStorage["BASE_URL_ACC_LOG"];
    }
    state.accessUrl = curUrlLog + "<br>" + hiLog + "<br>" + "  USE_URL：" + url + "  -UUID:" + state.uuid;

    if (url.indexOf(RouteUrl.SHOP_INDEX) > -1) {
      state.initIndexData.isShop = true;
      state.isShop = true;
      // _this.$common.storeCommit(_this, Mutations.SET_IS_SHOP, true);
    }

    // if (url.indexOf(RouteUrl.DETAILS) < 0) {
    let uuid = "";
    // url = decodeURIComponent(url);
    uuid = _this.$common.getShareUrlKey(url, Url_Key.SHARE_UUID);

    let shareData = {
      _this: _this,
      callBackUrl: "",
      singleMember: "",
      successfulUrl: "",
    }
    shareData.callBackUrl = _this.$common.getShareUrlKey(url, Url_Key.REDIRECT_URL);
    shareData.singleMember = _this.$common.getShareUrlKey(url, Url_Key.RECORDED_ID);
    shareData.successfulUrl = _this.$common.getShareUrlKey(url, Url_Key.SUCC_URL);
    _this.$common.storeCommit(_this, Mutations.SET_SHARE_DATA, shareData);

    let aggerData = {
      _this: _this,
      agreementCode: "",
      salerName: "",
      salerNumber: "",
      channelName: "",
    }
    aggerData.agreementCode = _this.$common.getShareUrlKey(url, Url_Key.AGREEMENT_CODE);
    aggerData.salerName = _this.$common.getShareUrlKey(url, Url_Key.SALER_NAME);
    aggerData.salerNumber = _this.$common.getShareUrlKey(url, Url_Key.SALER_NUMBER);
    aggerData.channelName = _this.$common.getShareUrlKey(url, Url_Key.CHANNEL_NAME);
    _this.$common.storeCommit(_this, Mutations.SAVE_SHARE_AGGER_DATA, aggerData);

    let shareInfo = {
      _this: _this,
      shareUid: "",
    }
    shareInfo.shareUid = _this.$common.getShareUrlKey(url, Url_Key.UID);
    _this.$common.storeCommit(_this, Mutations.SAVE_SHARE_OTHER_INFO, shareInfo);

    let dealUid = _this.$common.getShareUrlKey(url, Url_Key.DEAL_UUID);
    if (!_this.$common.isEmpty(dealUid)) {
      state.dealUid = dealUid;
    }

    if (uuid != null && uuid.length > 5 && uuid.indexOf("http") == -1) {
      state.initIndexData.uuid = uuid;
      sessionStorage["SESSION_SHARE_ID"] = uuid;
      _this.$common.storeCommit(_this, Mutations.REF_SHARE_U_UID, uuid);
      // alert("home:" + _this.saveInsure.refShareUUid);
      _this.$common.setUuid(_this, uuid);
    } else {
      if (typeof (sessionStorage["SESSION_SHARE_ID"]) != "undefined" && sessionStorage["SESSION_SHARE_ID"] != "undefined") {
        uuid = sessionStorage["SESSION_SHARE_ID"];
        state.initIndexData.uuid = uuid;
        _this.$common.storeCommit(_this, Mutations.REF_SHARE_U_UID, uuid);
        _this.$common.setUuid(_this, uuid);
      } else {
        uuid = "";
      }
    }
    // alert("home:" + uuid);
    // alert(uuid);
    // }
    // alert(sessionStorage["SESSION_SHARE_ID"]);
    // this.initIndexData.uuid = this.shareUuid;
    if (_this.user.isLogin) {
      state.initIndexData.userCode = state.user.userDto.userCode;
    }
    //本地token
    let localToken = localStorage[SaveName.JWT_TOKEN_NAME];
    state.initIndexData.markUuid = state.uuid;
    try {
      //  请求基本信息
      $.ajax({
        type: "POST",
        url: RequestUrl.PRODUCT_MENU_AD,
        data: state.initIndexData,
        async: false,
        headers: {
          "authorization": localToken,
          // "Accept": "application/json; charset=utf-8",
          // "Content-Type": "application/x-www-form-urlencoded",
        },
        success: function (res, status, xhr) {
          sessionStorage["ACC_LOG"] += "-成功";
          _this.$common.reJwtVal(res);
          // let jwtToken = xhr.getResponseHeader(tokenName);
          // // console.log("第一次获取：" + jwtToken);
          // //保存token
          // localStorage[tokenName] = jwtToken;

          // sessionStorage["dataTime"] = res.result.time;
          //第一次存储请求标识码，便于其他接口请求校验
          // sessionStorage[tokenName] = xhr.getResponseHeader(tokenName);
          if (res.result.agr != false && _this.$common.isNotEmpty(res.result.agr)) {
            _this.$common.storeCommit(_this, Mutations.SET_REFEREE_DATA, res.result.agr);

          }

          _this.$common.storeCommit(_this, Mutations.SAVE_HOT_AREA, res.result.hotarea);
          // let array = res.result.addre.split("	");
          // _this.$store.commit(Mutations.INSURE_SET_REGION, array);
          //---------------初始化 index广告信息-----------------
          let adList = [];
          for (let i = 0; i < res.result.advert.length; i++) {
            let adBase = {
              url: res.result.advert[i].toUrl,
              img: res.result.advert[i].imgUrl,
              title: res.result.advert[i].description,
              toType: res.result.advert[i].toType,
              toUrl: res.result.advert[i].toUrl,
              detail: res.result.advert[i].detail,
              id: res.result.advert[i].id,
              productId: res.result.advert[i].productId,
              adType: res.result.advert[i].adType,
            };
            if (res.result.advert[i].toType == TO_TYPE.POP || res.result.advert[i].toType == TO_TYPE.POP_ALL || res.result.advert[i].toType == TO_TYPE.ACTIVITY) {
              state.popAd.push(adBase);
            } else {
              adList.push(adBase);
            }
          }
          //---------------获得首页展示的信息-------------------
          // let indexMenuList = [];
          // let serverMenu = {};
          // let serverMenuType = [];

          // let shopMenu = {};
          // let shopMenuType = [];
          // let menuList = res.result.menu;
          // for (let i = 0; i < menuList.length; i++) {
          //   if (menuList[i].isFirst == "1") {
          //     indexMenuList.push(menuList[i]);
          //   }
          //   if (menuList[i].serviceType != "更多") {
          //     if (menuList[i].serviceCode == SERVICE_CODE.SERVICE) {
          //       if (typeof serverMenu[menuList[i].serviceType] == "undefined") {
          //         serverMenu[menuList[i].serviceType] = [];
          //         serverMenuType.push(menuList[i].serviceType);
          //       }
          //       serverMenu[menuList[i].serviceType].push(menuList[i]);
          //     } else if (menuList[i].serviceCode == SERVICE_CODE.SHOP) {
          //       if (typeof shopMenu[menuList[i].serviceType] == "undefined") {
          //         shopMenu[menuList[i].serviceType] = [];
          //         shopMenuType.push(menuList[i].serviceType);
          //       }
          //       shopMenu[menuList[i].serviceType].push(menuList[i]);
          //     }
          //   }
          // }
          // debugger

          //-----------------获得险种类别-----------------------
          // let riskCode = res.result.riskCode;
          // let riskCodeList = [];

          // for (let i = 0; i < res.result.initData.length; i++) {
          //   if (res.result.initData[i].code == "1") {
          //     let riskKeyValue = {
          //       key: res.result.initData[i].codeEname1,
          //       value: res.result.initData[i].codeType1
          //     };
          //     riskCodeList.push(riskKeyValue);
          //   }
          // }

          //-----------获得url-----------------------------------
          let urlList = res.result.initUrlData;
          let urlData = {};
          for (let i = 0; i < urlList.length; i++) {
            urlData[urlList[i].serviceName] = urlList[i].url;
          }

          for (let i = 0; i < res.result.product.length; i++) {
            let parm = {
              type: 'add',
              id: res.result.product[i].id,
              product: res.result.product[i]
            }
            _this.$common.storeCommit(_this, Mutations.SET_PRODUCT_LIST, parm);
          }


          let browser = {
            versions: function () {
              let u = navigator.userAgent, app = navigator.appVersion;
              return {//移动终端浏览器版本信息 
                trident: u.indexOf('Trident') > -1, //IE内核
                presto: u.indexOf('Presto') > -1, //opera内核
                webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
                gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
                mobile: !!u.match(/AppleWebKit.*Mobile.*/) || !!u.match(/AppleWebKit/), //是否为移动终端
                ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
                android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或者uc浏览器
                iPhone: u.indexOf('iPhone') > -1 || u.indexOf('Mac') > -1, //是否为iPhone或者QQHD浏览器
                iPad: u.indexOf('iPad') > -1, //是否iPad
                webApp: u.indexOf('Safari') == -1 //是否web应该程序，没有头部与底部
              };
            }(),
            language: (navigator.browserLanguage || navigator.language).toLowerCase()
          }
          if (browser.versions.ios || browser.versions.iPhone || browser.versions.iPad) {
            localStorage["BOTYPE"] = "I";

          }
          else if (browser.versions.android) {
            localStorage["BOTYPE"] = "A";
          } else {
            localStorage["BOTYPE"] = "O";
          }

          //初始化存储需要展示的信息
          let initData = {
            hotInsuList: res.result.product,
            // indexMenuList: indexMenuList,
            adList: adList,
            // serverMenu: serverMenu,
            // serverMenuType: serverMenuType,
            // riskCodeList: riskCodeList,
            // initData: res.result.initData,
            initUrl: urlData,
            // shopMenu: shopMenu,
            // shopMenuType: shopMenuType
          };
          _this.$common.storeCommit(
            _this,
            Mutations.INDEX_QUERY_DATA,
            initData
          );
          state.isQuery = false;
          state.isInitData = true;
        }, error: function (XMLHttpRequest, textStatus, errorThrown) {
          sessionStorage["ACC_LOG"] += "-失败";
          sessionStorage["ACC_LOG"] += "<br>请求数据:" + JSON.stringify(state.initIndexData) + "---JWT:" + localToken;
        },
      });
    } catch (e) {
      sessionStorage["ACC_LOG"] += "-失败:" + e;
      sessionStorage["ACC_LOG"] += "<br>请求数据:" + JSON.stringify(state.initIndexData) + "---JWT:" + localToken;
    }
  },
  //版本号控制用户数据是否最新
  [Mutations.IS_QUERY_HEADER](state, isQueryHeader) {
    state.isQueryHeader = isQueryHeader;
  },
  //请求广告
  // [Mutations.ADVERT_INIT](state, _this) {
  //   let formData = new FormData();
  //   formData.append("sorttype", "ASC");
  //   _this.$http.post(RequestUrl.ADVERT_INIT, formData).then(function (res) {
  //     let adList = [];
  //     for (let i = 0; i < res.result.length; i++) {
  //       let ad = res.result[i];
  //       let adBase = {
  //         url: ad.toUrl,
  //         img: ad.imgUrl,
  //         title: ad.description,
  //         toType: ad.toType,
  //         toUrl: ad.toUrl,
  //         detail: ad.detail,
  //         id: ad.id,
  //         productId: ad.productId,
  //         adType: ad.adType,
  //       };
  //       if (ad.toType == TO_TYPE.POP || ad.toType == TO_TYPE.POP_ALL || ad.toType == TO_TYPE.ACTIVITY) {
  //         state.popAd.push(adBase);
  //       } else {
  //         adList.push(adBase);
  //       }
  //     }
  //     state.isQueryAd = true;
  //     state.adList = adList;

  //   });
  // },
  //请求菜单信息
  [Mutations.QUERY_MENU_DATA](state, _this) {
    let formData = new FormData();
    formData.append("sorttype", "ASC");
    _this.$http.post(RequestUrl.QUERY_MENU_DATA, formData).then(function (res) {

      let indexMenuList = [];
      let serverMenu = {};
      let serverMenuType = [];

      let shopMenu = {};
      let shopMenuType = [];
      shopMenu['店铺管理'] = [];
      shopMenu['销售工具'] = [];
      shopMenu['我的团队'] = [];

      let menuList = res.result;
      for (let i = 0; i < menuList.length; i++) {
        if (menuList[i].isFirst == "1") {
          indexMenuList.push(menuList[i]);
        }
        if (menuList[i].serviceType != "更多") {
          if (menuList[i].serviceCode == SERVICE_CODE.SERVICE) {
            if (typeof serverMenu[menuList[i].serviceType] == "undefined") {
              serverMenu[menuList[i].serviceType] = [];
              serverMenuType.push(menuList[i].serviceType);
            }
            serverMenu[menuList[i].serviceType].push(menuList[i]);
          } else if (menuList[i].serviceCode == SERVICE_CODE.SHOP) {
            if (typeof shopMenu[menuList[i].serviceType] == "undefined") {
              shopMenu[menuList[i].serviceType] = [];
              shopMenuType.push(menuList[i].serviceType);
            }
            shopMenu[menuList[i].serviceType].push(menuList[i]);
          } else if (menuList[i].serviceCode == SERVICE_CODE.SERVICE_COLS) {
            if (_this.$common.isNotEmpty(menuList[i].toType)) {
              state.serviceRows = parseInt(menuList[i].toType);
            }
          } else if (menuList[i].serviceCode == SERVICE_CODE.SHOP_COLS) {
            if (_this.$common.isNotEmpty(menuList[i].toType)) {
              state.shopRows = parseInt(menuList[i].toType);
            }
          }
        }
      }

      state.indexMenuList = indexMenuList;
      state.serverMenu = serverMenu;
      state.serverMenuType = serverMenuType;
      state.shopMenu = shopMenu;
      state.shopMenuType = shopMenuType;

    });
  },
  //请求险种信息
  [Mutations.QUERY_INIT_RISK_DATA](state, _this) {
    // let _this = data._this;
    $.ajax({
      type: "POST",
      url: RequestUrl.QUERY_INIT_RISK_DATA,
      data: "",
      async: false,
      success: function (res, status, xhr) {
        let initDataList = res.result;

        let riskCodeList = [];
        let list = {};

        for (let i = 0; i < initDataList.length; i++) {
          if (initDataList[i].code == "1") {
            let riskKeyValue = {
              key: initDataList[i].codeEname1,
              value: initDataList[i].codeType1
            };
            list[initDataList[i].codeEname1] = {
              color: initDataList[i].code1,
              riskCode: initDataList[i].codeEname1,
              riskName: initDataList[i].codeType1
            }
            riskCodeList.push(riskKeyValue);
          } else if (initDataList[i].code == "2") {
            state.abroadTravel = initDataList[i].codeEname1;
          }

        }
        state.riskCodeList = riskCodeList;
        state.colorRiskTyle = list;

      }
    });
  },
  //版本号控制用户数据是否最新
  [Mutations.SET_OLD_VERSION](state, isOldVersion) {
    state.isOldVersion = isOldVersion;
  },
  [Mutations.SET_IS_SHOP](state, isShop) {
    state.isShop = isShop;
  },
  [Mutations.SET_QUERY_STATE](state, type) {
    if (type) {
      state.queryStyle = "background: #636365 !important;color: #fff !important;";
      state.baseIsQuery = false;
    } else {
      state.queryStyle = "background: #C8161D !important;color: #fff !important;";
      state.baseIsQuery = true;

    }
  },
  //测试用例
  [Mutations.ADD_TEST](state, num) {
    state.test = num;
  },
  [Mutations.MY_ADD_TEST](state) {
    state.test++;
  },
  [Mutations.SET_SHARE_UUID](state, shareUuid) {
    state.shareUuid = shareUuid;
  },
  [Mutations.INSURANCE_LIST_TOP](state, num) {
    state.insuranceListTop = "position: fixed;padding-top: " + num.top + "px;padding-bottom: " + num.btn + "px;z-index:0;" + num.other;
  },
  [Mutations.VUE_SCROLLER_BTN](state, num) {
    state.vueScrollerBtn = "background: #F0F0F0;height: " + num + "px;";
  },
  [Mutations.VUE_SCROLLER_NO_MORE_DATA](state, num) {
    state.vueScrollerNoMoreData = num;
  },
  [Mutations.SET_IS_WE_CHAR](state) {
    let ua = window.navigator.userAgent.toLowerCase();
    if (ua.match(/MicroMessenger/i) == 'micromessenger') {
      state.isWeChar = true;
      localStorage["IS_WE_CHAR"] = "T";
    } else {
      localStorage["IS_WE_CHAR"] = "F";
      state.isWeChar = false;
    }
  },
  [Mutations.SET_IS_WE_CHAR_STATE](state, parm) {
    state.isWeChar = parm;

  },
  [Mutations.IS_SHOW_MY](state, parm) {
    state.configBtn.isShopIndex = parm.index;
    state.configBtn.isShopMore = parm.more;
    state.configBtn.isShopService = parm.service;
    state.configBtn.isShowMy = parm.my;
    state.configBtn.isShowBtn = parm.btn;
    state.isWeChar = parm.isWeChar;
    state.isWeAuth = parm.isWeAuth;
  },
  [Mutations.SET_MSG_DATA](state, msgData) {
    state.msgData = msgData.msgData;
    let _this = msgData._this;
    _this.$common.goUrl(_this, RouteUrl.MSG);
  },
  [Mutations.FOOTER_STATE](state, num) {
    state.footerState = num;
  },
  [Mutations.POP_STATE](state, num) {
    state.popState = num;
  },
  [Mutations.UPDATE_LOADING_STATUS](state, payload) {
    state.isLoading = payload;
  },
  [Mutations.FOOTER_SHOW](state, footerShow) {
    state.footerShow = footerShow;
  },
  [Mutations.IS_SHOW_MSG_COM_DATA](state, data) {
    state.msgComData = data;
    state.isShowMsgComData = false;
    setTimeout(() => {
      state.isShowMsgComData = true;
    }, 0);
  },
  [Mutations.IS_SHOW_MSG_COM_DATA_STATE](state, data) {
    state.isShowMsgComData = data.state;
  },
  //--------- 存储首页基本数据----------------
  [Mutations.INDEX_QUERY_DATA](state, initData) {
    // state.seleCountry.push({
    //   cName: "全球",
    //   eName: ""
    // });
    state.hotInsuList = initData.hotInsuList;
    // state.indexMenuList = initData.indexMenuList;
    state.adList = initData.adList;
    state.initUrl = initData.initUrl;
    // state.serverMenu = initData.serverMenu;
    // state.serverMenuType = initData.serverMenuType;
    // state.riskCodeList = initData.riskCodeList;
    // state.shopMenu = initData.shopMenu;
    // state.shopMenuType = initData.shopMenuType;
    // let initDataList = initData.initData;
    // let list = {};
    // for (let i = 0; i < initDataList.length; i++) {
    //   if (initDataList[i].code == "1") {
    //     list[initDataList[i].codeEname1] = {
    //       color: initDataList[i].code1,
    //       riskCode: initDataList[i].codeEname1,
    //       riskName: initDataList[i].codeType1
    //     }
    //     // list.push(riskCode);
    //   } else if (initDataList[i].code == "2") {
    //     state.abroadTravel = initDataList[i].codeEname1;
    //   }
    // }
    // state.colorRiskTyle = list;

  },
  //存储保险基本信息
  // [Mutations.INSURE_BASE_INFO](state, insureBaseInfo) {
  //   if (typeof (state.insureBaseInfo[insureBaseInfo.riskCode]) == 'undefined') {
  //     state.insureBaseInfo[insureBaseInfo.riskCode] = [];
  //   }
  //   state.insureBaseInfo[insureBaseInfo.riskCode] = insureBaseInfo.dataList;
  // },
  //保存地区基本信息
  // [Mutations.INSURE_SET_REGION](state, region) {
  //   state.region = region;
  //   // state.insureBaseInfo[insureBaseInfo.riskCode] = insureBaseInfo.dataList;
  // },
  //iframe
  [Mutations.SET_IFRAME_DATA](state, iframe) {
    let _this = iframe._this;
    state.iframe.isIframe = iframe.isIframe;
    if (iframe.isIframe) {
      state.iframe.iframeUrl = iframe.iframeUrl;
      state.iframe.iframeName = iframe.iframeName;
    }
    _this.$common.goUrl(_this, RouteUrl.IFRAME);

  },
  [Mutations.INSURE_RES_DATA](state, redData) {
    if (redData.loading) {
      state.insureResData.res = redData.res;
      state.loading = redData.loading;
      state.payStyle = "background:#C8161D;"
      state.state = "支付";
    } else {
      state.payStyle = "background:#636365;"
      state.state = "正在投保中..";
    }
  },
  [Mutations.INSURE_RES_DATA_AXTX](state, redData) {
    if (redData.loading) {
      state.loadingAxtx = redData.loading;
      state.payStyleAxtx = "background:#C8161D;"
      state.stateAxtx = "支付";
    } else {
      state.payStyleAxtx = "background:#636365;"
      state.stateAxtx = "正在投保中..";
    }
    state.insureResDataAxtx.res = redData.res;
    state.insureResDataAxtx.curprice = redData.curprice;
  },
  [Mutations.INSURE_CURPRICE](state, parm) {
    state.insureResData.curprice = parm.curprice;
    state.seleCountry = parm.seleCountry;
  },
  [Mutations.SHOW_SELE_COUNTRY](state, data) {
    state.showSeleCountry = data.state;
  },
  [Mutations.AD_ACTIVITY](state, parm) {
    state.activity = parm;
  },
  [Mutations.CAR_SET_CAR_ADDR](state, parm) {
    state.newCarSele = parm;
  },
  [Mutations.SET_CAR_ADDR](state, occData) {
    state.carAddr = occData;
  },
  [Mutations.RED_TRIGGER](state, tir) {
    state.redTrigger = tir.state;
  },
  //查询单个软文
  [Mutations.SET_SOFT_PAPER](state, data) {
    state.isQuerySoftPaper = false;
    let _this = data._this;
    let id = data.id;
    let type = data.type;
    let query = {
      TYPE: "FORM",
      id: id
    }
    // state.softPaper = occData;
    _this.$common.storeCommit(_this, Mutations.UPDATE_LOADING_STATUS, true);//loading
    _this.$http.post(RequestUrl.FIND_SOFT_PAPER, query).then(function (res) {
      _this.$common.storeCommit(_this, Mutations.UPDATE_LOADING_STATUS, false);//loading
      state.softPaper = res.result;
      state.softPaper.desc = state.softPaper.desc.split("，");
      if (type == "go") {
        let parmList = [];
        // let urlParm = {
        //   key: Url_Key.SHARE_UUID,
        //   value: _this.shareUuid
        // }
        let urlPro = {
          key: Url_Key.SOFT_PAPER,
          value: id
        }
        parmList.push(urlPro);
        // parmList.push(urlParm);


        _this.$common.goUrl(_this, RouteUrl.SOFT_PAPER, _this.$common.setShareUrl(parmList));
      }
      state.isQuerySoftPaper = true;
      // _this.$common.goUrl(_this, RouteUrl.SOFT_PAPER);
    });
  },
  // [Mutations.IS_SHOW_RATE](state, data) {
  //   // state.isShowRate = false;
  //   // sessionStorage["IS_SHOW_RATE"] = "F"
  //   if (data.isLogin) {
  //     if (data.userDto.userType == "2" || data.userDto.userType == "5") {
  //       sessionStorage["IS_SHOW_RATE"] = "T";
  //       state.isShowRate = true;
  //     } else {
  //       sessionStorage["IS_SHOW_RATE"] = "F";
  //       state.isShowRate = false;
  //     }
  //   } else {
  //     sessionStorage["IS_SHOW_RATE"] = "F";
  //     state.isShowRate = false;
  //   }
  // },
  [Mutations.SET_POSTER_LIST](state, data) {
    if (data.type == "set") {
      state.isQueryPoster = true;
      state.poster = data.data;
    } else {
      state.isQueryPoster = false;
    }
  },
  [Mutations.SET_TRIGGER](state, data) {
    state.trigger = data;
    setTimeout(() => {
      state.trigger = false;
    }, 3000);
  },
  [Mutations.SET_SHARE_DATA](state, data) {
    let _this = data._this;
    let callBackUrl = data.callBackUrl;
    let singleMember = data.singleMember;
    let successfulUrl = data.successfulUrl;
    if (!_this.$common.isEmpty(callBackUrl)) {
      state.shareData.callBackUrl = callBackUrl;
    }
    if (!_this.$common.isEmpty(singleMember)) {
      state.shareData.singleMember = singleMember;
    }
    if (!_this.$common.isEmpty(successfulUrl)) {
      state.shareData.successfulUrl = successfulUrl;
    }
  },
  //查询单个海报
  [Mutations.SET_POSTER](state, data) {
    let _this = data._this;
    let id = parseInt(data.id);
    let type = data.type
    let query = {
      TYPE: "FORM",
      id: id
    }
    _this.$common.storeCommit(_this, Mutations.UPDATE_LOADING_STATUS, true);
    _this.$http.post(RequestUrl.FIND_ONE_POSTER, query).then(function (res) {
      if (res.state == 0) {
        state.isQueryPoster = true;//已查到海报
        state.poster = res.result;
        //分享参数
        if (type == "go") {
          let parmList = [];
          // let urlParm = {
          //   key: Url_Key.SHARE_UUID,
          //   value: _this.shareUuid
          // }
          let urlPro = {
            key: Url_Key.SHOW_POSTER,
            value: id
          }
          parmList.push(urlPro);
          // parmList.push(urlParm);

          _this.$common.goUrl(_this, RouteUrl.SHOW_POSTER, _this.$common.setShareUrl(parmList));
        }
      }

      //刷新时查询数据并等待图片加载完后绘制
      document.getElementById('my_poster').onload = function () {
        setTimeout(() => {
          _this.initImg();//绘制海报
          _this.$common.storeCommit(_this, Mutations.UPDATE_LOADING_STATUS, false);
        }, 1000);
      };
    });
  },
}
