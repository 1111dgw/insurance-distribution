import { Mutations, policyHolderType, Gender, PerIdentifyType, Car_Mutations, EntIdentifyType, benefType, insuredidentity, RouteUrl, Url_Key, INPUT_CONFIG_TYPE } from 'src/common/const';
import { RequestUrl } from 'src/common/url';
import DateUtil from 'src/common/util/dateUtil';

export default {
    state: {
        occupationValue: { name: "", value: "" },
        seleCodeOcc: {},

        channelName: "",

        isGetIndex: false,
        indexData: {},  //首页 产品 数据

        refereeData: false,

        refShareUUid: "",
        // acDetl:0,
        // acHot:0,

        // shopState: {
        //     acDetl: 0,
        //     // acDetlNumber: 0,
        //     acHot: 0,
        //     // acHotNumber: 0,
        // },
        refereePro: false,

        eleShow: {},
        //Detail 页面保存数据
        selePlanList: {},   //当前选择的女神计划plan 
        periodDto: {},  //Dto
        birthday: '',   //选择的生日

        insureData: {},

        hotArea: {},  //当前热门地区数据

        refereeId: "",   //推荐人id

        // savePlan基本信息
        policyDataDto: {
            agentDto: {
                agreementCode: "",
                salerName: "",
                salerNumber: "",

            },
            appliDto: {
                birth: "",
                email: "",
                identifyNumber: "",
                identifyType: PerIdentifyType.ID_CAR,
                insuredName: "",
                insuredType: policyHolderType.PERSONAL,
                linkMobile: "",
                payMth: "7",
                appliIsIdCar: false,
            },
            addressDto: {
                region: '',
                addressName: "",
                buildStructure: ""
            },
            insuredItemProcutDtoList: [{ //被保险人信息
                //被保险人信息
                insuredDto: {
                    insuredType: policyHolderType.PERSONAL,
                    insuredName: "",
                    identifyType: PerIdentifyType.ID_CAR,
                    identifyNumber: "",
                    birth: "",
                    sex: "",
                    insuredidentity: "",
                    email: "",
                    benefType: benefType.LEGAL,
                    linkMobile: "",
                    insureIsIdCar: false,
                    occupationCode: "",
                    occupationName: ""
                },
                itemKindDtoList: [],
                productDto: {}
            }],
            // itemKindDtoList: [{
            //   kindCode: "",
            //   itemCode: "",
            //   amount: "",
            //   premium: ""
            // }],
            mainHeadDto: {
                bussFromNo: "",
                currency: "CNY",
                language: "C",
                riskCode: "",
                argueSolution: "1",
                departureCountry: "CHN",
                departureCountryName: "中国大陆",
                arrivalCountry: "中国",
                arrivalCountryName: "中国大陆"
            },
            periodDto: {
                startDate: null,
                startHour: null,
                endDate: null,
                endHour: null
            },
            isHotArea: false,
            areaCode: "",
        },
        isHotArea: false,
        areaCode: "",

        shareAggerData: {
            agreementCode: "",
            salerName: "",
            salerNumber: "",
            channelName: "",
        },
        shareUid: "",
        isGetShareAggerData: false,

        applicantList: {},

        upLoadQuery: false,

        isGoQue: false,

        car: {
            isHotArea: false,
            areaCode: "",
            agreementCode: "",
            salerName: "",
            saleCode: "",
            channelName: "",
        }
    },
    mutations: {
        [Mutations.APPLI_FIND_ID_CAR](state, data) {
            let _this = data._this;
            let query = {
                TYPE: "FORM",
                idCar: data.idCar
            }
            let setData = function (res) {
                if (res != false) {
                    if (data.type == "axtx") {
                        if (_this.appliDto.name == "") {
                            _this.appliDto.name = res.name;
                        }
                        if (_this.appliDto.phoneNum == "") {
                            _this.appliDto.phoneNum = res.mobile;
                        }
                        if (_this.appliDto.mailAddress == "") {
                            _this.appliDto.mailAddress = res.email;
                        }
                    } else if (data.type == "carInsure") {
                        if (!data.isIdCarGet) {
                            if (_this.$common.isNotEmpty(res.birth)) {
                                _this.tprptCarOwnerDto.brithday = res.birth.split(' ')[0];
                                // _this.tprptCarOwnerDto.brithday = DateUtil.getDateStr_YmdByTs(res.birth);
                            }
                            _this.tprptCarOwnerDto.sex = res.sex;
                        }
                        if (_this.tprptCarOwnerDto.name == "") {
                            _this.tprptCarOwnerDto.name = res.name;
                        }
                        if (_this.tprptCarOwnerDto.cellPhoneNo == "") {
                            _this.tprptCarOwnerDto.cellPhoneNo = res.mobile;
                        }
                        // if (_this.tprptCarOwnerDto.email == "") {
                        //     _this.tprptCarOwnerDto.email = res.email;
                        // }
                    } else {
                        if (!data.isIdCarGet) {
                            if (_this.$common.isNotEmpty(res.birth)) {
                                _this.appliDto.birth = res.birth.split(' ')[0];
                                // _this.appliDto.birth = DateUtil.getDateStr_YmdByTs(res.birth);
                                _this.setIndexPrice();
                            }
                            _this.appliDto.sex = res.sex;
                        }
                        if (_this.appliDto.insuredName == "") {
                            _this.appliDto.insuredName = res.name;
                        }
                        if (_this.appliDto.linkMobile == "") {
                            _this.appliDto.linkMobile = res.mobile;
                        }
                        if (_this.appliDto.email == "") {
                            _this.appliDto.email = res.email;
                        }

                    }
                }
            };
            if (typeof (state.applicantList[data.idCar]) == "undefined" && data.idCar != "") {
                _this.$http.post(RequestUrl.FIND_ID_CAR, query)
                    .then(function (res) {
                        if (res.success) {
                            state.applicantList[data.idCar] = res.result;
                            setData(res.result);
                        } else {
                            state.applicantList[data.idCar] = false;
                        }
                    })
            }
            // else if (data.idCar == "") {
            //     setData(state.applicantList[data.idCar]);
            // }

        },
        [Mutations.SAVE_SHARE_OTHER_INFO](state, data) {
            let _this = data._this;
            let shareUid = data.shareUid;

            if (!_this.$common.isEmpty(shareUid)) {
                state.shareUid = shareUid;
            }
        },
        [Mutations.SAVE_SHARE_AGGER_DATA](state, data) {
            let _this = data._this;
            let agreementCode = data.agreementCode;
            let salerName = data.salerName;
            let salerNumber = data.salerNumber;
            let channelName = data.channelName;
            if (!_this.$common.isEmpty(agreementCode)) {
                state.isGetShareAggerData = true;
                state.shareAggerData.agreementCode = agreementCode;
            } else {
                state.isGetShareAggerData = false;
            }
            if (!_this.$common.isEmpty(salerName)) {
                state.shareAggerData.salerName = salerName;
            }
            if (!_this.$common.isEmpty(salerNumber)) {
                state.shareAggerData.salerNumber = salerNumber;
            }
            if (!_this.$common.isEmpty(channelName)) {
                state.shareAggerData.channelName = channelName;
            }
        },
        [Mutations.SET_INSURE_OCCUPATION_VALUE](state, occData) {
            let valSt = occData[0];
            if (valSt != "") {
                let data = valSt.split(":");
                let code = data[0];
                state.occupationValue.value = code;
                let name = data[1];
                state.occupationValue.name = name;
                state.seleCodeOcc[name] = code;
            }
        },
        [Mutations.GET_INSURE_OCCUPATION_DATA](state, occData) {
            let _this = occData._this;
            let index = occData.index;
            let name = state.occupationValue.name;
            let code = state.occupationValue.value;
            _this.insuredItemProcutDtoList[index].insuredDto.occupationCode = code;
            _this.insuredItemProcutDtoList[index].insuredDto.occupationName = name;
        },
        [Mutations.REF_SHARE_U_UID](state, refShareUUid) {
            // debugger
            if (typeof (refShareUUid) == "undefined") {
                state.refShareUUid = "";
            } else {
                state.refShareUUid = refShareUUid;
            }
        },
        [Mutations.SET_REFEREE_PRO](state, refereePro) {
            state.refereePro = refereePro;
        },
        [Mutations.SET_REFEREE_DATA](state, refereeData) {
            // debugger
            if (typeof (refereeData) == "undefined") {
                state.refereeData = false;
            } else {
                state.refereeData = refereeData;
            }
            // alert(state.refereeData.userCode);
            // state.shopState.acDetl = refereeData.typeState[0];
            // state.shopState.acHot = refereeData.typeState[1];
            // state.shopState.acDetlNumber = refereeData.volume;
            // state.shopState.acHotNumber = refereeData.popularity;
        },
        [Mutations.SET_IS_GET_INDEX](state, isGetIndex) {
            state.isGetIndex = isGetIndex;
        },
        [Mutations.INSURE_DATA_INDEX_DATA](state, insureData) {
            state.indexData = insureData.indexInsure;
            state.isGetIndex = true;
        },
        [Mutations.SAVE_INSURE_DEFAULT_SAVE](state, parm) {
            state.selePlanList = parm.selePlanList;
            state.periodDto = parm.periodDto;
            state.birthday = parm.birthday;
            state.eleShow = JSON.parse(state.indexData.element);


            //-------------------------------------------------
            if (typeof (state.eleShow.limitLicenseno) == "undefined") {
                state.eleShow.limitLicenseno = false;
            }
            if (typeof (state.eleShow.licenseno) == "undefined") {
                state.eleShow.licenseno = false;
            }
            if (typeof (state.eleShow.vinno) == "undefined") {
                state.eleShow.vinno = false;
            }
            if (typeof (state.eleShow.sameInsure) == "undefined") {
                state.eleShow.sameInsure = true;
            }
            if (typeof (state.eleShow.minInsure) == "undefined") {
                state.eleShow.minInsure = 0;
            }
            if (typeof (state.eleShow.isShowEnt) == "undefined") {
                state.eleShow.isShowEnt = true;
            }
            if (typeof (state.eleShow.mainInsureVal) == "undefined") {
                state.eleShow.mainInsureVal = false;
            }
            if (typeof (state.eleShow.occupationLimit) == "undefined") {
                state.eleShow.occupationLimit = false;
            }
            if (typeof (state.eleShow.uploadId) == "undefined") {
                state.eleShow.uploadId = "";
            }
            if (typeof (state.eleShow.queId) == "undefined") {
                state.eleShow.queId = "";
            }
            if (typeof (state.eleShow.inputConfigId) == "undefined") {
                state.eleShow.inputConfigId = "";
            }
            if (typeof (state.eleShow.isRelOther) == "undefined") {
                state.eleShow.isRelOther = false;
            }
            // let test = state.eleShow.aaa;
            // debugger
        },
        [Mutations.SET_SHOW_ELE](state, _this) {
            state.eleShow = JSON.parse(state.indexData.element);

            // let test = state.eleShow.aaa;

            //-------------------------------------------------
            if (typeof (state.eleShow.limitLicenseno) == "undefined") {
                state.eleShow.limitLicenseno = false;
            }
            if (typeof (state.eleShow.licenseno) == "undefined") {
                state.eleShow.licenseno = false;
            }
            if (typeof (state.eleShow.vinno) == "undefined") {
                state.eleShow.vinno = false;
            }
            if (typeof (state.eleShow.sameInsure) == "undefined") {
                state.eleShow.sameInsure = true;
            }
            if (typeof (state.eleShow.minInsure) == "undefined") {
                state.eleShow.minInsure = 0;
            }
            if (typeof (state.eleShow.isShowEnt) == "undefined") {
                state.eleShow.isShowEnt = true;
            }
            if (typeof (state.eleShow.mainInsureVal) == "undefined") {
                state.eleShow.mainInsureVal = false;
            }
            if (typeof (state.eleShow.occupationLimit) == "undefined") {
                state.eleShow.occupationLimit = false;
            }
            // debugger
            if (typeof (state.eleShow.uploadId) == "undefined") {
                state.eleShow.uploadId = "";
            }
            if (typeof (state.eleShow.queId) == "undefined") {
                state.eleShow.queId = "";
            }
            if (typeof (state.eleShow.inputConfigId) == "undefined") {
                state.eleShow.inputConfigId = "";
            }
            if (typeof (state.eleShow.isRelOther) == "undefined") {
                state.eleShow.isRelOther = false;
            }
            if (_this.$common.isNotEmpty(state.eleShow.inputConfigId)) {
                let parm = {
                    _this: _this,
                    id: state.eleShow.inputConfigId,
                    proId: state.indexData.id
                }
                _this.$common.storeCommit(_this, Mutations.FIND_BY_INPUT_ID, parm);
            }
            if (_this.$common.isNotEmpty(state.eleShow.uploadId)) {
                state.upLoadQuery = true;
                let parm = {
                    _this: _this,
                    id: state.eleShow.uploadId,
                    type: 2
                }
                _this.$common.storeCommit(_this, Mutations.FIND_BY_QUE_ID, parm);
            } else {
                state.upLoadQuery = false;
            }
            if (typeof (state.eleShow.queId) != "undefined" && _this.$common.isNotEmpty(state.eleShow.queId)) {
                let parm = {
                    _this: _this,
                    id: state.eleShow.queId,
                    type: 1
                }
                _this.$common.storeCommit(_this, Mutations.FIND_BY_QUE_ID, parm);
                state.isGoQue = true;
            } else {
                state.isGoQue = false;
            }
        },
        //保存数据
        [Mutations.SAVE_INSURE_DATA](state, parm) {

            if (typeof (state.insureData[state.indexData.id]) == "undefined") {
                state.insureData[state.indexData.id] = {};
            }
            let appliDto = parm.appliDto;
            let mainHeadDto = parm.mainHeadDto;
            let insuredItemProcutDtoList = parm.insuredItemProcutDtoList;;
            let addressDto = parm.addressDto;
            let isPolicyHolderList = parm.isPolicyHolderList;
            let policyHolderType = parm.policyHolderType;
            let insuredNum = parm.insuredNum;

            let titleList = parm.titleList;
            let initData = parm.initData;
            let listNo = parm.listNo;
            let isInitUpload = parm.isInitUpload;
            let showUpload = parm.showUpload;


            state.insureData[state.indexData.id].appliDto = appliDto;
            state.insureData[state.indexData.id].mainHeadDto = mainHeadDto;
            state.insureData[state.indexData.id].insuredItemProcutDtoList = insuredItemProcutDtoList;
            state.insureData[state.indexData.id].addressDto = addressDto;
            state.insureData[state.indexData.id].isPolicyHolderList = isPolicyHolderList;
            state.insureData[state.indexData.id].policyHolderType = policyHolderType;
            state.insureData[state.indexData.id].insuredNum = insuredNum;

            state.insureData[state.indexData.id].titleList = titleList;
            state.insureData[state.indexData.id].initData = initData;
            state.insureData[state.indexData.id].listNo = listNo;
            state.insureData[state.indexData.id].isInitUpload = isInitUpload;
            state.insureData[state.indexData.id].showUpload = showUpload;

            state.insureData[state.indexData.id].insureInputConfig = parm.insureInputConfig;
            state.insureData[state.indexData.id].appliInputConfig = parm.appliInputConfig;
            state.insureData[state.indexData.id].addInfoInputConfig = parm.addInfoInputConfig;
            state.insureData[state.indexData.id].uploadInputConfig = parm.uploadInputConfig;
        },
        //获得数据
        [Mutations.SAVE_INSURE_SET_DATA](state, parm) {

            let _this = parm._this;
            let minAge = parm.minAge;
            let deTime = DateUtil.getNowDateStr_Ymd();
            if (typeof (state.insureData[state.indexData.id]) == "undefined") {
                // savePlan基本信息
                let initPolicyDataDto = {
                    agentDto: {
                        agreementCode: "",
                        salerName: "",
                        salerNumber: "",
                    },
                    appliDto: {
                        birth: "",
                        email: "",
                        identifyNumber: "",
                        identifyType: PerIdentifyType.ID_CAR,
                        insuredName: "",
                        insuredType: policyHolderType.PERSONAL,
                        linkMobile: "",
                        payMth: "7",
                        appliIsIdCar: false,
                        occupationCode: "",
                        occupationName: ""
                    },
                    addressDto: {
                        region: '',
                        addressName: "",
                        buildStructure: ""
                    },
                    insuredItemProcutDtoList: [{ //被保险人信息
                        //被保险人信息
                        insuredDto: {
                            insuredType: policyHolderType.PERSONAL,
                            insuredName: "",
                            identifyType: PerIdentifyType.ID_CAR,
                            identifyNumber: "",
                            birth: "",
                            sex: "",
                            insuredidentity: "",
                            email: "",
                            benefType: benefType.LEGAL,
                            linkMobile: "",
                            insureIsIdCar: false,
                            occupationCode: "",
                            occupationName: ""
                        },
                        itemKindDtoList: [],
                        productDto: {}
                    }],
                    // itemKindDtoList: [{
                    //   kindCode: "",
                    //   itemCode: "",
                    //   amount: "",
                    //   premium: ""
                    // }],
                    mainHeadDto: {
                        bussFromNo: "",
                        currency: "CNY",
                        language: "C",
                        riskCode: "",
                        argueSolution: "1",
                        departureCountry: "CHN",
                        departureCountryName: "中国大陆",
                        arrivalCountry: "中国",
                        arrivalCountryName: "中国大陆",
                        licenseno: "",
                        vinno: "",
                        othEngage: "",
                    },
                    periodDto: {
                        startDate: null,
                        startHour: null,
                        endDate: null,
                        endHour: null
                    },
                };
                let isPolicyHolderList = [{
                    isPolicyHolder: true
                }];
                isPolicyHolderList[0].isPolicyHolder = state.eleShow.sameInsure;

                let policyHolderTypee = policyHolderType.PERSONAL;
                // setTimeout(() => {
                //     let test = _this.isPolicyHolderList;
                //     debugger
                // }, 3000);

                _this.appliDto = initPolicyDataDto.appliDto;
                _this.mainHeadDto = initPolicyDataDto.mainHeadDto;
                _this.addressDto = initPolicyDataDto.addressDto;
                _this.isPolicyHolderList = isPolicyHolderList;
                _this.policyHolderType = policyHolderTypee;
                _this.insuredNum = 1;
                _this.insuredItemProcutDtoList = initPolicyDataDto.insuredItemProcutDtoList;

                _this.listNo = 0;
                _this.titleList = [];
                _this.initData = {};
                _this.showUpload = false;
                _this.isInitUpload = false;

                setTimeout(function () {
                    if (_this.detailsStore.ageRelated) {
                        _this.appliDto.birth = state.birthday;
                        _this.insuredItemProcutDtoList[0].insuredDto.birth = state.birthday;
                    }
                }, 0);

            } else {
                let dataTime = [];
                let insureData = state.insureData[state.indexData.id];
                setTimeout(function () {
                    _this.appliDto = insureData.appliDto;
                    // _this.isPolicyHolderList = insureData.isPolicyHolderList;
                    // let test = _this.isPolicyHolderList;

                    _this.insuredItemProcutDtoList = insureData.insuredItemProcutDtoList;
                    for (let i = 0; i < _this.insuredItemProcutDtoList.length; i++) {
                        _this.insuredItemProcutDtoList[i].insuredDto.birth = dataTime[i];
                    }
                    //年龄相关 延时设置日期
                    // debugger
                    if (_this.detailsStore.ageRelated && (_this.appliDto.birth == deTime || _this.appliDto.birth == "")) {
                        _this.appliDto.birth = state.birthday;
                        _this.insuredItemProcutDtoList[0].insuredDto.birth = state.birthday;
                    }

                }, 0);
                _this.mainHeadDto = insureData.mainHeadDto;
                _this.addressDto = insureData.addressDto;
                _this.isPolicyHolderList = insureData.isPolicyHolderList;
                _this.policyHolderType = insureData.policyHolderType;
                _this.insuredNum = insureData.insuredNum;
                _this.insuredItemProcutDtoList = insureData.insuredItemProcutDtoList;

                _this.listNo = insureData.listNo;;
                _this.titleList = insureData.titleList;
                _this.initData = insureData.initData;
                _this.showUpload = insureData.showUpload;
                _this.isInitUpload = insureData.isInitUpload;

                for (let i = 0; i < insureData.insuredItemProcutDtoList.length; i++) {
                    dataTime.push(insureData.insuredItemProcutDtoList[i].insuredDto.birth);
                }
            }

            //设置日期用于计算
            if (_this.detailsStore.ageRelated && (_this.appliDto.birth == deTime || _this.appliDto.birth == "")) {
                _this.appliDto.birth = state.birthday;
                _this.insuredItemProcutDtoList[0].insuredDto.birth = state.birthday;
            }


            //计算价格
            for (let i = 0; i < _this.insuredItemProcutDtoList.length; i++) {
                if (_this.insuredItemProcutDtoList[i].insuredDto.birth == "") {
                    _this.getPrice("price", i, minAge);
                } else {
                    _this.travelInsurance(i);
                }
            }

            if (typeof (_this.seleValue.insureInputConfig[state.indexData.id]) != "undefined") {
                _this.insureInputConfig = _this.seleValue.insureInputConfig[state.indexData.id];
                _this.appliInputConfig = _this.seleValue.appliInputConfig[state.indexData.id];
                _this.addInfoInputConfig = _this.seleValue.addInfoInputConfig[state.indexData.id];
                _this.uploadInputConfig = _this.seleValue.uploadInputConfig[state.indexData.id];
                // debugger
            }
        },
        //组装提交报文
        [Mutations.SET_COMMIT_INSURE_DATA](state, parm) {
            let _this = parm._this;
            let curNsSelePlan = parm.curNsSelePlan;  //女神那边选择的计划
            let curSelePlan = parm.curSelePlan;
            let user = parm.user;
            let addName = "";
            // if (curNsSelePlan.proName.indexOf(curSelePlan.planName) == -1) {
            addName = " - " + curSelePlan.planName + " ";
            // }
            //  组成  productDto 
            let addProName = "";
            if (_this.$common.isNotEmpty(state.indexData.riskPrintName)) {
                addProName = state.indexData.riskPrintName + "^";
            }
            let productDto = {
                productCode: curNsSelePlan.planIcpcode,
                productName: addProName + state.indexData.productCname + addName,
                productId: curNsSelePlan.planId
            };

            state.policyDataDto.appliDto = state.insureData[state.indexData.id].appliDto;
            state.policyDataDto.mainHeadDto = state.insureData[state.indexData.id].mainHeadDto;
            state.policyDataDto.insuredItemProcutDtoList = state.insureData[state.indexData.id].insuredItemProcutDtoList;
            state.policyDataDto.addressDto = state.insureData[state.indexData.id].addressDto


            state.policyDataDto.mainHeadDto.riskCode = curNsSelePlan.proCode;

            //periodDto 
            state.policyDataDto.periodDto = state.periodDto;
            let isSetagr = true;
            // isHotArea:false,
            // areaCode:"",
            state.isHotArea = false;
            state.areaCode = "";
            // debugger
            if (state.isGetShareAggerData) {
                isSetagr = false;
                state.policyDataDto.agentDto.agreementCode = state.shareAggerData.agreementCode;
                state.policyDataDto.agentDto.salerName = state.shareAggerData.salerName;
                state.policyDataDto.agentDto.salerNumber = state.shareAggerData.salerNumber;
                state.channelName = state.shareAggerData.channelName;
            }
            //agentDto
            if (isSetagr && state.refereeData != false && typeof (state.refereeData) != "undefined") {
                if (typeof (state.refereeData.agrementNo) != "undefined" && state.refereeData.agrementNo != "" && state.refereeData.agrementNo != null) {
                    state.policyDataDto.agentDto.agreementCode = state.refereeData.agrementNo;
                    state.policyDataDto.agentDto.salerName = state.refereeData.saleName;
                    state.policyDataDto.agentDto.salerNumber = state.refereeData.saleCode;
                    isSetagr = false;
                    state.channelName = state.refereeData.channelName;
                }
            }


            if (user.isLogin && user.userDto.agrementNo != "" && user.userDto.agrementNo != null && isSetagr) {
                isSetagr = false;
                state.policyDataDto.agentDto.agreementCode = user.userDto.agrementNo;
                state.policyDataDto.agentDto.salerName = user.userDto.saleName;
                state.policyDataDto.agentDto.salerNumber = user.userDto.saleCode;
                state.channelName = user.userDto.channelName;
            }

            state.areaCode = state.hotArea.areaCode;
            if (isSetagr && state.refereePro != false && _this.$common.isNotEmpty(state.refereePro.areaCode)) {
                isSetagr = false;
                state.isHotArea = true;
                state.areaCode = state.refereePro.areaCode;
            }

            if (isSetagr && user.isLogin && !_this.$common.isEmpty(user.userDto.areaCode)) {
                isSetagr = false;
                state.isHotArea = true;
                state.areaCode = user.userDto.areaCode;
            }

            if (isSetagr) {
                state.policyDataDto.agentDto.agreementCode = state.hotArea.agreementNo;
                state.policyDataDto.agentDto.salerName = state.hotArea.saleName;
                state.policyDataDto.agentDto.salerNumber = state.hotArea.saleCode;
                state.channelName = state.hotArea.branchName;
            }


            if (state.channelName == null && state.channelName == "undefined" && state.channelName == "null") {
                state.channelName = "";
            }
            // alert("agreementCode:" + state.policyDataDto.agentDto.agreementCode + "     channelName:" + state.channelName);
            // alert(state.policyDataDto.agentDto.agreementCode);
            // appliDto
            // mainHeadDto
            // addressDto

            // 组装 insuredItemProcutDtoList 
            let kinds = curNsSelePlan.kinds;
            for (let i = 0; i < state.policyDataDto.insuredItemProcutDtoList.length; i++) {
                let planPrice = _this.insurePriceList[i];
                // let itemKindDtoList = [];
                // for (let j = 0; j < kinds.length; j++) {
                //     let percentage = kinds[j].percentage / 100;
                //     let itemKindDto = {
                //         kindCode: kinds[j].benefitsCode,
                //         itemCode: kinds[j].cvgCode,
                //         amount: kinds[j].adultMax,
                //         premium: planPrice * percentage
                //     };
                //     itemKindDtoList.push(itemKindDto);
                // }
                // state.policyDataDto.insuredItemProcutDtoList[i].itemKindDtoList = itemKindDtoList;
                state.policyDataDto.insuredItemProcutDtoList[i].productDto = productDto;
            }
            // state.policyDataDto.insuredItemProcutDtoList = state.insuredItemProcutDtoList;
        },
        //设置热门地区
        [Mutations.SAVE_HOT_AREA](state, hotArea) {
            state.hotArea = hotArea;
        },
        //解析分享地址参数并获取DB数据
        [Mutations.SHARE_URL_SET](state, share) {
            let _this = share._this;
            let url = share.url;
            let userCode = share.userCode;
            //解码URI--------------decodeURI不能完全解码,必须使用decodeURIComponent
            let uuid = "";
            // url = decodeURIComponent(url);
            // let uuid = _this.$common.getShareUrlKey(url, Url_Key.SHARE_UUID);
            let proId = _this.$common.getShareUrlKey(url, Url_Key.PRODUCT_ID);
            if (!_this.$common.isEmpty(proId) || !_this.$common.isEmpty(localStorage["INDEX_ID_LOCAL"])) {
                if (_this.$common.isEmpty(proId)) {
                    proId = localStorage["INDEX_ID_LOCAL"];
                }
                let parm = {
                    type: "get",
                    _this: _this,
                    id: proId,
                    _state: state,
                    refereeId: uuid,
                    userCode: userCode

                }

                _this.$common.storeCommit(_this, Mutations.SET_PRODUCT_LIST, parm);

            } else {
                _this.$common.goUrl(_this, RouteUrl.INDEX);
            }
        },
        [Mutations.SET_AXTX_AGR](state, data) {
            let _this = data._this;
            let user = data.user;
            let isSetagr = true;
            state.isHotArea = false;
            state.areaCode = "";
            // debugger
            if (state.isGetShareAggerData) {
                isSetagr = false;
                _this.appliDto.agreementCode = state.shareAggerData.agreementCode;
                _this.appliDto.salerName = state.shareAggerData.salerName;
                _this.appliDto.saleCode = state.shareAggerData.salerNumber;
                state.channelName = state.shareAggerData.channelName;
            }
            if (isSetagr && state.refereeData != false && typeof (state.refereeData) != "undefined") {
                if (typeof (state.refereeData.agrementNo) != "undefined" && state.refereeData.agrementNo != "" && state.refereeData.agrementNo != null) {
                    _this.appliDto.agreementCode = state.refereeData.agrementNo;
                    _this.appliDto.salerName = state.refereeData.saleName;
                    _this.appliDto.saleCode = state.refereeData.saleCode;
                    isSetagr = false;
                    state.channelName = state.refereeData.channelName;
                }
            }
            if (user.isLogin && user.userDto.agrementNo != "" && user.userDto.agrementNo != null && isSetagr) {
                isSetagr = false;
                _this.appliDto.agreementCode = user.userDto.agrementNo;
                _this.appliDto.salerName = user.userDto.saleName;
                _this.appliDto.saleCode = user.userDto.saleCode;
                state.channelName = user.userDto.channelName;

            }
            state.areaCode = state.hotArea.areaCode;
            if (isSetagr && state.refereePro != false && _this.$common.isNotEmpty(state.refereePro.areaCode)) {
                isSetagr = false;
                state.isHotArea = true;
                state.areaCode = state.refereePro.areaCode;
            }
            if (isSetagr && user.isLogin && !_this.$common.isEmpty(user.userDto.areaCode)) {
                isSetagr = false;
                state.isHotArea = true;
                state.areaCode = user.userDto.areaCode;
            }

            if (isSetagr) {
                _this.appliDto.agreementCode = state.hotArea.agreementNo;
                _this.appliDto.salerName = state.hotArea.saleName;
                _this.appliDto.saleCode = state.hotArea.saleCode;
                state.channelName = state.hotArea.branchName;
            }
            if (_this.channelName == null && _this.channelName == "undefined" && _this.channelName == "null") {
                _this.appliDto.channelName = "";
            }

        },
        [Mutations.SET_CAR_AGR](state, data) {
            let _this = data._this;
            let user = data.user;
            let isSetagr = true;
            state.car.isHotArea = false;
            state.car.areaCode = "";
            // debugger
            if (state.isGetShareAggerData) {
                isSetagr = false;
                state.car.agreementCode = state.shareAggerData.agreementCode;
                state.car.salerName = state.shareAggerData.salerName;
                state.car.saleCode = state.shareAggerData.salerNumber;
                state.car.channelName = state.shareAggerData.channelName;
            }
            if (isSetagr && state.refereeData != false && typeof (state.refereeData) != "undefined") {
                if (typeof (state.refereeData.agrementNo) != "undefined" && state.refereeData.agrementNo != "" && state.refereeData.agrementNo != null) {
                    state.car.agreementCode = state.refereeData.agrementNo;
                    state.car.salerName = state.refereeData.saleName;
                    state.car.saleCode = state.refereeData.saleCode;
                    isSetagr = false;
                    state.car.channelName = state.refereeData.channelName;
                }
            }
            if (user.isLogin && user.userDto.agrementNo != "" && user.userDto.agrementNo != null && isSetagr) {
                isSetagr = false;
                state.car.agreementCode = user.userDto.agrementNo;
                state.car.salerName = user.userDto.saleName;
                state.car.saleCode = user.userDto.saleCode;
                state.channelName = user.userDto.channelName;

            }
            state.areaCode = state.hotArea.areaCode;
            if (isSetagr && state.refereePro != false && _this.$common.isNotEmpty(state.refereePro.areaCode)) {
                isSetagr = false;
                state.car.isHotArea = true;
                state.car.areaCode = state.refereePro.areaCode;
            }
            if (isSetagr && user.isLogin && !_this.$common.isEmpty(user.userDto.areaCode)) {
                isSetagr = false;
                state.car.isHotArea = true;
                state.car.areaCode = user.userDto.areaCode;
            }

            if (isSetagr) {
                state.car.agreementCode = state.hotArea.agreementNo;
                state.car.salerName = state.hotArea.saleName;
                state.car.saleCode = state.hotArea.saleCode;
                state.car.channelName = state.hotArea.branchName;
            }
            if (_this.channelName == null && _this.channelName == "undefined" && _this.channelName == "null") {
                state.car.channelName = "";
            }
            // state.car.agreementCode = "CHQ001697";
            // state.car.agreementCode = "GUD000360";
            _this.$common.storeCommit(_this, Car_Mutations.SET_CAR_AGR_INFO, state.car);

        },
    },
    actions: {

    },
    getters: {

    }
}