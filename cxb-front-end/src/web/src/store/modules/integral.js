import { Mutations, RouteUrl } from 'src/common/const'
import { RequestUrl } from "src/common/url";
import DateUtil from 'src/common/util/dateUtil';
import Config from "src/common/config/integralConfig";
//highcharts的引入
import Highcharts from 'highcharts';
export default {
    state: {
        //----------我的积分------------------------------
        dtoData: {},//我的积分页面数据总对象
        integralData: {//我的积分当前默认数据
            //积分余额和累计奖励个数
            myscore: {
                //积分余额 = 已生效的积分 + 待生效的积分
                effctiveScore: 0,//已生效的积分
                noeffctiveScore: 0,//待生效的积分
                sumScore: 0//累计获得积分
            },
            //第一笔奖励
            expire: [],
            weekSum: 0,//近一周奖励
            mouthSum: 0,//近一个月奖励
            mouthList: []//近一个月奖励明细(图表)
        },

        //----------积分列表------------------------------
        inOrOutData: {},//参数和存储的全部数据
        inOrOutIsEnd: false,
        inOrOutIsQuery: false, //是否在进行请求，一直刷新操作  从而重复请求
        curIntegralList: [],//当前选中类型的数据列表
        selectCurType: Config.getIntegralType(0).key,//当前选中的类型标识,默认为all=全部

        //----------积分兑换记录------------------------------
        cashData: {},//参数和存储的全部数据
        cashIsEnd: false,
        cashIsQuery: false, //是否在进行请求，一直刷新操作  从而重复请求
        integralCashList: [],//数据列表

        //----------积分详情------------------------------
        integralDetail: {},//查看过的积分
        curIntegralDeatil: {},//当前积分明细
        curInOrOutType: "",//当前积分详情所属的收入与支出类型,in = 收入,out=支出
        showType: "",
        showData: "",
    },
    mutations: {
        [Mutations.SET_DATA_INTEGRAL](state, param) {
            // v-show="integral.showData==''||integral.showData>item.change_time"


            // if (param.showType == "") {
            //     state.showType = "";
            //     state.showData = "";
            // } else if (param.showType == "月") {
            //     state.showType = "近一月";
            //     state.showData = DateUtil.getSubMonth(new Date());
            // } else if (param.showType == "周") {
            //     state.showType = "近一周";
            //     state.showData = DateUtil.getSubWeek(new Date());
            // }
            // console.log(state.showType);
            // console.log(state.showData);
        },
        //我的积分页面数据
        [Mutations.MY_INTEGRAL](state, param) {
            let _this = param._this;
            let baseQueryData = {
                TYPE: 'FORM',
                userCode: param.userCode,
            }
            //根据userCode缓存已查看的数据
            // debugger
            // if (typeof (state.dtoData[baseQueryData.userCode]) == 'undefined') {
            //采用默认值
            state.dtoData[baseQueryData.userCode] = {
                myscore: { effctiveScore: 0, noeffctiveScore: 0, sumScore: 0 },
                expire: [],
                weekSum: 0,
                mouthSum: 0,
                mouthList: []
            };
            _this.$http.post(RequestUrl.MY_INTEGRAL, baseQueryData).then(function (res) {
                // debugger
                if (res.state == 0) {
                    if (_this.$common.isNotEmpty(res.result.myscore)) {
                        state.dtoData[baseQueryData.userCode].myscore = res.result.myscore;// 积分余额和累计奖励个数
                    }
                    if (_this.$common.isNotEmpty(res.result.expire)) {
                        state.dtoData[baseQueryData.userCode].expire = res.result.expire;// 最邻近到期时间的一笔
                    }
                    state.dtoData[baseQueryData.userCode].weekSum = res.result.weekSum;// 近一周奖励
                    state.dtoData[baseQueryData.userCode].mouthSum = res.result.mouthSum;// 近一个月奖励

                    if (_this.$common.isNotEmpty(res.result.mouthList)) {
                        state.dtoData[baseQueryData.userCode].mouthList = res.result.mouthList;// 近一个月奖励明细(图表)
                    }
                    //未查看的使用创建的数据对象
                    state.integralData = state.dtoData[baseQueryData.userCode];
                    _this.initChart();//渲染图表
                }
            });
            // } else {
            //     //已看的数据直接展示数据对象
            //     // debugger
            //     state.integralData = state.dtoData[baseQueryData.userCode];
            //     // debugger
            // }
        },
        //积分收支记录
        [Mutations.INTEGRAL_IN_OR_OUT_LIST](state, param) {
            let _this = param._this;
            let operation = param.operation;//Scroller滑动事件
            state.selectCurType = param.selectCurType;//更新当前选择的类型
            let key = state.selectCurType;
            let baseQueryData = {
                TYPE: 'FORM',
                userCode: param.userCode,
                type: param.selectCurType,
                pageNumber: 0,
                pageSize: 10
            };
            //如果没有定义就进行创建
            //当前选中类型数据为空时，封装查询参数
            if (typeof (state.inOrOutData[key]) == 'undefined') {
                let baseDataInfo = {
                    isisEnd: false,
                    queryData: baseQueryData,
                    curTypeList: []
                }
                state.curIntegralList = [];
                state.inOrOutIsEnd = false;
                state.inOrOutData[key] = baseDataInfo;
            }
            if (operation == "refresh") {
                //下拉刷新操作,赋初始值
                state.inOrOutIsEnd = false;
                state.inOrOutData[key].queryData = baseQueryData;
                state.inOrOutData[key].isisEnd = false;
                state.inOrOutData[key].curTypeList = [];
                state.inOrOutData[key].queryData.pageNumber++;
            }
            //初始查询且当前选中的类型数据为空
            if (!state.inOrOutIsQuery && !state.inOrOutData[key].isisEnd) {
                if (operation == "infinite") {
                    //上拉加载操作 页码加1
                    state.inOrOutData[key].queryData.pageNumber++;
                }
                //开始请求接口操作
                state.inOrOutIsQuery = true;
                _this.$http.post(RequestUrl.INTEGRAL_IN_OR_OUT_LIST, state.inOrOutData[key].queryData).then(function (res) {
                    if (res.state == 0) {
                        let lists = res.result;
                        for (let i = 0; i < lists.length; i++) {
                            //将当前记录封装属性名后添加到对应显示列表中
                            let obj = {
                                id: lists[i][0],//记录ID
                                in_out_type: lists[i][1],//扣减类型1=收入,2=扣减
                                change_type: lists[i][2],//变更类型,1活动2规则3其他(手工发放)4兑换5退保6过期7商城退还8退保差额
                                re_changescore: lists[i][3],//应收积分
                                ac_changescore: lists[i][4],//实收积分
                                reason: lists[i][5],//变更原因
                                reason_no: lists[i][6],//变更原因单号
                                change_time: DateUtil.getFullStrDateByTs2(lists[i][7]),//变更时间
                                remark: lists[i][8],//备注
                            };
                            state.inOrOutData[key].curTypeList.push(obj);
                        }
                        //当查询数据记录数小于当前选中类型数据的单页记录条数，则设置可继续查询
                        if (lists.length < state.inOrOutData[key].queryData.pageSize) {
                            state.inOrOutData[key].isisEnd = true;
                            state.inOrOutIsEnd = true;
                        }
                        //当前选中类型数据
                        state.curIntegralList = state.inOrOutData[key].curTypeList.slice();
                        state.inOrOutIsQuery = false;
                        // console.log(state.inOrOutData[key].curTypeList);
                    }
                });
            } else {
                //非初始查询或者当前选中类型的数据已存在
                state.inOrOutIsEnd = state.inOrOutData[key].isisEnd;
                //从已有的数组中返回选定的元素
                state.curIntegralList = state.inOrOutData[key].curTypeList.slice();
            }
        },
        //积分详情
        [Mutations.INTEGRAL_DETAIL](state, param) {
            let _this = param._this;
            state.curInOrOutType = param.in_out_type;//当前积分详情所属的收入与支出类型

            let type = "all";
            switch (param.in_out_type) {//获取或扣减类型
                case 1: type = "in"; break;//获取
                case 2: type = "out"; break;//扣减
                default: break;
            }
            let data = {
                TYPE: 'FORM',
                type: type,
                id: param.id,//积分ID
            }
            //根据ID缓存已查看的数据
            if (typeof (state.integralDetail[data.id]) == 'undefined') {
                _this.$common.storeCommit(_this, Mutations.UPDATE_LOADING_STATUS, true);//loading
                _this.$http.post(RequestUrl.FIND_INTEGRAL_ONE, data).then(function (res) {
                    _this.$common.storeCommit(_this, Mutations.UPDATE_LOADING_STATUS, false);//loading
                    if (res.state == 0) {
                        state.integralDetail[data.id] = res.result;
                        //未查看的使用创建的数据对象
                        state.curIntegralDeatil = state.integralDetail[data.id];
                    }
                });
            } else {
                //已看的数据直接展示数据对象
                state.curIntegralDeatil = state.integralDetail[data.id];
            }
            _this.$common.goUrl(_this, RouteUrl.INTEGRAL_DETAIL);//进入积分详情页
        },
        //积分提现记录
        [Mutations.INTEGRAL_EXCHANGE_LIST](state, param) {
            let _this = param._this;
            let operation = param.operation;//Scroller滑动事件
            let key = param.userCode;
            let baseQueryData = {
                TYPE: 'FORM',
                userCode: param.userCode,
                pageNumber: 0,
                pageSize: 10
            };
            // debugger
            //如果没有定义就进行创建
            if (typeof (state.cashData[key]) == 'undefined') {
                let baseDataInfo = {
                    isisEnd: false,
                    queryData: baseQueryData,
                    curTypeList: []
                }
                state.integralCashList = [];
                state.cashIsEnd = false;
                state.cashData[key] = baseDataInfo;
            }
            if (operation == "refresh") {
                //下拉刷新操作,赋初始值
                state.cashIsEnd = false;
                state.cashData[key].queryData = baseQueryData;
                state.cashData[key].isisEnd = false;
                state.cashData[key].curTypeList = [];
                state.cashData[key].queryData.pageNumber++;
            }
            //初始查询且当前选中的类型数据为空
            if (!state.cashIsQuery && !state.cashData[key].isisEnd) {
                if (operation == "infinite") {
                    //上拉加载操作 页码加1
                    state.cashData[key].queryData.pageNumber++;
                }
                //开始请求接口操作
                state.cashIsQuery = true;
                _this.$http.post(RequestUrl.FIND_CASH_ALL, state.cashData[key].queryData).then(function (res) {
                    if (res.state == 0) {
                        let lists = res.result.content;
                        // debugger
                        for (let i = 0; i < lists.length; i++) {
                            state.cashData[key].curTypeList.push(lists[i]);//零时存储当页数据
                        }
                        // debugger
                        //当查询数据记录数小于当前选中类型数据的单页记录条数，则设置可继续查询
                        if (lists.length < state.cashData[key].queryData.pageSize) {
                            state.cashData[key].isisEnd = true;
                            state.cashIsEnd = true;
                        }
                        //当前选中类型数据
                        state.integralCashList = state.cashData[key].curTypeList.slice();
                        state.cashIsQuery = false;
                        // console.log(state.cashData[key].curTypeList);
                    }
                });
            } else {
                //非初始查询或者当前选中类型的数据已存在
                state.cashIsEnd = state.cashData[key].isisEnd;
                //从已有的数组中返回选定的元素
                state.integralCashList = state.cashData[key].curTypeList.slice();
            }
        },
    },
    actions: {

    },
    getters: {

    }
}
