/*
Navicat Oracle Data Transfer
Oracle Client Version : 11.2.0.1.0

Source Server         : 10.132.3.42
Source Server Version : 110200
Source Host           : 10.132.3.42:1521
Source Schema         : CISPRE

Target Server Type    : MYSQL
Target Server Version : 50699
File Encoding         : 65001

Date: 2018-05-07 10:12:12
*/


-- ----------------------------
-- Table structure for FDRISKCONFIG
-- ----------------------------
DROP TABLE IF EXISTS `FDRISKCONFIG`;
CREATE TABLE `FDRISKCONFIG` (
`CODETYPE`  varchar(20) NOT NULL COMMENT '' ,
`CODEVALUE`  varchar(4) NOT NULL COMMENT '' ,
`UWYEAR`  varchar(4) NULL ,
`RISKCODE`  varchar(4) NOT NULL COMMENT '' ,
`STARTDATE`  datetime(6) NOT NULL COMMENT '' ,
`ENDDATE`  datetime(6) NULL COMMENT '' ,
`FLAG`  varchar(2) NULL COMMENT '' 
)

;

-- ----------------------------
-- Table structure for FHEXITEMKIND
-- ----------------------------
DROP TABLE IF EXISTS `FHEXITEMKIND`;
CREATE TABLE `FHEXITEMKIND` (
`TREATYNO`  varchar(8) NOT NULL ,
`SECTIONNO`  varchar(1) NOT NULL ,
`RISKCODE`  varchar(4) NOT NULL ,
`ITEMKIND`  varchar(3) NOT NULL ,
`ITEMKINDDESC`  text NULL ,
`FLAG`  varchar(10) NULL ,
PRIMARY KEY (`TREATYNO`, `SECTIONNO`, `RISKCODE`, `ITEMKIND`)
)

;

-- ----------------------------
-- Table structure for FHRISK
-- ----------------------------
DROP TABLE IF EXISTS `FHRISK`;
CREATE TABLE `FHRISK` (
`TREATYNO`  varchar(8) NOT NULL ,
`SECTIONNO`  varchar(1) NOT NULL ,
`RISKCODE`  varchar(4) NOT NULL ,
`FLAG`  varchar(10) NULL ,
`INRATEFLAG`  varchar(1) NULL COMMENT '是否使用分入比例控制规则 0 不使用 1 使用' ,
PRIMARY KEY (`TREATYNO`, `SECTIONNO`, `RISKCODE`)
)

;


-- ----------------------------
-- Table structure for FHTREATY
-- ----------------------------
DROP TABLE IF EXISTS `FHTREATY`;
CREATE TABLE `FHTREATY` (
`TREATYNO`  varchar(8) NOT NULL ,
`EXTREATYNO`  varchar(8) NULL ,
`CONNTREATYNO`  varchar(10) NULL ,
`OPTTYPE`  varchar(2) NULL ,
`REFNO`  varchar(255) NOT NULL ,
`TREATYNAME`  varchar(255) NULL ,
`TREATYENAME`  varchar(255) NULL ,
`CLOSEDATE`  datetime(6) NULL ,
`TREATYTYPE`  varchar(2) NULL ,
`UWYEAR`  varchar(4) NOT NULL ,
`STARTDATE`  datetime(6) NOT NULL ,
`ENDDATE`  datetime(6) NULL ,
`EXTENDDATE`  datetime(6) NULL ,
`LOGOUTDATE`  datetime(6) NULL ,
`REPREMIUMBASE`  varchar(2) NOT NULL ,
`CALCULATEBASE`  varchar(1) NOT NULL ,
`CURRENCY`  varchar(3) NULL ,
`ACCDATE`  decimal(8,0) NULL ,
`DUEDATE`  decimal(8,0) NULL ,
`ACCPERIOD`  varchar(8) NULL ,
`EXTENDFLAG`  varchar(1) NULL ,
`INREINSCODE`  varchar(10) NULL ,
`BROKERCODE`  varchar(10) NULL ,
`PAYCODE`  varchar(10) NULL ,
`INREINSNAME`  varchar(150) NULL ,
`BROKERNAME`  varchar(150) NULL ,
`PAYNAME`  varchar(150) NULL ,
`EXCHANGEFLAG`  varchar(1) NULL ,
`STATEFLAG`  varchar(1) NULL ,
`REMARKS`  varchar(255) NULL ,
`CREATERCODE`  varchar(10) NULL ,
`CREATEDATE`  datetime(6) NULL ,
`UPDATERCODE`  varchar(10) NULL ,
`UPDATEDATE`  datetime(6) NULL ,
`UNDERWRITECODE`  varchar(10) NULL ,
`UNDERWRITEENDDATE`  datetime(6) NULL ,
`FLAG`  varchar(10) NULL ,
`FHTREATYREMARKS`  varchar(255) NULL ,
PRIMARY KEY (`TREATYNO`)
)

;


-- ----------------------------
-- Table structure for FHXTREATY
-- ----------------------------
DROP TABLE IF EXISTS `FHXTREATY`;
CREATE TABLE `FHXTREATY` (
`TREATYNO`  varchar(8) NOT NULL ,
`EXTREATYNO`  varchar(8) NULL ,
`REFNO`  varchar(255) NOT NULL ,
`TREATYNAME`  varchar(255) NULL ,
`TREATYENAME`  varchar(255) NULL COMMENT '' ,
`TREATYTYPE`  varchar(2) NOT NULL COMMENT '' ,
`UWYEAR`  varchar(4) NOT NULL ,
`STARTDATE`  datetime(6) NOT NULL COMMENT '' ,
`ENDDATE`  datetime(6) NOT NULL COMMENT '' ,
`CLOSEDATE`  datetime(6) NULL ,
`CURRENCY`  varchar(3) NOT NULL ,
`EXTENDDATE`  datetime(6) NULL COMMENT '' ,
`EXTENDFLAG`  varchar(1) NULL COMMENT '' ,
`STATEFLAG`  varchar(1) NOT NULL COMMENT '' ,
`FLAG`  varchar(10) NULL COMMENT '' ,
`UPDATERCODE`  varchar(12) NULL ,
`UPDATERTIME`  datetime(6) NULL ,
`REMARKS`  varchar(255) NULL COMMENT '' ,
`CREATEDATE`  datetime(6) NULL ,
`CREATERCODE`  varchar(10) NULL ,
`FHXTREATYREMARKS`  varchar(255) NULL ,
PRIMARY KEY (`TREATYNO`)
)

;


-- ----------------------------
-- Table structure for FHSECTION
-- ----------------------------
DROP TABLE IF EXISTS `FHSECTION`;
CREATE TABLE `FHSECTION` (
`TREATYNO`  varchar(8) NOT NULL ,
`SECTIONNO`  varchar(1) NOT NULL ,
`CLASSCODE`  varchar(3) NULL ,
`SECTIONCDESC`  varchar(255) NULL ,
`SECTIONEDESC`  varchar(255) NULL ,
`CURRENCY`  varchar(3) NULL ,
`RETENTIONVALUE`  decimal(14,2) NULL ,
`REINSURERATE`  decimal(8,4) NULL ,
`INSHARERATE`  decimal(8,4) NULL ,
`LINES`  decimal(7,5) NULL ,
`LIMITVALUE`  decimal(14,2) NULL ,
`LOWERLIMITVALUE`  decimal(14,2) NULL ,
`COMMFLAG`  varchar(1) NULL ,
`LARGELOSSVALUE`  decimal(14,2) NULL ,
`CASHLOSSVALUE`  decimal(14,2) NULL ,
`CLEANMODE`  varchar(1) NULL ,
`MAINCOINSRATE`  decimal(14,2) NULL ,
`SUBCOINSRATE`  decimal(14,2) NULL ,
`SHAREHOLDERRATE`  decimal(14,2) NULL ,
`SHAREHOLDERLINES`  decimal(3,1) NULL ,
`INRATE`  decimal(14,2) NULL ,
`PCCLEANMODE`  varchar(2) NULL ,
`ACCMODE`  varchar(1) NOT NULL ,
`OSLOSSRATE`  decimal(14,2) NULL ,
`EXPENSERATE`  decimal(8,4) NOT NULL ,
`CARRIEDYRS`  decimal(2,0) NOT NULL ,
`PCSTARTMTHS`  decimal(2,0) NOT NULL ,
`EXPENSEIND`  varchar(1) NULL ,
`PCADJUSTMETH`  varchar(1) NULL ,
`PCMINRATE`  decimal(8,4) NULL ,
`PCMAXRATE`  decimal(8,4) NULL ,
`PCCOMPBASE`  decimal(8,4) NULL ,
`PCCOMPSTEP`  decimal(5,3) NULL ,
`PCRATESTEP`  decimal(5,3) NULL ,
`FLAG`  varchar(10) NULL ,
`CASHLOSSFLAG`  varchar(1) NULL ,
`CLEANYEAR`  decimal(3,1) NULL ,
`LOWPAIDRATE`  decimal(8,4) NULL ,
`UPPERPAIDRATE`  decimal(8,4) NULL ,
`BASEPAYRATE`  decimal(8,4) NULL ,
`ADJUSTCOMMRATE`  decimal(8,4) NULL ,
`ADJUSTRATE`  decimal(8,4) NULL ,
`INRATETYPE`  varchar(1) NULL COMMENT '分入业务转分出限制比例类型：0-分入比例为限制比例 1-固定限制比例' ,
`CLEANCUTREMARK`  varchar(200) NULL ,
`CLEANCUTSTATUS`  varchar(1) NULL COMMENT '0-待通过 1-通过 2- 不通过' ,
`MAINCOINSRATEGE`  decimal(14,2) NULL COMMENT '主共业务份额超过或等于50%' ,
`MAINCOINSRATEBELOW`  decimal(14,2) NULL COMMENT '主共业务份额低于50%' ,
`VATFLAG`  varchar(1) NULL COMMENT '应税标志' ,
`ADDVATRATE`  decimal(9,6) NULL COMMENT '附加税比例' ,
PRIMARY KEY (`TREATYNO`, `SECTIONNO`),
FOREIGN KEY (`TREATYNO`) REFERENCES `FHTREATY` (`TREATYNO`)
)

;

-- ----------------------------
-- Table structure for FHXLAYER
-- ----------------------------
DROP TABLE IF EXISTS `FHXLAYER`;
CREATE TABLE `FHXLAYER` (
`TREATYNO`  varchar(8) NOT NULL ,
`LAYERNO`  decimal(65,30) NOT NULL COMMENT '' ,
`LAYERTYPE`  varchar(2) NOT NULL COMMENT '' ,
`LAYERCDESC`  varchar(255) NULL COMMENT '' ,
`LAYEREDESC`  varchar(255) NULL COMMENT '' ,
`CURRENCY`  varchar(3) NOT NULL ,
`EXCESSLOSS`  decimal(14,2) NULL ,
`LAYERQUOTA`  decimal(14,2) NOT NULL ,
`TOTALQUOTA`  decimal(14,2) NOT NULL ,
`EGNPI`  decimal(14,2) NOT NULL ,
`GNPI`  decimal(14,2) NULL ,
`RATE`  decimal(9,6) NOT NULL ,
`ROL`  decimal(9,6) NULL ,
`MDPRATE`  decimal(9,6) NULL COMMENT '' ,
`MDP`  decimal(14,2) NOT NULL ,
`LAYERPREMIUM`  decimal(14,2) NOT NULL ,
`SHARERATE`  decimal(9,6) NOT NULL ,
`REINSTTIMES`  decimal(65,30) NULL COMMENT '' ,
`REINSTRATE`  decimal(9,6) NOT NULL ,
`RESIDUALREINSTSUM`  decimal(14,2) NOT NULL ,
`REINSTTYPE`  varchar(1) NOT NULL COMMENT '' ,
`REMARKS`  varchar(255) NULL COMMENT '' ,
`UPDATERCODE`  varchar(10) NULL ,
`UPDATERDATE`  datetime(6) NULL ,
`FLAG`  varchar(10) NULL COMMENT '' ,
`RWP`  decimal(14,2) NULL ,
`EXPIRELOSS`  decimal(14,2) NULL COMMENT '层止赔点' ,
`REMARKS1`  varchar(255) NULL ,
PRIMARY KEY (`TREATYNO`, `LAYERNO`),
FOREIGN KEY (`TREATYNO`) REFERENCES `FHXTREATY` (`TREATYNO`)
)

;


-- ----------------------------
-- Table structure for FHXSECTION
-- ----------------------------
DROP TABLE IF EXISTS `FHXSECTION`;
CREATE TABLE `FHXSECTION` (
`TREATYNO`  varchar(8) NOT NULL ,
`LAYERNO`  decimal(1,0) NOT NULL ,
`SECTIONNO`  varchar(1) NOT NULL ,
`SECTIONCDESC`  varchar(255) NULL ,
`SECTIONEDESC`  varchar(255) NULL ,
`GNPI`  decimal(14,2) NULL ,
`EXCESSLOSS`  decimal(14,2) NULL ,
`SECTIONQUOTA`  decimal(14,2) NULL ,
`EXPIRELOSS`  decimal(14,2) NULL ,
`EXCONAMOUNT`  decimal(14,2) NULL ,
PRIMARY KEY (`TREATYNO`, `LAYERNO`, `SECTIONNO`),
FOREIGN KEY (`TREATYNO`, `LAYERNO`) REFERENCES `FHXLAYER` (`TREATYNO`, `LAYERNO`)
)
COMMENT='超赔合约节信息表'

;

-- ----------------------------
-- Table structure for FHXRISK
-- ----------------------------
DROP TABLE IF EXISTS `FHXRISK`;
CREATE TABLE `FHXRISK` (
`TREATYNO`  varchar(8) NOT NULL ,
`LAYERNO`  decimal(65,30) NOT NULL COMMENT '' ,
`RISKCODE`  varchar(4) NOT NULL COMMENT '' ,
`SECTIONNO`  varchar(1) NOT NULL COMMENT '节号' ,
PRIMARY KEY (`TREATYNO`, `LAYERNO`, `RISKCODE`, `SECTIONNO`),
FOREIGN KEY (`TREATYNO`, `LAYERNO`, `SECTIONNO`) REFERENCES `FHXSECTION` (`TREATYNO`, `LAYERNO`, `SECTIONNO`)
)

;


-- ----------------------------
-- Table structure for PRPDCLASS
-- ----------------------------
DROP TABLE IF EXISTS `PRPDCLASS`;
CREATE TABLE `PRPDCLASS` (
`CLASSCODE`  varchar(3) NOT NULL ,
`CLASSNAME`  varchar(255) NULL ,
`CLASSENAME`  varchar(255) NULL ,
`ACCCODE`  varchar(6) NULL ,
`NEWCLASSCODE`  varchar(3) NOT NULL ,
`VALIDSTATUS`  varchar(1) NOT NULL ,
`FLAG`  varchar(10) NULL ,
`RISKCATEGORY`  varchar(1) NULL ,
`COMPOSITEFLAG`  varchar(10) NULL COMMENT '组合险标志 1-组合险 0-非组合险' ,
PRIMARY KEY (`CLASSCODE`)
)

;

-- ----------------------------
-- Table structure for PRPDCLAUSE
-- ----------------------------
DROP TABLE IF EXISTS `PRPDCLAUSE`;
CREATE TABLE `PRPDCLAUSE` (
`CLAUSECODE`  varchar(8) NOT NULL ,
`CLAUSENAME`  varchar(255) NULL ,
`LANGUAGE`  varchar(1) NOT NULL ,
`TITLEFLAG`  varchar(1) NULL ,
`LINENO`  varchar(15) NOT NULL ,
`CONTEXT`  text NULL ,
`NEWCLAUSECODE`  varchar(8) NOT NULL ,
`VALIDSTATUS`  varchar(1) NOT NULL ,
`FLAG`  varchar(10) NULL ,
`COMCODE`  text NULL COMMENT '特约适应公司。' ,
`CONTFLAG`  varchar(1) NULL COMMENT '内容是否可修改标志位。0代表内容可修改，1代表内容不可修改。' ,
`DELFLAG`  varchar(1) NULL COMMENT '特约条款是否可删除标志位。0代表可删除，1代表不可删除。' ,
`INIFLAG`  varchar(1) NULL COMMENT '初始化默认带出标志位.0代表初始化默认带出，1代表初始化不默认带出。' ,
`REMARK`  text NULL COMMENT '审核人备注' ,
`AGENTCODE`  text NULL COMMENT '代理人' ,
`CREATEDATE`  datetime(6) NOT NULL ,
`UPDATEDATE`  datetime(6) NOT NULL ,
PRIMARY KEY (`CLAUSECODE`, `LINENO`)
)

;

-- ----------------------------
-- Table structure for PRPDCODERISK
-- ----------------------------
DROP TABLE IF EXISTS `PRPDCODERISK`;
CREATE TABLE `PRPDCODERISK` (
`CODETYPE`  varchar(20) NOT NULL ,
`CODECODE`  varchar(20) NOT NULL ,
`RISKCODE`  varchar(4) NOT NULL ,
PRIMARY KEY (`CODETYPE`, `CODECODE`, `RISKCODE`)
)

;

-- ----------------------------
-- Table structure for PRPDCOMPANY
-- ----------------------------
DROP TABLE IF EXISTS `PRPDCOMPANY`;
CREATE TABLE `PRPDCOMPANY` (
`COMCODE`  varchar(24) NOT NULL ,
`COMCNAME`  varchar(240) NOT NULL ,
`COMENAME`  varchar(240) NULL ,
`ADDRESSCNAME`  text NULL ,
`ADDRESSENAME`  varchar(240) NULL ,
`POSTCODE`  varchar(18) NULL ,
`PHONENUMBER`  varchar(90) NULL ,
`TAXNUMBER`  varchar(90) NULL ,
`FAXNUMBER`  varchar(60) NULL ,
`UPPERCOMCODE`  varchar(24) NOT NULL ,
`INSURERNAME`  varchar(240) NULL ,
`COMATTRIBUTE`  varchar(3) NULL ,
`COMTYPE`  varchar(30) NULL ,
`COMLEVEL`  varchar(3) NULL ,
`MANAGER`  varchar(24) NULL ,
`ACCOUNTLEADER`  varchar(24) NULL ,
`CASHIER`  varchar(24) NULL ,
`ACCOUNTANT`  varchar(24) NULL ,
`REMARK`  varchar(120) NULL ,
`NEWCOMCODE`  varchar(60) NOT NULL ,
`VALIDSTATUS`  varchar(3) NOT NULL ,
`ACNTUNIT`  varchar(24) NULL ,
`ARTICLECODE`  varchar(30) NULL ,
`ACCCODE`  varchar(18) NULL ,
`CENTERFLAG`  varchar(3) NULL ,
`OUTERPAYCODE`  varchar(24) NULL ,
`INNERPAYCODE`  varchar(24) NULL ,
`FLAG`  varchar(6) NULL ,
`WEBADDRESS`  varchar(90) NULL ,
`SERVICEPHONE`  varchar(60) NULL ,
`REPORTPHONE`  varchar(60) NULL ,
`AGENTCODE`  varchar(39) NULL ,
`AGREEMENTNO`  varchar(60) NULL ,
`SYSAREACODE`  varchar(18) NULL ,
`COMBVISITRATE`  decimal(4,1) NULL ,
`PRINTCOMNAME`  text NULL ,
`PRINTADDRESS`  text NULL ,
`PRINGPOSTCODE`  text NULL ,
`COMSIGN`  varchar(30) NULL ,
`MOTORAREACODE`  varchar(30) NULL ,
`ACTUALUPPERCOMCODE`  varchar(24) NULL COMMENT '实际对应的上级机构，比如四级机构下面的部门应该挂在四级机构，其实现在挂在三级机构下面的' ,
`MOTORCOUNTYCODE`  varchar(30) NULL 
)

;

-- ----------------------------
-- Table structure for PRPDEFILE
-- ----------------------------
DROP TABLE IF EXISTS `PRPDEFILE`;
CREATE TABLE `PRPDEFILE` (
`EFILECODE`  varchar(40) NOT NULL COMMENT '备案号' ,
`MATCHLEVEL`  varchar(10) NULL COMMENT '匹配层级' ,
`EFILECNAME`  varchar(255) NULL COMMENT '备案号中文名称' ,
`EFILEENAME`  varchar(255) NULL COMMENT '备案号英文名称' ,
`RISKCODE`  varchar(10) NOT NULL COMMENT '归属险种代码' ,
`RISKVERSION`  varchar(10) NOT NULL COMMENT '归属险种版本' ,
`PLANCODE`  varchar(10) NULL COMMENT '归属方案代码' ,
`PLANNAME`  varchar(255) NULL COMMENT '归属方案名称' ,
`RISKNAME`  varchar(255) NULL COMMENT '归属险种名称' ,
`KINDCODE`  varchar(10) NOT NULL COMMENT '条款代码' ,
`KINDVERSION`  varchar(10) NOT NULL COMMENT '条款版本' ,
`KINDCNAME`  varchar(255) NULL COMMENT '条款名称' ,
`STARTDATE`  datetime(6) NULL COMMENT '备案号有效开始时间' ,
`ENDDATE`  datetime(6) NULL COMMENT '备案号有效结束时间' ,
`PATH`  varchar(255) NULL COMMENT '图片路径' ,
`IMGNAME`  varchar(255) NULL COMMENT '图片名称' ,
`IMGLENGTH`  decimal(65,30) NULL COMMENT '图片长' ,
`IMGWIDTH`  decimal(65,30) NULL COMMENT '图片宽' ,
`FLAG`  varchar(10) NULL COMMENT '标记位' ,
`VALIDATESTATUS`  varchar(10) NULL COMMENT '有效性' ,
`ATTRIBUTE1`  varchar(100) NULL COMMENT '备用字段1' ,
`ATTRIBUTE2`  varchar(100) NULL COMMENT '备用字段2' ,
`ATTRIBUTE3`  varchar(100) NULL COMMENT '备用字段3' ,
`ATTRIBUTE4`  varchar(100) NULL COMMENT '备用字段4' ,
`ATTRIBUTE5`  varchar(100) NULL COMMENT '备用字段5' ,
`ATTRIBUTE6`  varchar(100) NULL COMMENT '备用字段6' ,
`INSERTTIME`  datetime(6) NULL COMMENT '创建时间' ,
`UPDATEDATE`  datetime(6) NULL COMMENT '最后一次修改时间' ,
`IMGNAME2`  varchar(255) NULL COMMENT '第二张图片名称' ,
`IMGLENGTH2`  decimal(65,30) NULL COMMENT '第二张图片长度' ,
`IMGWIDTH2`  decimal(65,30) NULL COMMENT '第二张图片宽度' ,
`CHANNEL`  varchar(12) NULL COMMENT '1:传统渠道 2:电销渠道 3：网销渠道 渠道信息可以多选例如1,2,3:表示既是传统渠道，还是电销渠道，还是网销渠道' ,
`SUBMITCOM`  varchar(200) NULL COMMENT '报备单位' ,
`PRODUCTNAME`  varchar(100) NULL COMMENT '产品名称' ,
`SUBMITTYPE`  varchar(2) NULL COMMENT '报送类型' ,
`DEVTYPE`  varchar(1) NULL COMMENT '开发方式' ,
`PRODUCTTYPE`  varchar(100) NULL COMMENT '产品类别' ,
`MANAGETYPE`  varchar(100) NULL COMMENT '管理类别' ,
`RISKTYPE`  varchar(100) NULL COMMENT '险种类别' ,
`KINDTYPE`  varchar(1) NULL COMMENT '主/附险类型' ,
`OPENDATE`  datetime(6) NULL COMMENT '开办日期' ,
`APPROVALDATE`  datetime(6) NULL COMMENT '批复日期' ,
`BUSSCOPE`  varchar(100) NULL COMMENT '经营范围' ,
`BUSAREA`  varchar(100) NULL COMMENT '经营区域' ,
`DOCNUM`  varchar(100) NULL COMMENT '报送文件编号' ,
`YEAR`  varchar(4) NULL COMMENT '年度' ,
`PRODUCTNATURE`  varchar(100) NULL COMMENT '产品性质' ,
`RATE`  varchar(100) NULL COMMENT '费率或基础费率' ,
`RATERANGE`  varchar(100) NULL COMMENT '费率浮动区间 ' ,
`RATECOEFFICIENT`  varchar(100) NULL COMMENT '费率浮动系数 ' ,
`HASEPOLICY`  varchar(100) NULL COMMENT '是否有电子保单' ,
`PERIOD`  varchar(100) NULL COMMENT '保险期间' ,
`DEDUCTIBLE`  varchar(100) NULL COMMENT '绝对免赔率（额） ' ,
`RELDEDUCTIBE`  varchar(100) NULL COMMENT '相对免赔率（额） ' ,
`INSLIAB`  varchar(100) NULL COMMENT '保险责任 ' ,
`REMARK1`  text NULL COMMENT '备注1' ,
`REMARK2`  text NULL COMMENT '备注2' ,
`LINKNAME`  varchar(100) NULL COMMENT '产品联系人' ,
`LINKNUM`  varchar(100) NULL COMMENT '产品联系方式' ,
`PRODUCTATTR`  varchar(100) NULL COMMENT '产品个团属性' ,
`SIGNNAME`  varchar(100) NULL COMMENT '签发人' ,
`LEGALNAME`  varchar(100) NULL COMMENT '法律审核人' ,
`ACTUARIALNAME`  varchar(100) NULL COMMENT '精算审核人 ' ,
`SALEPROMNAME`  varchar(100) NULL COMMENT '销售推广名称' ,
`SALEAREA`  varchar(100) NULL COMMENT '销售区域 ' ,
`SALECHANNEL`  varchar(100) NULL COMMENT '销售渠道 ' ,
`OTHERINFO`  varchar(100) NULL COMMENT '其他产品信息' ,
`REMARK3`  text NULL COMMENT '审核人备注' ,
PRIMARY KEY (`EFILECODE`, `RISKCODE`, `RISKVERSION`, `KINDCODE`, `KINDVERSION`)
)
COMMENT='电子打印方案表'

;

-- ----------------------------
-- Table structure for PRPDITEM
-- ----------------------------
DROP TABLE IF EXISTS `PRPDITEM`;
CREATE TABLE `PRPDITEM` (
`ITEMCODE`  varchar(5) NOT NULL ,
`RISKCODE`  varchar(4) NOT NULL ,
`RISKVERSION`  varchar(50) NOT NULL ,
`PLANCODE`  varchar(15) NOT NULL ,
`ITEMCNAME`  text NOT NULL ,
`ITEMENAME`  text NULL ,
`ITEMFLAG`  varchar(1) NULL ,
`NEWITEMCODE`  varchar(6) NOT NULL ,
`VALIDSTATUS`  varchar(1) NOT NULL ,
`FLAG`  varchar(10) NULL ,
`KINDVERSION`  varchar(50) NOT NULL ,
PRIMARY KEY (`ITEMCODE`, `RISKVERSION`, `RISKCODE`, `PLANCODE`, `KINDVERSION`)
)

;

-- ----------------------------
-- Table structure for PRPDITEMLIBRARY
-- ----------------------------
DROP TABLE IF EXISTS `PRPDITEMLIBRARY`;
CREATE TABLE `PRPDITEMLIBRARY` (
`ITEMCODE`  varchar(5) NOT NULL COMMENT '标的项目代码' ,
`ITEMCNAME`  varchar(255) NOT NULL COMMENT '中文名称' ,
`ITEMENAME`  varchar(255) NULL COMMENT '英文名称' ,
`ITEMFLAG`  varchar(1) NULL COMMENT '类别标志' ,
`NEWITEMCODE`  varchar(6) NULL COMMENT '新的标的项目代码' ,
`VALIDSTATUS`  varchar(1) NULL COMMENT '有效状态' ,
`FLAG`  varchar(10) NULL COMMENT '扩展标识位' ,
`STARTDATE`  datetime(6) NULL COMMENT '生效日期' ,
`ENDDATE`  datetime(6) NULL COMMENT '失效日期' ,
`CREATEDATE`  datetime(6) NOT NULL ,
`UPDATEDATE`  datetime(6) NOT NULL ,
`ID`  varchar(100) NULL COMMENT '责任层级(0 - 一级，1 - 二级，2 - 无层级)' ,
PRIMARY KEY (`ITEMCODE`)
)

;

-- ----------------------------
-- Table structure for PRPDKIND
-- ----------------------------
DROP TABLE IF EXISTS `PRPDKIND`;
CREATE TABLE `PRPDKIND` (
`KINDCODE`  varchar(10) NOT NULL COMMENT '险别代码' ,
`KINDVERSION`  varchar(50) NOT NULL COMMENT '险别版本' ,
`RISKCODE`  varchar(4) NOT NULL COMMENT '险种代码' ,
`RISKVERSION`  varchar(50) NOT NULL COMMENT '险种版本' ,
`PLANCODE`  varchar(15) NOT NULL COMMENT '险种计划代码' ,
`KINDCNAME`  varchar(255) NOT NULL COMMENT '险别中文名称' ,
`KINDENAME`  varchar(255) NULL COMMENT '险别英文名称' ,
`STARTDATE`  datetime(6) NOT NULL COMMENT '生效日期' ,
`ENDDATE`  datetime(6) NOT NULL COMMENT '失效日期' ,
`CLAUSECODE`  varchar(8) NULL ,
`RELYONKINDCODE`  varchar(10) NULL COMMENT '依赖险别代码' ,
`RELYONSTARTDATE`  datetime(6) NULL COMMENT '依赖生效日期' ,
`RELYONENDDATE`  datetime(6) NULL COMMENT '依赖失效日期' ,
`CALCULATEFLAG`  varchar(8) NULL COMMENT '标志位' ,
`NEWKINDCODE`  varchar(10) NULL COMMENT '新险别代码' ,
`VALIDSTATUS`  varchar(1) NOT NULL COMMENT '有效状态' ,
`FLAG`  varchar(10) NULL COMMENT '扩展标识位' ,
`OWNERRISKCODE`  varchar(4) NULL COMMENT '归属险种代码' ,
`KINDWIDTH`  varchar(8) NULL COMMENT '图片宽' ,
`KINDHEIGHT`  varchar(8) NULL COMMENT '图片高' ,
`SHORTRATETYPE`  varchar(20) NULL ,
`EXT1`  varchar(50) NULL COMMENT '扩展字段1 0:司机 1:乘客 2:公共' ,
`EXT2`  varchar(50) NULL COMMENT '扩展字段2' ,
`EXT3`  varchar(50) NULL COMMENT '扩展字段3' ,
PRIMARY KEY (`KINDCODE`, `KINDVERSION`, `RISKCODE`, `RISKVERSION`, `PLANCODE`)
)

;

-- ----------------------------
-- Table structure for PRPDKINDITEM
-- ----------------------------
DROP TABLE IF EXISTS `PRPDKINDITEM`;
CREATE TABLE `PRPDKINDITEM` (
`RISKCODE`  varchar(4) NOT NULL COMMENT '险种代码' ,
`RISKVERSION`  varchar(50) NOT NULL COMMENT '险种版本' ,
`PLANCODE`  varchar(15) NOT NULL COMMENT '险种计划代码' ,
`KINDCODE`  varchar(10) NOT NULL COMMENT '险别代码' ,
`KINDVERSION`  varchar(50) NOT NULL COMMENT '险别版本' ,
`ITEMCODE`  varchar(5) NOT NULL COMMENT '标的/责任代码' ,
`VALIDSTATUS`  varchar(1) NOT NULL COMMENT '有效状态' ,
`FLAG`  varchar(10) NULL COMMENT '父级责任' ,
`EXT1`  varchar(50) NULL COMMENT ' 扩展字段1 0:司机 1:乘客 2:公共' ,
`EXT2`  varchar(50) NULL COMMENT '扩展字段2' ,
`EXT3`  varchar(50) NULL COMMENT '扩展字段3' ,
PRIMARY KEY (`KINDCODE`, `KINDVERSION`, `RISKCODE`, `RISKVERSION`, `PLANCODE`, `ITEMCODE`)
)

;

-- ----------------------------
-- Table structure for PRPDKINDLIBRARY
-- ----------------------------
DROP TABLE IF EXISTS `PRPDKINDLIBRARY`;
CREATE TABLE `PRPDKINDLIBRARY` (
`KINDCODE`  varchar(10) NOT NULL COMMENT '险别代码' ,
`KINDVERSION`  varchar(10) NOT NULL COMMENT '险别版本号' ,
`STARTDATE`  datetime(6) NOT NULL COMMENT '版本生效日期' ,
`ENDDATE`  datetime(6) NOT NULL COMMENT '版本失效日期' ,
`KINDCNAME`  varchar(255) NOT NULL COMMENT '险别中文名称' ,
`KINDENAME`  varchar(255) NULL COMMENT '险别英文名称' ,
`CLAUSECODE`  varchar(8) NULL COMMENT '条款代码' ,
`RELYONKINDCODE`  varchar(10) NULL COMMENT '依赖险别生效日期' ,
`RELYONSTARTDATE`  datetime(6) NULL COMMENT '依赖生效日期' ,
`RELYONENDDATE`  datetime(6) NULL COMMENT '依赖失效日期' ,
`CALCULATEFLAG`  varchar(8) NULL COMMENT '标志位' ,
`NEWKINDCODE`  varchar(10) NULL COMMENT '新的险别代码' ,
`VALIDSTATUS`  varchar(10) NULL COMMENT '有效状态' ,
`FLAG`  varchar(10) NULL COMMENT '扩展标识位' ,
`OWNERRISKCODE`  varchar(4) NULL COMMENT '归属险种' ,
`CREATEDATE`  datetime(6) NOT NULL ,
`UPDATEDATE`  datetime(6) NOT NULL ,
`EXT1`  varchar(50) NULL ,
`EXT2`  varchar(50) NULL ,
`VALIDFLAG`  varchar(1) NULL COMMENT '条款曾经是否生效过，1-已生效过' ,
`ID`  varchar(100) NULL ,
`EFILESTATUS`  varchar(1) NULL COMMENT '是否配置备案号' ,
`KINDHEIGHT`  varchar(8) NULL COMMENT '图片高' ,
`KINDWIDTH`  varchar(8) NULL COMMENT '图片宽' ,
`WAYSOFCALC`  varchar(1) NULL COMMENT '算费方式 0-人工计算 1-固定费率 2-ilog算费 3-加成比例' ,
`ILOGFLAG`  varchar(1) NULL COMMENT 'iLog规则配置状态' ,
`AGREEMENTRATE`  decimal(8,4) NULL COMMENT '约定费率' ,
`PLUSKINDCODE`  varchar(10) NULL COMMENT '对应加成条款代码' ,
`PLUSRATE`  decimal(8,4) NULL COMMENT '对应加成比例' ,
`POWAGENTCODE`  text NULL COMMENT '险别权限机构代码' ,
`REMARK`  text NULL COMMENT '审核人备注' ,
`SHORTRATETYPE`  varchar(20) NULL COMMENT '短期费率方式' ,
PRIMARY KEY (`KINDCODE`, `KINDVERSION`)
)

;

-- ----------------------------
-- Table structure for PRPDLOGSETTING
-- ----------------------------
DROP TABLE IF EXISTS `PRPDLOGSETTING`;
CREATE TABLE `PRPDLOGSETTING` (
`ROWID_OBJECT`  bigint(65) NOT NULL AUTO_INCREMENT COMMENT '序号' ,
`TABLENAME`  varchar(50) NULL COMMENT '表名' ,
`BUSINESSNAME`  varchar(50) NOT NULL COMMENT '业务名称' ,
`CREATEDBY`  varchar(20) NOT NULL COMMENT '创建人' ,
`CREATEDATE`  datetime(6) NOT NULL COMMENT '创建时间' ,
PRIMARY KEY (`ROWID_OBJECT`)
)

;

-- ----------------------------
-- Table structure for PRPDLOGOPERATION
-- ----------------------------
DROP TABLE IF EXISTS `PRPDLOGOPERATION`;
CREATE TABLE `PRPDLOGOPERATION` (
`LOGID`  bigint(65) NOT NULL COMMENT '日志号' ,
`BUSINESSKEYVALUE`  varchar(50) NOT NULL COMMENT '业务主键值' ,
`OPERATIONTYPE`  varchar(10) NOT NULL COMMENT '操作类型' ,
`CONTENT`  longtext NOT NULL COMMENT '操作内容' ,
FOREIGN KEY (`LOGID`) REFERENCES `PRPDLOGSETTING` (`ROWID_OBJECT`)
)

;


-- ----------------------------
-- Table structure for PRPDPROPS
-- ----------------------------
DROP TABLE IF EXISTS `PRPDPROPS`;
CREATE TABLE `PRPDPROPS` (
`PROPSCODE`  varchar(5) NOT NULL COMMENT '因子代码' ,
`PROPSCNAME`  varchar(250) NOT NULL COMMENT '因子中文名' ,
`PROPSENAME`  varchar(250) NULL COMMENT '因子英文名' ,
PRIMARY KEY (`PROPSCODE`)
)

;

-- ----------------------------
-- Table structure for PRPDPLANPROPS
-- ----------------------------
DROP TABLE IF EXISTS `PRPDPLANPROPS`;
CREATE TABLE `PRPDPLANPROPS` (
`PLANCODE`  varchar(15) NOT NULL COMMENT '方案代码' ,
`RISKCODE`  varchar(4) NOT NULL COMMENT '险种代码' ,
`RISKVERSION`  varchar(50) NOT NULL COMMENT '险种版本' ,
`PROPSCODE`  varchar(5) NOT NULL COMMENT '因子代码' ,
`PROPSTYPE`  varchar(2) NOT NULL COMMENT '因子类型（0-固定值，1-区间值）' ,
`LINENO`  varchar(2) NOT NULL ,
`VALIDSTATUS`  varchar(1) NOT NULL COMMENT '数据有效标志（0-无效，1-有效）' ,
`PROPSVALUE`  text NULL COMMENT '因子固定值' ,
`STARTVALUE`  varchar(80) NULL COMMENT '因子区间开始值' ,
`ENDVALUE`  varchar(80) NULL COMMENT '因子区间结束值' ,
`EXT1`  varchar(20) NULL COMMENT '备用字段1' ,
`EXT2`  varchar(20) NULL COMMENT '备用字段2' ,
PRIMARY KEY (`PLANCODE`, `RISKCODE`, `RISKVERSION`, `PROPSCODE`, `PROPSTYPE`, `LINENO`),
FOREIGN KEY (`PROPSCODE`) REFERENCES `PRPDPROPS` (`PROPSCODE`)
)

;

-- ----------------------------
-- Table structure for PRPDPLANSUB
-- ----------------------------
DROP TABLE IF EXISTS `PRPDPLANSUB`;
CREATE TABLE `PRPDPLANSUB` (
`PLANSUBCODE`  varchar(15) NOT NULL COMMENT '子方案代码' ,
`PLANCODE`  varchar(15) NOT NULL ,
`RISKCODE`  varchar(4) NOT NULL COMMENT '险种代码' ,
`RISKVERSION`  varchar(50) NOT NULL COMMENT '险种版本' ,
`PROPSCODE`  varchar(5) NOT NULL COMMENT '因子代码' ,
`PROPSTYPE`  varchar(2) NOT NULL COMMENT '因子类型（0-固定值，1-区间值）' ,
`LINENO`  varchar(2) NOT NULL ,
`VALIDSTATUS`  varchar(1) NOT NULL COMMENT '数据有效标志（0-无效，1-有效）' ,
`PROPSVALUE`  text NULL COMMENT '因子固定值' ,
`STARTVALUE`  varchar(80) NULL COMMENT '因子区间开始值' ,
`ENDVALUE`  varchar(80) NULL COMMENT '因子区间结束值' ,
`EXT1`  varchar(20) NULL COMMENT '备用字段1' ,
`EXT2`  varchar(20) NULL COMMENT '备用字段2' ,
PRIMARY KEY (`PLANCODE`, `RISKCODE`, `RISKVERSION`, `PROPSCODE`, `PROPSTYPE`, `LINENO`),
FOREIGN KEY (`PROPSCODE`) REFERENCES `PRPDPROPS` (`PROPSCODE`)
)

;

-- ----------------------------
-- Table structure for PRPDPOLICYPAY
-- ----------------------------
DROP TABLE IF EXISTS `PRPDPOLICYPAY`;
CREATE TABLE `PRPDPOLICYPAY` (
`COMCODE`  varchar(20) NOT NULL COMMENT '分/支公司代码' ,
`RISKCODE`  varchar(4) NOT NULL COMMENT '险种代码' ,
`AGENTCODE`  varchar(20) NOT NULL COMMENT '代理人代码' ,
`ISPOLICYPAY`  varchar(1) NOT NULL COMMENT '是否见费出单（Y-是，N-否）' ,
PRIMARY KEY (`COMCODE`, `RISKCODE`, `AGENTCODE`)
)

;


-- ----------------------------
-- Table structure for PRPDRATION
-- ----------------------------
DROP TABLE IF EXISTS `PRPDRATION`;
CREATE TABLE `PRPDRATION` (
`RATIONTYPE`  varchar(8) NOT NULL COMMENT 'a非定额(开放式)/b定额,固定险别/c定额，固定险别,费率/d定额，固定险别,保额,费率,折扣,保费\r\n' ,
`RISKCODE`  varchar(4) NOT NULL COMMENT '不保存' ,
`RISKVERSION`  varchar(50) NOT NULL COMMENT '根据当前产品的最大版本号自动生成\r\n001(三位序号)' ,
`PLANCODE`  varchar(15) NOT NULL COMMENT '可能是英文名称' ,
`PLANCNAME`  varchar(255) NOT NULL ,
`PLANENAME`  varchar(255) NULL ,
`SERIALNO`  decimal(15,0) NOT NULL ,
`KINDCODE`  varchar(10) NOT NULL ,
`KINDVERSION`  varchar(50) NOT NULL ,
`KINDCNAME`  varchar(255) NULL ,
`KINDENAME`  varchar(255) NULL ,
`STARTDATE`  datetime(6) NULL ,
`ENDDATE`  datetime(6) NULL ,
`CLAUSECODE`  varchar(8) NULL ,
`RELYONKINDCODE`  varchar(10) NULL COMMENT '只有依赖代码别选择时，险别代码才能被选择。' ,
`RELYONSTARTDATE`  datetime(6) NULL ,
`RELYONENDDATE`  datetime(6) NULL ,
`OWNERRISKCODE`  varchar(4) NOT NULL ,
`CALCULATEFLAG`  varchar(10) NULL ,
`ITEMCODE`  varchar(5) NULL ,
`ITEMCNAME`  varchar(255) NULL ,
`ITEMENAME`  varchar(255) NULL ,
`ITEMFLAG`  varchar(1) NULL COMMENT 'Y特约/N非特约' ,
`FREEITEM1`  varchar(30) NULL COMMENT '2724：最小座位数' ,
`FREEITEM2`  varchar(30) NULL COMMENT '2724：最大座位数' ,
`FREEITEM3`  varchar(30) NULL COMMENT '2724：责任归属 0-司机 1-乘客 2-整车' ,
`FREEITEM4`  varchar(30) NULL COMMENT '因子组序号' ,
`FREEITEM5`  varchar(30) NULL ,
`FREEITEM6`  varchar(30) NULL ,
`QUANTITY`  decimal(15,0) NULL COMMENT '--** 数量(人数/产品数量/户数)\r\n--** 牲畜数量(养殖)\r\n--** 投保面积(亩)(林木险)' ,
`CURRENCY`  varchar(3) NOT NULL ,
`AMOUNT`  decimal(14,2) NULL ,
`UNITAMOUNT`  decimal(14,2) NULL ,
`RATE`  decimal(13,8) NULL ,
`DISCOUNT`  decimal(13,8) NULL ,
`PREMIUM`  decimal(14,2) NULL ,
`UNITPREMIUM`  decimal(14,2) NULL ,
`VALIDSTATUS`  varchar(1) NOT NULL COMMENT '0：无效；\r\n1：有效' ,
`FLAG`  varchar(10) NULL COMMENT '(1)定额关系人类别\r\n00无\r\n01本人\r\n10配偶\r\n11丈夫\r\n12妻子\r\n20儿子\r\n30女儿\r\n40儿女\r\n50父母\r\n51父亲\r\n52母亲\r\n60雇佣\r\n61代理\r\n99其它\r\n双击域选择' ,
`WAYSOFCALC`  varchar(1) NULL COMMENT '保费计算方式' ,
`DELFLAG`  varchar(2) NULL COMMENT '条款是否删除标志（0-不允许，1-允许）' ,
PRIMARY KEY (`RISKCODE`, `RISKVERSION`, `PLANCODE`, `SERIALNO`)
)

;

-- ----------------------------
-- Table structure for PRPDRISK
-- ----------------------------
DROP TABLE IF EXISTS `PRPDRISK`;
CREATE TABLE `PRPDRISK` (
`RISKCODE`  varchar(4) NOT NULL ,
`RISKVERSION`  varchar(50) NOT NULL ,
`STATRISKCODE`  varchar(4) NULL ,
`RISKCNAME`  varchar(255) NOT NULL ,
`RISKENAME`  varchar(255) NOT NULL ,
`CLASSCODE`  varchar(2) NULL ,
`PRODUCTTYPECODE`  varchar(10) NULL ,
`GROUPCODE`  varchar(2) NULL ,
`CALCULATOR`  decimal(15,0) NULL ,
`SALESTARTDATE`  datetime(6) NULL ,
`SALEENDDATE`  datetime(6) NOT NULL ,
`ENDDATEFLAG`  varchar(1) NOT NULL ,
`EFFECTIVESTARTDATE`  datetime(6) NOT NULL ,
`INVALIDENDDATE`  datetime(6) NOT NULL ,
`INSURANCEPERIODTYPE`  varchar(10) NULL ,
`INSURANCEPERIOD`  decimal(15,0) NULL ,
`STARTHOUR`  decimal(15,0) NULL ,
`STARTMINUTE`  decimal(15,0) NULL ,
`ENDHOUR`  decimal(15,0) NULL ,
`ENDMINUTE`  decimal(15,0) NULL ,
`TEMPLETRISKCODE`  varchar(4) NULL ,
`RISKFLAG`  varchar(20) NULL ,
`NEWRISKCODE`  varchar(4) NOT NULL ,
`ARTICLECODE`  varchar(8) NULL ,
`MANAGEFLAG`  varchar(2) NULL ,
`SETTLETYPE`  varchar(2) NULL ,
`VALIDSTATUS`  varchar(1) NOT NULL ,
`FLAG`  varchar(10) NULL ,
`USECOMMONUW`  varchar(1) NULL COMMENT '是否使用公共核保规则\r\n1、使用\r\n0、不使用\r\n\' 不使用' ,
`CALCULATEPREMIUMTYPE`  varchar(1) NULL COMMENT '保费计算方式\r\n\r\n0、手工计算\r\n1、ilog计算\r\n2、自动计算，根据条款配置保费' ,
`COMPOSITEFLAG`  varchar(10) NULL COMMENT '组合险标志 1-组合险 0-非组合险' ,
PRIMARY KEY (`RISKCODE`, `RISKVERSION`),
FOREIGN KEY (`CLASSCODE`) REFERENCES `PRPDCLASS` (`CLASSCODE`)
)

;

-- ----------------------------
-- Table structure for PRPDRISKCLAUSE
-- ----------------------------
DROP TABLE IF EXISTS `PRPDRISKCLAUSE`;
CREATE TABLE `PRPDRISKCLAUSE` (
`RISKCODE`  varchar(8) NOT NULL COMMENT '险种代码' ,
`CLAUSECODE`  varchar(8) NOT NULL COMMENT '特约代码' ,
`CLAUSENAME`  varchar(255) NOT NULL COMMENT '特约名称' ,
`VALIDSTATUS`  varchar(2) NOT NULL COMMENT '有效状态--1.为有效。0为无效' ,
`FLAG`  varchar(2) NULL ,
`LANGUAGE`  varchar(2) NOT NULL COMMENT '中英文' ,
`PLANCODE`  text NULL COMMENT '方案/计划' ,
`COMCODE`  text NULL COMMENT '默认带出公司' ,
PRIMARY KEY (`RISKCODE`, `CLAUSECODE`)
)
COMMENT='险种-----特约表'

;

-- ----------------------------
-- Table structure for PRPDRISKPLAN
-- ----------------------------
DROP TABLE IF EXISTS `PRPDRISKPLAN`;
CREATE TABLE `PRPDRISKPLAN` (
`PLANCODE`  varchar(15) NOT NULL COMMENT '险种计划代码' ,
`RISKCODE`  varchar(4) NOT NULL COMMENT '险种代码' ,
`RISKVERSION`  varchar(50) NOT NULL COMMENT '险种版本' ,
`PLANCNAME`  varchar(255) NOT NULL COMMENT '计划中文名' ,
`PLANENAME`  varchar(255) NULL COMMENT '计划英文名' ,
`SERIALNO`  decimal(15,0) NOT NULL COMMENT '序号' ,
`VALIDSTATUS`  varchar(1) NOT NULL ,
`FLAG`  varchar(10) NULL COMMENT '扩展标识位' ,
`PLANDESC`  varchar(255) NULL COMMENT '方案描述' ,
`PLANTYPE`  varchar(1) NULL COMMENT '方案类型' ,
`APPLYCOMCODE`  text NULL COMMENT '方案权限适用机构' ,
`APPLYCHANNEL`  text NULL COMMENT '方案权限适用渠道' ,
`AUTOUNDWRT`  varchar(1) NULL COMMENT '是否自动核保' ,
`PERIODTYPE`  varchar(1) NULL COMMENT '保险期限类型' ,
`PERIOD`  decimal(5,0) NULL COMMENT '保险期限' ,
`OCCUPATIONCODE`  varchar(10) NULL COMMENT '职业代码' ,
`TRAVELDESTINATION`  text NULL COMMENT '旅行目的地' ,
`CREATEDATE`  datetime(6) NOT NULL ,
`UPDATEDATE`  datetime(6) NOT NULL ,
`AUTODOC`  varchar(1) NULL COMMENT '是否自动单证审核通过' ,
PRIMARY KEY (`RISKCODE`, `RISKVERSION`, `PLANCODE`)
)

;

-- ----------------------------
-- Table structure for PRPDSERIALNO
-- ----------------------------
DROP TABLE IF EXISTS `PRPDSERIALNO`;
CREATE TABLE `PRPDSERIALNO` (
`BUSINESSTYPE`  varchar(10) NOT NULL COMMENT '业务类型' ,
`CLASSCODE`  varchar(10) NULL COMMENT '险别' ,
`SERIALTYPE`  varchar(10) NULL COMMENT '流水号类型' ,
`SERIALNO`  decimal(5,0) NOT NULL COMMENT '流水号' 
)

;

-- ----------------------------
-- Table structure for PRPDSHORTRATELIBRARY
-- ----------------------------
DROP TABLE IF EXISTS `PRPDSHORTRATELIBRARY`;
CREATE TABLE `PRPDSHORTRATELIBRARY` (
`SHORTRATETYPE`  varchar(10) NOT NULL COMMENT '短期费率方式' ,
`RATE1`  decimal(8,4) NOT NULL COMMENT '一月费率' ,
`RATE2`  decimal(8,4) NOT NULL COMMENT '二月费率' ,
`RATE3`  decimal(8,4) NOT NULL COMMENT '三月费率' ,
`RATE4`  decimal(8,4) NOT NULL COMMENT '四月费率' ,
`RATE5`  decimal(8,4) NOT NULL COMMENT '五月费率' ,
`RATE6`  decimal(8,4) NOT NULL COMMENT '六月费率' ,
`RATE7`  decimal(8,4) NOT NULL COMMENT '七月费率' ,
`RATE8`  decimal(8,4) NOT NULL COMMENT '八月费率' ,
`RATE9`  decimal(8,4) NOT NULL COMMENT '九月费率' ,
`RATE10`  decimal(8,4) NOT NULL COMMENT '十月费率' ,
`RATE11`  decimal(8,4) NOT NULL COMMENT '十一月费率' ,
`RATE12`  decimal(8,4) NOT NULL COMMENT '十二月费率' ,
`VALIDSTATUS`  varchar(1) NOT NULL COMMENT '有效标识' ,
PRIMARY KEY (`SHORTRATETYPE`)
)

;

-- ----------------------------
-- Table structure for PRPMAXNO
-- ----------------------------
DROP TABLE IF EXISTS `PRPMAXNO`;
CREATE TABLE `PRPMAXNO` (
`GROUPNO`  varchar(18) NOT NULL ,
`TABLENAME`  varchar(25) NOT NULL ,
`MAXNO`  varchar(8) NOT NULL ,
`FLAG`  varchar(8) NULL ,
PRIMARY KEY (`GROUPNO`, `TABLENAME`, `MAXNO`)
)

;

-- ----------------------------
-- Table structure for PRPSAGENT
-- ----------------------------
DROP TABLE IF EXISTS `PRPSAGENT`;
CREATE TABLE `PRPSAGENT` (
`AGENTCODE`  varchar(13) NOT NULL COMMENT '代理人代码' ,
`AGENTNAME`  varchar(120) NOT NULL COMMENT '代理人名称' ,
`AGENTENAME`  varchar(120) NULL COMMENT '代理人英文名称' ,
`AGENTSHORTNAME`  varchar(120) NULL COMMENT '代理人简称' ,
`ADDRESSNAME`  text NULL COMMENT '代理人地址' ,
`POSTCODE`  varchar(6) NULL COMMENT '邮政编码' ,
`AGENTTYPE`  varchar(5) NOT NULL COMMENT '代理人类型,兼业/专业/个人代理' ,
`IDENTIFYNUMBER`  varchar(30) NULL COMMENT '代理人证件号码' ,
`IDENTIFYTYPE`  varchar(30) NULL COMMENT '代理人证件类型' ,
`PERMITFLAG`  varchar(1) NULL COMMENT '授权标志(0-未授权,1-已授权；)' ,
`PERMITTYPE`  varchar(1) NULL COMMENT '许可证类别' ,
`PERMITNO`  varchar(60) NULL COMMENT '许可证号（中介）,代理资格号（营销员）' ,
`CURRENTSTATE`  varchar(1) NULL ,
`BARGAINDATE`  datetime(6) NULL COMMENT '签约日期' ,
`TERMINATEDATE`  datetime(6) NULL COMMENT '签约截止日期' ,
`CLASSCODE`  varchar(100) NULL COMMENT '代理险种大类' ,
`COMCODE`  varchar(8) NOT NULL COMMENT '归属机构代码' ,
`HANDLERCODE`  varchar(10) NULL COMMENT '归属业务员' ,
`TEAMCODE`  varchar(12) NULL COMMENT '归属团队' ,
`UPPERAGENTCODE`  varchar(12) NULL COMMENT '上级代理人代码' ,
`NEWAGENTCODE`  varchar(12) NULL COMMENT '新的代理人代码' ,
`LINKERNAME`  varchar(60) NULL COMMENT '联系人' ,
`LINKERNUMBER`  varchar(30) NULL COMMENT '联系人电话' ,
`PRINCIPALNAME`  varchar(60) NULL COMMENT '负责人' ,
`PRIPHONENUMBER`  varchar(30) NULL COMMENT '负责人联系电话' ,
`PRIFAXNUMBER`  varchar(30) NULL COMMENT '负责人传真' ,
`PRIMOBILE`  varchar(30) NULL COMMENT '负责人手机' ,
`PRIEMAIL`  varchar(60) NULL COMMENT '负责人电子邮箱' ,
`PRINETADDRESS`  varchar(60) NULL COMMENT '负责人网址' ,
`BANK`  varchar(120) NULL COMMENT '银行' ,
`DEPOSITBANK`  varchar(120) NULL COMMENT '开户银行' ,
`ACCOUNT`  varchar(60) NULL COMMENT '银行帐号' ,
`ACCOUNTHOLDER`  varchar(120) NULL COMMENT '开户人姓名' ,
`LOWERVIEWFLAG`  varchar(1) NULL COMMENT '是否允许下级查看' ,
`BUSINESSLICENSE`  varchar(60) NULL COMMENT '营业执照号码' ,
`ORGCODE`  varchar(60) NULL COMMENT '组织机构代码' ,
`AGENTADMIN`  varchar(120) NULL COMMENT '渠道管理员' ,
`COOPERATIONLEVEL`  varchar(1) NULL COMMENT '合作等级' ,
`IDNO`  varchar(20) NULL COMMENT '营销员身份证号' ,
`SEX`  varchar(1) NULL COMMENT '营销员性别' ,
`BIRTH`  datetime(6) NULL COMMENT '营销员出生日期' ,
`RESIDENCEADDR`  varchar(120) NULL ,
`MARITALSTATUS`  varchar(6) NULL ,
`EDUCATION`  varchar(20) NULL COMMENT '营销员学历' ,
`DEGREE`  varchar(1) NULL ,
`PROFESSION`  varchar(60) NULL COMMENT '营销员所学专业' ,
`GRADUATESCHOOL`  varchar(60) NULL COMMENT '营销员毕业学校' ,
`JOBEXPERIENCE`  varchar(200) NULL COMMENT '营销员工作经验' ,
`BADREMARK`  varchar(200) NULL COMMENT '营销员不良记录' ,
`BADREMARKSTATUS`  varchar(1) NULL COMMENT '营销员是否有不良记录' ,
`STAFFSEQUENCE`  varchar(2) NULL ,
`STAFFRANK`  varchar(20) NULL ,
`WORKINGDATE`  datetime(6) NULL ,
`BUSINESSPLAN`  decimal(14,2) NULL COMMENT '年度业务计划（万元）' ,
`COMPLETEDPREMIUM`  decimal(14,2) NULL COMMENT '年度业务完成保费（万元）' ,
`EXHIBITIONNO`  varchar(60) NULL COMMENT '展业证号' ,
`CONTRACTNO`  varchar(60) NULL COMMENT '代理合同号' ,
`FUNDACCOUNT`  varchar(60) NULL COMMENT '基本公积金账号' ,
`FUNDACCOUNT1`  varchar(60) NULL COMMENT '补充公积金账号' ,
`INCREASENUM`  varchar(200) NULL COMMENT '增员人数' ,
`INCREASENAME`  varchar(200) NULL COMMENT '增员姓名' ,
`HONOR`  varchar(220) NULL COMMENT '所获奖励及荣誉' ,
`TRAININGPLAN`  varchar(220) NULL COMMENT '培训计划' ,
`BIGAGENTTYPE`  varchar(2) NULL COMMENT '归属大中介类型' ,
`CREDENTIALSNO`  varchar(50) NULL COMMENT '资格证书号' ,
`CREDENTIALSSTARTDATE`  datetime(6) NULL COMMENT '资格证有效起始日期' ,
`CREDENTIALSENDDATE`  datetime(6) NULL COMMENT '资格证有效终止日期' ,
`ACCOUNTNAME`  varchar(20) NULL COMMENT '帐户名称' ,
`PERMITSTARTDATE`  datetime(6) NULL COMMENT '许可证生效日期' ,
`PERMITENDDATE`  datetime(6) NULL COMMENT '许可证终止日期' ,
`COMPLETEDPROPORTION`  decimal(14,2) NULL COMMENT '年度业务完成比例' ,
`PREPREMIUM`  decimal(14,2) NULL COMMENT '预估保费规模' ,
`TRANO`  varchar(60) NULL COMMENT '培训证号' ,
`CHECKOUTSTATUS`  varchar(10) NOT NULL ,
`CREATORCODE`  varchar(10) NULL COMMENT '创建人代码' ,
`CREATEDATE`  datetime(6) NULL COMMENT '创建时间' ,
`UPDATERCODE`  varchar(10) NULL COMMENT '更新人代码' ,
`UPDATEDATE`  datetime(6) NULL COMMENT '更新时间' ,
`VALIDSTATUS`  varchar(1) NULL COMMENT '有效状态(0无效/1有效)' ,
`REMARK`  varchar(200) NULL COMMENT '备注' ,
`FLAG`  varchar(2) NULL COMMENT '标志位' ,
`ICFLAG`  varchar(1) NOT NULL COMMENT '外部设备标识位(0不需要/1icCard/2usbKey)' ,
`SALECHANNELCODE`  varchar(10) NULL COMMENT '备用字段2' ,
`BUSINESSNATURE`  varchar(5) NOT NULL COMMENT '渠道代码' ,
`ATTRIBUTE4`  varchar(50) NULL COMMENT '备用字段4' ,
`ATTRIBUTE5`  varchar(50) NULL COMMENT '备用字段5' ,
`ATTRIBUTE6`  varchar(50) NULL COMMENT '备用字段6' ,
`ATTRIBUTE7`  varchar(50) NULL COMMENT '备用字段7' ,
`ATTRIBUTE8`  varchar(50) NULL COMMENT '备用字段8' ,
`ATTRIBUTE9`  datetime(6) NULL COMMENT '日期备用字段9' ,
`ATTRIBUTE10`  datetime(6) NULL COMMENT '日期备用字段10' ,
`EXHIBITIONSTARTDATE`  datetime(6) NULL COMMENT '展业证起期' ,
`EXHIBITIONENDDATE`  datetime(6) NULL COMMENT '展业证止期' ,
`CONTRACTSTARTDATE`  datetime(6) NULL COMMENT '合同起期' ,
`CONTRACTENDDATE`  datetime(6) NULL COMMENT '合同止期' ,
`ACCOUNTADRESS`  varchar(100) NULL COMMENT '开户地' ,
`PROFESSIONNAME`  varchar(30) NULL COMMENT '职业证名称' ,
`PROFESSIONNO`  varchar(30) NULL COMMENT '职业证号' ,
`PROFESSIONSTARTDATE`  datetime(6) NULL COMMENT '职业证发证时间' ,
`INDUSTRY`  varchar(255) NULL COMMENT '所属行业' ,
`PROFESSIONENDDATE`  datetime(6) NULL COMMENT '职业证截止时间' ,
`PROVINCECODE`  varchar(16) NULL COMMENT '省代码' ,
`PROVINCENAME`  varchar(255) NULL COMMENT '省名称' ,
`CITYCODE`  varchar(16) NULL COMMENT '市代码' ,
`CITYNAME`  varchar(255) NULL COMMENT '市名称' ,
`COUNTYCODE`  varchar(16) NULL COMMENT '县代码' ,
`COUNTYNAME`  varchar(255) NULL COMMENT '县名称' ,
`ROADNAME`  text NULL COMMENT '街道及门牌号' ,
`CARBRAND`  text NULL COMMENT '品牌' ,
`FACTORY`  text NULL COMMENT '厂牌' ,
`CONTROLFLAG`  varchar(1) NULL COMMENT '是否控制厂牌品牌（1为控制出单）' ,
`CARSERIESNAME`  text NULL COMMENT '车系' ,
`ORGANZATIONSTARTDATE`  datetime(6) NULL COMMENT '组织机构代码起期' ,
`ORGANZATIONENDDATE`  datetime(6) NULL COMMENT '组织机构代码止期' ,
`TAXPAYERTYPE`  varchar(1) NULL COMMENT '纳税人识别号' ,
`TAXPAYERNO`  varchar(32) NULL ,
`AGREEMENTNO`  varchar(30) NULL COMMENT '经理协议方案代码' ,
`AGREEMENTNAME`  varchar(100) NULL COMMENT '经理协议方案名称' ,
`AGREEMENTNO1`  varchar(30) NULL COMMENT '主管协议方案代码' ,
`AGREEMENTNAME1`  varchar(100) NULL COMMENT '主管协议方案名称' ,
PRIMARY KEY (`AGENTCODE`)
)

;

-- ----------------------------
-- Indexes structure for table FHEXITEMKIND
-- ----------------------------

-- ----------------------------
-- Indexes structure for table FHRISK
-- ----------------------------

-- ----------------------------
-- Indexes structure for table FHSECTION
-- ----------------------------

-- ----------------------------
-- Indexes structure for table FHTREATY
-- ----------------------------

-- ----------------------------
-- Indexes structure for table FHXLAYER
-- ----------------------------

-- ----------------------------
-- Indexes structure for table FHXRISK
-- ----------------------------

-- ----------------------------
-- Indexes structure for table FHXSECTION
-- ----------------------------

-- ----------------------------
-- Indexes structure for table FHXTREATY
-- ----------------------------

-- ----------------------------
-- Indexes structure for table PRPDCLASS
-- ----------------------------

-- ----------------------------
-- Indexes structure for table PRPDCLAUSE
-- ----------------------------

-- ----------------------------
-- Indexes structure for table PRPDCODERISK
-- ----------------------------

-- ----------------------------
-- Indexes structure for table PRPDEFILE
-- ----------------------------

-- ----------------------------
-- Indexes structure for table PRPDITEM
-- ----------------------------

-- ----------------------------
-- Indexes structure for table PRPDITEMLIBRARY
-- ----------------------------

-- ----------------------------
-- Indexes structure for table PRPDKIND
-- ----------------------------

-- ----------------------------
-- Indexes structure for table PRPDKINDITEM
-- ----------------------------

-- ----------------------------
-- Indexes structure for table PRPDKINDLIBRARY
-- ----------------------------

-- ----------------------------
-- Indexes structure for table PRPDLOGSETTING
-- ----------------------------

-- ----------------------------
-- Indexes structure for table PRPDPLANPROPS
-- ----------------------------

-- ----------------------------
-- Indexes structure for table PRPDPLANSUB
-- ----------------------------

-- ----------------------------
-- Indexes structure for table PRPDPOLICYPAY
-- ----------------------------

-- ----------------------------
-- Indexes structure for table PRPDPROPS
-- ----------------------------

-- ----------------------------
-- Indexes structure for table PRPDRATION
-- ----------------------------

-- ----------------------------
-- Indexes structure for table PRPDRISK
-- ----------------------------
CREATE INDEX `IDX_PRPDRISK_CLASSCODE` ON `PRPDRISK`(`CLASSCODE`) ;

-- ----------------------------
-- Indexes structure for table PRPDRISKCLAUSE
-- ----------------------------

-- ----------------------------
-- Indexes structure for table PRPDRISKPLAN
-- ----------------------------

-- ----------------------------
-- Indexes structure for table PRPDSHORTRATELIBRARY
-- ----------------------------

-- ----------------------------
-- Indexes structure for table PRPMAXNO
-- ----------------------------
CREATE INDEX `IDX_PRPMAXNO_TABLENAME` ON `PRPMAXNO`(`TABLENAME`, `GROUPNO`) ;

-- ----------------------------
-- Indexes structure for table PRPSAGENT
-- ----------------------------
