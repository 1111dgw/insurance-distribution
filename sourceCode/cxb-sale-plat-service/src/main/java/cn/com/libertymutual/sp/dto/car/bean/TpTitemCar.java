package cn.com.libertymutual.sp.dto.car.bean;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 * Created by Ryan on 2016-09-05.
 */
public class TpTitemCar  implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 5681294967000863083L;
	private int id;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    private String flowId;
    
    private String countryCode;
    public String getFlowId() {
        return flowId;
    }
    public void setFlowId(String flowId) {
        this.flowId = flowId;
    }
    private String runAreaCode;
    public String getRunAreaCode() {
        return runAreaCode;
    }
    public void setRunAreaCode(String runAreaCode) {
        this.runAreaCode = runAreaCode;
    }
    private String whethelicenses;
    public String getwhethelicenses() {
        return whethelicenses;
    }
    public void setwhethelicenses(String whethelicenses) {
        this.whethelicenses = whethelicenses;
    }
    private String whethercar;
    public String getWhethercar() {
  		return whethercar;
  	}
  	public void setWhethercar(String whethercar) {
  		this.whethercar = whethercar;
  	}
    private String newOldLogo;
  
    public String getNewOldLogo() {
        return newOldLogo;
    }
    public void setNewOldLogo(String newOldLogo) {
        this.newOldLogo = newOldLogo;
    }
    private String licenseNo;
    public String getLicenseNo() {
        return licenseNo;
    }
    public void setLicenseNo(String licenseNo) {
        this.licenseNo = licenseNo;
    }
    private String carKindCode;
    public String getCarKindCode() {
        return carKindCode;
    }
    public void setCarKindCode(String carKindCode) {
        this.carKindCode = carKindCode;
    }
    private String useNatureCode;
    public String getUseNatureCode() {
        return useNatureCode;
    }
    public void setUseNatureCode(String useNatureCode) {
        this.useNatureCode = useNatureCode;
    }
    private String engineNo;
    public String getEngineNo() {
        return engineNo;
    }
    public void setEngineNo(String engineNo) {
        this.engineNo = engineNo;
    }
    private String vinNo;
    public String getVinNo() {
        return vinNo;
    }
    public void setVinNo(String vinNo) {
        this.vinNo = vinNo;
    }
    private String licenseKindCode;
    public String getLicenseKindCode() {
        return licenseKindCode;
    }
    public void setLicenseKindCode(String licenseKindCode) {
        this.licenseKindCode = licenseKindCode;
    }
    private Date enrollDate;
    public Date getEnrollDate() {
        return enrollDate;
    }
    public void setEnrollDate(Date enrollDate) {
        this.enrollDate = enrollDate;
    }
    private double purchasePrice;
    public double getPurchasePrice() {
        return purchasePrice;
    }
    public void setPurchasePrice(double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }
    private double tonCount;
    public double getTonCount() {
        return tonCount;
    }
    public void setTonCount(double tonCount) {
        this.tonCount = tonCount;
    }
    private double exhaustScale;
    public double getExhaustScale() {
        return exhaustScale;
    }
    public void setExhaustScale(double exhaustScale) {
        this.exhaustScale = exhaustScale;
    }
    private int seatCount;
    public int getSeatCount() {
        return seatCount;
    }
    public void setSeatCount(int seatCount) {
        this.seatCount = seatCount;
    }
    private String fuelType;
    public String getFuelType() {
        return fuelType;
    }
    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }
    private String carOwner;
    public String getCarOwner() {
        return carOwner;
    }
    public void setCarOwner(String carOwner) {
        this.carOwner = carOwner;
    }
    private String purchaseDate;
    public String getPurchaseDate() {
        return purchaseDate;
    }
    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }
    private String actualValue;
    public String getActualValue() {
        return actualValue;
    }
    public void setActualValue(String actualValue) {
        this.actualValue = actualValue;
    }
    private String modelCode;
    public String getModelCode() {
        return modelCode;
    }
    public void setModelCode(String modelCode) {
        this.modelCode = modelCode;
    }
    private String vehicleModel;
    public String getVehicleModel() {
        return vehicleModel;
    }
    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }
    private String vehicleCategoryCode;
    public String getVehicleCategoryCode() {
        return vehicleCategoryCode;
    }
    public void setVehicleCategoryCode(String vehicleCategoryCode) {
        this.vehicleCategoryCode = vehicleCategoryCode;
    }
    private String chgOwnerFlag;
    public String getChgOwnerFlag() {
        return chgOwnerFlag;
    }
    public void setChgOwnerFlag(String chgOwnerFlag) {
        this.chgOwnerFlag = chgOwnerFlag;
    }
    private String loanVehicleFlag;
    public String getLoanVehicleFlag() {
        return loanVehicleFlag;
    }
    public void setLoanVehicleFlag(String loanVehicleFlag) {
        this.loanVehicleFlag = loanVehicleFlag;
    }
    private String transferDate;
    public String getTransferDate() {
        return transferDate;
    }
    public void setTransferDate(String transferDate) {
        this.transferDate = transferDate;
    }
   
    private  String  poWeight;
    
    public String getPoWeight() {
		return poWeight;
	}
	public void setPoWeight(String poWeight) {
		this.poWeight = poWeight;
	}
	
	 private  String vinNoQueryFlag;
	public String getVinNoQueryFlag() {
		return vinNoQueryFlag;
	}
	public void setVinNoQueryFlag(String vinNoQueryFlag) {
		this.vinNoQueryFlag = vinNoQueryFlag;
	}
	
	/**
	 * 北京接口添加字段--------start
	 */
	
	private String certificateDate;
	public String getCertificateDate() {
		return certificateDate;
	}
	public void setCertificateDate(String certificateDate) {
		this.certificateDate = certificateDate;
	}
	private String certificateNo;
	public String getCertificateNo() {
		return certificateNo;
	}
	public void setCertificateNo(String certificateNo) {
		this.certificateNo = certificateNo;
	}
	private String certificateType;
	public String getCertificateType() {
		return certificateType;
	}
	public void setCertificateType(String certificateType) {
		this.certificateType = certificateType;
	}
	private String madeDate;
	public String getMadeDate() {
		return madeDate;
	}
	public void setMadeDate(String madeDate) {
		this.madeDate = madeDate;
	}
	private String vehicleStyleDesc;
	public String getVehicleStyleDesc() {
		return vehicleStyleDesc;
	}
	public void setVehicleStyleDesc(String vehicleStyleDesc) {
		this.vehicleStyleDesc = vehicleStyleDesc;
	}
	private String madeFactory;
	public String getMadeFactory() {
		return madeFactory;
	}
	public void setMadeFactory(String madeFactory) {
		this.madeFactory = madeFactory;
	}
	
	private String vehicleBrand;
	public String getVehicleBrand() {
		return vehicleBrand;
	}
	public void setVehicleBrand(String vehicleBrand) {
		this.vehicleBrand = vehicleBrand;
	}
	private String certificateDate2;//发证日期
	public String getCertificateDate2() {
		return certificateDate2;
	}
	public void setCertificateDate2(String certificateDate2) {
		this.certificateDate2 = certificateDate2;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TpTitemCar other = (TpTitemCar) obj;
		if (actualValue == null) {
			if (other.actualValue != null)
				return false;
		} else if (!actualValue.equals(other.actualValue))
			return false;
		if (carInsuredRelation == null) {
			if (other.carInsuredRelation != null)
				return false;
		} else if (!carInsuredRelation.equals(other.carInsuredRelation))
			return false;
		if (carKindCode == null) {
			if (other.carKindCode != null)
				return false;
		} else if (!carKindCode.equals(other.carKindCode))
			return false;
		if (carOwner == null) {
			if (other.carOwner != null)
				return false;
		} else if (!carOwner.equals(other.carOwner))
			return false;
		if (certificateDate == null) {
			if (other.certificateDate != null)
				return false;
		} else if (!certificateDate.equals(other.certificateDate))
			return false;
		if (certificateDate2 == null) {
			if (other.certificateDate2 != null)
				return false;
		} else if (!certificateDate2.equals(other.certificateDate2))
			return false;
		if (certificateNo == null) {
			if (other.certificateNo != null)
				return false;
		} else if (!certificateNo.equals(other.certificateNo))
			return false;
		if (certificateType == null) {
			if (other.certificateType != null)
				return false;
		} else if (!certificateType.equals(other.certificateType))
			return false;
		if (chgOwnerFlag == null) {
			if (other.chgOwnerFlag != null)
				return false;
		} else if (!chgOwnerFlag.equals(other.chgOwnerFlag))
			return false;
		if (companyType == null) {
			if (other.companyType != null)
				return false;
		} else if (!companyType.equals(other.companyType))
			return false;
		if (countryCode == null) {
			if (other.countryCode != null)
				return false;
		} else if (!countryCode.equals(other.countryCode))
			return false;
		if (engineNo == null) {
			if (other.engineNo != null)
				return false;
		} else if (!engineNo.equals(other.engineNo))
			return false;
		if (enrollDate == null) {
			if (other.enrollDate != null)
				return false;
		} else if (!enrollDate.equals(other.enrollDate))
			return false;
		if (Double.doubleToLongBits(exhaustScale) != Double
				.doubleToLongBits(other.exhaustScale))
			return false;
		if (flowId == null) {
			if (other.flowId != null)
				return false;
		} else if (!flowId.equals(other.flowId))
			return false;
		if (fuelType == null) {
			if (other.fuelType != null)
				return false;
		} else if (!fuelType.equals(other.fuelType))
			return false;
		if (id != other.id)
			return false;
		if (licenseKindCode == null) {
			if (other.licenseKindCode != null)
				return false;
		} else if (!licenseKindCode.equals(other.licenseKindCode))
			return false;
		if (licenseNo == null) {
			if (other.licenseNo != null)
				return false;
		} else if (!licenseNo.equals(other.licenseNo))
			return false;
		if (loanBeneficiary == null) {
			if (other.loanBeneficiary != null)
				return false;
		} else if (!loanBeneficiary.equals(other.loanBeneficiary))
			return false;
		if (loanVehicleFlag == null) {
			if (other.loanVehicleFlag != null)
				return false;
		} else if (!loanVehicleFlag.equals(other.loanVehicleFlag))
			return false;
		if (madeDate == null) {
			if (other.madeDate != null)
				return false;
		} else if (!madeDate.equals(other.madeDate))
			return false;
		if (madeFactory == null) {
			if (other.madeFactory != null)
				return false;
		} else if (!madeFactory.equals(other.madeFactory))
			return false;
		if (modelCode == null) {
			if (other.modelCode != null)
				return false;
		} else if (!modelCode.equals(other.modelCode))
			return false;
		if (newOldLogo == null) {
			if (other.newOldLogo != null)
				return false;
		} else if (!newOldLogo.equals(other.newOldLogo))
			return false;
		if (poWeight == null) {
			if (other.poWeight != null)
				return false;
		} else if (!poWeight.equals(other.poWeight))
			return false;
		if (purchaseDate == null) {
			if (other.purchaseDate != null)
				return false;
		} else if (!purchaseDate.equals(other.purchaseDate))
			return false;
		if (Double.doubleToLongBits(purchasePrice) != Double
				.doubleToLongBits(other.purchasePrice))
			return false;
		if (runAreaCode == null) {
			if (other.runAreaCode != null)
				return false;
		} else if (!runAreaCode.equals(other.runAreaCode))
			return false;
		if (seatCount != other.seatCount)
			return false;
		if (Double.doubleToLongBits(tonCount) != Double
				.doubleToLongBits(other.tonCount))
			return false;
		if (transferDate == null) {
			if (other.transferDate != null)
				return false;
		} else if (!transferDate.equals(other.transferDate))
			return false;
		if (useNatureCode == null) {
			if (other.useNatureCode != null)
				return false;
		} else if (!useNatureCode.equals(other.useNatureCode))
			return false;
		if (vehicleBrand == null) {
			if (other.vehicleBrand != null)
				return false;
		} else if (!vehicleBrand.equals(other.vehicleBrand))
			return false;
		if (vehicleCategoryCode == null) {
			if (other.vehicleCategoryCode != null)
				return false;
		} else if (!vehicleCategoryCode.equals(other.vehicleCategoryCode))
			return false;
		if (vehicleModel == null) {
			if (other.vehicleModel != null)
				return false;
		} else if (!vehicleModel.equals(other.vehicleModel))
			return false;
		if (vehicleStyleDesc == null) {
			if (other.vehicleStyleDesc != null)
				return false;
		} else if (!vehicleStyleDesc.equals(other.vehicleStyleDesc))
			return false;
		if (vinNo == null) {
			if (other.vinNo != null)
				return false;
		} else if (!vinNo.equals(other.vinNo))
			return false;
		if (vinNoQueryFlag == null) {
			if (other.vinNoQueryFlag != null)
				return false;
		} else if (!vinNoQueryFlag.equals(other.vinNoQueryFlag))
			return false;
		if (whethelicenses == null) {
			if (other.whethelicenses != null)
				return false;
		} else if (!whethelicenses.equals(other.whethelicenses))
			return false;
		if (whethercar == null) {
			if (other.whethercar != null)
				return false;
		} else if (!whethercar.equals(other.whethercar))
			return false;
		return true;
	}
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((actualValue == null) ? 0 : actualValue.hashCode());
		result = prime
				* result
				+ ((carInsuredRelation == null) ? 0 : carInsuredRelation
						.hashCode());
		result = prime * result
				+ ((carKindCode == null) ? 0 : carKindCode.hashCode());
		result = prime * result
				+ ((carOwner == null) ? 0 : carOwner.hashCode());
		result = prime * result
				+ ((certificateDate == null) ? 0 : certificateDate.hashCode());
		result = prime
				* result
				+ ((certificateDate2 == null) ? 0 : certificateDate2.hashCode());
		result = prime * result
				+ ((certificateNo == null) ? 0 : certificateNo.hashCode());
		result = prime * result
				+ ((certificateType == null) ? 0 : certificateType.hashCode());
		result = prime * result
				+ ((chgOwnerFlag == null) ? 0 : chgOwnerFlag.hashCode());
		result = prime * result
				+ ((companyType == null) ? 0 : companyType.hashCode());
		result = prime * result
				+ ((countryCode == null) ? 0 : countryCode.hashCode());
		result = prime * result
				+ ((engineNo == null) ? 0 : engineNo.hashCode());
		result = prime * result
				+ ((enrollDate == null) ? 0 : enrollDate.hashCode());
		long temp;
		temp = Double.doubleToLongBits(exhaustScale);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((flowId == null) ? 0 : flowId.hashCode());
		result = prime * result
				+ ((fuelType == null) ? 0 : fuelType.hashCode());
		result = prime * result + id;
		result = prime * result
				+ ((licenseKindCode == null) ? 0 : licenseKindCode.hashCode());
		result = prime * result
				+ ((licenseNo == null) ? 0 : licenseNo.hashCode());
		result = prime * result
				+ ((loanBeneficiary == null) ? 0 : loanBeneficiary.hashCode());
		result = prime * result
				+ ((loanVehicleFlag == null) ? 0 : loanVehicleFlag.hashCode());
		result = prime * result
				+ ((madeDate == null) ? 0 : madeDate.hashCode());
		result = prime * result
				+ ((madeFactory == null) ? 0 : madeFactory.hashCode());
		result = prime * result
				+ ((modelCode == null) ? 0 : modelCode.hashCode());
		result = prime * result
				+ ((newOldLogo == null) ? 0 : newOldLogo.hashCode());
		result = prime * result
				+ ((poWeight == null) ? 0 : poWeight.hashCode());
		result = prime * result
				+ ((purchaseDate == null) ? 0 : purchaseDate.hashCode());
		temp = Double.doubleToLongBits(purchasePrice);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result
				+ ((runAreaCode == null) ? 0 : runAreaCode.hashCode());
		result = prime * result + seatCount;
		temp = Double.doubleToLongBits(tonCount);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result
				+ ((transferDate == null) ? 0 : transferDate.hashCode());
		result = prime * result
				+ ((useNatureCode == null) ? 0 : useNatureCode.hashCode());
		result = prime * result
				+ ((vehicleBrand == null) ? 0 : vehicleBrand.hashCode());
		result = prime
				* result
				+ ((vehicleCategoryCode == null) ? 0 : vehicleCategoryCode
						.hashCode());
		result = prime * result
				+ ((vehicleModel == null) ? 0 : vehicleModel.hashCode());
		result = prime
				* result
				+ ((vehicleStyleDesc == null) ? 0 : vehicleStyleDesc.hashCode());
		result = prime * result + ((vinNo == null) ? 0 : vinNo.hashCode());
		result = prime * result
				+ ((vinNoQueryFlag == null) ? 0 : vinNoQueryFlag.hashCode());
		result = prime * result
				+ ((whethelicenses == null) ? 0 : whethelicenses.hashCode());
		result = prime * result
				+ ((whethercar == null) ? 0 : whethercar.hashCode());
		return result;
	}
	@Override
	public String toString() {
		return "TpTitemCar [id=" + id + ", flowId=" + flowId + ", countryCode="
				+ countryCode + ", runAreaCode=" + runAreaCode
				+ ", whethelicenses=" + whethelicenses + ", whethercar="
				+ whethercar + ", newOldLogo=" + newOldLogo + ", licenseNo="
				+ licenseNo + ", carKindCode=" + carKindCode
				+ ", useNatureCode=" + useNatureCode + ", engineNo=" + engineNo
				+ ", vinNo=" + vinNo + ", licenseKindCode=" + licenseKindCode
				+ ", enrollDate=" + enrollDate + ", purchasePrice="
				+ purchasePrice + ", tonCount=" + tonCount + ", exhaustScale="
				+ exhaustScale + ", seatCount=" + seatCount + ", fuelType="
				+ fuelType + ", carOwner=" + carOwner + ", purchaseDate="
				+ purchaseDate + ", actualValue=" + actualValue
				+ ", modelCode=" + modelCode + ", vehicleModel=" + vehicleModel
				+ ", vehicleCategoryCode=" + vehicleCategoryCode
				+ ", chgOwnerFlag=" + chgOwnerFlag + ", loanVehicleFlag="
				+ loanVehicleFlag + ", transferDate=" + transferDate
				+ ", poWeight=" + poWeight + ", vinNoQueryFlag="
				+ vinNoQueryFlag + ", certificateDate=" + certificateDate
				+ ", certificateNo=" + certificateNo + ", certificateType="
				+ certificateType + ", madeDate=" + madeDate
				+ ", vehicleStyleDesc=" + vehicleStyleDesc + ", madeFactory="
				+ madeFactory + ", vehicleBrand=" + vehicleBrand
				+ ", certificateDate2=" + certificateDate2
				+ ", carInsuredRelation=" + carInsuredRelation
				+ ", companyType=" + companyType + ", loanBeneficiary="
				+ loanBeneficiary + "]";
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
	private String 	carInsuredRelation ;//		车主类型	String		N	默认：3===3-自然人 4-非自然人
	private String	companyType	;//	企业类型	  String		N	0:一般企业,1:军队、武警等机关性质 
	private String	loanBeneficiary;	//	贷款车受益人	String		N	
	
	/**
	 * 车主类型默认：3===3-自然人 4-非自然人
	 * @return
	 */
	@Basic
	 @javax.persistence.Column(name = "CAR_INSURED_RELATION")
	public String getCarInsuredRelation() {
		return carInsuredRelation;
	}
	
	/**
	 * 企业类型	0:一般企业,1:军队、武警等机关性质  当车主为非自然人时必传
	 * @return
	 */
	@Basic
	 @javax.persistence.Column(name = "COMPANY_TYPE")
	public String getCompanyType() {
		return companyType;
	}
	
	/**
	 * 贷款车受益人
	 * @return
	 */
	@Basic
	 @javax.persistence.Column(name = "LOAN_BENEFICIARY")
	public String getLoanBeneficiary() {
		return loanBeneficiary;
	}
	
	public void setCarInsuredRelation(String carInsuredRelation) {
		this.carInsuredRelation = carInsuredRelation;
	}
	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}
	public void setLoanBeneficiary(String loanBeneficiary) {
		this.loanBeneficiary = loanBeneficiary;
	}
	
}