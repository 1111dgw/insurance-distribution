package cn.com.libertymutual.sp.dto.axtx;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CommonQueryRequestDTO extends RequestBaseDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4327671355137839832L;
	private String documentNo;// 业务号：投保单号、批单号、保单号等
	private List<String> documentNos;// 业务号：投保单号、批单号、保单号等

	public String getDocumentNo() {
		return documentNo;
	}

	public void setDocumentNo(String documentNo) {
		this.documentNo = documentNo;
	}

	public List<String> getDocumentNos() {
		return documentNos == null ? new ArrayList<>() : documentNos;
	}

	public void setDocumentNos(List<String> documentNos) {
		this.documentNos = documentNos;
	}

}