package cn.com.libertymutual.sp.dto.savePlan;

import cn.com.libertymutual.sp.dto.savePlan.base.ProposalBaseRequestDto;

public class ProposalSaveForRequestDto extends ProposalBaseRequestDto{
	/**
	 * 
	 */
	private static final long serialVersionUID = -893077075825220926L;
	
	
	private PolicyDataDto policyDataDto;
	 // 1为自动提交，0为不自动提交
   private String isAutoUndwrt;
   




	public PolicyDataDto getPolicyDataDto() {
		return policyDataDto;
	}

	public void setPolicyDataDto(PolicyDataDto policyDataDto) {
		this.policyDataDto = policyDataDto;
	}

	public String getIsAutoUndwrt() {
		return isAutoUndwrt;
	}

	public void setIsAutoUndwrt(String isAutoUndwrt) {
		this.isAutoUndwrt = isAutoUndwrt;
	}
	
}


