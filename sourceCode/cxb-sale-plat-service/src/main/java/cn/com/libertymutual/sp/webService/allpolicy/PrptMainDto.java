
package cn.com.libertymutual.sp.webService.allpolicy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for prptMainDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prptMainDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="affinityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="agreementNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="anonymityFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="arbitBoardName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="argueSolution" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="argueSolutionName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="businessFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="businessLanguage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="businessNatureName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="classCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="coinsFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="comCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="contractNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="disCountLimit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="editType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="endDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="endHour" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="handler1Code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inputDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="judicalScope" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mainRemark" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="makeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="operateDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="operatorCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="papolicyno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="policyNatureFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="policyNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="policySort" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="policyType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="proposalNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="riskCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="riskVersion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="signDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="startDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="startHour" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sumAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="sumPremium" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="underWriteCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="underWriteEndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="updaterCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prptMainDto", propOrder = {
    "affinityCode",
    "agreementNo",
    "anonymityFlag",
    "arbitBoardName",
    "argueSolution",
    "argueSolutionName",
    "businessFlag",
    "businessLanguage",
    "businessNatureName",
    "classCode",
    "coinsFlag",
    "comCode",
    "contractNo",
    "disCountLimit",
    "editType",
    "endDate",
    "endHour",
    "handler1Code",
    "inputDate",
    "judicalScope",
    "mainRemark",
    "makeCode",
    "operateDate",
    "operatorCode",
    "papolicyno",
    "policyNatureFlag",
    "policyNo",
    "policySort",
    "policyType",
    "proposalNo",
    "riskCode",
    "riskVersion",
    "signDate",
    "startDate",
    "startHour",
    "sumAmount",
    "sumPremium",
    "underWriteCode",
    "underWriteEndDate",
    "updaterCode"
})
public class PrptMainDto implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    protected String affinityCode;
    protected String agreementNo;
    protected String anonymityFlag;
    protected String arbitBoardName;
    protected String argueSolution;
    protected String argueSolutionName;
    protected String businessFlag;
    protected String businessLanguage;
    protected String businessNatureName;
    protected String classCode;
    protected String coinsFlag;
    protected String comCode;
    protected String contractNo;
    protected String disCountLimit;
    protected String editType;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar endDate;
    protected String endHour;
    protected String handler1Code;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar inputDate;
    protected String judicalScope;
    protected String mainRemark;
    protected String makeCode;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar operateDate;
    protected String operatorCode;
    protected String papolicyno;
    protected String policyNatureFlag;
    protected String policyNo;
    protected String policySort;
    protected String policyType;
    protected String proposalNo;
    protected String riskCode;
    protected String riskVersion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar signDate;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startDate;
    protected String startHour;
    protected double sumAmount;
    protected double sumPremium;
    protected String underWriteCode;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar underWriteEndDate;
    protected String updaterCode;

    /**
     * Gets the value of the affinityCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAffinityCode() {
        return affinityCode;
    }

    /**
     * Sets the value of the affinityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAffinityCode(String value) {
        this.affinityCode = value;
    }

    /**
     * Gets the value of the agreementNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgreementNo() {
        return agreementNo;
    }

    /**
     * Sets the value of the agreementNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgreementNo(String value) {
        this.agreementNo = value;
    }

    /**
     * Gets the value of the anonymityFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnonymityFlag() {
        return anonymityFlag;
    }

    /**
     * Sets the value of the anonymityFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnonymityFlag(String value) {
        this.anonymityFlag = value;
    }

    /**
     * Gets the value of the arbitBoardName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArbitBoardName() {
        return arbitBoardName;
    }

    /**
     * Sets the value of the arbitBoardName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArbitBoardName(String value) {
        this.arbitBoardName = value;
    }

    /**
     * Gets the value of the argueSolution property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArgueSolution() {
        return argueSolution;
    }

    /**
     * Sets the value of the argueSolution property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArgueSolution(String value) {
        this.argueSolution = value;
    }

    /**
     * Gets the value of the argueSolutionName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArgueSolutionName() {
        return argueSolutionName;
    }

    /**
     * Sets the value of the argueSolutionName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArgueSolutionName(String value) {
        this.argueSolutionName = value;
    }

    /**
     * Gets the value of the businessFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessFlag() {
        return businessFlag;
    }

    /**
     * Sets the value of the businessFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessFlag(String value) {
        this.businessFlag = value;
    }

    /**
     * Gets the value of the businessLanguage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessLanguage() {
        return businessLanguage;
    }

    /**
     * Sets the value of the businessLanguage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessLanguage(String value) {
        this.businessLanguage = value;
    }

    /**
     * Gets the value of the businessNatureName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessNatureName() {
        return businessNatureName;
    }

    /**
     * Sets the value of the businessNatureName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessNatureName(String value) {
        this.businessNatureName = value;
    }

    /**
     * Gets the value of the classCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassCode() {
        return classCode;
    }

    /**
     * Sets the value of the classCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassCode(String value) {
        this.classCode = value;
    }

    /**
     * Gets the value of the coinsFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoinsFlag() {
        return coinsFlag;
    }

    /**
     * Sets the value of the coinsFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoinsFlag(String value) {
        this.coinsFlag = value;
    }

    /**
     * Gets the value of the comCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCode() {
        return comCode;
    }

    /**
     * Sets the value of the comCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCode(String value) {
        this.comCode = value;
    }

    /**
     * Gets the value of the contractNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContractNo() {
        return contractNo;
    }

    /**
     * Sets the value of the contractNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContractNo(String value) {
        this.contractNo = value;
    }

    /**
     * Gets the value of the disCountLimit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisCountLimit() {
        return disCountLimit;
    }

    /**
     * Sets the value of the disCountLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisCountLimit(String value) {
        this.disCountLimit = value;
    }

    /**
     * Gets the value of the editType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEditType() {
        return editType;
    }

    /**
     * Sets the value of the editType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEditType(String value) {
        this.editType = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the endHour property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndHour() {
        return endHour;
    }

    /**
     * Sets the value of the endHour property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndHour(String value) {
        this.endHour = value;
    }

    /**
     * Gets the value of the handler1Code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHandler1Code() {
        return handler1Code;
    }

    /**
     * Sets the value of the handler1Code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHandler1Code(String value) {
        this.handler1Code = value;
    }

    /**
     * Gets the value of the inputDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInputDate() {
        return inputDate;
    }

    /**
     * Sets the value of the inputDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInputDate(XMLGregorianCalendar value) {
        this.inputDate = value;
    }

    /**
     * Gets the value of the judicalScope property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJudicalScope() {
        return judicalScope;
    }

    /**
     * Sets the value of the judicalScope property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJudicalScope(String value) {
        this.judicalScope = value;
    }

    /**
     * Gets the value of the mainRemark property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMainRemark() {
        return mainRemark;
    }

    /**
     * Sets the value of the mainRemark property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMainRemark(String value) {
        this.mainRemark = value;
    }

    /**
     * Gets the value of the makeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMakeCode() {
        return makeCode;
    }

    /**
     * Sets the value of the makeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMakeCode(String value) {
        this.makeCode = value;
    }

    /**
     * Gets the value of the operateDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOperateDate() {
        return operateDate;
    }

    /**
     * Sets the value of the operateDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOperateDate(XMLGregorianCalendar value) {
        this.operateDate = value;
    }

    /**
     * Gets the value of the operatorCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperatorCode() {
        return operatorCode;
    }

    /**
     * Sets the value of the operatorCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperatorCode(String value) {
        this.operatorCode = value;
    }

    /**
     * Gets the value of the papolicyno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPapolicyno() {
        return papolicyno;
    }

    /**
     * Sets the value of the papolicyno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPapolicyno(String value) {
        this.papolicyno = value;
    }

    /**
     * Gets the value of the policyNatureFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyNatureFlag() {
        return policyNatureFlag;
    }

    /**
     * Sets the value of the policyNatureFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyNatureFlag(String value) {
        this.policyNatureFlag = value;
    }

    /**
     * Gets the value of the policyNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyNo() {
        return policyNo;
    }

    /**
     * Sets the value of the policyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyNo(String value) {
        this.policyNo = value;
    }

    /**
     * Gets the value of the policySort property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicySort() {
        return policySort;
    }

    /**
     * Sets the value of the policySort property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicySort(String value) {
        this.policySort = value;
    }

    /**
     * Gets the value of the policyType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyType() {
        return policyType;
    }

    /**
     * Sets the value of the policyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyType(String value) {
        this.policyType = value;
    }

    /**
     * Gets the value of the proposalNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProposalNo() {
        return proposalNo;
    }

    /**
     * Sets the value of the proposalNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProposalNo(String value) {
        this.proposalNo = value;
    }

    /**
     * Gets the value of the riskCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiskCode() {
        return riskCode;
    }

    /**
     * Sets the value of the riskCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskCode(String value) {
        this.riskCode = value;
    }

    /**
     * Gets the value of the riskVersion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiskVersion() {
        return riskVersion;
    }

    /**
     * Sets the value of the riskVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskVersion(String value) {
        this.riskVersion = value;
    }

    /**
     * Gets the value of the signDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSignDate() {
        return signDate;
    }

    /**
     * Sets the value of the signDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSignDate(XMLGregorianCalendar value) {
        this.signDate = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the startHour property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartHour() {
        return startHour;
    }

    /**
     * Sets the value of the startHour property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartHour(String value) {
        this.startHour = value;
    }

    /**
     * Gets the value of the sumAmount property.
     * 
     */
    public double getSumAmount() {
        return sumAmount;
    }

    /**
     * Sets the value of the sumAmount property.
     * 
     */
    public void setSumAmount(double value) {
        this.sumAmount = value;
    }

    /**
     * Gets the value of the sumPremium property.
     * 
     */
    public double getSumPremium() {
        return sumPremium;
    }

    /**
     * Sets the value of the sumPremium property.
     * 
     */
    public void setSumPremium(double value) {
        this.sumPremium = value;
    }

    /**
     * Gets the value of the underWriteCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnderWriteCode() {
        return underWriteCode;
    }

    /**
     * Sets the value of the underWriteCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnderWriteCode(String value) {
        this.underWriteCode = value;
    }

    /**
     * Gets the value of the underWriteEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getUnderWriteEndDate() {
        return underWriteEndDate;
    }

    /**
     * Sets the value of the underWriteEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setUnderWriteEndDate(XMLGregorianCalendar value) {
        this.underWriteEndDate = value;
    }

    /**
     * Gets the value of the updaterCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUpdaterCode() {
        return updaterCode;
    }

    /**
     * Sets the value of the updaterCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUpdaterCode(String value) {
        this.updaterCode = value;
    }

}
