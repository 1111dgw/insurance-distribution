package cn.com.libertymutual.production.service.api;

import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdcompany;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdcompanyWithBLOBs;
import cn.com.libertymutual.production.pojo.request.Request;

import com.github.pagehelper.PageInfo;

public interface PrpdCompanyService {
	
	/**
	 * 查询所有机构
	 * @return
	 */
	public PageInfo<PrpdcompanyWithBLOBs> findPrpdCompanys(Request request);
	
	/**
	 * 查询分支机构
	 * @return
	 */
	public List<PrpdcompanyWithBLOBs> findCompanys();

	/**
	 * 查询分支机构
	 * @param comcode
	 * @return
     */
	List<PrpdcompanyWithBLOBs> findCompanysBycodes(List<String> comcodes);
}
