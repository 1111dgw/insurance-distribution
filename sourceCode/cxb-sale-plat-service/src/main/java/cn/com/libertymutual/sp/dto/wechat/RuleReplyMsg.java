package cn.com.libertymutual.sp.dto.wechat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Entity - 规则回复消息
 * 
 * @author yeyc
 *
 */
@Entity
@Table(name = "t_rule_reply_msg")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "t_sequence")
public class RuleReplyMsg implements Serializable {

	private static final long serialVersionUID = -3709672700161728615L;
	private int id; 
	/** 回复消息类型（text-文本，image-图片，news-图文，voice-语音，video-视频） */
	private String msgType;
	
	/** 回复文本消息内容 */
	private String content;
	
	/** 回复消息素材ID */
	private Long materialId;
	
	/** 超链接 */
	private String link;
	
	/** 链接名称 */
	private String linkName;
	
	/** 所属规则 */
	@JsonIgnore
	private Rule rule;
	
	/** 素材对象列表 */
	private List<Object> materials = new ArrayList<Object>();
	
	@NotNull
	@Column(nullable = false, length = 50)
	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	@Id
	 @Column(name = "ID")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	@Column(length = 500)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Long getMaterialId() {
		return materialId;
	}

	public void setMaterialId(Long materialId) {
		this.materialId = materialId;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getLinkName() {
		return linkName;
	}

	public void setLinkName(String linkName) {
		this.linkName = linkName;
	}

	@NotEmpty
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="rule")
	public Rule getRule() {
		return rule;
	}

	public void setRule(Rule rule) {
		this.rule = rule;
	}

	@Transient
	public List<Object> getMaterials() {
		return materials;
	}

	public void setMaterials(List<Object> materials) {
		this.materials = materials;
	}
	
}

