package cn.com.libertymutual.sp.dto;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
//@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="root")
public class Epayment implements Serializable, Cloneable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected Long id;			
	protected String businessType;			
	protected String businessNo;			
	protected String amount;			
	protected String platformId;			
	protected String payNo;			
	protected String payStatus;			
	protected Date payTime;			
	protected String property00;			
	protected String property01;			
	protected String property02;			
	protected String property03;			
	protected String property04;			
	protected String property05;			
	protected String property06;			
	protected String property07;			
	protected String property08;			
	protected String property09;			
	protected String property10;			
	protected String property11;			
	protected String property12;			
	protected String property13;			
	protected String property14;			
	protected String property15;			
	protected String property16;			
	protected String property17;			
	protected String property18;			
	protected String property19;			
	protected Date createTime;			
	protected Date updateTime;			
	protected String requestIp;			
	protected String requestReferer;			
	protected String redirectUrl;			
	protected String noticUrl;			
	protected String callbackUrl;			
	protected String callbackStatus;			
	protected String callbackCode;			
	protected String callbackMessage;			
	protected String remarks;			
	protected String responseMessage;			
	protected Long version;			
	protected String branchCode;			
	protected String businessSource;			
	protected String payType;			
	protected String securityCode;			
	/** 订单id,主键,传给各个交易平台的交易号使用此id进行生成 */					
	public Long getId() {					
		return id;				
	}					
						
	public void setId(Long id) {					
		this.id = id;				
	}					
						
	/** 业务类型,同一业务类型下,business_no不能重复.核心系统传入policy表示保单数据 */					
	public String getBusinessType() {					
		return businessType;				
	}					
						
	public void setBusinessType(String businessType) {					
		this.businessType = businessType;				
	}					
						
	/** 业务号,由各个业务系统传入 */					
	public String getBusinessNo() {					
		return businessNo;				
	}					
						
	public void setBusinessNo(String businessNo) {					
		this.businessNo = businessNo;				
	}					
						
	/** 交易金额,单位为元 */					
	public String getAmount() {					
		return amount;				
	}					
						
	public void setAmount(String amount) {					
		this.amount = amount;				
	}					
						
	/** 选择的支付平台 */					
	public String getPlatformId() {					
		return platformId;				
	}					
						
	public void setPlatformId(String platformId) {					
		this.platformId = platformId;				
	}					
						
	/** 支付后由支付平台返回的支付号 */					
	public String getPayNo() {					
		return payNo;				
	}					
						
	public void setPayNo(String payNo) {					
		this.payNo = payNo;				
	}					
						
	/** 支付状态,支付状态：1.支付成功；0.支付失败；2.尚未支付(查询接口如果没有查询的时没有支付的数据可能会返回此状态)；9：尚未回写支付结果(此状态表示可能用户正在支付) */					
	public String getPayStatus() {					
		return payStatus;				
	}					
						
	public void setPayStatus(String payStatus) {					
		this.payStatus = payStatus;				
	}					
						
	/** 支付时间 */					
	public Date getPayTime() {					
		return payTime;				
	}					
						
	public void setPayTime(Date payTime) {					
		this.payTime = payTime;				
	}					
						
	/** property_XX为预留字段(银联平台可以使用前10个字段,长度为64字节) */					
	public String getProperty00() {					
		return property00;				
	}					
						
	public void setProperty00(String property00) {					
		this.property00 = property00;				
	}					
						
	public String getProperty01() {					
		return property01;				
	}					
						
	public void setProperty01(String property01) {					
		this.property01 = property01;				
	}					
						
	public String getProperty02() {					
		return property02;				
	}					
						
	public void setProperty02(String property02) {					
		this.property02 = property02;				
	}					
						
	public String getProperty03() {					
		return property03;				
	}					
						
	public void setProperty03(String property03) {					
		this.property03 = property03;				
	}					
						
	public String getProperty04() {					
		return property04;				
	}					
						
	public void setProperty04(String property04) {					
		this.property04 = property04;				
	}					
						
	public String getProperty05() {					
		return property05;				
	}					
						
	public void setProperty05(String property05) {					
		this.property05 = property05;				
	}					
						
	public String getProperty06() {					
		return property06;				
	}					
						
	public void setProperty06(String property06) {					
		this.property06 = property06;				
	}					
						
	public String getProperty07() {					
		return property07;				
	}					
						
	public void setProperty07(String property07) {					
		this.property07 = property07;				
	}					
						
	public String getProperty08() {					
		return property08;				
	}					
						
	public void setProperty08(String property08) {					
		this.property08 = property08;				
	}					
						
	public String getProperty09() {					
		return property09;				
	}					
						
	public void setProperty09(String property09) {					
		this.property09 = property09;				
	}					
						
	public String getProperty10() {					
		return property10;				
	}					
						
	public void setProperty10(String property10) {					
		this.property10 = property10;				
	}					
						
	public String getProperty11() {					
		return property11;				
	}					
						
	public void setProperty11(String property11) {					
		this.property11 = property11;				
	}					
						
	public String getProperty12() {					
		return property12;				
	}					
						
	public void setProperty12(String property12) {					
		this.property12 = property12;				
	}					
						
	public String getProperty13() {					
		return property13;				
	}					
						
	public void setProperty13(String property13) {					
		this.property13 = property13;				
	}					
						
	public String getProperty14() {					
		return property14;				
	}					
						
	public void setProperty14(String property14) {					
		this.property14 = property14;				
	}					
						
	public String getProperty15() {					
		return property15;				
	}					
						
	public void setProperty15(String property15) {					
		this.property15 = property15;				
	}					
						
	public String getProperty16() {					
		return property16;				
	}					
						
	public void setProperty16(String property16) {					
		this.property16 = property16;				
	}					
						
	public String getProperty17() {					
		return property17;				
	}					
						
	public void setProperty17(String property17) {					
		this.property17 = property17;				
	}					
						
	public String getProperty18() {					
		return property18;				
	}					
						
	public void setProperty18(String property18) {					
		this.property18 = property18;				
	}					
						
	public String getProperty19() {					
		return property19;				
	}					
						
	public void setProperty19(String property19) {					
		this.property19 = property19;				
	}					
						
	/** 数据创建时间 */					
	public Date getCreateTime() {					
		return createTime;				
	}					
						
	public void setCreateTime(Date createTime) {					
		this.createTime = createTime;				
	}					
						
	/** 数据更新时间,可能有对同一笔数据进行多次支付,比如第一次失败,再次至今支付 */					
	public Date getUpdateTime() {					
		return updateTime;				
	}					
						
	public void setUpdateTime(Date updateTime) {					
		this.updateTime = updateTime;				
	}					
						
	/** 客户端的请求地址 */					
	public String getRequestIp() {					
		return requestIp;				
	}					
						
	public void setRequestIp(String requestIp) {					
		this.requestIp = requestIp;				
	}					
						
	/** 客户端请求来源 */					
	public String getRequestReferer() {					
		return requestReferer;				
	}					
						
	public void setRequestReferer(String requestReferer) {					
		this.requestReferer = requestReferer;				
	}					
						
	/** 支付完成后的跳转地址 */					
	public String getRedirectUrl() {					
		return redirectUrl;				
	}					
						
	public void setRedirectUrl(String redirectUrl) {					
		this.redirectUrl = redirectUrl;				
	}					
						
	/** 支付完成后后台的通知地址 */					
	public String getNoticUrl() {					
		return noticUrl;				
	}					
						
	public void setNoticUrl(String noticUrl) {					
		this.noticUrl = noticUrl;				
	}					
						
	/** 支付完成后同步回调的地址 */					
	public String getCallbackUrl() {					
		return callbackUrl;				
	}					
						
	public void setCallbackUrl(String callbackUrl) {					
		this.callbackUrl = callbackUrl;				
	}					
						
	/** 调用callbackUrl的状态：1.成功；0.失败；8.不需要调用（没有传入callbackUrl）；9.尚未调用； */					
	public String getCallbackStatus() {					
		return callbackStatus;				
	}					
						
	public void setCallbackStatus(String callbackStatus) {					
		this.callbackStatus = callbackStatus;				
	}					
						
	/** callbackUrl返回的XML中的code字段 */					
	public String getCallbackCode() {					
		return callbackCode;				
	}					
						
	public void setCallbackCode(String callbackCode) {					
		this.callbackCode = callbackCode;				
	}					
						
	/** callbackUrl返回的XML中的message字段 */					
	public String getCallbackMessage() {					
		return callbackMessage;				
	}					
						
	public void setCallbackMessage(String callbackMessage) {					
		this.callbackMessage = callbackMessage;				
	}					
						
	/** 订单描述, */					
	public String getRemarks() {					
		return remarks;				
	}					
						
	public void setRemarks(String remarks) {					
		this.remarks = remarks;				
	}					
						
	/** 最近一次与平台交互的响应数据 */					
	public String getResponseMessage() {					
		return responseMessage;				
	}					
						
	public void setResponseMessage(String responseMessage) {					
		this.responseMessage = responseMessage;				
	}					
						
	/** 用于数据同步锁定 */					
	public Long getVersion() {					
		return version;				
	}					
						
	public void setVersion(Long version) {					
		this.version = version;				
	}					
						
	/** 归属机构代码 */					
	public String getBranchCode() {					
		return branchCode;				
	}					
						
	public void setBranchCode(String branchCode) {					
		this.branchCode = branchCode;				
	}					
						
	/** 数据来源,默认为core */					
	public String getBusinessSource() {					
		return businessSource;				
	}					
						
	public void setBusinessSource(String businessSource) {					
		this.businessSource = businessSource;				
	}					
						
	/** 支付方式(online:网上支付;phone:电话支付) */					
	public String getPayType() {					
		return payType;				
	}					
						
	public void setPayType(String payType) {					
		this.payType = payType;				
	}					
						
	/** 支付提交时的数据签名字符串,第二次提交时两次的签名字符串必须一致,如果不一致则不允许提交 */					
	public String getSecurityCode() {					
		return securityCode;				
	}					
						
	public void setSecurityCode(String securityCode) {					
		this.securityCode = securityCode;				
	}

	@Override
	public String toString() {
		return "Epayment [id=" + id + ", businessType=" + businessType
				+ ", businessNo=" + businessNo + ", amount=" + amount
				+ ", platformId=" + platformId + ", payNo=" + payNo
				+ ", payStatus=" + payStatus + ", payTime=" + payTime
				+ ", property00=" + property00 + ", property01=" + property01
				+ ", property02=" + property02 + ", property03=" + property03
				+ ", property04=" + property04 + ", property05=" + property05
				+ ", property06=" + property06 + ", property07=" + property07
				+ ", property08=" + property08 + ", property09=" + property09
				+ ", property10=" + property10 + ", property11=" + property11
				+ ", property12=" + property12 + ", property13=" + property13
				+ ", property14=" + property14 + ", property15=" + property15
				+ ", property16=" + property16 + ", property17=" + property17
				+ ", property18=" + property18 + ", property19=" + property19
				+ ", createTime=" + createTime + ", updateTime=" + updateTime
				+ ", requestIp=" + requestIp + ", requestReferer="
				+ requestReferer + ", redirectUrl=" + redirectUrl
				+ ", noticUrl=" + noticUrl + ", callbackUrl=" + callbackUrl
				+ ", callbackStatus=" + callbackStatus + ", callbackCode="
				+ callbackCode + ", callbackMessage=" + callbackMessage
				+ ", remarks=" + remarks + ", responseMessage="
				+ responseMessage + ", version=" + version + ", branchCode="
				+ branchCode + ", businessSource=" + businessSource
				+ ", payType=" + payType + ", securityCode=" + securityCode
				+ "]";
	}		
	
	
}							


