package cn.com.libertymutual.sp.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import cn.com.libertymutual.core.redis.util.RedisUtils;
import cn.com.libertymutual.core.util.Constants;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpKeyword;
import cn.com.libertymutual.sp.dao.KeywordDao;
import cn.com.libertymutual.sp.dao.UserDao;
import cn.com.libertymutual.sp.service.api.KeywordService;
import cn.com.libertymutual.wx.service.WeChatService;

@Service("keywordService")
public class KeywordServiceImpl implements KeywordService{

	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private KeywordDao keywordDao;
	@Autowired
	private UserDao userDao;
	@Resource
	private WeChatService weChatService;
	
	@Resource
	private RedisUtils redisUtils;
	
	
	@Override
	public ServiceResult allKeywords(String type, Integer pageNumber, Integer pageSize) {
		ServiceResult sr = new ServiceResult();
		Sort sort = new Sort(Direction.DESC, "id");
		sr.setResult(keywordDao.findAll(new Specification<TbSpKeyword>() {
			public Predicate toPredicate(Root<TbSpKeyword> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicate = new ArrayList<Predicate>();
				if (StringUtils.isNotEmpty(type) && !"-1".equals(type)) {
					log.info(type);
					predicate.add(cb.equal(root.get("type").as(String.class),  type ));
				}
				Predicate[] pre = new Predicate[predicate.size()];
				return query.where(predicate.toArray(pre)).getRestriction();
			}
		}, PageRequest.of(pageNumber - 1, pageSize, sort)));
		
		return sr;
	}



	@Override
	public ServiceResult addOrUpdateKeyword(Integer id,String type, String content) {
		ServiceResult sr = new ServiceResult();
		if (null == id) {//新增
			TbSpKeyword k = new TbSpKeyword();
			k.setType(type);
			k.setContent(content);
			sr.setResult(keywordDao.save(k));
		}else {//修改
			TbSpKeyword dbk = keywordDao.findById(id).get();
			dbk.setType(type);
			dbk.setContent(content);
			sr.setResult(keywordDao.save(dbk));
		}
		
		redisUtils.deletelike(Constants.SHOP_KEYWORD);
		return sr;
	}



	@Override
	public ServiceResult saveShopName(String userCode, String shopName, String shopIntroduct) {
		ServiceResult sr = new ServiceResult();
		Map<String, List<TbSpKeyword>> keywordMap = (HashMap<String, List<TbSpKeyword>>) redisUtils
				.get(Constants.SHOP_KEYWORD);
		List<TbSpKeyword> nameList = null;
		List<TbSpKeyword> introList = null;
		if(shopName!=null) {
			shopName= shopName.trim();
		}
		if (null != keywordMap && keywordMap.size() != 0) {
			nameList = keywordMap.get("1");
			introList = keywordMap.get("2");
		} else {
			//存redis
			Map<String, List<TbSpKeyword>> redisMap = Maps.newHashMap();
			Iterable<TbSpKeyword> kdbList =  keywordDao.findAll();
			nameList = Lists.newArrayList();
			introList = Lists.newArrayList();
			for( TbSpKeyword keyword :kdbList ){
				if ("1".equals(keyword.getType())) {//店铺名称
					nameList.add(keyword);
				}else {
					introList.add(keyword);
				}
			}
			redisMap.put("1", nameList);
			redisMap.put("2", introList);
			redisUtils.set(Constants.SHOP_KEYWORD, redisMap);
		}
		
		//店铺名称校验
		if (StringUtils.isNotBlank(shopName)) {
			
			//过滤表情字符
			shopName = weChatService.replaceEmoji(shopName);
			
			for( TbSpKeyword nameKeyword :nameList ){
				if (shopName.indexOf(nameKeyword.getContent()) != -1) {
					sr.setFail();
					sr.setResult("包含敏感词汇！"+nameKeyword.getContent());
					return sr;
				}
			}
		}else {
			sr.setFail();
			sr.setResult("店铺名字不能为空!");
			return sr;	
		}
		//店铺介绍校验
		if (StringUtils.isNotEmpty(shopIntroduct)) {
			//过滤表情字符
			shopIntroduct = weChatService.replaceEmoji(shopIntroduct);
			for( TbSpKeyword introKeyword :introList ){
				if (shopIntroduct.indexOf(introKeyword.getContent()) != -1) {
					sr.setFail();
					sr.setResult("包含敏感词汇！"+introKeyword.getContent());
					return sr;
				}
			}
		}
		//校验通过
		int i = userDao.updateShopName(userCode,shopName,shopIntroduct);
		if (i == 1) {
			sr.setSuccess();
			sr.setResult(i);
		}else {
			sr.setFail();
			sr.setResult("数据异常！");
		}
		return sr;
	}
}
