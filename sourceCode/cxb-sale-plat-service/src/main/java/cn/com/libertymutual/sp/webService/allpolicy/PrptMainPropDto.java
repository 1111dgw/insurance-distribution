
package cn.com.libertymutual.sp.webService.allpolicy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for prptMainPropDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prptMainPropDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="businessSort" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="businessSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="occupationSort" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="occupationSortName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="primaryBusiness" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prptMainPropDto", propOrder = {
    "businessSort",
    "businessSource",
    "occupationSort",
    "occupationSortName",
    "primaryBusiness"
})
public class PrptMainPropDto implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected String businessSort;
    protected String businessSource;
    protected String occupationSort;
    protected String occupationSortName;
    protected String primaryBusiness;

    /**
     * Gets the value of the businessSort property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessSort() {
        return businessSort;
    }

    /**
     * Sets the value of the businessSort property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessSort(String value) {
        this.businessSort = value;
    }

    /**
     * Gets the value of the businessSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessSource() {
        return businessSource;
    }

    /**
     * Sets the value of the businessSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessSource(String value) {
        this.businessSource = value;
    }

    /**
     * Gets the value of the occupationSort property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOccupationSort() {
        return occupationSort;
    }

    /**
     * Sets the value of the occupationSort property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOccupationSort(String value) {
        this.occupationSort = value;
    }

    /**
     * Gets the value of the occupationSortName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOccupationSortName() {
        return occupationSortName;
    }

    /**
     * Sets the value of the occupationSortName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOccupationSortName(String value) {
        this.occupationSortName = value;
    }

    /**
     * Gets the value of the primaryBusiness property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryBusiness() {
        return primaryBusiness;
    }

    /**
     * Sets the value of the primaryBusiness property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryBusiness(String value) {
        this.primaryBusiness = value;
    }

}
