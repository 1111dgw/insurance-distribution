package cn.com.libertymutual.sp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import cn.com.libertymutual.sp.bean.TbSpInScoreLog;
import cn.com.libertymutual.sp.bean.TbSpOutScoreLog;
@Repository
public interface OutScoreLogDao extends PagingAndSortingRepository<TbSpOutScoreLog, Integer>, JpaSpecificationExecutor<TbSpOutScoreLog>{

	@Query("select COALESCE(sum(t.acChangeScore),0) from TbSpOutScoreLog t where t.userCode=?1 and t.changeType=?2 and t.status=1")
	int findByUCodeAndType(String userCode, String changeType);

	
	@Query(value="select t1.change_type,t1.re_changescore,t1.ac_changescore,t1.reason,t1.reason_no,t1.change_time,t1.remark from tb_sp_outscorelog as t1 where t1.user_code = :userCode limit :pageNumber , :pageSize",nativeQuery=true)
	List<String> findAllOut(String userCode, int pageNumber, int pageSize);

	
}
