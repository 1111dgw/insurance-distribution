package cn.com.libertymutual.sp.dto.car.bean;

import java.io.Serializable;
import java.util.Date;

//@Entity
//@Table(name = "tb_tp_titem_distribute", catalog = "")
public class TpTitemDistribute implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4115438179914993807L;
	private Integer distributeId;
	private String flowId;

	private String name;
	private String idNo;
	private String cellPhoneNo;
	private String address;
	private String email;
	private String courierNumber;
	private String attendantCompany;
	private String deliveryTime;
	// 0 未录入配货信息 1:已录入配货信息,默认0
	private String status;
	private Date updateTime;
	private Date createTime;
	private String remark;
	private String callbackUrl;

	// @Id
	// @Column(name = "distribute_ID", nullable = false)
	public Integer getDistributeId() {
		return distributeId;
	}

	public void setDistributeId(Integer distributeId) {
		this.distributeId = distributeId;
	}

	// @Basic
	// @Column(name = "FLOW_ID", nullable = false, length = 64)
	public String getFlowId() {
		return flowId;
	}

	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}

	private String proposalNo;

	// @Basic
	// @javax.persistence.Column(name = "PROPOSAL_NO", nullable = true, length = 36)
	public String getProposalNo() {
		return proposalNo;
	}

	public void setProposalNo(String proposalNo) {
		this.proposalNo = proposalNo;
	}

	private String mtplProposalNo;

	// @Basic
	// @javax.persistence.Column(name = "MTPL_PROPOSAL_NO", nullable = true, length
	// = 36)
	public String getMtplProposalNo() {
		return mtplProposalNo;
	}

	public void setMtplProposalNo(String mtplProposalNo) {
		this.mtplProposalNo = mtplProposalNo;
	}

	// @Basic
	// @Column(name = "NAME", nullable = false, length = 32)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	// @Basic
	// @Column(name = "ID_NO", nullable = false, length = 18)
	public String getIdNo() {
		return idNo;
	}

	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}

	// @Basic
	// @Column(name = "CELL_PHONE_NO", nullable = true, length = 11)
	public String getCellPhoneNo() {
		return cellPhoneNo;
	}

	public void setCellPhoneNo(String cellPhoneNo) {
		this.cellPhoneNo = cellPhoneNo;
	}

	// @Basic
	// @Column(name = "ADDRESS", nullable = true, length = 100)
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	// @Basic
	// @Column(name = "EMAIL", nullable = true, length = 32)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	// @Basic
	// @Column(name = "courier_Number", nullable = true, length = 64)
	public String getCourierNumber() {
		return courierNumber;
	}

	public void setCourierNumber(String courierNumber) {
		this.courierNumber = courierNumber;
	}

	// @Basic
	// @Column(name = "attendant_Company", nullable = true, length = 100)
	public String getAttendantCompany() {
		return attendantCompany;
	}

	public void setAttendantCompany(String attendantCompany) {
		this.attendantCompany = attendantCompany;
	}

	// @Basic
	// @Column(name = "delivery_Time", nullable = true, length = 20)
	public String getDeliveryTime() {
		return deliveryTime;
	}

	public void setDeliveryTime(String deliveryTime) {
		this.deliveryTime = deliveryTime;
	}

	// @Basic
	// @Column(name = "STATUS", nullable = false, length = 2)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	// @Basic
	// @Column(name = "UPDATE_TIME", nullable = true)
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	// @Basic
	// @Column(name = "CREATE_TIME", nullable = false)
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	// @Basic
	// @Column(name = "callback_Url", nullable = true, length = 400)
	public String getCallbackUrl() {
		return callbackUrl;
	}

	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}

	// @Basic
	// @Column(name = "REMARK", nullable = true, length = 400)
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TpTitemDistribute other = (TpTitemDistribute) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (attendantCompany == null) {
			if (other.attendantCompany != null)
				return false;
		} else if (!attendantCompany.equals(other.attendantCompany))
			return false;
		if (callbackUrl == null) {
			if (other.callbackUrl != null)
				return false;
		} else if (!callbackUrl.equals(other.callbackUrl))
			return false;
		if (cellPhoneNo == null) {
			if (other.cellPhoneNo != null)
				return false;
		} else if (!cellPhoneNo.equals(other.cellPhoneNo))
			return false;
		if (courierNumber == null) {
			if (other.courierNumber != null)
				return false;
		} else if (!courierNumber.equals(other.courierNumber))
			return false;
		if (createTime == null) {
			if (other.createTime != null)
				return false;
		} else if (!createTime.equals(other.createTime))
			return false;
		if (deliveryTime == null) {
			if (other.deliveryTime != null)
				return false;
		} else if (!deliveryTime.equals(other.deliveryTime))
			return false;
		if (distributeId == null) {
			if (other.distributeId != null)
				return false;
		} else if (!distributeId.equals(other.distributeId))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (flowId == null) {
			if (other.flowId != null)
				return false;
		} else if (!flowId.equals(other.flowId))
			return false;
		if (idNo == null) {
			if (other.idNo != null)
				return false;
		} else if (!idNo.equals(other.idNo))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (remark == null) {
			if (other.remark != null)
				return false;
		} else if (!remark.equals(other.remark))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (updateTime == null) {
			if (other.updateTime != null)
				return false;
		} else if (!updateTime.equals(other.updateTime))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((attendantCompany == null) ? 0 : attendantCompany.hashCode());
		result = prime * result + ((callbackUrl == null) ? 0 : callbackUrl.hashCode());
		result = prime * result + ((cellPhoneNo == null) ? 0 : cellPhoneNo.hashCode());
		result = prime * result + ((courierNumber == null) ? 0 : courierNumber.hashCode());
		result = prime * result + ((createTime == null) ? 0 : createTime.hashCode());
		result = prime * result + ((deliveryTime == null) ? 0 : deliveryTime.hashCode());
		result = prime * result + ((distributeId == null) ? 0 : distributeId.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((flowId == null) ? 0 : flowId.hashCode());
		result = prime * result + ((idNo == null) ? 0 : idNo.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((remark == null) ? 0 : remark.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((updateTime == null) ? 0 : updateTime.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "TpTitemDistribute [distributeId=" + distributeId + ", flowId=" + flowId + ", name=" + name + ", idNo=" + idNo + ", cellPhoneNo="
				+ cellPhoneNo + ", address=" + address + ", email=" + email + ", courierNumber=" + courierNumber + ", attendantCompany="
				+ attendantCompany + ", deliveryTime=" + deliveryTime + ", status=" + status + ", updateTime=" + updateTime + ", createTime="
				+ createTime + ", remark=" + remark + ", callbackUrl=" + callbackUrl + "]";
	}
}