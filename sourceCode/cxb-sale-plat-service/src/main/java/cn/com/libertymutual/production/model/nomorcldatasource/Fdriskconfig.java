package cn.com.libertymutual.production.model.nomorcldatasource;

import java.util.Date;

public class Fdriskconfig {
    private String codetype;

    private String codevalue;

    private String uwyear;

    private String riskcode;

    private Date startdate;

    private Date enddate;

    private String flag;

    public String getCodetype() {
        return codetype;
    }

    public void setCodetype(String codetype) {
        this.codetype = codetype == null ? null : codetype.trim();
    }

    public String getCodevalue() {
        return codevalue;
    }

    public void setCodevalue(String codevalue) {
        this.codevalue = codevalue == null ? null : codevalue.trim();
    }

    public String getUwyear() {
        return uwyear;
    }

    public void setUwyear(String uwyear) {
        this.uwyear = uwyear == null ? null : uwyear.trim();
    }

    public String getRiskcode() {
        return riskcode;
    }

    public void setRiskcode(String riskcode) {
        this.riskcode = riskcode == null ? null : riskcode.trim();
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag == null ? null : flag.trim();
    }
}