package cn.com.libertymutual.sp.action;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.entity.StringEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.annotation.SystemValidate;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.biz.IdentifierMarkBiz;
import cn.com.libertymutual.sp.dto.RequestPayDto;
import cn.com.libertymutual.sp.dto.TQueryPolicyRequstDto;
import cn.com.libertymutual.sp.service.api.PayService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/nol/pay")
public class PayController {
	@Autowired
	private PayService payService;
	@Resource
	private IdentifierMarkBiz identifierMarkBiz;

	@RequestMapping(value = "/paySuccess")
	@SystemValidate(validate = false, description = "无需校验接口")
	public String payNsync(ServletRequest servletRequest, HttpServletRequest request, HttpServletResponse response, HttpResponse httpResponse) {
		payService.payNsync(servletRequest, request, response, false);
		HttpEntity entity = new StringEntity("success", "UTF-8");
		httpResponse.setEntity(entity);

		return "success";
	}

	@RequestMapping(value = "/payRed")
	@SystemValidate(validate = false, description = "无需校验接口")
	public void paySync(ServletRequest servletRequest, HttpServletRequest request, HttpServletResponse response, HttpResponse httpResponse) {
		payService.paySync(servletRequest, request, response, true);
	}

	@RequestMapping(value = "/getPayInfo")
	public ServiceResult getPayInfo(HttpServletRequest request, HttpServletResponse response, String identCode,
			@RequestBody RequestPayDto requestPayDto) {
		return payService.getPayData(requestPayDto, request);
	}

	@RequestMapping(value = "/getPayInfoFind")
	public ServiceResult getPayInfoFind(HttpServletRequest request, HttpServletResponse response, String identCode,
			@RequestBody RequestPayDto requestPayDto) {
		return payService.payInfoFind(requestPayDto, request);
	}

	@ApiOperation(value = "通过证件号查询待支付列表", notes = "通过证件号查询待支付列表")
	@RequestMapping(value = "/queryPayList", method = RequestMethod.POST)
	public ServiceResult queryPayList(HttpServletRequest request, HttpServletResponse response, @RequestBody TQueryPolicyRequstDto requestPayDto) {
		return payService.queryPayList(requestPayDto, request);
	}

	@RequestMapping(value = "/callBackUrl", method = RequestMethod.POST)
	@SystemValidate(validate = false, description = "无需校验接口")
	public ServiceResult callbackUrl(String documentNo, Boolean isJump, HttpServletResponse response) {
		return payService.callbackUrl(documentNo, isJump, response);
	}

//	@RequestMapping(value = "/updateOrderState", method = RequestMethod.POST)
//	public ServiceResult updateOrderState(@RequestBody Object data) {
//		return payService.updateOrderState(data);
//	}

}
