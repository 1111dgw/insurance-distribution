package cn.com.libertymutual.production.model.nomorcldatasource;

public class Prpdpolicypay extends PrpdpolicypayKey {
    private String ispolicypay;

    public String getIspolicypay() {
        return ispolicypay;
    }

    public void setIspolicypay(String ispolicypay) {
        this.ispolicypay = ispolicypay == null ? null : ispolicypay.trim();
    }
}