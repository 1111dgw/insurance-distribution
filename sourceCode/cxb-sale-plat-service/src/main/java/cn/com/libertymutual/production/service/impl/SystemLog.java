package cn.com.libertymutual.production.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.libertymutual.core.util.Current;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdlogoperation;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdlogsetting;
import cn.com.libertymutual.production.service.api.PrpdLogOperationService;
import cn.com.libertymutual.production.service.api.PrpdLogSettingService;
import cn.com.libertymutual.production.utils.Constant.BUSINESSLOGTYPE;
import cn.com.libertymutual.production.utils.Constant.OPERTYPE;

@Service
public class SystemLog {

	@Autowired
	private PrpdLogSettingService prpdlogsettingService;
	@Autowired
	private PrpdLogOperationService prpdlogoperationService;
	
	public void save(BUSINESSLOGTYPE businessName,
					OPERTYPE operation,
					String businessKeyValue,
					String content) {
		
		Prpdlogsetting logSetting = new Prpdlogsetting();
		logSetting.setBusinessname(businessName.toString());
		logSetting.setCreatedby(Current.userId.get());
		logSetting.setCreatedate(new Date());
		Long logid = prpdlogsettingService.insert(logSetting);
		
		Prpdlogoperation logOperation = new Prpdlogoperation();
		logOperation.setLogid(logid);
		logOperation.setBusinesskeyvalue(businessKeyValue);
		logOperation.setOperationtype(operation.toString());
		logOperation.setContent(content);
		prpdlogoperationService.insert(logOperation);
	}
	
}
