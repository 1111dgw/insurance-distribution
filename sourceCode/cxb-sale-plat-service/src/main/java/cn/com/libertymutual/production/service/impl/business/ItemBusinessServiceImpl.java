package cn.com.libertymutual.production.service.impl.business;

import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.libertymutual.production.model.nomorcldatasource.*;
import cn.com.libertymutual.production.pojo.request.PrpdItemLibraryRequest;
import cn.com.libertymutual.production.pojo.response.Response;
import cn.com.libertymutual.production.service.api.business.ItemBusinessService;

import com.github.pagehelper.PageInfo;

/**
 * 事务管理注意事项：
 * 1. 新增/修改操作必须启用事务
 * 2. 方法体内部抛出异常即可启用事务
 * @author Steven.Li
 * @date 2017年7月28日
 * 
 */
@Service
@Transactional(rollbackFor = Exception.class, transactionManager = "transactionManagerCoreData")
public class ItemBusinessServiceImpl extends ItemBusinessService {

	@Override
	public Response selectItem(PrpdItemLibraryRequest request) {
		Response result = new Response();
		try {
			PageInfo<PrpditemlibraryWithBLOBs> pageInfo = prpdItemLibraryService.selectItem(request);
			result.setTotal(pageInfo.getTotal());
			result.setResult(pageInfo.getList());
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	@Override
	public Response update(PrpdItemLibraryRequest request) throws Exception {
		Response result = new Response();
		try {
			PrpditemlibraryWithBLOBs record = new PrpditemlibraryWithBLOBs();
			BeanUtils.copyProperties(request, record);
			record.setUpdatedate(new Date());
			prpdItemLibraryService.update(record);
			
			//修改关联表相关信息
			PrpditemWithBLOBs prpdItem = new PrpditemWithBLOBs();
			prpdItem.setItemcode(request.getItemcode());
			prpdItem.setItemcname(request.getItemcname());
			prpdItem.setItemename(request.getItemename());
			prpdItemService.update(prpdItem);
			
			Prpdration prpdRation = new Prpdration();
			prpdRation.setItemcode(request.getItemcode());
			prpdRation.setItemcname(request.getItemcname());
			prpdRation.setItemename(request.getItemename());
			prpdrationService.updateByCriteria(prpdRation,prpdRation);
			
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			throw e;
		}
		return result;
	}

	@Override
	public Response selectTopItems() {
		Response result = new Response();
		try {
			List<PrpditemlibraryWithBLOBs> items = prpdItemLibraryService.selectTopItems();
			result.setResult(items);
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}
}
