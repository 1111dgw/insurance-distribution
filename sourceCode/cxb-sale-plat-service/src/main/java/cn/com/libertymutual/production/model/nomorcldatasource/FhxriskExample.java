package cn.com.libertymutual.production.model.nomorcldatasource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class FhxriskExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public FhxriskExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andTreatynoIsNull() {
            addCriterion("TREATYNO is null");
            return (Criteria) this;
        }

        public Criteria andTreatynoIsNotNull() {
            addCriterion("TREATYNO is not null");
            return (Criteria) this;
        }

        public Criteria andTreatynoEqualTo(String value) {
            addCriterion("TREATYNO =", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoNotEqualTo(String value) {
            addCriterion("TREATYNO <>", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoGreaterThan(String value) {
            addCriterion("TREATYNO >", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoGreaterThanOrEqualTo(String value) {
            addCriterion("TREATYNO >=", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoLessThan(String value) {
            addCriterion("TREATYNO <", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoLessThanOrEqualTo(String value) {
            addCriterion("TREATYNO <=", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoLike(String value) {
            addCriterion("TREATYNO like", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoNotLike(String value) {
            addCriterion("TREATYNO not like", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoIn(List<String> values) {
            addCriterion("TREATYNO in", values, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoNotIn(List<String> values) {
            addCriterion("TREATYNO not in", values, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoBetween(String value1, String value2) {
            addCriterion("TREATYNO between", value1, value2, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoNotBetween(String value1, String value2) {
            addCriterion("TREATYNO not between", value1, value2, "treatyno");
            return (Criteria) this;
        }

        public Criteria andLayernoIsNull() {
            addCriterion("LAYERNO is null");
            return (Criteria) this;
        }

        public Criteria andLayernoIsNotNull() {
            addCriterion("LAYERNO is not null");
            return (Criteria) this;
        }

        public Criteria andLayernoEqualTo(BigDecimal value) {
            addCriterion("LAYERNO =", value, "layerno");
            return (Criteria) this;
        }

        public Criteria andLayernoNotEqualTo(BigDecimal value) {
            addCriterion("LAYERNO <>", value, "layerno");
            return (Criteria) this;
        }

        public Criteria andLayernoGreaterThan(BigDecimal value) {
            addCriterion("LAYERNO >", value, "layerno");
            return (Criteria) this;
        }

        public Criteria andLayernoGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("LAYERNO >=", value, "layerno");
            return (Criteria) this;
        }

        public Criteria andLayernoLessThan(BigDecimal value) {
            addCriterion("LAYERNO <", value, "layerno");
            return (Criteria) this;
        }

        public Criteria andLayernoLessThanOrEqualTo(BigDecimal value) {
            addCriterion("LAYERNO <=", value, "layerno");
            return (Criteria) this;
        }

        public Criteria andLayernoIn(List<BigDecimal> values) {
            addCriterion("LAYERNO in", values, "layerno");
            return (Criteria) this;
        }

        public Criteria andLayernoNotIn(List<BigDecimal> values) {
            addCriterion("LAYERNO not in", values, "layerno");
            return (Criteria) this;
        }

        public Criteria andLayernoBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("LAYERNO between", value1, value2, "layerno");
            return (Criteria) this;
        }

        public Criteria andLayernoNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("LAYERNO not between", value1, value2, "layerno");
            return (Criteria) this;
        }

        public Criteria andRiskcodeIsNull() {
            addCriterion("RISKCODE is null");
            return (Criteria) this;
        }

        public Criteria andRiskcodeIsNotNull() {
            addCriterion("RISKCODE is not null");
            return (Criteria) this;
        }

        public Criteria andRiskcodeEqualTo(String value) {
            addCriterion("RISKCODE =", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeNotEqualTo(String value) {
            addCriterion("RISKCODE <>", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeGreaterThan(String value) {
            addCriterion("RISKCODE >", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeGreaterThanOrEqualTo(String value) {
            addCriterion("RISKCODE >=", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeLessThan(String value) {
            addCriterion("RISKCODE <", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeLessThanOrEqualTo(String value) {
            addCriterion("RISKCODE <=", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeLike(String value) {
            addCriterion("RISKCODE like", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeNotLike(String value) {
            addCriterion("RISKCODE not like", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeIn(List<String> values) {
            addCriterion("RISKCODE in", values, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeNotIn(List<String> values) {
            addCriterion("RISKCODE not in", values, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeBetween(String value1, String value2) {
            addCriterion("RISKCODE between", value1, value2, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeNotBetween(String value1, String value2) {
            addCriterion("RISKCODE not between", value1, value2, "riskcode");
            return (Criteria) this;
        }

        public Criteria andSectionnoIsNull() {
            addCriterion("SECTIONNO is null");
            return (Criteria) this;
        }

        public Criteria andSectionnoIsNotNull() {
            addCriterion("SECTIONNO is not null");
            return (Criteria) this;
        }

        public Criteria andSectionnoEqualTo(String value) {
            addCriterion("SECTIONNO =", value, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoNotEqualTo(String value) {
            addCriterion("SECTIONNO <>", value, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoGreaterThan(String value) {
            addCriterion("SECTIONNO >", value, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoGreaterThanOrEqualTo(String value) {
            addCriterion("SECTIONNO >=", value, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoLessThan(String value) {
            addCriterion("SECTIONNO <", value, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoLessThanOrEqualTo(String value) {
            addCriterion("SECTIONNO <=", value, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoLike(String value) {
            addCriterion("SECTIONNO like", value, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoNotLike(String value) {
            addCriterion("SECTIONNO not like", value, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoIn(List<String> values) {
            addCriterion("SECTIONNO in", values, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoNotIn(List<String> values) {
            addCriterion("SECTIONNO not in", values, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoBetween(String value1, String value2) {
            addCriterion("SECTIONNO between", value1, value2, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoNotBetween(String value1, String value2) {
            addCriterion("SECTIONNO not between", value1, value2, "sectionno");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}