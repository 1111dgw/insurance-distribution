package cn.com.libertymutual.sp.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import cn.com.libertymutual.core.util.Current;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpOrder;
import cn.com.libertymutual.sp.bean.TbSpScoreConfig;
import cn.com.libertymutual.sp.bean.TbSpUser;
import cn.com.libertymutual.sp.dao.OrderDao;
import cn.com.libertymutual.sp.dao.UserDao;
import cn.com.libertymutual.sp.req.FileUploadParam;
import cn.com.libertymutual.sp.service.api.ScoreService;
import cn.com.libertymutual.sys.bean.SysRoleUser;
import cn.com.libertymutual.sys.bean.SysUser;
import cn.com.libertymutual.sys.dao.ISysRoleUserDao;
import cn.com.libertymutual.sys.dao.ISysUserDao;
import cn.com.libertymutual.sys.nio.NioClient;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = "/admin/scoreWeb")
public class ScoreControllerWeb {

	/** 上传文件同一目录 */
	@Value("${file.transfer.uploadImagesPath}")
	private String uploadImagesPath;
	private Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	private ScoreService scoreService;
	///@Autowired
	///private SFTP2 sftp2;
	@Autowired
	private NioClient nioClient;
	@Autowired
	private UserDao userDao;
	@Autowired
	private OrderDao orderDao;
	@Autowired
	private ISysUserDao sysUserDao;
	@Autowired
	private ISysRoleUserDao sysRoleUserDao;
	
	
	
	@RequestMapping(value = "/excelScore")
	@ResponseBody
	public ServiceResult excelScore(@RequestParam("file") MultipartFile file, FileUploadParam parampath) {
		// public ServiceResult excelScore(@RequestParam("file") MultipartFile
		// file,String parampath,String remark) {
		ServiceResult sr = new ServiceResult();
		if (file.isEmpty()) {
			sr.setFail();
			sr.setResult("文件为空");
			return sr;
		}
		// 获取文件名
		String fileName = file.getOriginalFilename();
		log.info("上传的文件名为：" + fileName);
		// 获取文件的后缀名
		String suffixName = fileName.substring(fileName.lastIndexOf("."));
		log.info("上传的后缀名为：" + suffixName);
		// String filePath = "E://image/" + parampath;
		// String filePath = "E://image/" + parampath.getParampath();
		String filePath = this.uploadImagesPath + parampath.getParampath();
		// 解决中文问题，liunx下中文路径，图片显示问题
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");// 设置日期格式
		fileName = df.format(new Date()) + suffixName;
		File dirPath = new File(filePath);
		if (!dirPath.exists()) {
			dirPath.mkdirs();
		}
		File tempFile = new File(filePath, fileName);
		String path = tempFile.getPath();
		log.info("tempFile path ===>{}", path);
		try {
			file.transferTo(tempFile);
			log.info("首次上传文件的大小-->:{}", new File(path).length());
			log.info("tempFile path ===>{}", path);
			// sftp2.SFTPupload(path, parampath.getParampath());
			nioClient.upload(path, parampath.getParampath());
			return (scoreService.scoreByHand(path, parampath.getRemark(), file));
			// return(scoreService.scoreByHand(path,remark,file));
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			sr.setFail();
			sr.setResult("表格数据有问题，请仔细检查！");
			e.printStackTrace();
		}
		return sr;
	}

	/*
	 * 手工发放积分审批前校验！
	 */

	@ApiOperation(value = "手工发放积分审批前校验", notes = "手工发放积分审批前校验")
	@PostMapping(value = "/beforeApprove")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "id", value = "入库id", required = true, paramType = "query", dataType = "Long"), })
	public ServiceResult beforeApprove(Integer id) {
		ServiceResult sr = new ServiceResult();

		sr = scoreService.beforeApprove(id);
		return sr;

	}

	/*
	 * 积分审批！
	 */

	@ApiOperation(value = "积分审批", notes = "积分审批")
	@PostMapping(value = "/approve")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "approveType", value = "审批类型", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "id", value = "入库id", required = true, paramType = "query"),
			@ApiImplicitParam(name = "type", value = "（同意、拒绝）", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "remark", value = "备注", required = true, paramType = "query", dataType = "String"), })
	public ServiceResult approve(String approveType, String id, String type, String remark) {
		ServiceResult sr = new ServiceResult();

		sr = scoreService.approve(approveType, id, type, remark);
		return sr;

	}

	/*
	 * 手工发放管理--查询
	 */

	@ApiOperation(value = "手工发放查询", notes = "手工发放查询")
	@PostMapping(value = "/listByhand")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "startDate", value = "计划入库日期", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "endDate", value = "计划入库日期", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "status", value = "状态", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "每页条数", required = true, paramType = "query", dataType = "Long"), })
	public ServiceResult listByhand(String startDate, String endDate, String status, int pageNumber, int pageSize) {
		ServiceResult sr = new ServiceResult();

		sr = scoreService.listByhand(startDate, endDate, status, pageNumber, pageSize);
		return sr;

	}

	/*
	 * 发放明细查询
	 */

	@ApiOperation(value = "发放明细查询", notes = "发放明细查询")
	@PostMapping(value = "/inScoreDetail")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "comCode", value = "", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "startDate", value = "消费日期", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "endDate", value = "消费日期", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "changeType", value = "消费类型", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "userCode", value = "客户编码", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "branchCode", value = "发放机构编码", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "policyNo", value = "保单号", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobile", value = "客户手机号", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "每页条数", required = true, paramType = "query", dataType = "Long"), })
	public ServiceResult inScoreDetail(String comCode,String startDate, String endDate, String changeType, String userCode, String branchCode,String policyNo,String mobile, int pageNumber,
			int pageSize) {
		ServiceResult sr = new ServiceResult();

		sr = scoreService.inStorageDetail(comCode,startDate, endDate, changeType, userCode, branchCode,policyNo,mobile, pageNumber, pageSize);
		return sr;

	}

	/*
	 * 客户积分扣减明细查询
	 */

	@ApiOperation(value = "客户积分扣减明细查询", notes = "客户积分扣减明细查询")
	@PostMapping(value = "/outScoreDetail")
	@ApiImplicitParams(value = { 
			@ApiImplicitParam(name = "comCode", value = "", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "startDate", value = "消费日期", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "endDate", value = "消费日期", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "changeType", value = "消费类型", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "userCode", value = "客户编码", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobile", value = "客户手机号", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "policyNo", value = "保单号", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "每页条数", required = true, paramType = "query", dataType = "Long"), })
	public ServiceResult outScoreDetail(String comCode,String startDate, String endDate,String policyNo, String changeType, String userCode, String mobile, int pageNumber,
			int pageSize) {
		ServiceResult sr = new ServiceResult();

		sr = scoreService.outStorageDetail(comCode,startDate, endDate, policyNo,changeType, userCode, mobile, pageNumber, pageSize);
		return sr;

	}

	/*
	 * 客户积分提现明细查询
	 */

	@ApiOperation(value = "客户积分提现明细查询", notes = "客户积分提现明细查询")
	@PostMapping(value = "/cashLog")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "comCode", value = "", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "startDate", value = "提现日期", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "endDate", value = "提现日期", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "userCode", value = "用户编码", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobile", value = "客户手机号", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "businessNo", value = "业务号", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "status", value = "提现状态", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "每页条数", required = true, paramType = "query", dataType = "Long"), })
	public ServiceResult cashLog(String comCode,String startDate, String endDate, String status, String userCode, String mobile,String businessNo,int pageNumber, int pageSize) {
		ServiceResult sr = new ServiceResult();

		sr = scoreService.cashLog(comCode,startDate, endDate, status, userCode, mobile,businessNo, pageNumber, pageSize);
		return sr;

	}

	/**
	 * 积分兑换提现——补录
	 * 
	 * @param userCode
	 * @param score
	 * @return
	 */
	@ApiOperation(value = "积分兑换提现——补录", notes = "积分兑换提现——补录")
	@PostMapping(value = "/integralCashPatch")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "businessNo", value = "补录的业务单号", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "branchCode", value = "机构代码", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "bankId", value = "银行卡Id", required = false, paramType = "query", dataType = "Integer") })
	public ServiceResult cashPatch(String businessNo, String branchCode, Integer bankId) {
		ServiceResult sr = new ServiceResult();

		sr = scoreService.cashPatch(businessNo, branchCode, bankId);
		return sr;
	}

	/*
	 * 积分基本配置-规则开关、有效期、兑换比例等
	 */

	@ApiOperation(value = "积分基本配置", notes = "积分基本配置")
	@PostMapping(value = "/scoreConfig")
	public ServiceResult scoreConfig() {
		ServiceResult sr = new ServiceResult();

		sr = scoreService.scoreConfig();
		return sr;

	}

	/*
	 * 积分基本配置-规则开关、有效期、兑换比例等
	 */

	@ApiOperation(value = "积分基本配置", notes = "积分基本配置")
	@PostMapping(value = "/updateScoreConfig")
	public ServiceResult updateScoreConfig(
			@RequestBody @ApiParam(name = "scoreConfig", value = "scoreConfig", required = true) TbSpScoreConfig scoreConfig) {
		ServiceResult sr = new ServiceResult();

		sr = scoreService.updateScoreConfig(scoreConfig);
		return sr;

	}

	/*
	 * 客户积分查询
	 */

	@ApiOperation(value = "客户积分查询", notes = "客户积分查询")
	@PostMapping(value = "/userScore")
	@ApiImplicitParams(value = { 
			@ApiImplicitParam(name = "comCode", value = "", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "startDate", value = "统计日期", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "endDate", value = "统计日期", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "userCode", value = "用户编码", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobile", value = "客户手机号", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "每页条数", required = true, paramType = "query", dataType = "Long"), })
	public ServiceResult userScore(String comCode,String startDate, String endDate, String userCode, String mobile, int pageNumber, int pageSize) {
		ServiceResult sr = new ServiceResult();

		sr = scoreService.userScore(comCode,startDate, endDate, userCode, mobile, pageNumber, pageSize);
		return sr;

	}

	/*
	 * 积分抽奖记录--查询
	 */

	@ApiOperation(value = "积分抽奖记录", notes = "积分抽奖记录")
	@PostMapping(value = "/drawLog")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "startDate", value = "开始日期", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "endDate", value = "结束日期", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "status", value = "状态", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "policyNo", value = "保单号", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "userCode", value = "用户编码", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobile", value = "手机号", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "每页条数", required = true, paramType = "query", dataType = "Long"), })
	public ServiceResult drawLogList(String startDate, String endDate, String status, String policyNo, String mobile, String userCode, int pageNumber,
			int pageSize) {
		ServiceResult sr = new ServiceResult();

		sr = scoreService.drawLogList(startDate, endDate, status, policyNo, mobile, userCode, pageNumber, pageSize);
		return sr;

	}

	@ApiOperation(value = "快钱交易查询", notes = "快钱交易查询")
	@PostMapping(value = "/queryCnp")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "branchCode", value = "分公司代码", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "businessNo", value = "业务单号", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "页码:从0开始", required = false, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "每页条数", required = false, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "startDate", value = "交易开始时间 (格式:yyyyMMdd)", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "endDate", value = "交易结束时间 (格式:yy" + "yyMMdd)", required = false, paramType = "query", dataType = "String") })
	public ServiceResult queryCnp(String businessNo, String branchCode, int pageNumber, int pageSize, String startDate, String endDate) {
		ServiceResult sr = new ServiceResult();

		if (StringUtils.isNotBlank(startDate)) {
			log.info(startDate);
			startDate = startDate.replace("-", "");
			log.info(startDate);
		}
		if (StringUtils.isNotBlank(endDate)) {
			endDate = endDate.replace("-", "");
		}
		sr = scoreService.queryCnp(businessNo, branchCode, startDate, endDate, pageNumber, pageSize);
		return sr;

	}
	
	
	/*
	 * 提现明细对应的收入来源明细查询
	 */

	@ApiOperation(value = "提现明细对应的收入来源明细查询", notes = "提现明细对应的收入来源明细查询")
	@PostMapping(value = "/cashToInScoreDetail")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "comCode", value = "", required = false, paramType = "query", dataType = "String"),
		    @ApiImplicitParam(name = "startDate", value = "提现日期", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "endDate", value = "提现日期", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "userCode", value = "客户编码", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobile", value = "客户手机号", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "branchCode", value = "机构编码", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "每页条数", required = true, paramType = "query", dataType = "Long"), })
	public ServiceResult cashToInScoreDetail(String comCode,String startDate, String endDate, String branchCode, String userCode, String mobile, int pageNumber,
			int pageSize) {
		ServiceResult sr = new ServiceResult();

		sr = scoreService.cashToInScoreDetail(comCode,startDate, endDate, branchCode, userCode, mobile, pageNumber, pageSize);
		return sr;

	}

	/*
	 * 提现测试
	 */

	@ApiOperation(value = "提现测试", notes = "提现测试")
	@PostMapping(value = "/cashTest")
	@ApiImplicitParams(value = { 
			@ApiImplicitParam(name = "userCode", value = "用户编码", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "score", value = "客户手机号", required = false, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "policyNo", value = "页码", required = true, paramType = "query", dataType = "String"),
    })
	public ServiceResult cashTest(String userCode, Double score, String policyNo) {
		ServiceResult sr = new ServiceResult();

		sr = scoreService.cashTest(userCode, score, policyNo);
		return sr;

	}
	
	
	@ApiOperation(value = "渠道送积分测试", notes = "渠道送积分")
	@PostMapping(value = "/channelInScoreTest")
	public ServiceResult channelInScoreTest() {
		ServiceResult sr = new ServiceResult();

		TbSpUser user = userDao.findByUserCode("BS9501001423");
		log.info("user--{}",user.toString());
		List<TbSpOrder> order = orderDao.findByPolicyNo("8127265100180000074000");
		log.info("order--{}",order.get(0).toString());
		sr.setResult(scoreService.cxbInScore(order.get(0),user));
		return sr;

	}

	//恒华批量导入用户
	@RequestMapping(value = "/insertUser")
	@ResponseBody
//	public ServiceResult insertUser(@RequestParam("file") MultipartFile file, FileUploadParam parampath) {
	public ServiceResult insertUser(@RequestParam("file") MultipartFile file, String parampath) {
		ServiceResult sr = new ServiceResult();
		if (file.isEmpty()) {
			sr.setFail();
			sr.setResult("文件为空");
			return sr;
		}
		// 获取文件名
		String fileName = file.getOriginalFilename();
		log.info("上传的文件名为：" + fileName);
		// 获取文件的后缀名
		String suffixName = fileName.substring(fileName.lastIndexOf("."));
		log.info("上传的后缀名为：" + suffixName);
		 String filePath = "E://image/" + parampath;
		// String filePath = "E://image/" + parampath.getParampath();
//		String filePath = this.uploadImagesPath + parampath.getParampath();
		// 解决中文问题，liunx下中文路径，图片显示问题
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");// 设置日期格式
		fileName = df.format(new Date()) + suffixName;
		File dirPath = new File(filePath);
		if (!dirPath.exists()) {
			dirPath.mkdirs();
		}
		File tempFile = new File(filePath, fileName);
		String path = tempFile.getPath();
		log.info("tempFile path ===>{}", path);
		try {
			file.transferTo(tempFile);
			log.info("首次上传文件的大小-->:{}", new File(path).length());
			log.info("tempFile path ===>{}", path);
			// sftp2.SFTPupload(path, parampath.getParampath());
//			nioClient.upload(path, parampath.getParampath());
//			return (scoreService.scoreByHand(path, parampath.getRemark(), file));
			// return(scoreService.scoreByHand(path,remark,file));
			FileInputStream is = new FileInputStream(path);
			String userId = Current.userInfo.get().getUserId();
			// InputStream is = file.getInputStream();
			ClassLoader classloader = org.apache.poi.poifs.filesystem.POIFSFileSystem.class.getClassLoader();
			URL res = classloader.getResource("org/apache/poi/poifs/filesystem/POIFSFileSystem.class");
			String dpath = res.getPath();
			System.out.println("Core POI came from " + dpath);
			// 获取工作表
			Workbook workbook = WorkbookFactory.create(is);

			log.info("-----------workbook--------{}", workbook);
			int sheetNumber = workbook.getNumberOfSheets();
			log.info("sheetNumber-----{}", sheetNumber);
			Sheet sheet = null;
			Row row = null;
			int totalRows = 0;
			for (int nowsheet = 0; nowsheet < sheetNumber; nowsheet++) {
				log.info("-----------校验sheetNumber--------{}", sheetNumber);
				// Excel工作表
				sheet = workbook.getSheetAt(nowsheet);
				log.info("-----------行数--------{}", sheet.getPhysicalNumberOfRows());
				// Excel工作表总行数
				totalRows += sheet.getPhysicalNumberOfRows();
				log.info("totalRows-------{}", totalRows);
				log.info("totalgetLastRowNum-------{}", sheet.getLastRowNum());
				// 校验电话号码
				Date date = new Date();
				SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				String time = df1.format(date);
				Timestamp CreateDate = Timestamp.valueOf(time);
				for (int r = 1; r < totalRows; r++) {
					row = sheet.getRow(r);
					if (null == row) {
						continue;
					}
					if (StringUtils.isEmpty(row.getCell(3).toString())||null==row.getCell(3)) {
						continue;
					}
					//手机端
					TbSpUser user = new TbSpUser();
					user.setUserCode("BS"+row.getCell(0).toString());
					user.setUserName(row.getCell(1).toString());
//					if ("男".equals(row.getCell(2).toString())) {
//						user.setWxSex("1");
//					}else {
						user.setWxSex("2");
//					}
					if (row.getCell(2).toString().length()>18) {
						user.setIdType("2");
						user.setIdNumber(row.getCell(2).toString());
					}else {
						user.setIdType("1");
						user.setIdNumber(row.getCell(2).toString());
					}
						
					user.setStoreFlag("1");
//					user.setStoreCreateTime(new Date().getTime());
					user.setMobile(row.getCell(3).toString());
					user.setRefereeId("BS"+row.getCell(7).toString());
					user.setState("1");
					user.setPassword("670b14728ad9902aecba32e22fa4f6bd");
					user.setStoreCreateTime(CreateDate);
					user.setStoreFlag("1");
					user.setHeadUrl("upload/userHeadImg/default/headImgDefault.jpg");
					user.setLoginTimes(1);
					user.setLastloginDate(CreateDate);
					user.setBranchCode(row.getCell(4).toString());
					if (null==row.getCell(7)||StringUtils.isEmpty(row.getCell(7).toString())) {
						user.setComCode(row.getCell(4).toString());
					}else {
						user.setComCode("BS"+row.getCell(7).toString());
					}
					
					user.setRegisteDate(CreateDate);
					user.setShopName(row.getCell(1).toString()+"的店铺");
					user.setLoginState("0");
					user.setRegisterType("0");
					
					//后台用户
					SysUser sysUser = new SysUser();
					sysUser.setUserid("BS"+row.getCell(0).toString());
					sysUser.setUserCode("BS"+row.getCell(0).toString());
					sysUser.setPassword("670b14728ad9902aecba32e22fa4f6bd");
					sysUser.setUsername(row.getCell(1).toString());
					sysUser.setMobileno(row.getCell(3).toString());
					sysUser.setType("1");
					sysUser.setStatus("1");
					log.info("角色：{}", row.getCell(4).toString());
					SysRoleUser sru = new SysRoleUser();
					sru.setUserid("BS"+row.getCell(0).toString());
					switch(row.getCell(6).toString().trim()) {
						 case "保险方案定制师专业工作室":
							 sysUser.setClevel("2");
							 sysUser.setRoleCode("2");
							 sru.setRoleid(2);
							 break;
						 case "独立代理人":
							 sysUser.setClevel("3");
							 sysUser.setRoleCode("3");
							 sru.setRoleid(3);
							 break;
						 case "保险方案导师":
							 sysUser.setClevel("4");
							 sysUser.setRoleCode("4");
							 sru.setRoleid(4);
							 break;
						 case "创业督导":
							 sysUser.setClevel("5");
							 sysUser.setRoleCode("5");
							 sru.setRoleid(5);
							 break;
						 case "市场总监":
							 sysUser.setClevel("6");
							 sysUser.setRoleCode("6");
							 sru.setRoleid(6);
							 break;
						 case "保险方案定制师":
							 sysUser.setClevel("7");
							 sysUser.setRoleCode("7");
							 sru.setRoleid(7);
							 break;
						 case "秘书":
							 sysUser.setClevel("8");
							 sysUser.setRoleCode("8");
							 sru.setRoleid(8);
							 break;
					}
					sysUserDao.save(sysUser);
					userDao.save(user);
					sysRoleUserDao.save(sru);
				}
			}
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			sr.setFail();
			sr.setResult("表格数据有问题，请仔细检查！");
			e.printStackTrace();
		}
		return sr;
	}
}
