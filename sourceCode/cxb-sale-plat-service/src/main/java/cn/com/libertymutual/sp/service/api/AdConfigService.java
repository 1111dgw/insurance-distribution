package cn.com.libertymutual.sp.service.api;

import java.sql.DataTruncation;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpAdconfig;

public interface AdConfigService {

	ServiceResult advertList(String sorttype);

	ServiceResult adConfigListWeb(String isShow, String toUrl, String adType, int pageNumber, int pageSize);

	ServiceResult addAdvert(TbSpAdconfig tbSpAdconfig) throws DataTruncation;

	ServiceResult updateAdvert(TbSpAdconfig tbSpAdconfig);

	ServiceResult removeAdvert(String[] list);

	ServiceResult getAdDetail(Integer id);

	ServiceResult updateAdDetail(Integer id, String detail);

}
