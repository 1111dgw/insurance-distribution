package cn.com.libertymutual.production.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import cn.com.libertymutual.production.dao.nomorcldatasource.PrpdkindlibraryMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdkindlibrary;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdkindlibraryExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdkindlibraryWithBLOBs;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdkindlibraryExample.Criteria;
import cn.com.libertymutual.production.pojo.request.PrpdKindLibraryRequest;
import cn.com.libertymutual.production.service.api.PrpdKindLibraryService;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

/** 
 * @author Steven.Li
 * @date 2017年7月28日
 *  
 */
@Service
public class PrpdKindLibraryServiceImpl implements PrpdKindLibraryService {

	@Autowired
	private PrpdkindlibraryMapper prpdkindlibraryMapper;
	
	@Override
	public PageInfo<PrpdkindlibraryWithBLOBs> findPrpdKindLibrary(PrpdKindLibraryRequest prpdKindLibraryRequest) {
		PrpdkindlibraryExample example = new PrpdkindlibraryExample();
		Criteria criteria = example.createCriteria();
		String kindCode = prpdKindLibraryRequest.getKindcode();
		String kindVersion = prpdKindLibraryRequest.getKindversion();
		String kindCName = prpdKindLibraryRequest.getKindcname();
		String kindEName = prpdKindLibraryRequest.getKindename();
		Date createDate = prpdKindLibraryRequest.getCreatedate();
		List<String> riskCodes = prpdKindLibraryRequest.getRiskcodes();
		String validStatus = prpdKindLibraryRequest.getValidstatus();
		String orderBy = prpdKindLibraryRequest.getOrderBy();
		String order = prpdKindLibraryRequest.getOrder();
		
		example.setOrderByClause("createdate desc");
		if(!StringUtils.isEmpty(kindCode)) {
			criteria.andKindcodeLike("%" + kindCode.toUpperCase() + "%");
		}
		if (!StringUtils.isEmpty(kindVersion)) {
			criteria.andKindversionEqualTo(kindVersion);
		}
		if (!StringUtils.isEmpty(kindCName)) {
			criteria.andKindcnameLike("%" + kindCName + "%");
		}
		if (!StringUtils.isEmpty(kindEName)) {
			criteria.andKindenameLike("%" + kindEName + "%");
		}
		if (riskCodes != null && !riskCodes.isEmpty()) {
			criteria.andOwnerriskcodeIn(riskCodes);
		}
		if (createDate != null) {
			criteria.andCreatedateGreaterThanOrEqualTo(createDate);
			Calendar c = Calendar.getInstance();  
	        c.setTime(createDate);  
	        c.add(Calendar.DAY_OF_MONTH, 1);// 今天+1天  
	        Date tomorrow = c.getTime();
	        criteria.andCreatedateLessThanOrEqualTo(tomorrow);
		}
		if (!StringUtils.isEmpty(validStatus)) {
			criteria.andValidstatusEqualTo(validStatus);
		}
		if (!StringUtils.isEmpty(orderBy) && !"undefined".equals(orderBy)) {
			example.setOrderByClause(orderBy + " " + order);
		}
		PageHelper.startPage(prpdKindLibraryRequest.getCurrentPage(), prpdKindLibraryRequest.getPageSize());
		PageInfo<PrpdkindlibraryWithBLOBs> pageInfo = new PageInfo<>(prpdkindlibraryMapper.selectByExampleWithBLOBs(example));
		return pageInfo;
	}

	@Override
	public void insert(PrpdkindlibraryWithBLOBs record) {
		prpdkindlibraryMapper.insertSelective(record);
	}

	@Override
	public void update(PrpdkindlibraryWithBLOBs record) {
		PrpdkindlibraryExample example = new PrpdkindlibraryExample();
		Criteria criteria = example.createCriteria();
		criteria.andKindcodeEqualTo(record.getKindcode());
		if(!StringUtils.isEmpty(record.getKindversion())) {
			criteria.andKindversionEqualTo(record.getKindversion());
		}
		prpdkindlibraryMapper.updateByExampleSelective(record, example);
	}

	@Override
	public List<PrpdkindlibraryWithBLOBs> fetchKinds(String kindCode,
			String kindVersion, String ownerRiskCode, String validStatus) {
		PrpdkindlibraryExample example = new PrpdkindlibraryExample();
		Criteria criteria = example.createCriteria();
		if(!StringUtils.isEmpty(kindCode)) {
			criteria.andKindcodeEqualTo(kindCode);
		}
		if (!StringUtils.isEmpty(kindVersion)) {
			criteria.andKindversionEqualTo(kindVersion);
		}
		if (!StringUtils.isEmpty(ownerRiskCode)) {
			criteria.andOwnerriskcodeEqualTo(ownerRiskCode);
		}
		if (!StringUtils.isEmpty(validStatus)) {
			criteria.andValidstatusEqualTo(validStatus);
		}
		List<PrpdkindlibraryWithBLOBs> list = prpdkindlibraryMapper.selectByExampleWithBLOBs(example);
		return list;
	}

	@Override
	public List<PrpdkindlibraryWithBLOBs> fetchKinds(String kindCode,
			String kindVersion, String validStatus) {
		return fetchKinds(kindCode,kindVersion,null,validStatus);
	}

	@Override
	public void updateEFileStatus(PrpdkindlibraryWithBLOBs record, String kindCode, String kindVersion) {
		PrpdkindlibraryExample example = new PrpdkindlibraryExample();
		Criteria criteria = example.createCriteria();
		criteria.andKindcodeEqualTo(kindCode);
		criteria.andKindversionEqualTo(kindVersion);
		prpdkindlibraryMapper.updateByExampleSelective(record, example);
	}

	@Override
	public PageInfo<Prpdkindlibrary> findByOwnerRisk(PrpdKindLibraryRequest prpdKindLibraryRequest) {
		String ownerriskcode = null;
		String kindcode = null;
		String kindcname = null;
		String riskcode = prpdKindLibraryRequest.getRisk().getRiskcode();
		String riskversion = prpdKindLibraryRequest.getRisk().getRiskversion();
		/**
		 * 险种关联条款时，查询出所有条款
		 */
//		if(!"1".equals(prpdKindLibraryRequest.getRisk().getCompositeflag())) {
//			ownerriskcode = prpdKindLibraryRequest.getOwnerriskcode();
//		}
		if(!StringUtils.isEmpty(prpdKindLibraryRequest.getKindcode())) {
			kindcode = prpdKindLibraryRequest.getKindcode();
		}
		if(!StringUtils.isEmpty(prpdKindLibraryRequest.getKindcname())) {
			kindcname = prpdKindLibraryRequest.getKindcname();
		}
		PageInfo<Prpdkindlibrary> pageInfo = new PageInfo<>(prpdkindlibraryMapper.selectByOwnerRisk(ownerriskcode, 
																									kindcode, 
																									kindcname, 
																									riskcode, 
																									riskversion, 
																									prpdKindLibraryRequest.getCurrentPage(), 
																									prpdKindLibraryRequest.getPageSize()));
		return pageInfo;
	}
}
