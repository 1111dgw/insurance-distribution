
package cn.com.libertymutual.sp.webService.policy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for prptBeneficiaryDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prptBeneficiaryDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="benefBenefitRate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="benefIdentifyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="benefIdentifyType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="benefInsuredIdentity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="benefNSex" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="benefPrpInsuredInsuredCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="benefPrpInsuredInsuredName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="relationSerialNo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prptBeneficiaryDto", propOrder = {
    "benefBenefitRate",
    "benefIdentifyNumber",
    "benefIdentifyType",
    "benefInsuredIdentity",
    "benefNSex",
    "benefPrpInsuredInsuredCode",
    "benefPrpInsuredInsuredName",
    "relationSerialNo"
})
public class PrptBeneficiaryDto implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    protected String benefBenefitRate;
    protected String benefIdentifyNumber;
    protected String benefIdentifyType;
    protected String benefInsuredIdentity;
    protected String benefNSex;
    protected String benefPrpInsuredInsuredCode;
    protected String benefPrpInsuredInsuredName;
    protected int relationSerialNo;

    /**
     * Gets the value of the benefBenefitRate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBenefBenefitRate() {
        return benefBenefitRate;
    }

    /**
     * Sets the value of the benefBenefitRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBenefBenefitRate(String value) {
        this.benefBenefitRate = value;
    }

    /**
     * Gets the value of the benefIdentifyNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBenefIdentifyNumber() {
        return benefIdentifyNumber;
    }

    /**
     * Sets the value of the benefIdentifyNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBenefIdentifyNumber(String value) {
        this.benefIdentifyNumber = value;
    }

    /**
     * Gets the value of the benefIdentifyType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBenefIdentifyType() {
        return benefIdentifyType;
    }

    /**
     * Sets the value of the benefIdentifyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBenefIdentifyType(String value) {
        this.benefIdentifyType = value;
    }

    /**
     * Gets the value of the benefInsuredIdentity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBenefInsuredIdentity() {
        return benefInsuredIdentity;
    }

    /**
     * Sets the value of the benefInsuredIdentity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBenefInsuredIdentity(String value) {
        this.benefInsuredIdentity = value;
    }

    /**
     * Gets the value of the benefNSex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBenefNSex() {
        return benefNSex;
    }

    /**
     * Sets the value of the benefNSex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBenefNSex(String value) {
        this.benefNSex = value;
    }

    /**
     * Gets the value of the benefPrpInsuredInsuredCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBenefPrpInsuredInsuredCode() {
        return benefPrpInsuredInsuredCode;
    }

    /**
     * Sets the value of the benefPrpInsuredInsuredCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBenefPrpInsuredInsuredCode(String value) {
        this.benefPrpInsuredInsuredCode = value;
    }

    /**
     * Gets the value of the benefPrpInsuredInsuredName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBenefPrpInsuredInsuredName() {
        return benefPrpInsuredInsuredName;
    }

    /**
     * Sets the value of the benefPrpInsuredInsuredName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBenefPrpInsuredInsuredName(String value) {
        this.benefPrpInsuredInsuredName = value;
    }

    /**
     * Gets the value of the relationSerialNo property.
     * 
     */
    public int getRelationSerialNo() {
        return relationSerialNo;
    }

    /**
     * Sets the value of the relationSerialNo property.
     * 
     */
    public void setRelationSerialNo(int value) {
        this.relationSerialNo = value;
    }

}
