package cn.com.libertymutual.wx.message.responsedto;

import java.util.Calendar;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import cn.com.libertymutual.wx.common.MessageType;

public class TextMessage extends ResponseBaseMessage {
	@XStreamAlias("Content")
	private String content;

	public TextMessage() {
		setMsgType(MessageType.REQ_MESSAGE_TYPE_TEXT);
	}

	public TextMessage(ResponseBaseMessage rbm) {
		long ct = Calendar.getInstance().getTimeInMillis();
		setCreateTime(ct);
		setFromUserName(rbm.getFromUserName());
		setToUserName(rbm.getToUserName());
		setMsgType(MessageType.REQ_MESSAGE_TYPE_TEXT);
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "TextMessage [content=" + content + ", toString()=" + super.toString() + "]";
	}

}
