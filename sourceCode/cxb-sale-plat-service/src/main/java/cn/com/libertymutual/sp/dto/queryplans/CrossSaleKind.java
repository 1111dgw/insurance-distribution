package cn.com.libertymutual.sp.dto.queryplans;

import java.io.Serializable;

public class CrossSaleKind implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2944377213462047088L;

	private String cvgCode;//险别代码
	private String cvgName;//险别名称
	private String cvgEName;//险别英文名
	private String adultMax;//保额
	private String adultStr;//保额 千分号显示
	private String benefitsCode;//
	private String paraphrase;//
	private  String percentage;//保费占比
	private  String premium;//保费
	private Double unitamount;
	 private Integer quantity;
	
	public String getCvgCode() {
		return cvgCode;
	}
	public void setCvgCode(String cvgCode) {
		this.cvgCode = cvgCode;
	}
	public String getCvgName() {
		return cvgName;
	}
	public void setCvgName(String cvgName) {
		this.cvgName = cvgName;
	}
	public String getCvgEName() {
		return cvgEName;
	}
	public void setCvgEName(String cvgEName) {
		this.cvgEName = cvgEName;
	}
	public String getAdultMax() {
		return adultMax;
	}
	public void setAdultMax(String adultMax) {
		this.adultMax = adultMax;
	}
	public String getAdultStr() {
		return adultStr;
	}
	public void setAdultStr(String adultStr) {
		this.adultStr = adultStr;
	}
	public String getBenefitsCode() {
		return benefitsCode;
	}
	public void setBenefitsCode(String benefitsCode) {
		this.benefitsCode = benefitsCode;
	}
	public String getParaphrase() {
		return paraphrase;
	}
	public void setParaphrase(String paraphrase) {
		this.paraphrase = paraphrase;
	}
	public String getPercentage() {
		return percentage;
	}
	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}
	public String getPremium() {
		return premium;
	}
	public void setPremium(String premium) {
		this.premium = premium;
	}
	public Double getUnitamount() {
		return unitamount;
	}
	public void setUnitamount(Double unitamount) {
		this.unitamount = unitamount;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	
}
