package cn.com.libertymutual.production.model.nomorcldatasource;

import cn.com.libertymutual.production.utils.LogFiled;

public class Prpdriskclause extends PrpdriskclauseKey {
    private String clausename;

    private String validstatus;

    private String flag;

    private String language;
    @LogFiled(chineseName="适用默认带出险种方案")
    private String plancode;
    @LogFiled(chineseName="适用险种")
    private String comcode;

    public String getClausename() {
        return clausename;
    }

    public void setClausename(String clausename) {
        this.clausename = clausename == null ? null : clausename.trim();
    }

    public String getValidstatus() {
        return validstatus;
    }

    public void setValidstatus(String validstatus) {
        this.validstatus = validstatus == null ? null : validstatus.trim();
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag == null ? null : flag.trim();
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language == null ? null : language.trim();
    }

    public String getPlancode() {
        return plancode;
    }

    public void setPlancode(String plancode) {
        this.plancode = plancode == null ? null : plancode.trim();
    }

    public String getComcode() {
        return comcode;
    }

    public void setComcode(String comcode) {
        this.comcode = comcode == null ? null : comcode.trim();
    }
}