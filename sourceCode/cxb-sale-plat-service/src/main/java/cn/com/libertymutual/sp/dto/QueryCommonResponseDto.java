package cn.com.libertymutual.sp.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import cn.com.libertymutual.core.base.dto.ResponseBasePageDto;


public class QueryCommonResponseDto extends ResponseBasePageDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6584878496537588914L;


	private List<Map<String,Object>> resList;


	public List<Map<String, Object>> getResList() {
		return resList;
	}


	public void setResList(List<Map<String, Object>> resList) {
		this.resList = resList;
	}
	
}
