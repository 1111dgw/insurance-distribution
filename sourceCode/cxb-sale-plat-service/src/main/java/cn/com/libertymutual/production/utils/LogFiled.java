package cn.com.libertymutual.production.utils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** 
 * @Description: 对象模型注解 
 * @author Steven.Li
 * @date 2017年7月5日
 *  
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface LogFiled {
    /**
     * 目标对象模型字段名称
     * @return
     */
    public String chineseName();
}
