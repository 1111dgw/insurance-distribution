package cn.com.libertymutual.sp.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpServiceconfig;
import cn.com.libertymutual.sp.service.api.ServiceMenuService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = "/admin/serviceMenuWeb")
public class ServiceMenuControllerWeb {

	private Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	private ServiceMenuService serviceMenuService;
	
	
	@ApiOperation(value = "后台服务菜单列表", notes = "后台服务菜单列表")
	@PostMapping(value = "/menuList")
	@ApiImplicitParams(value = {
            @ApiImplicitParam(name = "isValidate", value = "是否有效", required = true, paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "serviceCname", value = "服务中文名", required = true, paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "serviceType", value = "服务类型", required = true, paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "toType", value = "跳转类别", required = true, paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
            @ApiImplicitParam(name = "pageSize", value = "条数", required = true, paramType = "query", dataType = "Long")
    })
	public ServiceResult menuListweb(String isValidate,String serviceCname,String serviceType,String toType,int pageNumber,int pageSize){
		ServiceResult sr = new ServiceResult();
		sr = serviceMenuService.menuListWeb(isValidate,serviceCname,serviceType,toType,pageNumber,pageSize);
		return sr;
		
	}
	
	
	@ApiOperation(value = "后台服务菜单新增", notes = "后台服务菜单新增")
	@PostMapping(value = "/addService")
	public ServiceResult addService(@RequestBody @ApiParam(name = "tbSpServiceconfig", value = "tbSpServiceconfig", required = true) TbSpServiceconfig tbSpServiceconfig){
		ServiceResult sr = new ServiceResult();
		sr = serviceMenuService.addService(tbSpServiceconfig);
		return sr;
		
	}
	
	
	@ApiOperation(value = "后台服务菜单修改", notes = "后台服务菜单修改")
	@PostMapping(value = "/updateService")
	public ServiceResult updateService(@RequestBody @ApiParam(name = "tbSpServiceconfig", value = "tbSpServiceconfig", required = true) TbSpServiceconfig tbSpServiceconfig){
		ServiceResult sr = new ServiceResult();
		sr = serviceMenuService.updateService(tbSpServiceconfig);
		return sr;
		
	}
	
	@ApiOperation(value = "后台服务菜单删除", notes = "后台服务菜单删除")
	@PostMapping(value = "/removeService")
	public ServiceResult removeService(String ids){
		ServiceResult sr = new ServiceResult();
		String[] list = ids.split(",");
		
		sr = serviceMenuService.removeService(list);
		return sr;
		
	}
	
	
	
	/*
	 * 查询三方地址
	 */
	@ApiOperation(value = "查询三方地址", notes = "查询三方地址")
	@ApiImplicitParams(value = {
        @ApiImplicitParam(name = "url", value = "地址", required = true, paramType = "query", dataType = "String"),
        @ApiImplicitParam(name = "userName", value = "名称", required = true, paramType = "query", dataType = "String"),
        @ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
        @ApiImplicitParam(name = "pageSize", value = "条数", required = true, paramType = "query", dataType = "Long")
    })
	@PostMapping(value = "/serviceInfo")
	public ServiceResult thirdServiceInfo(String url,String userName,Integer pageNumber,Integer pageSize){
		ServiceResult sr = new ServiceResult();
		sr = serviceMenuService.thirdServiceInfo(url,userName,pageNumber,pageSize);
		return sr;
		
	}
	/*
	 * 修改三方地址
	 */
	@ApiOperation(value = "修改三方地址", notes = "修改三方地址")
	@PostMapping(value = "/updateServiceInfo")
	@ApiImplicitParams(value = {
		@ApiImplicitParam(name = "modifyDate", value = "修改时间", required = true, paramType = "query", dataType = "String"),
        @ApiImplicitParam(name = "id", value = "id", required = true, paramType = "query", dataType = "Long"),
        @ApiImplicitParam(name = "url", value = "三方地址", required = true, paramType = "query", dataType = "String"),
    })
	public ServiceResult updateServiceInfo(Integer id,String modifyDate,String url){
		ServiceResult sr = new ServiceResult();
		sr = serviceMenuService.updateServiceInfo(id,modifyDate,url);
		return sr;
		
	}
}
