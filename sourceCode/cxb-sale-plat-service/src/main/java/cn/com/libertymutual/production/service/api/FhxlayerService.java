package cn.com.libertymutual.production.service.api;

import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Fhxlayer;

/**
 * @author steven.li
 * @create 2017/12/20
 */
public interface FhxlayerService {

    void insert(Fhxlayer record);

    List<Fhxlayer> findAll(String treatyno);

}
