package cn.com.libertymutual.sp.bean;

import java.io.Serializable;

import javax.persistence.*;

import io.swagger.annotations.ApiModel;

/**
 * Created by Ryan on 2016-09-09.
 */
@ApiModel
@Entity
@Table(name = "tb_sys_hotarea")
public class TbSysHotarea   implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1516648960407453035L;
	private int id;
    private String branchCode;
    private String branchName;
    private String branchEname;
    private String areaCode;
    private String areaName;
    private String status;
    private String agreementNo;//业务关系代码
    private String saleCode;//销售人员代码 （ 广东 、四川机构特有）
    private String saleName;//销售人员名称（ 广东 、四川机构特有）
    private String invoiceStatus; //是否显示发票
    private String insurancePolicyStatus; //是否显示保单
    private String  contactMobile;
    private String  contactEmail;
    
    
    
    public TbSysHotarea(){
    	super();
    	
    }

	@Id    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Column(name = "BRANCH_CODE", length = 20)
    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    @Column(name = "BRANCH_NAME", length = 20)
	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	
	 @Column(name = "BRANCH_ENAME", length = 20)
	public String getBranchEname() {
		return branchEname;
	}

	public void setBranchEname(String branchEname) {
		this.branchEname = branchEname;
	}

	@Column(name = "AREA_CODE", length = 20)
	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	@Column(name = "AREA_NAME", length = 20)
	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	@Column(name = "STATUS", length = 2)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name = "AGREEMENTNO")
	public String getAgreementNo() {
		return agreementNo;
	}

	public void setAgreementNo(String agreementNo) {
		this.agreementNo = agreementNo;
	}
	@Column(name = "saleCode")
	public String getSaleCode() {
		return saleCode;
	}

	public void setSaleCode(String saleCode) {
		this.saleCode = saleCode;
	}
	@Column(name = "saleName")
	public String getSaleName() {
		return saleName;
	}

	public void setSaleName(String saleName) {
		this.saleName = saleName;
	}

	@Column(name = "Invoice_status")
	public String getInvoiceStatus() {
		return invoiceStatus;
	}

	public void setInvoiceStatus(String invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	@Column(name = "InsurancePolicy_status")
	public String getInsurancePolicyStatus() {
		return insurancePolicyStatus;
	}

	public void setInsurancePolicyStatus(String insurancePolicyStatus) {
		this.insurancePolicyStatus = insurancePolicyStatus;
	}

	@Column(name = "CONTACT_MOBILE")
	public String getContactMobile() {
		return contactMobile;
	}

	public void setContactMobile(String contactMobile) {
		this.contactMobile = contactMobile;
	}

	@Column(name = "CONTACT_EMAIL")
	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}







}


