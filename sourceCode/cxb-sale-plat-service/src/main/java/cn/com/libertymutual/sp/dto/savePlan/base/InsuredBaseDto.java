package cn.com.libertymutual.sp.dto.savePlan.base;

import java.util.List;

import cn.com.libertymutual.sp.dto.queryplans.InsuredDto;

/**
 * 销售平台基础被保人Dto
 */
public class InsuredBaseDto {

	private InsuredDto insuredDto;
	private List<ItemKindBaseDto> itemKindDtoList;
	private ProductBaseDto productDto;
	public List<ItemKindBaseDto> getItemKindDtoList() {
		return itemKindDtoList;
	}
	public void setItemKindDtoList(List<ItemKindBaseDto> itemKindDtoList) {
		this.itemKindDtoList = itemKindDtoList;
	}

	public ProductBaseDto getProductDto() {
		return productDto;
	}
	public void setProductDto(ProductBaseDto productDto) {
		this.productDto = productDto;
	}
	public InsuredDto getInsuredDto() {
		return insuredDto;
	}
	public void setInsuredDto(InsuredDto insuredDto) {
		this.insuredDto = insuredDto;
	}	

}
