package cn.com.libertymutual.sp.service.api;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.SysOperationLog;
import cn.com.libertymutual.sp.bean.TbSpSaleLog;

public interface OperationLogService {

	public boolean saveLog(TbSpSaleLog saleLog, SysOperationLog operationLog);

	ServiceResult logList(int pageNumber, int pageSize);

	ServiceResult queryLog(String userId, String level, String startTime, String endTime, int pageNumber, int pageSize);

	ServiceResult querySaleLog(String requestData, String responseData, String startTime, String endTime, String userCode, String mark,
			String operation, int pageNumber, int pageSize);

	ServiceResult deleteSaleLog(Integer id);

}
