package cn.com.libertymutual.sp.action.car;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.service.api.car.CarInitService;
import cn.com.libertymutual.sp.service.api.car.CarInsureService;

@RestController
@RequestMapping(value = "/nol/carInit")
public class CarInitController {
	@Autowired
	private CarInsureService carInsureService;
	@Autowired
	private CarInitService carInitService;

	/**
	 * 投保界面初始化信息
	 * @param trRequest
	 */
	@RequestMapping(value = "/getCarInit")
	public ServiceResult saleRenewalQuery(String riskCode) {
		return carInitService.getInitOffer(riskCode);
	}

	/**
	 * 获得选择初始化信息
	 * @param trRequest
	 */
	@RequestMapping(value = "/getCarSeleInit")
	public ServiceResult getSeleInit(String riskCode, String branchCode) {
		return carInitService.querySeleInit(riskCode, branchCode);
	}

	/**
	 * 获得车辆使用性质
	 * @param trRequest
	 */
	@RequestMapping(value = "/getUserNature")
	public ServiceResult getUserNature(String insuredNature, String carKind, String riskCode) {
		return carInitService.queryUseNature(insuredNature, carKind, riskCode);
	}

}
