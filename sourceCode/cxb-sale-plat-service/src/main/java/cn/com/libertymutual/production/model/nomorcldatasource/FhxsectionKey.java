package cn.com.libertymutual.production.model.nomorcldatasource;

public class FhxsectionKey {
    private String treatyno;

    private Short layerno;

    private String sectionno;

    public String getTreatyno() {
        return treatyno;
    }

    public void setTreatyno(String treatyno) {
        this.treatyno = treatyno == null ? null : treatyno.trim();
    }

    public Short getLayerno() {
        return layerno;
    }

    public void setLayerno(Short layerno) {
        this.layerno = layerno;
    }

    public String getSectionno() {
        return sectionno;
    }

    public void setSectionno(String sectionno) {
        this.sectionno = sectionno == null ? null : sectionno.trim();
    }
}