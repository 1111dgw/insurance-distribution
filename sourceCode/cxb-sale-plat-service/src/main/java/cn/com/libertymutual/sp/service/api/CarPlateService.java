package cn.com.libertymutual.sp.service.api;

import cn.com.libertymutual.core.web.ServiceResult;

public interface CarPlateService {

	ServiceResult plateList(Integer pageNumber, Integer pageSize);

	ServiceResult addPlate(String plateNo);

	ServiceResult removePlate(String plateNo);

}
