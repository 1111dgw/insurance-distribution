package cn.com.libertymutual.production.service.impl;

import cn.com.libertymutual.production.dao.nomorcldatasource.FhxlayerMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Fhxlayer;
import cn.com.libertymutual.production.model.nomorcldatasource.FhxlayerExample;
import cn.com.libertymutual.production.service.api.FhxlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author steven.li
 * @create 2017/12/20
 */
@Service
public class FhxlayerServiceImpl implements FhxlayerService {

    @Autowired
    private FhxlayerMapper fhxlayerMapper;

    @Override
    public void insert(Fhxlayer record) {
        fhxlayerMapper.insertSelective(record);
    }

    @Override
    public List<Fhxlayer> findAll(String treatyno) {
        FhxlayerExample criteria = new FhxlayerExample();
        criteria.createCriteria().andTreatynoEqualTo(treatyno);
        return fhxlayerMapper.selectByExample(criteria);
    }
}
