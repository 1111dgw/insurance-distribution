package cn.com.libertymutual.sp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.libertymutual.sp.bean.TbSpProductConfig;
import cn.com.libertymutual.sp.dao.OrderDao;
import cn.com.libertymutual.sp.dao.ProductConfigDao;
import cn.com.libertymutual.sp.service.api.ProductConfigService;

@Service("ProductConfigService")
public class ProductConfigImpl implements ProductConfigService{

	@Autowired
	private ProductConfigDao productConfigDao;
	
	@Override
	public List<TbSpProductConfig> findByBranchCode(String branchCode) {
		return productConfigDao.findByBranchCodeAndStatus(branchCode,"1");
	}

}
