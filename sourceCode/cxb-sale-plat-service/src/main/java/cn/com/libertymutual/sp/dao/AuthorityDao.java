package cn.com.libertymutual.sp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import cn.com.libertymutual.sp.bean.TbSpAuthority;

public interface AuthorityDao extends PagingAndSortingRepository<TbSpAuthority, Integer>, JpaSpecificationExecutor<TbSpAuthority>{

	@Query("from TbSpAuthority where userCode=?1 and status=1")
	List<TbSpAuthority> findByUserCode(String userCode);

	@Query("from TbSpAuthority where userCode=?1 and channelCode =?2 and status=1")
	TbSpAuthority findByUserCodeAndChannelCode(String userCode, String branchNo);

	@Transactional
	@Modifying
	@Query(value="delete from tb_sp_authority where USER_CODE=?1 ",nativeQuery=true)
	void deleteByUserCode(String userCode);
	
	@Query("select channelCode from TbSpAuthority where userCode=?1 and status=1")
	List<String> findNoByUserCode(String userCode);

	
	@Transactional
	@Modifying
	@Query(value="delete from tb_sp_authority where USER_CODE=?1 and CHANNEL_CODE=?2",nativeQuery=true)
	void deleteByUserCodeAndChannelCode(String userCode, String notHas);

}
