package cn.com.libertymutual.wx.message.responsedto;

import java.util.Calendar;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import cn.com.libertymutual.wx.common.MessageType;

public class ImageMessage extends ResponseBaseMessage {
	@XStreamAlias("MediaId")
	private String mediaId;
	@XStreamAlias("PicUrl")
	private String picUrl;

	public ImageMessage() {
		setMsgType(MessageType.REQ_MESSAGE_TYPE_IMAGE);
	}

	public ImageMessage(ResponseBaseMessage rbm) {
		long ct = Calendar.getInstance().getTimeInMillis();
		setCreateTime(ct);
		setFromUserName(rbm.getFromUserName());
		setToUserName(rbm.getToUserName());
		setMsgType(MessageType.REQ_MESSAGE_TYPE_IMAGE);
	}

	public String getMediaId() {
		return this.mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	@Override
	public String toString() {
		return "ImageMessage [mediaId=" + mediaId + ", picUrl=" + picUrl + ", getMediaId()=" + getMediaId() + ", getPicUrl()=" + getPicUrl()
				+ ", getToUserName()=" + getToUserName() + ", getFromUserName()=" + getFromUserName() + ", getCreateTime()=" + getCreateTime()
				+ ", getMsgType()=" + getMsgType() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}
}
