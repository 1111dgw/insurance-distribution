package cn.com.libertymutual.sp.dto;

import java.util.List;

import cn.com.libertymutual.core.base.dto.ResponseBasePageDto;

import com.google.common.collect.Lists;


public class TQueryPolicyResponseDto extends ResponseBasePageDto{
	
    /**
	 * 
	 */
	private static final long serialVersionUID = -6586972590296422983L;
	List<TPolicyDto> policys;
	public List<TPolicyDto> getPolicys() {
		if(null==policys){
			policys=Lists.newArrayList();
		}
		return policys;
	}
	public void setPolicys(List<TPolicyDto> policys) {
		this.policys = policys;
	}
	
}
