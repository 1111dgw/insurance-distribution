package cn.com.libertymutual.production.model.nomorcldatasource;

public class PrpdriskclauseWithBLOBs extends Prpdriskclause {
    private String plancode;

    private String comcode;

    public String getPlancode() {
        return plancode;
    }

    public void setPlancode(String plancode) {
        this.plancode = plancode == null ? null : plancode.trim();
    }

    public String getComcode() {
        return comcode;
    }

    public void setComcode(String comcode) {
        this.comcode = comcode == null ? null : comcode.trim();
    }
}