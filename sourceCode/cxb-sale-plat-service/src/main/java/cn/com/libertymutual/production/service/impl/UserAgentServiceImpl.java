package cn.com.libertymutual.production.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.libertymutual.production.dao.nomorcldatasource.PrpsagentMapper;
import cn.com.libertymutual.production.dao.nomorcldatasource.TbSpUserMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpsagent;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpsagentExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpsagentWithBLOBs;
import cn.com.libertymutual.production.model.nomorcldatasource.TbSpUser;
import cn.com.libertymutual.production.model.nomorcldatasource.TbSpUserExample;
import cn.com.libertymutual.production.pojo.request.Request;
import cn.com.libertymutual.production.service.api.PrpsAgentService;
import cn.com.libertymutual.production.service.api.UserAgentService;

/** 
 * @author GuoYue
 * @date 2017年9月5日
 *  
 */
@Service
public class UserAgentServiceImpl implements UserAgentService {
	
	@Autowired
	private TbSpUserMapper userMapper;

	@Override
	public List<TbSpUser> findUsersAgentByComCode(List<String> comCodes, Request request) {
		List<TbSpUser> rs = new ArrayList<TbSpUser>();
		Set<TbSpUser> uniqueAgents = new HashSet<TbSpUser>();
		if(comCodes.isEmpty()) {
			return null;
		} else if (comCodes.contains("ALL")){
			TbSpUserExample example = new TbSpUserExample();
			example.createCriteria().andStateEqualTo("1");
			rs = userMapper.selectByExample(example );
		} else {
			for(String comcode : comCodes) {
				List<TbSpUser> agents = userMapper.selectWithComcode(comcode);
				uniqueAgents.addAll(agents);
			}
			rs.addAll(uniqueAgents);
		}
		return rs;
	}

}
