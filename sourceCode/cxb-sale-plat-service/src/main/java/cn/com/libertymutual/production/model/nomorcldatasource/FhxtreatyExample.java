package cn.com.libertymutual.production.model.nomorcldatasource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FhxtreatyExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public FhxtreatyExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andTreatynoIsNull() {
            addCriterion("TREATYNO is null");
            return (Criteria) this;
        }

        public Criteria andTreatynoIsNotNull() {
            addCriterion("TREATYNO is not null");
            return (Criteria) this;
        }

        public Criteria andTreatynoEqualTo(String value) {
            addCriterion("TREATYNO =", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoNotEqualTo(String value) {
            addCriterion("TREATYNO <>", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoGreaterThan(String value) {
            addCriterion("TREATYNO >", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoGreaterThanOrEqualTo(String value) {
            addCriterion("TREATYNO >=", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoLessThan(String value) {
            addCriterion("TREATYNO <", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoLessThanOrEqualTo(String value) {
            addCriterion("TREATYNO <=", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoLike(String value) {
            addCriterion("TREATYNO like", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoNotLike(String value) {
            addCriterion("TREATYNO not like", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoIn(List<String> values) {
            addCriterion("TREATYNO in", values, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoNotIn(List<String> values) {
            addCriterion("TREATYNO not in", values, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoBetween(String value1, String value2) {
            addCriterion("TREATYNO between", value1, value2, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoNotBetween(String value1, String value2) {
            addCriterion("TREATYNO not between", value1, value2, "treatyno");
            return (Criteria) this;
        }

        public Criteria andExtreatynoIsNull() {
            addCriterion("EXTREATYNO is null");
            return (Criteria) this;
        }

        public Criteria andExtreatynoIsNotNull() {
            addCriterion("EXTREATYNO is not null");
            return (Criteria) this;
        }

        public Criteria andExtreatynoEqualTo(String value) {
            addCriterion("EXTREATYNO =", value, "extreatyno");
            return (Criteria) this;
        }

        public Criteria andExtreatynoNotEqualTo(String value) {
            addCriterion("EXTREATYNO <>", value, "extreatyno");
            return (Criteria) this;
        }

        public Criteria andExtreatynoGreaterThan(String value) {
            addCriterion("EXTREATYNO >", value, "extreatyno");
            return (Criteria) this;
        }

        public Criteria andExtreatynoGreaterThanOrEqualTo(String value) {
            addCriterion("EXTREATYNO >=", value, "extreatyno");
            return (Criteria) this;
        }

        public Criteria andExtreatynoLessThan(String value) {
            addCriterion("EXTREATYNO <", value, "extreatyno");
            return (Criteria) this;
        }

        public Criteria andExtreatynoLessThanOrEqualTo(String value) {
            addCriterion("EXTREATYNO <=", value, "extreatyno");
            return (Criteria) this;
        }

        public Criteria andExtreatynoLike(String value) {
            addCriterion("EXTREATYNO like", value, "extreatyno");
            return (Criteria) this;
        }

        public Criteria andExtreatynoNotLike(String value) {
            addCriterion("EXTREATYNO not like", value, "extreatyno");
            return (Criteria) this;
        }

        public Criteria andExtreatynoIn(List<String> values) {
            addCriterion("EXTREATYNO in", values, "extreatyno");
            return (Criteria) this;
        }

        public Criteria andExtreatynoNotIn(List<String> values) {
            addCriterion("EXTREATYNO not in", values, "extreatyno");
            return (Criteria) this;
        }

        public Criteria andExtreatynoBetween(String value1, String value2) {
            addCriterion("EXTREATYNO between", value1, value2, "extreatyno");
            return (Criteria) this;
        }

        public Criteria andExtreatynoNotBetween(String value1, String value2) {
            addCriterion("EXTREATYNO not between", value1, value2, "extreatyno");
            return (Criteria) this;
        }

        public Criteria andRefnoIsNull() {
            addCriterion("REFNO is null");
            return (Criteria) this;
        }

        public Criteria andRefnoIsNotNull() {
            addCriterion("REFNO is not null");
            return (Criteria) this;
        }

        public Criteria andRefnoEqualTo(String value) {
            addCriterion("REFNO =", value, "refno");
            return (Criteria) this;
        }

        public Criteria andRefnoNotEqualTo(String value) {
            addCriterion("REFNO <>", value, "refno");
            return (Criteria) this;
        }

        public Criteria andRefnoGreaterThan(String value) {
            addCriterion("REFNO >", value, "refno");
            return (Criteria) this;
        }

        public Criteria andRefnoGreaterThanOrEqualTo(String value) {
            addCriterion("REFNO >=", value, "refno");
            return (Criteria) this;
        }

        public Criteria andRefnoLessThan(String value) {
            addCriterion("REFNO <", value, "refno");
            return (Criteria) this;
        }

        public Criteria andRefnoLessThanOrEqualTo(String value) {
            addCriterion("REFNO <=", value, "refno");
            return (Criteria) this;
        }

        public Criteria andRefnoLike(String value) {
            addCriterion("REFNO like", value, "refno");
            return (Criteria) this;
        }

        public Criteria andRefnoNotLike(String value) {
            addCriterion("REFNO not like", value, "refno");
            return (Criteria) this;
        }

        public Criteria andRefnoIn(List<String> values) {
            addCriterion("REFNO in", values, "refno");
            return (Criteria) this;
        }

        public Criteria andRefnoNotIn(List<String> values) {
            addCriterion("REFNO not in", values, "refno");
            return (Criteria) this;
        }

        public Criteria andRefnoBetween(String value1, String value2) {
            addCriterion("REFNO between", value1, value2, "refno");
            return (Criteria) this;
        }

        public Criteria andRefnoNotBetween(String value1, String value2) {
            addCriterion("REFNO not between", value1, value2, "refno");
            return (Criteria) this;
        }

        public Criteria andTreatynameIsNull() {
            addCriterion("TREATYNAME is null");
            return (Criteria) this;
        }

        public Criteria andTreatynameIsNotNull() {
            addCriterion("TREATYNAME is not null");
            return (Criteria) this;
        }

        public Criteria andTreatynameEqualTo(String value) {
            addCriterion("TREATYNAME =", value, "treatyname");
            return (Criteria) this;
        }

        public Criteria andTreatynameNotEqualTo(String value) {
            addCriterion("TREATYNAME <>", value, "treatyname");
            return (Criteria) this;
        }

        public Criteria andTreatynameGreaterThan(String value) {
            addCriterion("TREATYNAME >", value, "treatyname");
            return (Criteria) this;
        }

        public Criteria andTreatynameGreaterThanOrEqualTo(String value) {
            addCriterion("TREATYNAME >=", value, "treatyname");
            return (Criteria) this;
        }

        public Criteria andTreatynameLessThan(String value) {
            addCriterion("TREATYNAME <", value, "treatyname");
            return (Criteria) this;
        }

        public Criteria andTreatynameLessThanOrEqualTo(String value) {
            addCriterion("TREATYNAME <=", value, "treatyname");
            return (Criteria) this;
        }

        public Criteria andTreatynameLike(String value) {
            addCriterion("TREATYNAME like", value, "treatyname");
            return (Criteria) this;
        }

        public Criteria andTreatynameNotLike(String value) {
            addCriterion("TREATYNAME not like", value, "treatyname");
            return (Criteria) this;
        }

        public Criteria andTreatynameIn(List<String> values) {
            addCriterion("TREATYNAME in", values, "treatyname");
            return (Criteria) this;
        }

        public Criteria andTreatynameNotIn(List<String> values) {
            addCriterion("TREATYNAME not in", values, "treatyname");
            return (Criteria) this;
        }

        public Criteria andTreatynameBetween(String value1, String value2) {
            addCriterion("TREATYNAME between", value1, value2, "treatyname");
            return (Criteria) this;
        }

        public Criteria andTreatynameNotBetween(String value1, String value2) {
            addCriterion("TREATYNAME not between", value1, value2, "treatyname");
            return (Criteria) this;
        }

        public Criteria andTreatyenameIsNull() {
            addCriterion("TREATYENAME is null");
            return (Criteria) this;
        }

        public Criteria andTreatyenameIsNotNull() {
            addCriterion("TREATYENAME is not null");
            return (Criteria) this;
        }

        public Criteria andTreatyenameEqualTo(String value) {
            addCriterion("TREATYENAME =", value, "treatyename");
            return (Criteria) this;
        }

        public Criteria andTreatyenameNotEqualTo(String value) {
            addCriterion("TREATYENAME <>", value, "treatyename");
            return (Criteria) this;
        }

        public Criteria andTreatyenameGreaterThan(String value) {
            addCriterion("TREATYENAME >", value, "treatyename");
            return (Criteria) this;
        }

        public Criteria andTreatyenameGreaterThanOrEqualTo(String value) {
            addCriterion("TREATYENAME >=", value, "treatyename");
            return (Criteria) this;
        }

        public Criteria andTreatyenameLessThan(String value) {
            addCriterion("TREATYENAME <", value, "treatyename");
            return (Criteria) this;
        }

        public Criteria andTreatyenameLessThanOrEqualTo(String value) {
            addCriterion("TREATYENAME <=", value, "treatyename");
            return (Criteria) this;
        }

        public Criteria andTreatyenameLike(String value) {
            addCriterion("TREATYENAME like", value, "treatyename");
            return (Criteria) this;
        }

        public Criteria andTreatyenameNotLike(String value) {
            addCriterion("TREATYENAME not like", value, "treatyename");
            return (Criteria) this;
        }

        public Criteria andTreatyenameIn(List<String> values) {
            addCriterion("TREATYENAME in", values, "treatyename");
            return (Criteria) this;
        }

        public Criteria andTreatyenameNotIn(List<String> values) {
            addCriterion("TREATYENAME not in", values, "treatyename");
            return (Criteria) this;
        }

        public Criteria andTreatyenameBetween(String value1, String value2) {
            addCriterion("TREATYENAME between", value1, value2, "treatyename");
            return (Criteria) this;
        }

        public Criteria andTreatyenameNotBetween(String value1, String value2) {
            addCriterion("TREATYENAME not between", value1, value2, "treatyename");
            return (Criteria) this;
        }

        public Criteria andTreatytypeIsNull() {
            addCriterion("TREATYTYPE is null");
            return (Criteria) this;
        }

        public Criteria andTreatytypeIsNotNull() {
            addCriterion("TREATYTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andTreatytypeEqualTo(String value) {
            addCriterion("TREATYTYPE =", value, "treatytype");
            return (Criteria) this;
        }

        public Criteria andTreatytypeNotEqualTo(String value) {
            addCriterion("TREATYTYPE <>", value, "treatytype");
            return (Criteria) this;
        }

        public Criteria andTreatytypeGreaterThan(String value) {
            addCriterion("TREATYTYPE >", value, "treatytype");
            return (Criteria) this;
        }

        public Criteria andTreatytypeGreaterThanOrEqualTo(String value) {
            addCriterion("TREATYTYPE >=", value, "treatytype");
            return (Criteria) this;
        }

        public Criteria andTreatytypeLessThan(String value) {
            addCriterion("TREATYTYPE <", value, "treatytype");
            return (Criteria) this;
        }

        public Criteria andTreatytypeLessThanOrEqualTo(String value) {
            addCriterion("TREATYTYPE <=", value, "treatytype");
            return (Criteria) this;
        }

        public Criteria andTreatytypeLike(String value) {
            addCriterion("TREATYTYPE like", value, "treatytype");
            return (Criteria) this;
        }

        public Criteria andTreatytypeNotLike(String value) {
            addCriterion("TREATYTYPE not like", value, "treatytype");
            return (Criteria) this;
        }

        public Criteria andTreatytypeIn(List<String> values) {
            addCriterion("TREATYTYPE in", values, "treatytype");
            return (Criteria) this;
        }

        public Criteria andTreatytypeNotIn(List<String> values) {
            addCriterion("TREATYTYPE not in", values, "treatytype");
            return (Criteria) this;
        }

        public Criteria andTreatytypeBetween(String value1, String value2) {
            addCriterion("TREATYTYPE between", value1, value2, "treatytype");
            return (Criteria) this;
        }

        public Criteria andTreatytypeNotBetween(String value1, String value2) {
            addCriterion("TREATYTYPE not between", value1, value2, "treatytype");
            return (Criteria) this;
        }

        public Criteria andUwyearIsNull() {
            addCriterion("UWYEAR is null");
            return (Criteria) this;
        }

        public Criteria andUwyearIsNotNull() {
            addCriterion("UWYEAR is not null");
            return (Criteria) this;
        }

        public Criteria andUwyearEqualTo(String value) {
            addCriterion("UWYEAR =", value, "uwyear");
            return (Criteria) this;
        }

        public Criteria andUwyearNotEqualTo(String value) {
            addCriterion("UWYEAR <>", value, "uwyear");
            return (Criteria) this;
        }

        public Criteria andUwyearGreaterThan(String value) {
            addCriterion("UWYEAR >", value, "uwyear");
            return (Criteria) this;
        }

        public Criteria andUwyearGreaterThanOrEqualTo(String value) {
            addCriterion("UWYEAR >=", value, "uwyear");
            return (Criteria) this;
        }

        public Criteria andUwyearLessThan(String value) {
            addCriterion("UWYEAR <", value, "uwyear");
            return (Criteria) this;
        }

        public Criteria andUwyearLessThanOrEqualTo(String value) {
            addCriterion("UWYEAR <=", value, "uwyear");
            return (Criteria) this;
        }

        public Criteria andUwyearLike(String value) {
            addCriterion("UWYEAR like", value, "uwyear");
            return (Criteria) this;
        }

        public Criteria andUwyearNotLike(String value) {
            addCriterion("UWYEAR not like", value, "uwyear");
            return (Criteria) this;
        }

        public Criteria andUwyearIn(List<String> values) {
            addCriterion("UWYEAR in", values, "uwyear");
            return (Criteria) this;
        }

        public Criteria andUwyearNotIn(List<String> values) {
            addCriterion("UWYEAR not in", values, "uwyear");
            return (Criteria) this;
        }

        public Criteria andUwyearBetween(String value1, String value2) {
            addCriterion("UWYEAR between", value1, value2, "uwyear");
            return (Criteria) this;
        }

        public Criteria andUwyearNotBetween(String value1, String value2) {
            addCriterion("UWYEAR not between", value1, value2, "uwyear");
            return (Criteria) this;
        }

        public Criteria andStartdateIsNull() {
            addCriterion("STARTDATE is null");
            return (Criteria) this;
        }

        public Criteria andStartdateIsNotNull() {
            addCriterion("STARTDATE is not null");
            return (Criteria) this;
        }

        public Criteria andStartdateEqualTo(Date value) {
            addCriterion("STARTDATE =", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateNotEqualTo(Date value) {
            addCriterion("STARTDATE <>", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateGreaterThan(Date value) {
            addCriterion("STARTDATE >", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateGreaterThanOrEqualTo(Date value) {
            addCriterion("STARTDATE >=", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateLessThan(Date value) {
            addCriterion("STARTDATE <", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateLessThanOrEqualTo(Date value) {
            addCriterion("STARTDATE <=", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateIn(List<Date> values) {
            addCriterion("STARTDATE in", values, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateNotIn(List<Date> values) {
            addCriterion("STARTDATE not in", values, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateBetween(Date value1, Date value2) {
            addCriterion("STARTDATE between", value1, value2, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateNotBetween(Date value1, Date value2) {
            addCriterion("STARTDATE not between", value1, value2, "startdate");
            return (Criteria) this;
        }

        public Criteria andEnddateIsNull() {
            addCriterion("ENDDATE is null");
            return (Criteria) this;
        }

        public Criteria andEnddateIsNotNull() {
            addCriterion("ENDDATE is not null");
            return (Criteria) this;
        }

        public Criteria andEnddateEqualTo(Date value) {
            addCriterion("ENDDATE =", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateNotEqualTo(Date value) {
            addCriterion("ENDDATE <>", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateGreaterThan(Date value) {
            addCriterion("ENDDATE >", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateGreaterThanOrEqualTo(Date value) {
            addCriterion("ENDDATE >=", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateLessThan(Date value) {
            addCriterion("ENDDATE <", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateLessThanOrEqualTo(Date value) {
            addCriterion("ENDDATE <=", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateIn(List<Date> values) {
            addCriterion("ENDDATE in", values, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateNotIn(List<Date> values) {
            addCriterion("ENDDATE not in", values, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateBetween(Date value1, Date value2) {
            addCriterion("ENDDATE between", value1, value2, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateNotBetween(Date value1, Date value2) {
            addCriterion("ENDDATE not between", value1, value2, "enddate");
            return (Criteria) this;
        }

        public Criteria andClosedateIsNull() {
            addCriterion("CLOSEDATE is null");
            return (Criteria) this;
        }

        public Criteria andClosedateIsNotNull() {
            addCriterion("CLOSEDATE is not null");
            return (Criteria) this;
        }

        public Criteria andClosedateEqualTo(Date value) {
            addCriterion("CLOSEDATE =", value, "closedate");
            return (Criteria) this;
        }

        public Criteria andClosedateNotEqualTo(Date value) {
            addCriterion("CLOSEDATE <>", value, "closedate");
            return (Criteria) this;
        }

        public Criteria andClosedateGreaterThan(Date value) {
            addCriterion("CLOSEDATE >", value, "closedate");
            return (Criteria) this;
        }

        public Criteria andClosedateGreaterThanOrEqualTo(Date value) {
            addCriterion("CLOSEDATE >=", value, "closedate");
            return (Criteria) this;
        }

        public Criteria andClosedateLessThan(Date value) {
            addCriterion("CLOSEDATE <", value, "closedate");
            return (Criteria) this;
        }

        public Criteria andClosedateLessThanOrEqualTo(Date value) {
            addCriterion("CLOSEDATE <=", value, "closedate");
            return (Criteria) this;
        }

        public Criteria andClosedateIn(List<Date> values) {
            addCriterion("CLOSEDATE in", values, "closedate");
            return (Criteria) this;
        }

        public Criteria andClosedateNotIn(List<Date> values) {
            addCriterion("CLOSEDATE not in", values, "closedate");
            return (Criteria) this;
        }

        public Criteria andClosedateBetween(Date value1, Date value2) {
            addCriterion("CLOSEDATE between", value1, value2, "closedate");
            return (Criteria) this;
        }

        public Criteria andClosedateNotBetween(Date value1, Date value2) {
            addCriterion("CLOSEDATE not between", value1, value2, "closedate");
            return (Criteria) this;
        }

        public Criteria andCurrencyIsNull() {
            addCriterion("CURRENCY is null");
            return (Criteria) this;
        }

        public Criteria andCurrencyIsNotNull() {
            addCriterion("CURRENCY is not null");
            return (Criteria) this;
        }

        public Criteria andCurrencyEqualTo(String value) {
            addCriterion("CURRENCY =", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyNotEqualTo(String value) {
            addCriterion("CURRENCY <>", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyGreaterThan(String value) {
            addCriterion("CURRENCY >", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyGreaterThanOrEqualTo(String value) {
            addCriterion("CURRENCY >=", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyLessThan(String value) {
            addCriterion("CURRENCY <", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyLessThanOrEqualTo(String value) {
            addCriterion("CURRENCY <=", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyLike(String value) {
            addCriterion("CURRENCY like", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyNotLike(String value) {
            addCriterion("CURRENCY not like", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyIn(List<String> values) {
            addCriterion("CURRENCY in", values, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyNotIn(List<String> values) {
            addCriterion("CURRENCY not in", values, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyBetween(String value1, String value2) {
            addCriterion("CURRENCY between", value1, value2, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyNotBetween(String value1, String value2) {
            addCriterion("CURRENCY not between", value1, value2, "currency");
            return (Criteria) this;
        }

        public Criteria andExtenddateIsNull() {
            addCriterion("EXTENDDATE is null");
            return (Criteria) this;
        }

        public Criteria andExtenddateIsNotNull() {
            addCriterion("EXTENDDATE is not null");
            return (Criteria) this;
        }

        public Criteria andExtenddateEqualTo(Date value) {
            addCriterion("EXTENDDATE =", value, "extenddate");
            return (Criteria) this;
        }

        public Criteria andExtenddateNotEqualTo(Date value) {
            addCriterion("EXTENDDATE <>", value, "extenddate");
            return (Criteria) this;
        }

        public Criteria andExtenddateGreaterThan(Date value) {
            addCriterion("EXTENDDATE >", value, "extenddate");
            return (Criteria) this;
        }

        public Criteria andExtenddateGreaterThanOrEqualTo(Date value) {
            addCriterion("EXTENDDATE >=", value, "extenddate");
            return (Criteria) this;
        }

        public Criteria andExtenddateLessThan(Date value) {
            addCriterion("EXTENDDATE <", value, "extenddate");
            return (Criteria) this;
        }

        public Criteria andExtenddateLessThanOrEqualTo(Date value) {
            addCriterion("EXTENDDATE <=", value, "extenddate");
            return (Criteria) this;
        }

        public Criteria andExtenddateIn(List<Date> values) {
            addCriterion("EXTENDDATE in", values, "extenddate");
            return (Criteria) this;
        }

        public Criteria andExtenddateNotIn(List<Date> values) {
            addCriterion("EXTENDDATE not in", values, "extenddate");
            return (Criteria) this;
        }

        public Criteria andExtenddateBetween(Date value1, Date value2) {
            addCriterion("EXTENDDATE between", value1, value2, "extenddate");
            return (Criteria) this;
        }

        public Criteria andExtenddateNotBetween(Date value1, Date value2) {
            addCriterion("EXTENDDATE not between", value1, value2, "extenddate");
            return (Criteria) this;
        }

        public Criteria andExtendflagIsNull() {
            addCriterion("EXTENDFLAG is null");
            return (Criteria) this;
        }

        public Criteria andExtendflagIsNotNull() {
            addCriterion("EXTENDFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andExtendflagEqualTo(String value) {
            addCriterion("EXTENDFLAG =", value, "extendflag");
            return (Criteria) this;
        }

        public Criteria andExtendflagNotEqualTo(String value) {
            addCriterion("EXTENDFLAG <>", value, "extendflag");
            return (Criteria) this;
        }

        public Criteria andExtendflagGreaterThan(String value) {
            addCriterion("EXTENDFLAG >", value, "extendflag");
            return (Criteria) this;
        }

        public Criteria andExtendflagGreaterThanOrEqualTo(String value) {
            addCriterion("EXTENDFLAG >=", value, "extendflag");
            return (Criteria) this;
        }

        public Criteria andExtendflagLessThan(String value) {
            addCriterion("EXTENDFLAG <", value, "extendflag");
            return (Criteria) this;
        }

        public Criteria andExtendflagLessThanOrEqualTo(String value) {
            addCriterion("EXTENDFLAG <=", value, "extendflag");
            return (Criteria) this;
        }

        public Criteria andExtendflagLike(String value) {
            addCriterion("EXTENDFLAG like", value, "extendflag");
            return (Criteria) this;
        }

        public Criteria andExtendflagNotLike(String value) {
            addCriterion("EXTENDFLAG not like", value, "extendflag");
            return (Criteria) this;
        }

        public Criteria andExtendflagIn(List<String> values) {
            addCriterion("EXTENDFLAG in", values, "extendflag");
            return (Criteria) this;
        }

        public Criteria andExtendflagNotIn(List<String> values) {
            addCriterion("EXTENDFLAG not in", values, "extendflag");
            return (Criteria) this;
        }

        public Criteria andExtendflagBetween(String value1, String value2) {
            addCriterion("EXTENDFLAG between", value1, value2, "extendflag");
            return (Criteria) this;
        }

        public Criteria andExtendflagNotBetween(String value1, String value2) {
            addCriterion("EXTENDFLAG not between", value1, value2, "extendflag");
            return (Criteria) this;
        }

        public Criteria andStateflagIsNull() {
            addCriterion("STATEFLAG is null");
            return (Criteria) this;
        }

        public Criteria andStateflagIsNotNull() {
            addCriterion("STATEFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andStateflagEqualTo(String value) {
            addCriterion("STATEFLAG =", value, "stateflag");
            return (Criteria) this;
        }

        public Criteria andStateflagNotEqualTo(String value) {
            addCriterion("STATEFLAG <>", value, "stateflag");
            return (Criteria) this;
        }

        public Criteria andStateflagGreaterThan(String value) {
            addCriterion("STATEFLAG >", value, "stateflag");
            return (Criteria) this;
        }

        public Criteria andStateflagGreaterThanOrEqualTo(String value) {
            addCriterion("STATEFLAG >=", value, "stateflag");
            return (Criteria) this;
        }

        public Criteria andStateflagLessThan(String value) {
            addCriterion("STATEFLAG <", value, "stateflag");
            return (Criteria) this;
        }

        public Criteria andStateflagLessThanOrEqualTo(String value) {
            addCriterion("STATEFLAG <=", value, "stateflag");
            return (Criteria) this;
        }

        public Criteria andStateflagLike(String value) {
            addCriterion("STATEFLAG like", value, "stateflag");
            return (Criteria) this;
        }

        public Criteria andStateflagNotLike(String value) {
            addCriterion("STATEFLAG not like", value, "stateflag");
            return (Criteria) this;
        }

        public Criteria andStateflagIn(List<String> values) {
            addCriterion("STATEFLAG in", values, "stateflag");
            return (Criteria) this;
        }

        public Criteria andStateflagNotIn(List<String> values) {
            addCriterion("STATEFLAG not in", values, "stateflag");
            return (Criteria) this;
        }

        public Criteria andStateflagBetween(String value1, String value2) {
            addCriterion("STATEFLAG between", value1, value2, "stateflag");
            return (Criteria) this;
        }

        public Criteria andStateflagNotBetween(String value1, String value2) {
            addCriterion("STATEFLAG not between", value1, value2, "stateflag");
            return (Criteria) this;
        }

        public Criteria andFlagIsNull() {
            addCriterion("FLAG is null");
            return (Criteria) this;
        }

        public Criteria andFlagIsNotNull() {
            addCriterion("FLAG is not null");
            return (Criteria) this;
        }

        public Criteria andFlagEqualTo(String value) {
            addCriterion("FLAG =", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotEqualTo(String value) {
            addCriterion("FLAG <>", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThan(String value) {
            addCriterion("FLAG >", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThanOrEqualTo(String value) {
            addCriterion("FLAG >=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThan(String value) {
            addCriterion("FLAG <", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThanOrEqualTo(String value) {
            addCriterion("FLAG <=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLike(String value) {
            addCriterion("FLAG like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotLike(String value) {
            addCriterion("FLAG not like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagIn(List<String> values) {
            addCriterion("FLAG in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotIn(List<String> values) {
            addCriterion("FLAG not in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagBetween(String value1, String value2) {
            addCriterion("FLAG between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotBetween(String value1, String value2) {
            addCriterion("FLAG not between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeIsNull() {
            addCriterion("UPDATERCODE is null");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeIsNotNull() {
            addCriterion("UPDATERCODE is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeEqualTo(String value) {
            addCriterion("UPDATERCODE =", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeNotEqualTo(String value) {
            addCriterion("UPDATERCODE <>", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeGreaterThan(String value) {
            addCriterion("UPDATERCODE >", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeGreaterThanOrEqualTo(String value) {
            addCriterion("UPDATERCODE >=", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeLessThan(String value) {
            addCriterion("UPDATERCODE <", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeLessThanOrEqualTo(String value) {
            addCriterion("UPDATERCODE <=", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeLike(String value) {
            addCriterion("UPDATERCODE like", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeNotLike(String value) {
            addCriterion("UPDATERCODE not like", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeIn(List<String> values) {
            addCriterion("UPDATERCODE in", values, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeNotIn(List<String> values) {
            addCriterion("UPDATERCODE not in", values, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeBetween(String value1, String value2) {
            addCriterion("UPDATERCODE between", value1, value2, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeNotBetween(String value1, String value2) {
            addCriterion("UPDATERCODE not between", value1, value2, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatertimeIsNull() {
            addCriterion("UPDATERTIME is null");
            return (Criteria) this;
        }

        public Criteria andUpdatertimeIsNotNull() {
            addCriterion("UPDATERTIME is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatertimeEqualTo(Date value) {
            addCriterion("UPDATERTIME =", value, "updatertime");
            return (Criteria) this;
        }

        public Criteria andUpdatertimeNotEqualTo(Date value) {
            addCriterion("UPDATERTIME <>", value, "updatertime");
            return (Criteria) this;
        }

        public Criteria andUpdatertimeGreaterThan(Date value) {
            addCriterion("UPDATERTIME >", value, "updatertime");
            return (Criteria) this;
        }

        public Criteria andUpdatertimeGreaterThanOrEqualTo(Date value) {
            addCriterion("UPDATERTIME >=", value, "updatertime");
            return (Criteria) this;
        }

        public Criteria andUpdatertimeLessThan(Date value) {
            addCriterion("UPDATERTIME <", value, "updatertime");
            return (Criteria) this;
        }

        public Criteria andUpdatertimeLessThanOrEqualTo(Date value) {
            addCriterion("UPDATERTIME <=", value, "updatertime");
            return (Criteria) this;
        }

        public Criteria andUpdatertimeIn(List<Date> values) {
            addCriterion("UPDATERTIME in", values, "updatertime");
            return (Criteria) this;
        }

        public Criteria andUpdatertimeNotIn(List<Date> values) {
            addCriterion("UPDATERTIME not in", values, "updatertime");
            return (Criteria) this;
        }

        public Criteria andUpdatertimeBetween(Date value1, Date value2) {
            addCriterion("UPDATERTIME between", value1, value2, "updatertime");
            return (Criteria) this;
        }

        public Criteria andUpdatertimeNotBetween(Date value1, Date value2) {
            addCriterion("UPDATERTIME not between", value1, value2, "updatertime");
            return (Criteria) this;
        }

        public Criteria andRemarksIsNull() {
            addCriterion("REMARKS is null");
            return (Criteria) this;
        }

        public Criteria andRemarksIsNotNull() {
            addCriterion("REMARKS is not null");
            return (Criteria) this;
        }

        public Criteria andRemarksEqualTo(String value) {
            addCriterion("REMARKS =", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotEqualTo(String value) {
            addCriterion("REMARKS <>", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksGreaterThan(String value) {
            addCriterion("REMARKS >", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksGreaterThanOrEqualTo(String value) {
            addCriterion("REMARKS >=", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksLessThan(String value) {
            addCriterion("REMARKS <", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksLessThanOrEqualTo(String value) {
            addCriterion("REMARKS <=", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksLike(String value) {
            addCriterion("REMARKS like", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotLike(String value) {
            addCriterion("REMARKS not like", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksIn(List<String> values) {
            addCriterion("REMARKS in", values, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotIn(List<String> values) {
            addCriterion("REMARKS not in", values, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksBetween(String value1, String value2) {
            addCriterion("REMARKS between", value1, value2, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotBetween(String value1, String value2) {
            addCriterion("REMARKS not between", value1, value2, "remarks");
            return (Criteria) this;
        }

        public Criteria andCreatedateIsNull() {
            addCriterion("CREATEDATE is null");
            return (Criteria) this;
        }

        public Criteria andCreatedateIsNotNull() {
            addCriterion("CREATEDATE is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedateEqualTo(Date value) {
            addCriterion("CREATEDATE =", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateNotEqualTo(Date value) {
            addCriterion("CREATEDATE <>", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateGreaterThan(Date value) {
            addCriterion("CREATEDATE >", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("CREATEDATE >=", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateLessThan(Date value) {
            addCriterion("CREATEDATE <", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateLessThanOrEqualTo(Date value) {
            addCriterion("CREATEDATE <=", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateIn(List<Date> values) {
            addCriterion("CREATEDATE in", values, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateNotIn(List<Date> values) {
            addCriterion("CREATEDATE not in", values, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateBetween(Date value1, Date value2) {
            addCriterion("CREATEDATE between", value1, value2, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateNotBetween(Date value1, Date value2) {
            addCriterion("CREATEDATE not between", value1, value2, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatercodeIsNull() {
            addCriterion("CREATERCODE is null");
            return (Criteria) this;
        }

        public Criteria andCreatercodeIsNotNull() {
            addCriterion("CREATERCODE is not null");
            return (Criteria) this;
        }

        public Criteria andCreatercodeEqualTo(String value) {
            addCriterion("CREATERCODE =", value, "creatercode");
            return (Criteria) this;
        }

        public Criteria andCreatercodeNotEqualTo(String value) {
            addCriterion("CREATERCODE <>", value, "creatercode");
            return (Criteria) this;
        }

        public Criteria andCreatercodeGreaterThan(String value) {
            addCriterion("CREATERCODE >", value, "creatercode");
            return (Criteria) this;
        }

        public Criteria andCreatercodeGreaterThanOrEqualTo(String value) {
            addCriterion("CREATERCODE >=", value, "creatercode");
            return (Criteria) this;
        }

        public Criteria andCreatercodeLessThan(String value) {
            addCriterion("CREATERCODE <", value, "creatercode");
            return (Criteria) this;
        }

        public Criteria andCreatercodeLessThanOrEqualTo(String value) {
            addCriterion("CREATERCODE <=", value, "creatercode");
            return (Criteria) this;
        }

        public Criteria andCreatercodeLike(String value) {
            addCriterion("CREATERCODE like", value, "creatercode");
            return (Criteria) this;
        }

        public Criteria andCreatercodeNotLike(String value) {
            addCriterion("CREATERCODE not like", value, "creatercode");
            return (Criteria) this;
        }

        public Criteria andCreatercodeIn(List<String> values) {
            addCriterion("CREATERCODE in", values, "creatercode");
            return (Criteria) this;
        }

        public Criteria andCreatercodeNotIn(List<String> values) {
            addCriterion("CREATERCODE not in", values, "creatercode");
            return (Criteria) this;
        }

        public Criteria andCreatercodeBetween(String value1, String value2) {
            addCriterion("CREATERCODE between", value1, value2, "creatercode");
            return (Criteria) this;
        }

        public Criteria andCreatercodeNotBetween(String value1, String value2) {
            addCriterion("CREATERCODE not between", value1, value2, "creatercode");
            return (Criteria) this;
        }

        public Criteria andFhxtreatyremarksIsNull() {
            addCriterion("FHXTREATYREMARKS is null");
            return (Criteria) this;
        }

        public Criteria andFhxtreatyremarksIsNotNull() {
            addCriterion("FHXTREATYREMARKS is not null");
            return (Criteria) this;
        }

        public Criteria andFhxtreatyremarksEqualTo(String value) {
            addCriterion("FHXTREATYREMARKS =", value, "fhxtreatyremarks");
            return (Criteria) this;
        }

        public Criteria andFhxtreatyremarksNotEqualTo(String value) {
            addCriterion("FHXTREATYREMARKS <>", value, "fhxtreatyremarks");
            return (Criteria) this;
        }

        public Criteria andFhxtreatyremarksGreaterThan(String value) {
            addCriterion("FHXTREATYREMARKS >", value, "fhxtreatyremarks");
            return (Criteria) this;
        }

        public Criteria andFhxtreatyremarksGreaterThanOrEqualTo(String value) {
            addCriterion("FHXTREATYREMARKS >=", value, "fhxtreatyremarks");
            return (Criteria) this;
        }

        public Criteria andFhxtreatyremarksLessThan(String value) {
            addCriterion("FHXTREATYREMARKS <", value, "fhxtreatyremarks");
            return (Criteria) this;
        }

        public Criteria andFhxtreatyremarksLessThanOrEqualTo(String value) {
            addCriterion("FHXTREATYREMARKS <=", value, "fhxtreatyremarks");
            return (Criteria) this;
        }

        public Criteria andFhxtreatyremarksLike(String value) {
            addCriterion("FHXTREATYREMARKS like", value, "fhxtreatyremarks");
            return (Criteria) this;
        }

        public Criteria andFhxtreatyremarksNotLike(String value) {
            addCriterion("FHXTREATYREMARKS not like", value, "fhxtreatyremarks");
            return (Criteria) this;
        }

        public Criteria andFhxtreatyremarksIn(List<String> values) {
            addCriterion("FHXTREATYREMARKS in", values, "fhxtreatyremarks");
            return (Criteria) this;
        }

        public Criteria andFhxtreatyremarksNotIn(List<String> values) {
            addCriterion("FHXTREATYREMARKS not in", values, "fhxtreatyremarks");
            return (Criteria) this;
        }

        public Criteria andFhxtreatyremarksBetween(String value1, String value2) {
            addCriterion("FHXTREATYREMARKS between", value1, value2, "fhxtreatyremarks");
            return (Criteria) this;
        }

        public Criteria andFhxtreatyremarksNotBetween(String value1, String value2) {
            addCriterion("FHXTREATYREMARKS not between", value1, value2, "fhxtreatyremarks");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}