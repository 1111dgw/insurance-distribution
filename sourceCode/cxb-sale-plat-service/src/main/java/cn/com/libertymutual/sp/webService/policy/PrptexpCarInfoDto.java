
package cn.com.libertymutual.sp.webService.policy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for prptexpCarInfoDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prptexpCarInfoDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="acceptLicenseDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="acceptLicenseDate1" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="acceptLicenseDate2" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ciPayingTimes1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ciPayingTimes2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="driverName1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="driverName2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="educationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="illegalRecords" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="illegalRecords2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="insuredName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="language" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="occupation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prfsnalDrvFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="proposalNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="riskCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="scPayingTimes1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="scPayingTimes2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serialno" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="specialFixFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="threeYearVehicle1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="threeYearVehicle2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prptexpCarInfoDto", propOrder = {
    "acceptLicenseDate",
    "acceptLicenseDate1",
    "acceptLicenseDate2",
    "ciPayingTimes1",
    "ciPayingTimes2",
    "driverName1",
    "driverName2",
    "educationCode",
    "illegalRecords",
    "illegalRecords2",
    "insuredName",
    "language",
    "occupation",
    "prfsnalDrvFlag",
    "proposalNo",
    "riskCode",
    "scPayingTimes1",
    "scPayingTimes2",
    "serialno",
    "specialFixFlag",
    "threeYearVehicle1",
    "threeYearVehicle2"
})
public class PrptexpCarInfoDto implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar acceptLicenseDate;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar acceptLicenseDate1;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar acceptLicenseDate2;
    protected String ciPayingTimes1;
    protected String ciPayingTimes2;
    protected String driverName1;
    protected String driverName2;
    protected String educationCode;
    protected String illegalRecords;
    protected String illegalRecords2;
    protected String insuredName;
    protected String language;
    protected String occupation;
    protected String prfsnalDrvFlag;
    protected String proposalNo;
    protected String riskCode;
    protected String scPayingTimes1;
    protected String scPayingTimes2;
    protected double serialno;
    protected String specialFixFlag;
    protected String threeYearVehicle1;
    protected String threeYearVehicle2;

    /**
     * Gets the value of the acceptLicenseDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAcceptLicenseDate() {
        return acceptLicenseDate;
    }

    /**
     * Sets the value of the acceptLicenseDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAcceptLicenseDate(XMLGregorianCalendar value) {
        this.acceptLicenseDate = value;
    }

    /**
     * Gets the value of the acceptLicenseDate1 property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAcceptLicenseDate1() {
        return acceptLicenseDate1;
    }

    /**
     * Sets the value of the acceptLicenseDate1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAcceptLicenseDate1(XMLGregorianCalendar value) {
        this.acceptLicenseDate1 = value;
    }

    /**
     * Gets the value of the acceptLicenseDate2 property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAcceptLicenseDate2() {
        return acceptLicenseDate2;
    }

    /**
     * Sets the value of the acceptLicenseDate2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAcceptLicenseDate2(XMLGregorianCalendar value) {
        this.acceptLicenseDate2 = value;
    }

    /**
     * Gets the value of the ciPayingTimes1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiPayingTimes1() {
        return ciPayingTimes1;
    }

    /**
     * Sets the value of the ciPayingTimes1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiPayingTimes1(String value) {
        this.ciPayingTimes1 = value;
    }

    /**
     * Gets the value of the ciPayingTimes2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiPayingTimes2() {
        return ciPayingTimes2;
    }

    /**
     * Sets the value of the ciPayingTimes2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiPayingTimes2(String value) {
        this.ciPayingTimes2 = value;
    }

    /**
     * Gets the value of the driverName1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverName1() {
        return driverName1;
    }

    /**
     * Sets the value of the driverName1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverName1(String value) {
        this.driverName1 = value;
    }

    /**
     * Gets the value of the driverName2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverName2() {
        return driverName2;
    }

    /**
     * Sets the value of the driverName2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverName2(String value) {
        this.driverName2 = value;
    }

    /**
     * Gets the value of the educationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEducationCode() {
        return educationCode;
    }

    /**
     * Sets the value of the educationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEducationCode(String value) {
        this.educationCode = value;
    }

    /**
     * Gets the value of the illegalRecords property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIllegalRecords() {
        return illegalRecords;
    }

    /**
     * Sets the value of the illegalRecords property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIllegalRecords(String value) {
        this.illegalRecords = value;
    }

    /**
     * Gets the value of the illegalRecords2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIllegalRecords2() {
        return illegalRecords2;
    }

    /**
     * Sets the value of the illegalRecords2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIllegalRecords2(String value) {
        this.illegalRecords2 = value;
    }

    /**
     * Gets the value of the insuredName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuredName() {
        return insuredName;
    }

    /**
     * Sets the value of the insuredName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuredName(String value) {
        this.insuredName = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Gets the value of the occupation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOccupation() {
        return occupation;
    }

    /**
     * Sets the value of the occupation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOccupation(String value) {
        this.occupation = value;
    }

    /**
     * Gets the value of the prfsnalDrvFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrfsnalDrvFlag() {
        return prfsnalDrvFlag;
    }

    /**
     * Sets the value of the prfsnalDrvFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrfsnalDrvFlag(String value) {
        this.prfsnalDrvFlag = value;
    }

    /**
     * Gets the value of the proposalNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProposalNo() {
        return proposalNo;
    }

    /**
     * Sets the value of the proposalNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProposalNo(String value) {
        this.proposalNo = value;
    }

    /**
     * Gets the value of the riskCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiskCode() {
        return riskCode;
    }

    /**
     * Sets the value of the riskCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskCode(String value) {
        this.riskCode = value;
    }

    /**
     * Gets the value of the scPayingTimes1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScPayingTimes1() {
        return scPayingTimes1;
    }

    /**
     * Sets the value of the scPayingTimes1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScPayingTimes1(String value) {
        this.scPayingTimes1 = value;
    }

    /**
     * Gets the value of the scPayingTimes2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScPayingTimes2() {
        return scPayingTimes2;
    }

    /**
     * Sets the value of the scPayingTimes2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScPayingTimes2(String value) {
        this.scPayingTimes2 = value;
    }

    /**
     * Gets the value of the serialno property.
     * 
     */
    public double getSerialno() {
        return serialno;
    }

    /**
     * Sets the value of the serialno property.
     * 
     */
    public void setSerialno(double value) {
        this.serialno = value;
    }

    /**
     * Gets the value of the specialFixFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecialFixFlag() {
        return specialFixFlag;
    }

    /**
     * Sets the value of the specialFixFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecialFixFlag(String value) {
        this.specialFixFlag = value;
    }

    /**
     * Gets the value of the threeYearVehicle1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThreeYearVehicle1() {
        return threeYearVehicle1;
    }

    /**
     * Sets the value of the threeYearVehicle1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThreeYearVehicle1(String value) {
        this.threeYearVehicle1 = value;
    }

    /**
     * Gets the value of the threeYearVehicle2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThreeYearVehicle2() {
        return threeYearVehicle2;
    }

    /**
     * Sets the value of the threeYearVehicle2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThreeYearVehicle2(String value) {
        this.threeYearVehicle2 = value;
    }

}
