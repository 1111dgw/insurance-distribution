package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.com.libertymutual.production.model.nomorcldatasource.Fhxsection;
import cn.com.libertymutual.production.model.nomorcldatasource.FhxsectionExample;
import cn.com.libertymutual.production.model.nomorcldatasource.FhxsectionKey;
@Mapper
public interface FhxsectionMapper {
    int countByExample(FhxsectionExample example);

    int deleteByExample(FhxsectionExample example);

    int deleteByPrimaryKey(FhxsectionKey key);

    int insert(Fhxsection record);

    int insertSelective(Fhxsection record);

    List<Fhxsection> selectByExample(FhxsectionExample example);

    Fhxsection selectByPrimaryKey(FhxsectionKey key);

    int updateByExampleSelective(@Param("record") Fhxsection record, @Param("example") FhxsectionExample example);

    int updateByExample(@Param("record") Fhxsection record, @Param("example") FhxsectionExample example);

    int updateByPrimaryKeySelective(Fhxsection record);

    int updateByPrimaryKey(Fhxsection record);
}