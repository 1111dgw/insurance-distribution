package cn.com.libertymutual.sp.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import cn.com.libertymutual.core.util.Current;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpSaveData;
import cn.com.libertymutual.sp.service.api.InsureService;
import cn.com.libertymutual.sp.service.api.SaveDataService;

@RestController
@RequestMapping("/nol/insure")
public class InsureController {

	private Logger log = LoggerFactory.getLogger(getClass());

	/** 上传文件同一目录 */
	@Value("${file.transfer.uploadImagesPath}")
	private String uploadImagesPath;

	@Autowired
	private InsureService insureService;

	@Autowired
	private SaveDataService saveDataService;

	@RequestMapping(value = "/insureUpload")
	public ServiceResult reportClaims(HttpServletRequest request, HttpServletResponse response, String imgFileName, List<MultipartFile> imgFiles,
			String proposalNo) {
		return insureService.uploadFfle(request, response, imgFileName, imgFiles, proposalNo);
	}

	/**
	 * 查询输入配置
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/getConfigData")
	public ServiceResult getConfigData(String id) {
		return insureService.getConfigData(id);
	}

	/**
	 * 获得所有好友信息
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/findAllFriend")
	public ServiceResult getFindAllFriend(String type, String sort, String serch) {
		return insureService.getFindAllFriend(Current.userCode.get(), type, sort, serch);
	}

	/**
	 * 保存信息
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/saveData")
	public ServiceResult saveData(@RequestBody TbSpSaveData saveData) {
		return saveDataService.createSaveData(saveData);
	}

	/**
	 * 查找保存信息 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/findSaveData")
	public ServiceResult findData(Integer pageNumber, Integer pageSize, String riskCode) {
		return saveDataService.findUserSaveData(pageNumber, pageSize, Current.userCode.get(), riskCode);
	}

	/**
	 * 查找保存信息 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/findSaveDataId")
	public ServiceResult findSaveDataId(Integer id) {
		return saveDataService.findSaveDataId(id);
	}

}
