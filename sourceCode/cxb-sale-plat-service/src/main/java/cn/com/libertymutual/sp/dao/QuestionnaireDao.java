package cn.com.libertymutual.sp.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import cn.com.libertymutual.sp.bean.TbSpQuestionnaire;

@Repository
public interface QuestionnaireDao extends PagingAndSortingRepository<TbSpQuestionnaire, Integer>, JpaSpecificationExecutor<TbSpQuestionnaire> {

	@Query("from TbSpQuestionnaire q where q.questId = ?1")
	TbSpQuestionnaire findByQuestId(String id);
}
