package cn.com.libertymutual.sp.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;

@ApiModel
@Entity
@Table(name = "tb_sp_scorelog_out")
public class TbSpScoreLogOut implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7448720751168778314L;

	private Integer id;
	private String changeType;
	private Double reChangeScore;
	private Double acChangeScore;
	private String granter;
	private String policyNo;
	private String reasonNo;
	private String reason;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date changeTime;
	private String status;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date effictiveDate;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date invalidDate;
	private String userCode;
	private String telephone;
	private String openId;
	private String remark;
	private String versionNo;

	private TbSpScoreLogIn scoreLogIn;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "change_type")
	public String getChangeType() {
		return changeType;
	}

	public void setChangeType(String changeType) {
		this.changeType = changeType;
	}

	@Column(name = "re_changescore")
	public Double getReChangeScore() {
		return reChangeScore;
	}

	public void setReChangeScore(Double reChangeScore) {
		this.reChangeScore = reChangeScore;
	}

	@Column(name = "ac_changescore")
	public Double getAcChangeScore() {
		return acChangeScore;
	}

	public void setAcChangeScore(Double acChangeScore) {
		this.acChangeScore = acChangeScore;
	}

	@Column(name = "granter")
	public String getGranter() {
		return granter;
	}

	public void setGranter(String granter) {
		this.granter = granter;
	}

	@Column(name = "policy_no")
	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	@Column(name = "reason_no")
	public String getReasonNo() {
		return reasonNo;
	}

	public void setReasonNo(String reasonNo) {
		this.reasonNo = reasonNo;
	}

	@Column(name = "reason")
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Column(name = "change_time")
	public Date getChangeTime() {
		return changeTime;
	}

	public void setChangeTime(Date changeTime) {
		this.changeTime = changeTime;
	}

	@Column(name = "status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "effictive_date")
	public Date getEffictiveDate() {
		return effictiveDate;
	}

	public void setEffictiveDate(Date effictiveDate) {
		this.effictiveDate = effictiveDate;
	}

	@Column(name = "invalid_date")
	public Date getInvalidDate() {
		return invalidDate;
	}

	public void setInvalidDate(Date invalidDate) {
		this.invalidDate = invalidDate;
	}

	@Column(name = "user_code")
	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	@Column(name = "telephone")
	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	@Column(name = "openid")
	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	@Column(name = "remark")
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "versionNo")
	public String getVersionNo() {
		return versionNo;
	}

	public void setVersionNo(String versionNo) {
		this.versionNo = versionNo;
	}

	@Transient
	public TbSpScoreLogIn getScoreLogIn() {
		return scoreLogIn;
	}

	@Transient
	public void setScoreLogIn(TbSpScoreLogIn scoreLogIn) {
		this.scoreLogIn = scoreLogIn;
	}

}
