package cn.com.libertymutual.wx.message.requestdto;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import cn.com.libertymutual.wx.message.BaseMessage;


public class RequestBaseMessage extends BaseMessage {
	@XStreamAlias("MsgId")
	private long msgId;

	public long getMsgId() {
		return this.msgId;
	}

	public void setMsgId(long msgId) {
		this.msgId = msgId;
	}
}
