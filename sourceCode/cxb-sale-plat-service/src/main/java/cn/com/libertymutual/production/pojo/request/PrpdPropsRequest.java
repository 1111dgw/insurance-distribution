package cn.com.libertymutual.production.pojo.request;

public class PrpdPropsRequest extends Request {

 	private String propscode;

    private String propscname;

    private String propsename;

	public String getPropscode() {
		return propscode;
	}

	public void setPropscode(String propscode) {
		this.propscode = propscode;
	}

	public String getPropscname() {
		return propscname;
	}

	public void setPropscname(String propscname) {
		this.propscname = propscname;
	}

	public String getPropsename() {
		return propsename;
	}

	public void setPropsename(String propsename) {
		this.propsename = propsename;
	}
	    
}
