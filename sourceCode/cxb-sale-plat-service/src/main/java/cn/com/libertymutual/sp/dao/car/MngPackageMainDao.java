package cn.com.libertymutual.sp.dao.car;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.com.libertymutual.sp.bean.car.MngPackageMain;

public interface MngPackageMainDao extends PagingAndSortingRepository<MngPackageMain, Integer>, JpaSpecificationExecutor<MngPackageMain> {
	@Query("from MngPackageMain where pkBranchNo = ?1 and packRisk = ?2")
	List<MngPackageMain> findRiskBranch(String pkBranchNo, String packRisk);
}
