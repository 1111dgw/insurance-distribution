package cn.com.libertymutual.sp.dao.car;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.com.libertymutual.sp.bean.car.MngPackageKind;

public interface MngPackageKindDao extends PagingAndSortingRepository<MngPackageKind, Integer>, JpaSpecificationExecutor<MngPackageKind> {
	@Query("from MngPackageKind where pkId = ?1")
	List<MngPackageKind> findPkId(String pkId);
}
