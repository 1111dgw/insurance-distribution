package cn.com.libertymutual.production.service.impl.business;

import cn.com.libertymutual.production.exception.UserException;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdpolicypay;
import cn.com.libertymutual.production.pojo.request.PrpdPolicyPayRequest;
import cn.com.libertymutual.production.pojo.response.Response;
import cn.com.libertymutual.production.service.api.business.PolicyPayBusinessService;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author steven.li
 * @create 2018/1/31
 */
@Service
@Transactional(rollbackFor = Exception.class, transactionManager = "transactionManagerCoreData")
public class PolicyPayBusinessServiceImpl extends PolicyPayBusinessService {

    @Override
    public Response findPolicyPays(PrpdPolicyPayRequest prpdPolicyPayRequest) {
        Response result = new Response();
        try {
            PageInfo<Prpdpolicypay> pageInfo = prpdPolicyPayService.selectByPage(prpdPolicyPayRequest);
            result.setTotal(pageInfo.getTotal());
            result.setResult(pageInfo.getList());
            result.setSuccess();
        } catch (Exception e) {
            log.error("系统异常错误！", e);
            result.setAppFail();
            result.setResult(e.getMessage());
        }
        return result;
    }

    @Override
    public Response insert(PrpdPolicyPayRequest prpdPolicyPayRequest) throws Exception {
        Response result = new Response();
        try {

            prpdPolicyPayRequest.setComcode(prpdPolicyPayRequest.getComcode().substring(0,2));

            PrpdPolicyPayRequest criteria = new PrpdPolicyPayRequest();
            criteria.setComcode(prpdPolicyPayRequest.getComcode());
            criteria.setRiskcode(prpdPolicyPayRequest.getRiskcode());
            criteria.setAgentcode(prpdPolicyPayRequest.getAgentcode());
            PageInfo<Prpdpolicypay> rs = prpdPolicyPayService.selectByPage(criteria);
            if (!rs.getList().isEmpty()) {
                throw new UserException("该配置项已存在！");
            }


            if ("ALL".equals(prpdPolicyPayRequest.getAgentcode())) {
                Prpdpolicypay where = new Prpdpolicypay();
                where.setComcode(prpdPolicyPayRequest.getComcode());
                where.setRiskcode(prpdPolicyPayRequest.getRiskcode());
                prpdPolicyPayService.delete(where);
            }

            Prpdpolicypay record = new Prpdpolicypay();
            record.setComcode(prpdPolicyPayRequest.getComcode());
            record.setRiskcode(prpdPolicyPayRequest.getRiskcode());
            record.setAgentcode(prpdPolicyPayRequest.getAgentcode());
            record.setIspolicypay(prpdPolicyPayRequest.getIspolicypay());
            prpdPolicyPayService.insert(record);
        } catch (Exception e) {
            log.error("系统异常错误！", e);
            throw e;
        }
        return result;
    }

    @Override
    public Response update(PrpdPolicyPayRequest prpdPolicyPayRequest) throws Exception  {
        Response result = new Response();
        try {
            Prpdpolicypay record = new Prpdpolicypay();
            record.setComcode(prpdPolicyPayRequest.getComcode());
            record.setRiskcode(prpdPolicyPayRequest.getRiskcode());
            record.setAgentcode(prpdPolicyPayRequest.getAgentcode());
            record.setIspolicypay(prpdPolicyPayRequest.getIspolicypay());
            prpdPolicyPayService.update(record);
        } catch (Exception e) {
            log.error("系统异常错误！", e);
            throw e;
        }
        return result;
    }

    @Override
    public Response delete(PrpdPolicyPayRequest prpdPolicyPayRequest) {
        Response result = new Response();
        try {
            Prpdpolicypay criteria = new Prpdpolicypay();
            prpdPolicyPayRequest.getSelectedRows().forEach((data) -> {
                criteria.setComcode(data.getComcode());
                criteria.setRiskcode(data.getRiskcode());
                criteria.setAgentcode(data.getAgentcode());
                prpdPolicyPayService.delete(criteria);
            });
        } catch (Exception e) {
            log.error("系统异常错误！", e);
            throw e;
        }
        return result;
    }
}
