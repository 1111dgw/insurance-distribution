package cn.com.libertymutual.wx.message.responsedto;

import java.util.Calendar;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import cn.com.libertymutual.wx.common.MessageType;

public class MusicMessage extends ResponseBaseMessage {
	@XStreamAlias("MediaId")
	private String mediaId;
	@XStreamAlias("Format")
	private String format;
	@XStreamAlias("Title")
	private String title;
	@XStreamAlias("Description")
	private String description;
	@XStreamAlias("MusicUrl")
	private String musicUrl;
	@XStreamAlias("HQMusicUrl")
	private String hQMusicUrl;
	@XStreamAlias("ThumbMediaId")
	private String thumbMediaId;

	public MusicMessage() {
		setMsgType(MessageType.RESP_MESSAGE_TYPE_MUSIC);
	}

	public MusicMessage(ResponseBaseMessage rbm) {
		long ct = Calendar.getInstance().getTimeInMillis();
		setCreateTime(ct);
		setFromUserName(rbm.getFromUserName());
		setToUserName(rbm.getToUserName());
		setMsgType(MessageType.RESP_MESSAGE_TYPE_MUSIC);
	}

	public String getMediaId() {
		return this.mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getFormat() {
		return this.format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMusicUrl() {
		return this.musicUrl;
	}

	public void setMusicUrl(String musicUrl) {
		this.musicUrl = musicUrl;
	}

	public String gethQMusicUrl() {
		return this.hQMusicUrl;
	}

	public void sethQMusicUrl(String hQMusicUrl) {
		this.hQMusicUrl = hQMusicUrl;
	}

	public String getThumbMediaId() {
		return this.thumbMediaId;
	}

	public void setThumbMediaId(String thumbMediaId) {
		this.thumbMediaId = thumbMediaId;
	}

	@Override
	public String toString() {
		return "MusicMessage [mediaId=" + mediaId + ", format=" + format + ", title=" + title + ", description=" + description + ", musicUrl="
				+ musicUrl + ", hQMusicUrl=" + hQMusicUrl + ", thumbMediaId=" + thumbMediaId + ", getMediaId()=" + getMediaId() + ", getFormat()="
				+ getFormat() + ", getTitle()=" + getTitle() + ", getDescription()=" + getDescription() + ", getMusicUrl()=" + getMusicUrl()
				+ ", gethQMusicUrl()=" + gethQMusicUrl() + ", getThumbMediaId()=" + getThumbMediaId() + ", getToUserName()=" + getToUserName()
				+ ", getFromUserName()=" + getFromUserName() + ", getCreateTime()=" + getCreateTime() + ", getMsgType()=" + getMsgType()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}
}
