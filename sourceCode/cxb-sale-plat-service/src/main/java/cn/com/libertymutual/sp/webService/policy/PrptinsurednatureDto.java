
package cn.com.libertymutual.sp.webService.policy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for prptinsurednatureDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prptinsurednatureDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="age" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="birthday" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dutyLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dutyType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="education" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="familyMonthIncome" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="familySumQuantity" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="flag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="health" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="houseProperty" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="incomeSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="insuredFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jobTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="loacalWorkYears" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="localPoliceStation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="marriage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="occupationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="proposalNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="roomAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="roomPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="roomPostCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="selfMonthIncome" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="serialNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sex" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="spouseBornDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="spouseID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="spouseJobTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="spouseName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="spouseUnit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="spouseUnitPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="stature" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="totalWorkYears" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="unit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="unitAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="unitPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="unitPostCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="unitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="weight" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prptinsurednatureDto", propOrder = {
    "age",
    "birthday",
    "dutyLevel",
    "dutyType",
    "education",
    "familyMonthIncome",
    "familySumQuantity",
    "flag",
    "health",
    "houseProperty",
    "incomeSource",
    "insuredFlag",
    "jobTitle",
    "loacalWorkYears",
    "localPoliceStation",
    "marriage",
    "mobile",
    "occupationCode",
    "proposalNo",
    "roomAddress",
    "roomPhone",
    "roomPostCode",
    "selfMonthIncome",
    "serialNo",
    "sex",
    "spouseBornDate",
    "spouseID",
    "spouseJobTitle",
    "spouseName",
    "spouseUnit",
    "spouseUnitPhone",
    "stature",
    "totalWorkYears",
    "unit",
    "unitAddress",
    "unitPhoneNumber",
    "unitPostCode",
    "unitType",
    "weight"
})
public class PrptinsurednatureDto implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    protected double age;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar birthday;
    protected String dutyLevel;
    protected String dutyType;
    protected String education;
    protected double familyMonthIncome;
    protected double familySumQuantity;
    protected String flag;
    protected String health;
    protected String houseProperty;
    protected String incomeSource;
    protected String insuredFlag;
    protected String jobTitle;
    protected double loacalWorkYears;
    protected String localPoliceStation;
    protected String marriage;
    protected String mobile;
    protected String occupationCode;
    protected String proposalNo;
    protected String roomAddress;
    protected String roomPhone;
    protected String roomPostCode;
    protected double selfMonthIncome;
    protected String serialNo;
    protected String sex;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar spouseBornDate;
    protected String spouseID;
    protected String spouseJobTitle;
    protected String spouseName;
    protected String spouseUnit;
    protected String spouseUnitPhone;
    protected double stature;
    protected double totalWorkYears;
    protected String unit;
    protected String unitAddress;
    protected String unitPhoneNumber;
    protected String unitPostCode;
    protected String unitType;
    protected double weight;

    /**
     * Gets the value of the age property.
     * 
     */
    public double getAge() {
        return age;
    }

    /**
     * Sets the value of the age property.
     * 
     */
    public void setAge(double value) {
        this.age = value;
    }

    /**
     * Gets the value of the birthday property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBirthday() {
        return birthday;
    }

    /**
     * Sets the value of the birthday property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBirthday(XMLGregorianCalendar value) {
        this.birthday = value;
    }

    /**
     * Gets the value of the dutyLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDutyLevel() {
        return dutyLevel;
    }

    /**
     * Sets the value of the dutyLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDutyLevel(String value) {
        this.dutyLevel = value;
    }

    /**
     * Gets the value of the dutyType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDutyType() {
        return dutyType;
    }

    /**
     * Sets the value of the dutyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDutyType(String value) {
        this.dutyType = value;
    }

    /**
     * Gets the value of the education property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEducation() {
        return education;
    }

    /**
     * Sets the value of the education property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEducation(String value) {
        this.education = value;
    }

    /**
     * Gets the value of the familyMonthIncome property.
     * 
     */
    public double getFamilyMonthIncome() {
        return familyMonthIncome;
    }

    /**
     * Sets the value of the familyMonthIncome property.
     * 
     */
    public void setFamilyMonthIncome(double value) {
        this.familyMonthIncome = value;
    }

    /**
     * Gets the value of the familySumQuantity property.
     * 
     */
    public double getFamilySumQuantity() {
        return familySumQuantity;
    }

    /**
     * Sets the value of the familySumQuantity property.
     * 
     */
    public void setFamilySumQuantity(double value) {
        this.familySumQuantity = value;
    }

    /**
     * Gets the value of the flag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlag() {
        return flag;
    }

    /**
     * Sets the value of the flag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlag(String value) {
        this.flag = value;
    }

    /**
     * Gets the value of the health property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHealth() {
        return health;
    }

    /**
     * Sets the value of the health property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHealth(String value) {
        this.health = value;
    }

    /**
     * Gets the value of the houseProperty property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHouseProperty() {
        return houseProperty;
    }

    /**
     * Sets the value of the houseProperty property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHouseProperty(String value) {
        this.houseProperty = value;
    }

    /**
     * Gets the value of the incomeSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIncomeSource() {
        return incomeSource;
    }

    /**
     * Sets the value of the incomeSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIncomeSource(String value) {
        this.incomeSource = value;
    }

    /**
     * Gets the value of the insuredFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuredFlag() {
        return insuredFlag;
    }

    /**
     * Sets the value of the insuredFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuredFlag(String value) {
        this.insuredFlag = value;
    }

    /**
     * Gets the value of the jobTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobTitle() {
        return jobTitle;
    }

    /**
     * Sets the value of the jobTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobTitle(String value) {
        this.jobTitle = value;
    }

    /**
     * Gets the value of the loacalWorkYears property.
     * 
     */
    public double getLoacalWorkYears() {
        return loacalWorkYears;
    }

    /**
     * Sets the value of the loacalWorkYears property.
     * 
     */
    public void setLoacalWorkYears(double value) {
        this.loacalWorkYears = value;
    }

    /**
     * Gets the value of the localPoliceStation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocalPoliceStation() {
        return localPoliceStation;
    }

    /**
     * Sets the value of the localPoliceStation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocalPoliceStation(String value) {
        this.localPoliceStation = value;
    }

    /**
     * Gets the value of the marriage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarriage() {
        return marriage;
    }

    /**
     * Sets the value of the marriage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarriage(String value) {
        this.marriage = value;
    }

    /**
     * Gets the value of the mobile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * Sets the value of the mobile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobile(String value) {
        this.mobile = value;
    }

    /**
     * Gets the value of the occupationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOccupationCode() {
        return occupationCode;
    }

    /**
     * Sets the value of the occupationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOccupationCode(String value) {
        this.occupationCode = value;
    }

    /**
     * Gets the value of the proposalNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProposalNo() {
        return proposalNo;
    }

    /**
     * Sets the value of the proposalNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProposalNo(String value) {
        this.proposalNo = value;
    }

    /**
     * Gets the value of the roomAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoomAddress() {
        return roomAddress;
    }

    /**
     * Sets the value of the roomAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoomAddress(String value) {
        this.roomAddress = value;
    }

    /**
     * Gets the value of the roomPhone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoomPhone() {
        return roomPhone;
    }

    /**
     * Sets the value of the roomPhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoomPhone(String value) {
        this.roomPhone = value;
    }

    /**
     * Gets the value of the roomPostCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoomPostCode() {
        return roomPostCode;
    }

    /**
     * Sets the value of the roomPostCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoomPostCode(String value) {
        this.roomPostCode = value;
    }

    /**
     * Gets the value of the selfMonthIncome property.
     * 
     */
    public double getSelfMonthIncome() {
        return selfMonthIncome;
    }

    /**
     * Sets the value of the selfMonthIncome property.
     * 
     */
    public void setSelfMonthIncome(double value) {
        this.selfMonthIncome = value;
    }

    /**
     * Gets the value of the serialNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerialNo() {
        return serialNo;
    }

    /**
     * Sets the value of the serialNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerialNo(String value) {
        this.serialNo = value;
    }

    /**
     * Gets the value of the sex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSex() {
        return sex;
    }

    /**
     * Sets the value of the sex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSex(String value) {
        this.sex = value;
    }

    /**
     * Gets the value of the spouseBornDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSpouseBornDate() {
        return spouseBornDate;
    }

    /**
     * Sets the value of the spouseBornDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSpouseBornDate(XMLGregorianCalendar value) {
        this.spouseBornDate = value;
    }

    /**
     * Gets the value of the spouseID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpouseID() {
        return spouseID;
    }

    /**
     * Sets the value of the spouseID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpouseID(String value) {
        this.spouseID = value;
    }

    /**
     * Gets the value of the spouseJobTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpouseJobTitle() {
        return spouseJobTitle;
    }

    /**
     * Sets the value of the spouseJobTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpouseJobTitle(String value) {
        this.spouseJobTitle = value;
    }

    /**
     * Gets the value of the spouseName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpouseName() {
        return spouseName;
    }

    /**
     * Sets the value of the spouseName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpouseName(String value) {
        this.spouseName = value;
    }

    /**
     * Gets the value of the spouseUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpouseUnit() {
        return spouseUnit;
    }

    /**
     * Sets the value of the spouseUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpouseUnit(String value) {
        this.spouseUnit = value;
    }

    /**
     * Gets the value of the spouseUnitPhone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpouseUnitPhone() {
        return spouseUnitPhone;
    }

    /**
     * Sets the value of the spouseUnitPhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpouseUnitPhone(String value) {
        this.spouseUnitPhone = value;
    }

    /**
     * Gets the value of the stature property.
     * 
     */
    public double getStature() {
        return stature;
    }

    /**
     * Sets the value of the stature property.
     * 
     */
    public void setStature(double value) {
        this.stature = value;
    }

    /**
     * Gets the value of the totalWorkYears property.
     * 
     */
    public double getTotalWorkYears() {
        return totalWorkYears;
    }

    /**
     * Sets the value of the totalWorkYears property.
     * 
     */
    public void setTotalWorkYears(double value) {
        this.totalWorkYears = value;
    }

    /**
     * Gets the value of the unit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Sets the value of the unit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnit(String value) {
        this.unit = value;
    }

    /**
     * Gets the value of the unitAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitAddress() {
        return unitAddress;
    }

    /**
     * Sets the value of the unitAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitAddress(String value) {
        this.unitAddress = value;
    }

    /**
     * Gets the value of the unitPhoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitPhoneNumber() {
        return unitPhoneNumber;
    }

    /**
     * Sets the value of the unitPhoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitPhoneNumber(String value) {
        this.unitPhoneNumber = value;
    }

    /**
     * Gets the value of the unitPostCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitPostCode() {
        return unitPostCode;
    }

    /**
     * Sets the value of the unitPostCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitPostCode(String value) {
        this.unitPostCode = value;
    }

    /**
     * Gets the value of the unitType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitType() {
        return unitType;
    }

    /**
     * Sets the value of the unitType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitType(String value) {
        this.unitType = value;
    }

    /**
     * Gets the value of the weight property.
     * 
     */
    public double getWeight() {
        return weight;
    }

    /**
     * Sets the value of the weight property.
     * 
     */
    public void setWeight(double value) {
        this.weight = value;
    }

}
