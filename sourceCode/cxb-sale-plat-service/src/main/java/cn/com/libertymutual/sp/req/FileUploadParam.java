package cn.com.libertymutual.sp.req;

import java.io.Serializable;

public class FileUploadParam implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4309975283784190103L;
	private String parampath;
	private String remark;
	public String getParampath() {
		return parampath;
	}

	public void setParampath(String parampath) {
		this.parampath = parampath;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "FileUploadParam [parampath=" + parampath + ", remark=" + remark + "]";
	}

	
}
