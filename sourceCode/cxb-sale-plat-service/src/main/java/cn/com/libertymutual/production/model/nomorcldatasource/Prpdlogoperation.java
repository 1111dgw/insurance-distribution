package cn.com.libertymutual.production.model.nomorcldatasource;

public class Prpdlogoperation {
    private Long logid;

    private String businesskeyvalue;

    private String operationtype;

    private String content;

    public Long getLogid() {
        return logid;
    }

    public void setLogid(Long logid) {
        this.logid = logid;
    }

    public String getBusinesskeyvalue() {
        return businesskeyvalue;
    }

    public void setBusinesskeyvalue(String businesskeyvalue) {
        this.businesskeyvalue = businesskeyvalue == null ? null : businesskeyvalue.trim();
    }

    public String getOperationtype() {
        return operationtype;
    }

    public void setOperationtype(String operationtype) {
        this.operationtype = operationtype == null ? null : operationtype.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
}