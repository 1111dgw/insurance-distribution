package cn.com.libertymutual.sp.dto;


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * PrpCmain entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="PRPCMAIN")

public class PrpCmain  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String policyno;
     private String classcode;
     private String riskcode;
     private String proposalno;
     private String contractno;
     private String policysort;
     private String printno;
     private String businessnature;
     private String language;
     private String policytype;
     private String applicode;
     private String appliname;
     private String appliaddress;
     private String insuredcode;
     private String insuredname;
     private String insuredaddress;
     private Date operatedate;
     private Date startdate;
     private Long starthour;
     private Date enddate;
     private Long endhour;
     private Double purerate;
     private Double disrate;
     private Double discount;
     private String currency;
     private Double sumvalue;
     private Double sumamount;
     private Double sumdiscount;
     private Double sumpremium;
     private Double sumsubprem;
     private Long sumquantity;
     private String judicalcode;
     private String judicalscope;
     private String autotransrenewflag;
     private String arguesolution;
     private String arbitboardname;
     private Long paytimes;
     private Long endorsetimes;
     private Long claimtimes;
     private String makecom;
     private String operatesite;
     private String comcode;
     private String handlercode;
     private String handler1code;
     private String approvercode;
     private String underwritecode;
     private String underwritename;
     private String operatorcode;
     private Date inputdate;
     private Long inputhour;
     private Date underwriteenddate;
     private Date statisticsym;
     private String agentcode;
     private String coinsflag;
     private String reinsflag;
     private String allinsflag;
     private String underwriteflag;
     private String othflag;
     private String flag;
     private Double disrate1;
     private String businessflag;
     private String updatercode;
     private Date updatedate;
     private String updatehour;
     private Date signdate;
     private String shareholderflag;
     private String agreementno;
     private String inquiryno;
     private String paymode;
     private String remark;
     private String visacode;
     private String manualtype;
     private Byte startminute;
     private Byte endminute;
     private String nationflag;
     private String papolicyno;
     private String businesstype;
     private String businesstype1;
     private String originpolicyno;
     private String conveyflag;
     private String riskmark;
     private String uwapprovalno;
     private String custconnflag;
     private Double prepaidrate;
     private Double sumestimatedpremium;
     private Double lowestpremium;
     private Double sumestimatedamount;
     private String openpolicyflag;
     private String currency1;
     private String groupbusinessflag;
     private String groupbusinesscode;
     private String tradingautosflag;
     private String affinitycode;
     private String ispaymentreturnflag;
     private String surveyflag;
     private String agentname;
     private String agreementnamename;
     private String affinityname;
     private String agreementname;
     private String anonymityflag;
     private Double planexchangerate;
     private String riskversion;
     private String comsign;
     private String discountlimit;
     private String payidfrompayment;
     private String addinputflag;
     private String policynatureflag;
     private String clienttype;
     private String handlername;
     private Double agentlosration;
     private String enforceonly;
     private String salernumber;
     private String salername;
     private String polisource;
     private String totalloss;
     private Date firstrecorddate;
     private Date firstunderwritedate;
     private String deliverymode;
     private String ecardtype;
     private String settleflag;
     private String websource;
     private String cartypecode;
     private String carinsurancetype;
     private String acctname;
     private String servicecode;
     private String channeltype;
     private String specialrisk;
     private String customcode;
     private String channelmanagercode;
     private String channelmanagername;
     private String servicename;
     private String channeltypename;
     private Date inputtime;
     private String vericode;
     private String policyprintstyle;
     private String liuflag;
     private String withholdrate;
     private String declaretype;
     private Double pureriskpremium;
     private String personalagentlicenseno;
     private String intermediarylicenseno;
     private String endorwhole;
     private String countycode;
     private Date layoffdate;
     private Date estimatedresumedate;
     private Date actualresumedate;
     private Date estimatedenddate;
     private Date actualenddate;
     private String pureriskpremiumflag;
     private Double referencepurerisk;
     private String agentbatch;
     private String seefeeflag;
     private String kindmaintype;
     private Double sumextaxpremium;
     private Double sumvalueaddedtax;
     private String trdsalescode;



    // Constructors

    /** default constructor */
    public PrpCmain() {
    }

	/** minimal constructor */
    public PrpCmain(String policyno, String classcode, String riskcode, String businessnature, String makecom, String comcode, String handlercode, String operatorcode) {
        this.policyno = policyno;
        this.classcode = classcode;
        this.riskcode = riskcode;
        this.businessnature = businessnature;
        this.makecom = makecom;
        this.comcode = comcode;
        this.handlercode = handlercode;
        this.operatorcode = operatorcode;
    }
    
    /** full constructor */
    public PrpCmain(String policyno, String classcode, String riskcode, String proposalno, String contractno, String policysort, String printno, String businessnature, String language, String policytype, String applicode, String appliname, String appliaddress, String insuredcode, String insuredname, String insuredaddress, Date operatedate, Date startdate, Long starthour, Date enddate, Long endhour, Double purerate, Double disrate, Double discount, String currency, Double sumvalue, Double sumamount, Double sumdiscount, Double sumpremium, Double sumsubprem, Long sumquantity, String judicalcode, String judicalscope, String autotransrenewflag, String arguesolution, String arbitboardname, Long paytimes, Long endorsetimes, Long claimtimes, String makecom, String operatesite, String comcode, String handlercode, String handler1code, String approvercode, String underwritecode, String underwritename, String operatorcode, Date inputdate, Long inputhour, Date underwriteenddate, Date statisticsym, String agentcode, String coinsflag, String reinsflag, String allinsflag, String underwriteflag, String othflag, String flag, Double disrate1, String businessflag, String updatercode, Date updatedate, String updatehour, Date signdate, String shareholderflag, String agreementno, String inquiryno, String paymode, String remark, String visacode, String manualtype, Byte startminute, Byte endminute, String nationflag, String papolicyno, String businesstype, String businesstype1, String originpolicyno, String conveyflag, String riskmark, String uwapprovalno, String custconnflag, Double prepaidrate, Double sumestimatedpremium, Double lowestpremium, Double sumestimatedamount, String openpolicyflag, String currency1, String groupbusinessflag, String groupbusinesscode, String tradingautosflag, String affinitycode, String ispaymentreturnflag, String surveyflag, String agentname, String agreementnamename, String affinityname, String agreementname, String anonymityflag, Double planexchangerate, String riskversion, String comsign, String discountlimit, String payidfrompayment, String addinputflag, String policynatureflag, String clienttype, String handlername, Double agentlosration, String enforceonly, String salernumber, String salername, String polisource, String totalloss, Date firstrecorddate, Date firstunderwritedate, String deliverymode, String ecardtype, String settleflag, String websource, String cartypecode, String carinsurancetype, String acctname, String servicecode, String channeltype, String specialrisk, String customcode, String channelmanagercode, String channelmanagername, String servicename, String channeltypename, Date inputtime, String vericode, String policyprintstyle, String liuflag, String withholdrate, String declaretype, Double pureriskpremium, String personalagentlicenseno, String intermediarylicenseno, String endorwhole, String countycode, Date layoffdate, Date estimatedresumedate, Date actualresumedate, Date estimatedenddate, Date actualenddate, String pureriskpremiumflag, Double referencepurerisk, String agentbatch, String seefeeflag, String kindmaintype, Double sumextaxpremium, Double sumvalueaddedtax, String trdsalescode) {
        this.policyno = policyno;
        this.classcode = classcode;
        this.riskcode = riskcode;
        this.proposalno = proposalno;
        this.contractno = contractno;
        this.policysort = policysort;
        this.printno = printno;
        this.businessnature = businessnature;
        this.language = language;
        this.policytype = policytype;
        this.applicode = applicode;
        this.appliname = appliname;
        this.appliaddress = appliaddress;
        this.insuredcode = insuredcode;
        this.insuredname = insuredname;
        this.insuredaddress = insuredaddress;
        this.operatedate = operatedate;
        this.startdate = startdate;
        this.starthour = starthour;
        this.enddate = enddate;
        this.endhour = endhour;
        this.purerate = purerate;
        this.disrate = disrate;
        this.discount = discount;
        this.currency = currency;
        this.sumvalue = sumvalue;
        this.sumamount = sumamount;
        this.sumdiscount = sumdiscount;
        this.sumpremium = sumpremium;
        this.sumsubprem = sumsubprem;
        this.sumquantity = sumquantity;
        this.judicalcode = judicalcode;
        this.judicalscope = judicalscope;
        this.autotransrenewflag = autotransrenewflag;
        this.arguesolution = arguesolution;
        this.arbitboardname = arbitboardname;
        this.paytimes = paytimes;
        this.endorsetimes = endorsetimes;
        this.claimtimes = claimtimes;
        this.makecom = makecom;
        this.operatesite = operatesite;
        this.comcode = comcode;
        this.handlercode = handlercode;
        this.handler1code = handler1code;
        this.approvercode = approvercode;
        this.underwritecode = underwritecode;
        this.underwritename = underwritename;
        this.operatorcode = operatorcode;
        this.inputdate = inputdate;
        this.inputhour = inputhour;
        this.underwriteenddate = underwriteenddate;
        this.statisticsym = statisticsym;
        this.agentcode = agentcode;
        this.coinsflag = coinsflag;
        this.reinsflag = reinsflag;
        this.allinsflag = allinsflag;
        this.underwriteflag = underwriteflag;
        this.othflag = othflag;
        this.flag = flag;
        this.disrate1 = disrate1;
        this.businessflag = businessflag;
        this.updatercode = updatercode;
        this.updatedate = updatedate;
        this.updatehour = updatehour;
        this.signdate = signdate;
        this.shareholderflag = shareholderflag;
        this.agreementno = agreementno;
        this.inquiryno = inquiryno;
        this.paymode = paymode;
        this.remark = remark;
        this.visacode = visacode;
        this.manualtype = manualtype;
        this.startminute = startminute;
        this.endminute = endminute;
        this.nationflag = nationflag;
        this.papolicyno = papolicyno;
        this.businesstype = businesstype;
        this.businesstype1 = businesstype1;
        this.originpolicyno = originpolicyno;
        this.conveyflag = conveyflag;
        this.riskmark = riskmark;
        this.uwapprovalno = uwapprovalno;
        this.custconnflag = custconnflag;
        this.prepaidrate = prepaidrate;
        this.sumestimatedpremium = sumestimatedpremium;
        this.lowestpremium = lowestpremium;
        this.sumestimatedamount = sumestimatedamount;
        this.openpolicyflag = openpolicyflag;
        this.currency1 = currency1;
        this.groupbusinessflag = groupbusinessflag;
        this.groupbusinesscode = groupbusinesscode;
        this.tradingautosflag = tradingautosflag;
        this.affinitycode = affinitycode;
        this.ispaymentreturnflag = ispaymentreturnflag;
        this.surveyflag = surveyflag;
        this.agentname = agentname;
        this.agreementnamename = agreementnamename;
        this.affinityname = affinityname;
        this.agreementname = agreementname;
        this.anonymityflag = anonymityflag;
        this.planexchangerate = planexchangerate;
        this.riskversion = riskversion;
        this.comsign = comsign;
        this.discountlimit = discountlimit;
        this.payidfrompayment = payidfrompayment;
        this.addinputflag = addinputflag;
        this.policynatureflag = policynatureflag;
        this.clienttype = clienttype;
        this.handlername = handlername;
        this.agentlosration = agentlosration;
        this.enforceonly = enforceonly;
        this.salernumber = salernumber;
        this.salername = salername;
        this.polisource = polisource;
        this.totalloss = totalloss;
        this.firstrecorddate = firstrecorddate;
        this.firstunderwritedate = firstunderwritedate;
        this.deliverymode = deliverymode;
        this.ecardtype = ecardtype;
        this.settleflag = settleflag;
        this.websource = websource;
        this.cartypecode = cartypecode;
        this.carinsurancetype = carinsurancetype;
        this.acctname = acctname;
        this.servicecode = servicecode;
        this.channeltype = channeltype;
        this.specialrisk = specialrisk;
        this.customcode = customcode;
        this.channelmanagercode = channelmanagercode;
        this.channelmanagername = channelmanagername;
        this.servicename = servicename;
        this.channeltypename = channeltypename;
        this.inputtime = inputtime;
        this.vericode = vericode;
        this.policyprintstyle = policyprintstyle;
        this.liuflag = liuflag;
        this.withholdrate = withholdrate;
        this.declaretype = declaretype;
        this.pureriskpremium = pureriskpremium;
        this.personalagentlicenseno = personalagentlicenseno;
        this.intermediarylicenseno = intermediarylicenseno;
        this.endorwhole = endorwhole;
        this.countycode = countycode;
        this.layoffdate = layoffdate;
        this.estimatedresumedate = estimatedresumedate;
        this.actualresumedate = actualresumedate;
        this.estimatedenddate = estimatedenddate;
        this.actualenddate = actualenddate;
        this.pureriskpremiumflag = pureriskpremiumflag;
        this.referencepurerisk = referencepurerisk;
        this.agentbatch = agentbatch;
        this.seefeeflag = seefeeflag;
        this.kindmaintype = kindmaintype;
        this.sumextaxpremium = sumextaxpremium;
        this.sumvalueaddedtax = sumvalueaddedtax;
        this.trdsalescode = trdsalescode;
    }

   
    // Property accessors
    @Id 
    
    @Column(name="POLICYNO", unique=true, nullable=false, length=22)

    public String getPolicyno() {
        return this.policyno;
    }
    
    public void setPolicyno(String policyno) {
        this.policyno = policyno;
    }
    
    @Column(name="CLASSCODE", nullable=false, length=3)

    public String getClasscode() {
        return this.classcode;
    }
    
    public void setClasscode(String classcode) {
        this.classcode = classcode;
    }
    
    @Column(name="RISKCODE", nullable=false, length=4)

    public String getRiskcode() {
        return this.riskcode;
    }
    
    public void setRiskcode(String riskcode) {
        this.riskcode = riskcode;
    }
    
    @Column(name="PROPOSALNO", length=22)

    public String getProposalno() {
        return this.proposalno;
    }
    
    public void setProposalno(String proposalno) {
        this.proposalno = proposalno;
    }
    
    @Column(name="CONTRACTNO", length=24)

    public String getContractno() {
        return this.contractno;
    }
    
    public void setContractno(String contractno) {
        this.contractno = contractno;
    }
    
    @Column(name="POLICYSORT", length=1)

    public String getPolicysort() {
        return this.policysort;
    }
    
    public void setPolicysort(String policysort) {
        this.policysort = policysort;
    }
    
    @Column(name="PRINTNO", length=22)

    public String getPrintno() {
        return this.printno;
    }
    
    public void setPrintno(String printno) {
        this.printno = printno;
    }
    
    @Column(name="BUSINESSNATURE", nullable=false, length=1)

    public String getBusinessnature() {
        return this.businessnature;
    }
    
    public void setBusinessnature(String businessnature) {
        this.businessnature = businessnature;
    }
    
    @Column(name="LANGUAGE", length=1)

    public String getLanguage() {
        return this.language;
    }
    
    public void setLanguage(String language) {
        this.language = language;
    }
    
    @Column(name="POLICYTYPE", length=2)

    public String getPolicytype() {
        return this.policytype;
    }
    
    public void setPolicytype(String policytype) {
        this.policytype = policytype;
    }
    
    @Column(name="APPLICODE", length=20)

    public String getApplicode() {
        return this.applicode;
    }
    
    public void setApplicode(String applicode) {
        this.applicode = applicode;
    }
    
    @Column(name="APPLINAME", length=1000)

    public String getAppliname() {
        return this.appliname;
    }
    
    public void setAppliname(String appliname) {
        this.appliname = appliname;
    }
    
    @Column(name="APPLIADDRESS")

    public String getAppliaddress() {
        return this.appliaddress;
    }
    
    public void setAppliaddress(String appliaddress) {
        this.appliaddress = appliaddress;
    }
    
    @Column(name="INSUREDCODE", length=20)

    public String getInsuredcode() {
        return this.insuredcode;
    }
    
    public void setInsuredcode(String insuredcode) {
        this.insuredcode = insuredcode;
    }
    
    @Column(name="INSUREDNAME", length=1000)

    public String getInsuredname() {
        return this.insuredname;
    }
    
    public void setInsuredname(String insuredname) {
        this.insuredname = insuredname;
    }
    
    @Column(name="INSUREDADDRESS")

    public String getInsuredaddress() {
        return this.insuredaddress;
    }
    
    public void setInsuredaddress(String insuredaddress) {
        this.insuredaddress = insuredaddress;
    }
    
    @Column(name="OPERATEDATE", length=7)

    public Date getOperatedate() {
        return this.operatedate;
    }
    
    public void setOperatedate(Date operatedate) {
        this.operatedate = operatedate;
    }
    
    @Column(name="STARTDATE", length=7)

    public Date getStartdate() {
        return this.startdate;
    }
    
    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }
    
    @Column(name="STARTHOUR", precision=15, scale=0)

    public Long getStarthour() {
        return this.starthour;
    }
    
    public void setStarthour(Long starthour) {
        this.starthour = starthour;
    }
    
    @Column(name="ENDDATE", length=7)

    public Date getEnddate() {
        return this.enddate;
    }
    
    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }
    
    @Column(name="ENDHOUR", precision=15, scale=0)

    public Long getEndhour() {
        return this.endhour;
    }
    
    public void setEndhour(Long endhour) {
        this.endhour = endhour;
    }
    
    @Column(name="PURERATE", precision=8, scale=4)

    public Double getPurerate() {
        return this.purerate;
    }
    
    public void setPurerate(Double purerate) {
        this.purerate = purerate;
    }
    
    @Column(name="DISRATE", precision=8, scale=4)

    public Double getDisrate() {
        return this.disrate;
    }
    
    public void setDisrate(Double disrate) {
        this.disrate = disrate;
    }
    
    @Column(name="DISCOUNT", precision=12, scale=8)

    public Double getDiscount() {
        return this.discount;
    }
    
    public void setDiscount(Double discount) {
        this.discount = discount;
    }
    
    @Column(name="CURRENCY", length=3)

    public String getCurrency() {
        return this.currency;
    }
    
    public void setCurrency(String currency) {
        this.currency = currency;
    }
    
    @Column(name="SUMVALUE", precision=14)

    public Double getSumvalue() {
        return this.sumvalue;
    }
    
    public void setSumvalue(Double sumvalue) {
        this.sumvalue = sumvalue;
    }
    
    @Column(name="SUMAMOUNT", precision=14)

    public Double getSumamount() {
        return this.sumamount;
    }
    
    public void setSumamount(Double sumamount) {
        this.sumamount = sumamount;
    }
    
    @Column(name="SUMDISCOUNT", precision=14)

    public Double getSumdiscount() {
        return this.sumdiscount;
    }
    
    public void setSumdiscount(Double sumdiscount) {
        this.sumdiscount = sumdiscount;
    }
    
    @Column(name="SUMPREMIUM", precision=14)

    public Double getSumpremium() {
        return this.sumpremium;
    }
    
    public void setSumpremium(Double sumpremium) {
        this.sumpremium = sumpremium;
    }
    
    @Column(name="SUMSUBPREM", precision=14)

    public Double getSumsubprem() {
        return this.sumsubprem;
    }
    
    public void setSumsubprem(Double sumsubprem) {
        this.sumsubprem = sumsubprem;
    }
    
    @Column(name="SUMQUANTITY", precision=15, scale=0)

    public Long getSumquantity() {
        return this.sumquantity;
    }
    
    public void setSumquantity(Long sumquantity) {
        this.sumquantity = sumquantity;
    }
    
    @Column(name="JUDICALCODE", length=1)

    public String getJudicalcode() {
        return this.judicalcode;
    }
    
    public void setJudicalcode(String judicalcode) {
        this.judicalcode = judicalcode;
    }
    
    @Column(name="JUDICALSCOPE")

    public String getJudicalscope() {
        return this.judicalscope;
    }
    
    public void setJudicalscope(String judicalscope) {
        this.judicalscope = judicalscope;
    }
    
    @Column(name="AUTOTRANSRENEWFLAG", length=1)

    public String getAutotransrenewflag() {
        return this.autotransrenewflag;
    }
    
    public void setAutotransrenewflag(String autotransrenewflag) {
        this.autotransrenewflag = autotransrenewflag;
    }
    
    @Column(name="ARGUESOLUTION", length=1)

    public String getArguesolution() {
        return this.arguesolution;
    }
    
    public void setArguesolution(String arguesolution) {
        this.arguesolution = arguesolution;
    }
    
    @Column(name="ARBITBOARDNAME", length=60)

    public String getArbitboardname() {
        return this.arbitboardname;
    }
    
    public void setArbitboardname(String arbitboardname) {
        this.arbitboardname = arbitboardname;
    }
    
    @Column(name="PAYTIMES", precision=15, scale=0)

    public Long getPaytimes() {
        return this.paytimes;
    }
    
    public void setPaytimes(Long paytimes) {
        this.paytimes = paytimes;
    }
    
    @Column(name="ENDORSETIMES", precision=15, scale=0)

    public Long getEndorsetimes() {
        return this.endorsetimes;
    }
    
    public void setEndorsetimes(Long endorsetimes) {
        this.endorsetimes = endorsetimes;
    }
    
    @Column(name="CLAIMTIMES", precision=15, scale=0)

    public Long getClaimtimes() {
        return this.claimtimes;
    }
    
    public void setClaimtimes(Long claimtimes) {
        this.claimtimes = claimtimes;
    }
    
    @Column(name="MAKECOM", nullable=false, length=8)

    public String getMakecom() {
        return this.makecom;
    }
    
    public void setMakecom(String makecom) {
        this.makecom = makecom;
    }
    
    @Column(name="OPERATESITE", length=40)

    public String getOperatesite() {
        return this.operatesite;
    }
    
    public void setOperatesite(String operatesite) {
        this.operatesite = operatesite;
    }
    
    @Column(name="COMCODE", nullable=false, length=8)

    public String getComcode() {
        return this.comcode;
    }
    
    public void setComcode(String comcode) {
        this.comcode = comcode;
    }
    
    @Column(name="HANDLERCODE", nullable=false, length=14)

    public String getHandlercode() {
        return this.handlercode;
    }
    
    public void setHandlercode(String handlercode) {
        this.handlercode = handlercode;
    }
    
    @Column(name="HANDLER1CODE", length=10)

    public String getHandler1code() {
        return this.handler1code;
    }
    
    public void setHandler1code(String handler1code) {
        this.handler1code = handler1code;
    }
    
    @Column(name="APPROVERCODE", length=10)

    public String getApprovercode() {
        return this.approvercode;
    }
    
    public void setApprovercode(String approvercode) {
        this.approvercode = approvercode;
    }
    
    @Column(name="UNDERWRITECODE", length=10)

    public String getUnderwritecode() {
        return this.underwritecode;
    }
    
    public void setUnderwritecode(String underwritecode) {
        this.underwritecode = underwritecode;
    }
    
    @Column(name="UNDERWRITENAME", length=120)

    public String getUnderwritename() {
        return this.underwritename;
    }
    
    public void setUnderwritename(String underwritename) {
        this.underwritename = underwritename;
    }
    
    @Column(name="OPERATORCODE", nullable=false, length=10)

    public String getOperatorcode() {
        return this.operatorcode;
    }
    
    public void setOperatorcode(String operatorcode) {
        this.operatorcode = operatorcode;
    }
    
    @Column(name="INPUTDATE", length=7)

    public Date getInputdate() {
        return this.inputdate;
    }
    
    public void setInputdate(Date inputdate) {
        this.inputdate = inputdate;
    }
    
    @Column(name="INPUTHOUR", precision=15, scale=0)

    public Long getInputhour() {
        return this.inputhour;
    }
    
    public void setInputhour(Long inputhour) {
        this.inputhour = inputhour;
    }
    
    @Column(name="UNDERWRITEENDDATE", length=7)

    public Date getUnderwriteenddate() {
        return this.underwriteenddate;
    }
    
    public void setUnderwriteenddate(Date underwriteenddate) {
        this.underwriteenddate = underwriteenddate;
    }
    
    @Column(name="STATISTICSYM", length=7)

    public Date getStatisticsym() {
        return this.statisticsym;
    }
    
    public void setStatisticsym(Date statisticsym) {
        this.statisticsym = statisticsym;
    }
    
    @Column(name="AGENTCODE", length=13)

    public String getAgentcode() {
        return this.agentcode;
    }
    
    public void setAgentcode(String agentcode) {
        this.agentcode = agentcode;
    }
    
    @Column(name="COINSFLAG", length=1)

    public String getCoinsflag() {
        return this.coinsflag;
    }
    
    public void setCoinsflag(String coinsflag) {
        this.coinsflag = coinsflag;
    }
    
    @Column(name="REINSFLAG", length=1)

    public String getReinsflag() {
        return this.reinsflag;
    }
    
    public void setReinsflag(String reinsflag) {
        this.reinsflag = reinsflag;
    }
    
    @Column(name="ALLINSFLAG", length=1)

    public String getAllinsflag() {
        return this.allinsflag;
    }
    
    public void setAllinsflag(String allinsflag) {
        this.allinsflag = allinsflag;
    }
    
    @Column(name="UNDERWRITEFLAG", length=1)

    public String getUnderwriteflag() {
        return this.underwriteflag;
    }
    
    public void setUnderwriteflag(String underwriteflag) {
        this.underwriteflag = underwriteflag;
    }
    
    @Column(name="OTHFLAG", length=23)

    public String getOthflag() {
        return this.othflag;
    }
    
    public void setOthflag(String othflag) {
        this.othflag = othflag;
    }
    
    @Column(name="FLAG", length=10)

    public String getFlag() {
        return this.flag;
    }
    
    public void setFlag(String flag) {
        this.flag = flag;
    }
    
    @Column(name="DISRATE1", precision=8, scale=4)

    public Double getDisrate1() {
        return this.disrate1;
    }
    
    public void setDisrate1(Double disrate1) {
        this.disrate1 = disrate1;
    }
    
    @Column(name="BUSINESSFLAG", length=1)

    public String getBusinessflag() {
        return this.businessflag;
    }
    
    public void setBusinessflag(String businessflag) {
        this.businessflag = businessflag;
    }
    
    @Column(name="UPDATERCODE", length=10)

    public String getUpdatercode() {
        return this.updatercode;
    }
    
    public void setUpdatercode(String updatercode) {
        this.updatercode = updatercode;
    }
    
    @Column(name="UPDATEDATE", length=7)

    public Date getUpdatedate() {
        return this.updatedate;
    }
    
    public void setUpdatedate(Date updatedate) {
        this.updatedate = updatedate;
    }
    
    @Column(name="UPDATEHOUR", length=8)

    public String getUpdatehour() {
        return this.updatehour;
    }
    
    public void setUpdatehour(String updatehour) {
        this.updatehour = updatehour;
    }
    
    @Column(name="SIGNDATE", length=7)

    public Date getSigndate() {
        return this.signdate;
    }
    
    public void setSigndate(Date signdate) {
        this.signdate = signdate;
    }
    
    @Column(name="SHAREHOLDERFLAG", length=1)

    public String getShareholderflag() {
        return this.shareholderflag;
    }
    
    public void setShareholderflag(String shareholderflag) {
        this.shareholderflag = shareholderflag;
    }
    
    @Column(name="AGREEMENTNO", length=22)

    public String getAgreementno() {
        return this.agreementno;
    }
    
    public void setAgreementno(String agreementno) {
        this.agreementno = agreementno;
    }
    
    @Column(name="INQUIRYNO", length=22)

    public String getInquiryno() {
        return this.inquiryno;
    }
    
    public void setInquiryno(String inquiryno) {
        this.inquiryno = inquiryno;
    }
    
    @Column(name="PAYMODE", length=1)

    public String getPaymode() {
        return this.paymode;
    }
    
    public void setPaymode(String paymode) {
        this.paymode = paymode;
    }
    
    @Column(name="REMARK", length=2000)

    public String getRemark() {
        return this.remark;
    }
    
    public void setRemark(String remark) {
        this.remark = remark;
    }
    
    @Column(name="VISACODE", length=20)

    public String getVisacode() {
        return this.visacode;
    }
    
    public void setVisacode(String visacode) {
        this.visacode = visacode;
    }
    
    @Column(name="MANUALTYPE", length=1)

    public String getManualtype() {
        return this.manualtype;
    }
    
    public void setManualtype(String manualtype) {
        this.manualtype = manualtype;
    }
    
    @Column(name="STARTMINUTE", precision=2, scale=0)

    public Byte getStartminute() {
        return this.startminute;
    }
    
    public void setStartminute(Byte startminute) {
        this.startminute = startminute;
    }
    
    @Column(name="ENDMINUTE", precision=2, scale=0)

    public Byte getEndminute() {
        return this.endminute;
    }
    
    public void setEndminute(Byte endminute) {
        this.endminute = endminute;
    }
    
    @Column(name="NATIONFLAG", length=5)

    public String getNationflag() {
        return this.nationflag;
    }
    
    public void setNationflag(String nationflag) {
        this.nationflag = nationflag;
    }
    
    @Column(name="PAPOLICYNO", length=1000)

    public String getPapolicyno() {
        return this.papolicyno;
    }
    
    public void setPapolicyno(String papolicyno) {
        this.papolicyno = papolicyno;
    }
    
    @Column(name="BUSINESSTYPE", length=30)

    public String getBusinesstype() {
        return this.businesstype;
    }
    
    public void setBusinesstype(String businesstype) {
        this.businesstype = businesstype;
    }
    
    @Column(name="BUSINESSTYPE1", length=30)

    public String getBusinesstype1() {
        return this.businesstype1;
    }
    
    public void setBusinesstype1(String businesstype1) {
        this.businesstype1 = businesstype1;
    }
    
    @Column(name="ORIGINPOLICYNO", length=30)

    public String getOriginpolicyno() {
        return this.originpolicyno;
    }
    
    public void setOriginpolicyno(String originpolicyno) {
        this.originpolicyno = originpolicyno;
    }
    
    @Column(name="CONVEYFLAG", length=2)

    public String getConveyflag() {
        return this.conveyflag;
    }
    
    public void setConveyflag(String conveyflag) {
        this.conveyflag = conveyflag;
    }
    
    @Column(name="RISKMARK", length=20)

    public String getRiskmark() {
        return this.riskmark;
    }
    
    public void setRiskmark(String riskmark) {
        this.riskmark = riskmark;
    }
    
    @Column(name="UWAPPROVALNO", length=20)

    public String getUwapprovalno() {
        return this.uwapprovalno;
    }
    
    public void setUwapprovalno(String uwapprovalno) {
        this.uwapprovalno = uwapprovalno;
    }
    
    @Column(name="CUSTCONNFLAG", length=1)

    public String getCustconnflag() {
        return this.custconnflag;
    }
    
    public void setCustconnflag(String custconnflag) {
        this.custconnflag = custconnflag;
    }
    
    @Column(name="PREPAIDRATE", precision=8, scale=4)

    public Double getPrepaidrate() {
        return this.prepaidrate;
    }
    
    public void setPrepaidrate(Double prepaidrate) {
        this.prepaidrate = prepaidrate;
    }
    
    @Column(name="SUMESTIMATEDPREMIUM", precision=14)

    public Double getSumestimatedpremium() {
        return this.sumestimatedpremium;
    }
    
    public void setSumestimatedpremium(Double sumestimatedpremium) {
        this.sumestimatedpremium = sumestimatedpremium;
    }
    
    @Column(name="LOWESTPREMIUM", precision=14)

    public Double getLowestpremium() {
        return this.lowestpremium;
    }
    
    public void setLowestpremium(Double lowestpremium) {
        this.lowestpremium = lowestpremium;
    }
    
    @Column(name="SUMESTIMATEDAMOUNT", precision=14)

    public Double getSumestimatedamount() {
        return this.sumestimatedamount;
    }
    
    public void setSumestimatedamount(Double sumestimatedamount) {
        this.sumestimatedamount = sumestimatedamount;
    }
    
    @Column(name="OPENPOLICYFLAG", length=1)

    public String getOpenpolicyflag() {
        return this.openpolicyflag;
    }
    
    public void setOpenpolicyflag(String openpolicyflag) {
        this.openpolicyflag = openpolicyflag;
    }
    
    @Column(name="CURRENCY1", length=3)

    public String getCurrency1() {
        return this.currency1;
    }
    
    public void setCurrency1(String currency1) {
        this.currency1 = currency1;
    }
    
    @Column(name="GROUPBUSINESSFLAG", length=1)

    public String getGroupbusinessflag() {
        return this.groupbusinessflag;
    }
    
    public void setGroupbusinessflag(String groupbusinessflag) {
        this.groupbusinessflag = groupbusinessflag;
    }
    
    @Column(name="GROUPBUSINESSCODE", length=2)

    public String getGroupbusinesscode() {
        return this.groupbusinesscode;
    }
    
    public void setGroupbusinesscode(String groupbusinesscode) {
        this.groupbusinesscode = groupbusinesscode;
    }
    
    @Column(name="TRADINGAUTOSFLAG", length=1)

    public String getTradingautosflag() {
        return this.tradingautosflag;
    }
    
    public void setTradingautosflag(String tradingautosflag) {
        this.tradingautosflag = tradingautosflag;
    }
    
    @Column(name="AFFINITYCODE", length=8)

    public String getAffinitycode() {
        return this.affinitycode;
    }
    
    public void setAffinitycode(String affinitycode) {
        this.affinitycode = affinitycode;
    }
    
    @Column(name="ISPAYMENTRETURNFLAG", length=1)

    public String getIspaymentreturnflag() {
        return this.ispaymentreturnflag;
    }
    
    public void setIspaymentreturnflag(String ispaymentreturnflag) {
        this.ispaymentreturnflag = ispaymentreturnflag;
    }
    
    @Column(name="SURVEYFLAG", length=1)

    public String getSurveyflag() {
        return this.surveyflag;
    }
    
    public void setSurveyflag(String surveyflag) {
        this.surveyflag = surveyflag;
    }
    
    @Column(name="AGENTNAME")

    public String getAgentname() {
        return this.agentname;
    }
    
    public void setAgentname(String agentname) {
        this.agentname = agentname;
    }
    
    @Column(name="AGREEMENTNAMENAME", length=120)

    public String getAgreementnamename() {
        return this.agreementnamename;
    }
    
    public void setAgreementnamename(String agreementnamename) {
        this.agreementnamename = agreementnamename;
    }
    
    @Column(name="AFFINITYNAME", length=50)

    public String getAffinityname() {
        return this.affinityname;
    }
    
    public void setAffinityname(String affinityname) {
        this.affinityname = affinityname;
    }
    
    @Column(name="AGREEMENTNAME")

    public String getAgreementname() {
        return this.agreementname;
    }
    
    public void setAgreementname(String agreementname) {
        this.agreementname = agreementname;
    }
    
    @Column(name="ANONYMITYFLAG", length=2)

    public String getAnonymityflag() {
        return this.anonymityflag;
    }
    
    public void setAnonymityflag(String anonymityflag) {
        this.anonymityflag = anonymityflag;
    }
    
    @Column(name="PLANEXCHANGERATE", precision=8, scale=5)

    public Double getPlanexchangerate() {
        return this.planexchangerate;
    }
    
    public void setPlanexchangerate(Double planexchangerate) {
        this.planexchangerate = planexchangerate;
    }
    
    @Column(name="RISKVERSION", length=50)

    public String getRiskversion() {
        return this.riskversion;
    }
    
    public void setRiskversion(String riskversion) {
        this.riskversion = riskversion;
    }
    
    @Column(name="COMSIGN", length=3)

    public String getComsign() {
        return this.comsign;
    }
    
    public void setComsign(String comsign) {
        this.comsign = comsign;
    }
    
    @Column(name="DISCOUNTLIMIT", length=10)

    public String getDiscountlimit() {
        return this.discountlimit;
    }
    
    public void setDiscountlimit(String discountlimit) {
        this.discountlimit = discountlimit;
    }
    
    @Column(name="PAYIDFROMPAYMENT", length=25)

    public String getPayidfrompayment() {
        return this.payidfrompayment;
    }
    
    public void setPayidfrompayment(String payidfrompayment) {
        this.payidfrompayment = payidfrompayment;
    }
    
    @Column(name="ADDINPUTFLAG", length=1)

    public String getAddinputflag() {
        return this.addinputflag;
    }
    
    public void setAddinputflag(String addinputflag) {
        this.addinputflag = addinputflag;
    }
    
    @Column(name="POLICYNATUREFLAG", length=1)

    public String getPolicynatureflag() {
        return this.policynatureflag;
    }
    
    public void setPolicynatureflag(String policynatureflag) {
        this.policynatureflag = policynatureflag;
    }
    
    @Column(name="CLIENTTYPE", length=1)

    public String getClienttype() {
        return this.clienttype;
    }
    
    public void setClienttype(String clienttype) {
        this.clienttype = clienttype;
    }
    
    @Column(name="HANDLERNAME", length=60)

    public String getHandlername() {
        return this.handlername;
    }
    
    public void setHandlername(String handlername) {
        this.handlername = handlername;
    }
    
    @Column(name="AGENTLOSRATION", precision=3)

    public Double getAgentlosration() {
        return this.agentlosration;
    }
    
    public void setAgentlosration(Double agentlosration) {
        this.agentlosration = agentlosration;
    }
    
    @Column(name="ENFORCEONLY", length=1)

    public String getEnforceonly() {
        return this.enforceonly;
    }
    
    public void setEnforceonly(String enforceonly) {
        this.enforceonly = enforceonly;
    }
    
    @Column(name="SALERNUMBER", length=100)

    public String getSalernumber() {
        return this.salernumber;
    }
    
    public void setSalernumber(String salernumber) {
        this.salernumber = salernumber;
    }
    
    @Column(name="SALERNAME")

    public String getSalername() {
        return this.salername;
    }
    
    public void setSalername(String salername) {
        this.salername = salername;
    }
    
    @Column(name="POLISOURCE", length=10)

    public String getPolisource() {
        return this.polisource;
    }
    
    public void setPolisource(String polisource) {
        this.polisource = polisource;
    }
    
    @Column(name="TOTALLOSS", length=1)

    public String getTotalloss() {
        return this.totalloss;
    }
    
    public void setTotalloss(String totalloss) {
        this.totalloss = totalloss;
    }
    
    @Column(name="FIRSTRECORDDATE", length=7)

    public Date getFirstrecorddate() {
        return this.firstrecorddate;
    }
    
    public void setFirstrecorddate(Date firstrecorddate) {
        this.firstrecorddate = firstrecorddate;
    }
    
    @Column(name="FIRSTUNDERWRITEDATE", length=7)

    public Date getFirstunderwritedate() {
        return this.firstunderwritedate;
    }
    
    public void setFirstunderwritedate(Date firstunderwritedate) {
        this.firstunderwritedate = firstunderwritedate;
    }
    
    @Column(name="DELIVERYMODE", length=10)

    public String getDeliverymode() {
        return this.deliverymode;
    }
    
    public void setDeliverymode(String deliverymode) {
        this.deliverymode = deliverymode;
    }
    
    @Column(name="ECARDTYPE", length=10)

    public String getEcardtype() {
        return this.ecardtype;
    }
    
    public void setEcardtype(String ecardtype) {
        this.ecardtype = ecardtype;
    }
    
    @Column(name="SETTLEFLAG", length=1)

    public String getSettleflag() {
        return this.settleflag;
    }
    
    public void setSettleflag(String settleflag) {
        this.settleflag = settleflag;
    }
    
    @Column(name="WEBSOURCE", length=10)

    public String getWebsource() {
        return this.websource;
    }
    
    public void setWebsource(String websource) {
        this.websource = websource;
    }
    
    @Column(name="CARTYPECODE", length=6)

    public String getCartypecode() {
        return this.cartypecode;
    }
    
    public void setCartypecode(String cartypecode) {
        this.cartypecode = cartypecode;
    }
    
    @Column(name="CARINSURANCETYPE", length=3)

    public String getCarinsurancetype() {
        return this.carinsurancetype;
    }
    
    public void setCarinsurancetype(String carinsurancetype) {
        this.carinsurancetype = carinsurancetype;
    }
    
    @Column(name="ACCTNAME", length=200)

    public String getAcctname() {
        return this.acctname;
    }
    
    public void setAcctname(String acctname) {
        this.acctname = acctname;
    }
    
    @Column(name="SERVICECODE", length=100)

    public String getServicecode() {
        return this.servicecode;
    }
    
    public void setServicecode(String servicecode) {
        this.servicecode = servicecode;
    }
    
    @Column(name="CHANNELTYPE", length=10)

    public String getChanneltype() {
        return this.channeltype;
    }
    
    public void setChanneltype(String channeltype) {
        this.channeltype = channeltype;
    }
    
    @Column(name="SPECIALRISK", length=1)

    public String getSpecialrisk() {
        return this.specialrisk;
    }
    
    public void setSpecialrisk(String specialrisk) {
        this.specialrisk = specialrisk;
    }
    
    @Column(name="CUSTOMCODE", length=100)

    public String getCustomcode() {
        return this.customcode;
    }
    
    public void setCustomcode(String customcode) {
        this.customcode = customcode;
    }
    
    @Column(name="CHANNELMANAGERCODE", length=100)

    public String getChannelmanagercode() {
        return this.channelmanagercode;
    }
    
    public void setChannelmanagercode(String channelmanagercode) {
        this.channelmanagercode = channelmanagercode;
    }
    
    @Column(name="CHANNELMANAGERNAME")

    public String getChannelmanagername() {
        return this.channelmanagername;
    }
    
    public void setChannelmanagername(String channelmanagername) {
        this.channelmanagername = channelmanagername;
    }
    
    @Column(name="SERVICENAME")

    public String getServicename() {
        return this.servicename;
    }
    
    public void setServicename(String servicename) {
        this.servicename = servicename;
    }
    
    @Column(name="CHANNELTYPENAME")

    public String getChanneltypename() {
        return this.channeltypename;
    }
    
    public void setChanneltypename(String channeltypename) {
        this.channeltypename = channeltypename;
    }
    
    @Column(name="INPUTTIME", length=7)

    public Date getInputtime() {
        return this.inputtime;
    }
    
    public void setInputtime(Date inputtime) {
        this.inputtime = inputtime;
    }
    
    @Column(name="VERICODE", length=30)

    public String getVericode() {
        return this.vericode;
    }
    
    public void setVericode(String vericode) {
        this.vericode = vericode;
    }
    
    @Column(name="POLICYPRINTSTYLE", length=2)

    public String getPolicyprintstyle() {
        return this.policyprintstyle;
    }
    
    public void setPolicyprintstyle(String policyprintstyle) {
        this.policyprintstyle = policyprintstyle;
    }
    
    @Column(name="LIUFLAG", length=1)

    public String getLiuflag() {
        return this.liuflag;
    }
    
    public void setLiuflag(String liuflag) {
        this.liuflag = liuflag;
    }
    
    @Column(name="WITHHOLDRATE", length=2)

    public String getWithholdrate() {
        return this.withholdrate;
    }
    
    public void setWithholdrate(String withholdrate) {
        this.withholdrate = withholdrate;
    }
    
    @Column(name="DECLARETYPE", length=1)

    public String getDeclaretype() {
        return this.declaretype;
    }
    
    public void setDeclaretype(String declaretype) {
        this.declaretype = declaretype;
    }
    
    @Column(name="PURERISKPREMIUM", precision=14, scale=6)

    public Double getPureriskpremium() {
        return this.pureriskpremium;
    }
    
    public void setPureriskpremium(Double pureriskpremium) {
        this.pureriskpremium = pureriskpremium;
    }
    
    @Column(name="PERSONALAGENTLICENSENO", length=30)

    public String getPersonalagentlicenseno() {
        return this.personalagentlicenseno;
    }
    
    public void setPersonalagentlicenseno(String personalagentlicenseno) {
        this.personalagentlicenseno = personalagentlicenseno;
    }
    
    @Column(name="INTERMEDIARYLICENSENO", length=30)

    public String getIntermediarylicenseno() {
        return this.intermediarylicenseno;
    }
    
    public void setIntermediarylicenseno(String intermediarylicenseno) {
        this.intermediarylicenseno = intermediarylicenseno;
    }
    
    @Column(name="ENDORWHOLE", length=1)

    public String getEndorwhole() {
        return this.endorwhole;
    }
    
    public void setEndorwhole(String endorwhole) {
        this.endorwhole = endorwhole;
    }
    
    @Column(name="COUNTYCODE", length=100)

    public String getCountycode() {
        return this.countycode;
    }
    
    public void setCountycode(String countycode) {
        this.countycode = countycode;
    }
    
    @Column(name="LAYOFFDATE", length=7)

    public Date getLayoffdate() {
        return this.layoffdate;
    }
    
    public void setLayoffdate(Date layoffdate) {
        this.layoffdate = layoffdate;
    }
    
    @Column(name="ESTIMATEDRESUMEDATE", length=7)

    public Date getEstimatedresumedate() {
        return this.estimatedresumedate;
    }
    
    public void setEstimatedresumedate(Date estimatedresumedate) {
        this.estimatedresumedate = estimatedresumedate;
    }
    
    @Column(name="ACTUALRESUMEDATE", length=7)

    public Date getActualresumedate() {
        return this.actualresumedate;
    }
    
    public void setActualresumedate(Date actualresumedate) {
        this.actualresumedate = actualresumedate;
    }
    
    @Column(name="ESTIMATEDENDDATE", length=7)

    public Date getEstimatedenddate() {
        return this.estimatedenddate;
    }
    
    public void setEstimatedenddate(Date estimatedenddate) {
        this.estimatedenddate = estimatedenddate;
    }
    
    @Column(name="ACTUALENDDATE", length=7)

    public Date getActualenddate() {
        return this.actualenddate;
    }
    
    public void setActualenddate(Date actualenddate) {
        this.actualenddate = actualenddate;
    }
    
    @Column(name="PURERISKPREMIUMFLAG", length=100)

    public String getPureriskpremiumflag() {
        return this.pureriskpremiumflag;
    }
    
    public void setPureriskpremiumflag(String pureriskpremiumflag) {
        this.pureriskpremiumflag = pureriskpremiumflag;
    }
    
    @Column(name="REFERENCEPURERISK", precision=14, scale=6)

    public Double getReferencepurerisk() {
        return this.referencepurerisk;
    }
    
    public void setReferencepurerisk(Double referencepurerisk) {
        this.referencepurerisk = referencepurerisk;
    }
    
    @Column(name="AGENTBATCH", length=20)

    public String getAgentbatch() {
        return this.agentbatch;
    }
    
    public void setAgentbatch(String agentbatch) {
        this.agentbatch = agentbatch;
    }
    
    @Column(name="SEEFEEFLAG", length=2)

    public String getSeefeeflag() {
        return this.seefeeflag;
    }
    
    public void setSeefeeflag(String seefeeflag) {
        this.seefeeflag = seefeeflag;
    }
    
    @Column(name="KINDMAINTYPE", length=50)

    public String getKindmaintype() {
        return this.kindmaintype;
    }
    
    public void setKindmaintype(String kindmaintype) {
        this.kindmaintype = kindmaintype;
    }
    
    @Column(name="SUMEXTAXPREMIUM", precision=14)

    public Double getSumextaxpremium() {
        return this.sumextaxpremium;
    }
    
    public void setSumextaxpremium(Double sumextaxpremium) {
        this.sumextaxpremium = sumextaxpremium;
    }
    
    @Column(name="SUMVALUEADDEDTAX", precision=14)

    public Double getSumvalueaddedtax() {
        return this.sumvalueaddedtax;
    }
    
    public void setSumvalueaddedtax(Double sumvalueaddedtax) {
        this.sumvalueaddedtax = sumvalueaddedtax;
    }
    
    @Column(name="TRDSALESCODE", length=22)

    public String getTrdsalescode() {
        return this.trdsalescode;
    }
    
    public void setTrdsalescode(String trdsalescode) {
        this.trdsalescode = trdsalescode;
    }


}