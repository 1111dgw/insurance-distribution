package cn.com.libertymutual.sp.service.impl.car;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.alibaba.fastjson.JSON;

import cn.com.libertymutual.core.base.dto.RequestBaseDto;
import cn.com.libertymutual.core.redis.util.RedisUtils;
import cn.com.libertymutual.core.util.BeanUtilExt;
import cn.com.libertymutual.core.util.Constants;
import cn.com.libertymutual.core.util.DateUtil;
import cn.com.libertymutual.core.util.enums.CoreServiceEnum;
import cn.com.libertymutual.core.util.uuid.UUID;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.core.web.util.RequestUtils;
import cn.com.libertymutual.sp.bean.TbSpApplicant;
import cn.com.libertymutual.sp.bean.TbSpOrder;
import cn.com.libertymutual.sp.bean.TbSpSaleLog;
import cn.com.libertymutual.sp.dao.ApplicantDao;
import cn.com.libertymutual.sp.dao.OrderDao;
import cn.com.libertymutual.sp.dao.SpSaleLogDao;
import cn.com.libertymutual.sp.dto.InsureQuery;
import cn.com.libertymutual.sp.dto.car.bean.TpTitemApplicant;
import cn.com.libertymutual.sp.dto.car.dto.QueryVehicleRequestDto;
import cn.com.libertymutual.sp.dto.car.dto.TMotorPremiumRequestDTO;
import cn.com.libertymutual.sp.dto.car.dto.TProposalSaveRequestDto;
import cn.com.libertymutual.sp.dto.car.dto.TProposalSaveResponseDTO;
import cn.com.libertymutual.sp.dto.car.dto.TPrptMainDto;
import cn.com.libertymutual.sp.dto.car.dto.TRenewalQueryRequestDto;
import cn.com.libertymutual.sp.dto.car.dto.TVerifyRequstDTO;
import cn.com.libertymutual.sp.dto.queryplans.CrossSalePlan;
import cn.com.libertymutual.sp.dto.queryplans.QueryPlansResponseDTO;
import cn.com.libertymutual.sp.dto.queryplans.QuerySPPlansRequestDTO;
import cn.com.libertymutual.sp.dto.savePlan.PropsalSaveRequestDto;
import cn.com.libertymutual.sp.service.api.InsureService;
import cn.com.libertymutual.sp.service.api.PayService;
import cn.com.libertymutual.sp.service.api.PolicyService;
import cn.com.libertymutual.sp.service.api.ShopService;
import cn.com.libertymutual.sp.service.api.UserService;
import cn.com.libertymutual.sp.service.api.car.CarInsureService;
import cn.com.libertymutual.sys.bean.SysServiceInfo;
import cn.com.libertymutual.sys.service.api.ISequenceService;

@Service("CarInsureService")
public class CarInsureServiceImpl implements CarInsureService {
	private Logger log = LoggerFactory.getLogger(getClass());
	@Resource
	private RedisUtils redisUtils;
	@Autowired
	private JdbcTemplate readJdbcTemplate;
	@Resource
	private RestTemplate restTemplate;
	@Autowired
	private SpSaleLogDao spSaleLogDao;
	@Autowired
	private ShopService shopService;
	@Autowired
	private ISequenceService iSequenceService;
	@Autowired
	private OrderDao orderDao;
	@Autowired
	private UserService userService;
	@Autowired
	private PayService payService;
	@Autowired
	private InsureService insureService;

	@Autowired
	private ApplicantDao applicantDao;
	@Autowired
	private PolicyService policyService;

	@Override
	public String getToken() {
		Map map = (Map) redisUtils.get(Constants.SYS_SERVICE_INFO);
		SysServiceInfo sysServiceInfo = (SysServiceInfo) map.get(CoreServiceEnum.Car_Token.getUrlKey());
		RequestBaseDto qv = new RequestBaseDto();
		HttpEntity<RequestBaseDto> requestEntity = new HttpEntity<RequestBaseDto>(qv,
				RequestUtils.genEncryptHttpHeaders(sysServiceInfo.getUserName(), sysServiceInfo.getPassword()));
		String queryUrl = sysServiceInfo.getUrl()
				+ "?client_id=lingxi-finance&client_secret=ee11f52fe04780e1153024362b63e3e6&grant_type=password&username=admin&password=admin";
		Object obj = restTemplate.postForObject(queryUrl, requestEntity, Object.class);

		return null;
	}

	@Override
	public ServiceResult renewalQuery(TRenewalQueryRequestDto trRequest) {
		ServiceResult sr = new ServiceResult();
		Map map = (Map) redisUtils.get(Constants.SYS_SERVICE_INFO);
		SysServiceInfo sysServiceInfo = (SysServiceInfo) map.get(CoreServiceEnum.Renewal_Query.getUrlKey());

		trRequest.setPartnerAccountCode(sysServiceInfo.getUserName()); // LBISLE
		trRequest.setPassKey(sysServiceInfo.getPassword()); // E0EE5F4991879A8556FB79FA4BB0FE64
		trRequest.setFlowId(sysServiceInfo.getUserName() + DateUtil.getStringDateYyMMdd() + UUID.getShortUuid()); // LBISLE180112ZyIKAtwM
		trRequest.setOperatorDate(DateUtil.getStringDate()); // 2018-01-12 10:37:42
		trRequest.setRecordCode("CXPTA2016061401");
		// trRequest.setAgreementNo(sysServiceInfo.getTocken()); // CHQ001697
		trRequest.setPdk("8f250d4de892b51ebaceb5840a69a3f5");
		trRequest.setAccess_token("salePlat-lbi");
		HttpEntity<TRenewalQueryRequestDto> requestEntity = null;
		TbSpSaleLog saleLog = new TbSpSaleLog();
		saleLog.setOperation(sysServiceInfo.getDescription());
		// saleLog.setMark(sysServiceInfo.getUrl());
		saleLog.setRequestTime(new Date());
		try {
			requestEntity = new HttpEntity<TRenewalQueryRequestDto>(trRequest,
					RequestUtils.genEncryptHttpHeaders(sysServiceInfo.getUserName(), sysServiceInfo.getPassword()));
			saleLog.setRequestData(BeanUtilExt.toJsonString(requestEntity));
			sr = restTemplate.postForObject(sysServiceInfo.getUrl(), requestEntity, ServiceResult.class);
		} catch (Exception e) {
			saleLog.setResponseData(e.toString());
			sr.setFail();
		}
		// if (sr.getState() != ServiceResult.STATE_SUCCESS) {
		// if (StringUtil.isEmpty(saleLog.getResponseData())) {
		// saleLog.setResponseData(BeanUtilExt.toJsonString(sr));
		// }
		// }
		saleLog.setResponseData(BeanUtilExt.toJsonString(sr));
		spSaleLogDao.save(saleLog);
		return sr;
	}

	// public void save
	@Override
	public ServiceResult queryVehicle(QueryVehicleRequestDto venDto) {
		ServiceResult sr = new ServiceResult();
		Map map = (Map) redisUtils.get(Constants.SYS_SERVICE_INFO);
		SysServiceInfo sysServiceInfo = (SysServiceInfo) map.get(CoreServiceEnum.Query_Vehicle.getUrlKey());

		venDto.setPartnerAccountCode(sysServiceInfo.getUserName()); // LBISLE
		venDto.setPassKey(sysServiceInfo.getPassword()); // E0EE5F4991879A8556FB79FA4BB0FE64
		venDto.setFlowId(sysServiceInfo.getUserName() + DateUtil.getStringDateYyMMdd() + UUID.getShortUuid()); // LBISLE180112ZyIKAtwM
		venDto.setOperatorDate(DateUtil.getStringDate()); // 2018-01-12 10:37:42
		venDto.setRecordCode("CXPTA2016061401");
		// venDto.setAgreementNo(sysServiceInfo.getTocken()); // CHQ001697
		venDto.setPdk("8f250d4de892b51ebaceb5840a69a3f5");
		venDto.setAccess_token("salePlat-lbi");

		HttpEntity<QueryVehicleRequestDto> requestEntity = null;
		TbSpSaleLog saleLog = new TbSpSaleLog();
		saleLog.setOperation(sysServiceInfo.getDescription());
		saleLog.setRequestTime(new Date());
		try {
			requestEntity = new HttpEntity<QueryVehicleRequestDto>(venDto,
					RequestUtils.genEncryptHttpHeaders(sysServiceInfo.getUserName(), sysServiceInfo.getPassword()));
			saleLog.setRequestData(BeanUtilExt.toJsonString(requestEntity));
			sr = restTemplate.postForObject(sysServiceInfo.getUrl(), requestEntity, ServiceResult.class);
		} catch (Exception e) {
			saleLog.setResponseData(e.toString());
			sr.setFail();
		}
		saleLog.setResponseData(BeanUtilExt.toJsonString(sr));
		spSaleLogDao.save(saleLog);
		return sr;
	}

	@Override
	public ServiceResult queryVin(QueryVehicleRequestDto venDto) {
		ServiceResult sr = new ServiceResult();
		Map map = (Map) redisUtils.get(Constants.SYS_SERVICE_INFO);
		SysServiceInfo sysServiceInfo = (SysServiceInfo) map.get(CoreServiceEnum.VIN_QUERY.getUrlKey());
		venDto.setAccess_token("2c524bd7-cc87-4bed-ac30-a92c5fe9cbf9");
		// venDto.setAgreementNo("ZJN000003");
		venDto.setFlowId(sysServiceInfo.getUserName() + DateUtil.getStringDateYyMMdd() + UUID.getShortUuid());
		venDto.setOperatorDate(DateUtil.getStringDate());
		venDto.setPartnerAccountCode(sysServiceInfo.getUserName());
		venDto.setPassKey(sysServiceInfo.getPassword());
		venDto.setPdk("016045e8c2ba508bd5c1b526f1f69d3e");
		venDto.setRecordCode("CXPTA2016061401");
		venDto.setSubQuotationId("575e492bb2d746f6a1b8e733e43bb050");
		// venDto.setVinNo("LDC703N23A0180839");
		HttpEntity<QueryVehicleRequestDto> requestEntity = null;
		try {
			requestEntity = new HttpEntity<QueryVehicleRequestDto>(venDto,
					RequestUtils.genEncryptHttpHeaders(sysServiceInfo.getUserName(), sysServiceInfo.getPassword()));

			sr = restTemplate.postForObject(sysServiceInfo.getUrl(), requestEntity, ServiceResult.class);
		} catch (Exception e) {
			TbSpSaleLog saleLog = new TbSpSaleLog();
			saleLog.setOperation(sysServiceInfo.getDescription());
			saleLog.setRequestTime(new Date());
			saleLog.setRequestData(BeanUtilExt.toJsonString(requestEntity));
			saleLog.setResponseData(e.toString());
			spSaleLogDao.save(saleLog);
			sr.setFail();
		}
		return sr;

	}

	@Override
	public ServiceResult combinecalculate(PropsalSaveRequestDto requestDto) {
		InsureQuery insureQuery = requestDto.getInsureQuery();
		ServiceResult sr = new ServiceResult();
		TMotorPremiumRequestDTO tMotorPremiumRequestDTO = requestDto.gettMotorPremiumRequestDTO();
		// Object obj = new Object();
		Map map = (Map) redisUtils.get(Constants.SYS_SERVICE_INFO);
		SysServiceInfo sysServiceInfo = (SysServiceInfo) map.get(CoreServiceEnum.Premium_CalCar.getUrlKey());
		SysServiceInfo callServiceInfo = (SysServiceInfo) map.get(CoreServiceEnum.CALLBACK_URL.getUrlKey());
		tMotorPremiumRequestDTO.getTprpTmainDto().setCallBackUrl(callServiceInfo.getUrl());
		tMotorPremiumRequestDTO.setPartnerAccountCode(sysServiceInfo.getUserName()); // LBISLE
		tMotorPremiumRequestDTO.setPassKey(sysServiceInfo.getPassword()); // E0EE5F4991879A8556FB79FA4BB0FE64
		tMotorPremiumRequestDTO.setFlowId(sysServiceInfo.getUserName() + DateUtil.getStringDateYyMMdd() + UUID.getShortUuid()); // LBISLE180112ZyIKAtwM
		tMotorPremiumRequestDTO.setOperatorDate(DateUtil.getStringDate()); // 2018-01-12 10:37:42
		tMotorPremiumRequestDTO.setRecordCode("CXPTA2016061401");
		// tMotorPremiumRequestDTO.setAgreementNo(sysServiceInfo.getTocken()); //
		// CHQ001697
		tMotorPremiumRequestDTO.setAgreementNo(insureQuery.getAgreementCode());
		tMotorPremiumRequestDTO.setPdk("8f250d4de892b51ebaceb5840a69a3f5");
		tMotorPremiumRequestDTO.setAccess_token("salePlat-lbi");
		// venDto.setVinNo("LDC703N23A0180839");
		HttpEntity<TMotorPremiumRequestDTO> requestEntity = null;
		TbSpSaleLog saleLog = new TbSpSaleLog();
		saleLog.setOperation(sysServiceInfo.getDescription());
		saleLog.setRequestTime(new Date());
		try {
			requestEntity = new HttpEntity<TMotorPremiumRequestDTO>(tMotorPremiumRequestDTO,
					RequestUtils.genEncryptHttpHeaders(sysServiceInfo.getUserName(), sysServiceInfo.getPassword()));
			saleLog.setRequestData(BeanUtilExt.toJsonString(requestEntity));
			sr = restTemplate.postForObject(sysServiceInfo.getUrl(), requestEntity, ServiceResult.class);
		} catch (Exception e) {
			saleLog.setResponseData(e.toString());
			spSaleLogDao.save(saleLog);
			sr.setFail();
			return sr;
		}
		saleLog.setResponseData(BeanUtilExt.toJsonString(sr));
		spSaleLogDao.save(saleLog);
		return sr;
	}

	@Override
	public ServiceResult preminumCommit(PropsalSaveRequestDto requestDto) {
		InsureQuery insureQuery = requestDto.getInsureQuery();
		TProposalSaveRequestDto tProposalSaveDTO = requestDto.gettProposalSaveDTO();
		ServiceResult sr = new ServiceResult();
		TPrptMainDto tprptCarOwnerDto = requestDto.gettMotorPremiumRequestDTO().getTprpTmainDto();
		Map map = (Map) redisUtils.get(Constants.SYS_SERVICE_INFO);
		SysServiceInfo sysServiceInfo = (SysServiceInfo) map.get(CoreServiceEnum.Proposal_Save.getUrlKey());
		tProposalSaveDTO.setPartnerAccountCode(sysServiceInfo.getUserName()); // LBISLE
		tProposalSaveDTO.setPassKey(sysServiceInfo.getPassword()); // E0EE5F4991879A8556FB79FA4BB0FE64
		tProposalSaveDTO.setOperatorDate(DateUtil.getStringDate()); // 2018-01-12 10:37:42
		tProposalSaveDTO.setRecordCode("CXPTA2016061401");
		tProposalSaveDTO.setAgreementNo(insureQuery.getAgreementCode()); // CHQ001697
		tProposalSaveDTO.setPdk("8f250d4de892b51ebaceb5840a69a3f5");
		tProposalSaveDTO.setAccess_token("salePlat-lbi");
		// venDto.setVinNo("LDC703N23A0180839");
		HttpEntity<TProposalSaveRequestDto> requestEntity = null;
		TbSpSaleLog saleLog = new TbSpSaleLog();
		saleLog.setOperation(sysServiceInfo.getDescription());
		saleLog.setRequestTime(new Date());
		try {
			requestEntity = new HttpEntity<TProposalSaveRequestDto>(tProposalSaveDTO,
					RequestUtils.genEncryptHttpHeaders(sysServiceInfo.getUserName(), sysServiceInfo.getPassword()));
			saleLog.setRequestData(BeanUtilExt.toJsonString(requestEntity));
			sr = restTemplate.postForObject(sysServiceInfo.getUrl(), requestEntity, ServiceResult.class);
		} catch (Exception e) {
			saleLog.setResponseData(e.toString());
			saleLog.setMark(TbSpOrder.ERROR_03);
			spSaleLogDao.save(saleLog);
			sr.setFail();
			sr.setResult("投保失败,03,请稍后再试！");
			log.error(TbSpOrder.ERROR_03);
			// 03:请求超时
			return sr;
		}

		if (ServiceResult.STATE_SUCCESS == sr.getState()) {
			TProposalSaveResponseDTO saveRequest = new TProposalSaveResponseDTO();
			LinkedHashMap<String, String> saveMap = new LinkedHashMap<String, String>();
			saveMap = (LinkedHashMap<String, String>) sr.getResult();
			String state = "";

			if ("0".equals(saveMap.get("resultCode"))) {
				state = payService.resetState(saveMap.get("proposalStatus"));
			} else {
				sr.setResult("投保失败,05,请稍后再试！");
				saleLog.setMark(TbSpOrder.ERROR_05);
				spSaleLogDao.save(saleLog);
				log.error(TbSpOrder.ERROR_05);
				// 05:返回信息不存在responseJson
				sr.setFail();
				return sr;
			}
			saveRequest.setProposalMTPLNo(saveMap.get("proposalMTPLNo"));
			saveRequest.setProposalNo(saveMap.get("proposalNo"));

			// TProposalSaveResponseDTO saveRequest = (TProposalSaveResponseDTO)
			// sr.getResult();
			try {
				String proposal = "";
				Date crossStartDate = new Date();
				Date crossEndDate = new Date();
				// 车险订单信息保存
				if (StringUtils.isNotBlank(saveRequest.getProposalMTPLNo()) && saveRequest.getProposalMTPLNo().startsWith("9")) {
					saveCarInfo(saveRequest.getProposalMTPLNo(), requestDto, state, "0502", null, null, null);
					proposal = saveRequest.getProposalMTPLNo();
					crossStartDate = DateUtil.strToDate(tprptCarOwnerDto.getMtplStartDate());
					crossEndDate = DateUtil.strToDate(tprptCarOwnerDto.getMtplEndDate());
				}
				if (StringUtils.isNotBlank(saveRequest.getProposalNo()) && saveRequest.getProposalNo().startsWith("9")) {
					saveCarInfo(saveRequest.getProposalNo(), requestDto, state, "0502", null, null, null);
					proposal = saveRequest.getProposalNo();
					crossStartDate = DateUtil.strToDate(tprptCarOwnerDto.getStartDate());
					crossEndDate = DateUtil.strToDate(tprptCarOwnerDto.getEndDate());
				}
				// 查询交叉销售计划
				ServiceResult croSer = CrossSalePlanFind(proposal, tProposalSaveDTO.getAgreementNo());
				if (ServiceResult.STATE_SUCCESS == croSer.getState()) {
					List<CrossSalePlan> planLists = (List<CrossSalePlan>) croSer.getResult();
					if (CollectionUtils.isNotEmpty(planLists)) {
						for (int i = 0; i < planLists.size(); i++) {
							saveCarInfo(saveRequest.getProposalNo(), requestDto, state, "0502", planLists.get(i), crossStartDate, crossEndDate);
						}
					}
				}
			} catch (Exception e) {
				sr.setResult("投保失败,07,请稍后再试！");
				saleLog.setMark(TbSpOrder.ERROR_07);
				saleLog.setResponseData(e.toString());
				spSaleLogDao.save(saleLog);
				log.error(TbSpOrder.ERROR_07);
				// 05:返回信息不存在responseJson
				sr.setFail();
				return sr;
			}
		}

		saleLog.setResponseData(BeanUtilExt.toJsonString(sr));
		spSaleLogDao.save(saleLog);
		return sr;
	}

	public void saveAppli(TpTitemApplicant carAppli, String orderId, String type) throws Exception {
		TbSpApplicant appli = new TbSpApplicant();
		appli.setOrderNo(orderId);
		appli.setNatureType(carAppli.getNatureOfRole());
		appli.setType(type);
		appli.setName(carAppli.getName());
		appli.setCarType(carAppli.getIdType());
		appli.setCarId(carAppli.getIdNo());
		appli.setMobile(carAppli.getCellPhoneNo());
		appli.setEmail(carAppli.getEmail());
		appli.setOccupation(""); // 职业类别
		appli.setRelation(""); // 与投保人关系
		appli.setRemark(""); // 备注
		appli.setBirth(carAppli.getBrithday());
		appli.setSex("");
		applicantDao.save(appli);
	}

	/**
	 * 车险订单保存
	 */
	public void saveCarInfo(String proposal, PropsalSaveRequestDto request, String state, String riskCode, CrossSalePlan crossSalePlan,
			Date startDate, Date endDate) throws Exception {
		TProposalSaveRequestDto tProposalSaveDTO = request.gettProposalSaveDTO();
		TbSpOrder order = new TbSpOrder();
		InsureQuery insureQuery = request.getInsureQuery();
		order = insureService.setOrderInitData(order, insureQuery);
		ServiceResult sr = new ServiceResult();
		String flowId = iSequenceService.getOrderSequence(riskCode);
		TpTitemApplicant tprptApplicantDto = tProposalSaveDTO.getTprptApplicantDto(); // 投保人信息
		saveAppli(tprptApplicantDto, flowId, TbSpApplicant.TYPE_POLICY);
		// 车险信息保存
		if (null == crossSalePlan) {
			TpTitemApplicant tprptCarOwnerDto = tProposalSaveDTO.getTprptCarOwnerDto(); // 车主信息
			saveAppli(tprptCarOwnerDto, flowId, TbSpApplicant.TYPE_CAR_OWNER);
			TpTitemApplicant tprptInsuredDto = tProposalSaveDTO.getTprptInsuredDto(); // 被保人信息
			saveAppli(tprptInsuredDto, flowId, TbSpApplicant.TYPE_INSURED);
			order = insureService.setQueryInfo(order, proposal);
		} else {
			order = insureService.setCrossSaleOrderInitData(order, crossSalePlan);
			order.setStartDate(startDate);
			order.setEndDate(endDate);
		}
		order.setApplicantName(tprptApplicantDto.getName());
		order.setIdNo(tprptApplicantDto.getIdNo());
		order.setOrderNo(flowId);
		order.setRiskCode(riskCode);
		order.setProposalNo(proposal);
		order.setAgreementNo(insureQuery.getAgreementCode());
		order.setStatus(state);
		order.setOrderDetail(JSON.toJSONString(insureQuery.getTprpTitemKindListDto())); // 订单详情
		order.setUserId(userService.getTopUserCode(request.getUser().getUserCode()));
		order.setRefereeId(userService.getTopUserCode(insureQuery.getRefereeId()));
		order = insureService.setInsureBrachCode(order, insureQuery, request);
		order.setRelationId(tProposalSaveDTO.getFlowId());
		// -----------------------业务关系代码 产销人员-----------------------------
		order = insureService.setAgrInfo(order, insureQuery.getAgreementCode(), insureQuery.getRefereeId());
		orderDao.save(order);
	}

	@Override
	public ServiceResult CrossSalePlan(Integer seatCount, Integer age, String agreementCode) {
		// getToken();
		ServiceResult sr = new ServiceResult();
		QuerySPPlansRequestDTO requestDto = new QuerySPPlansRequestDTO();
		requestDto.setSeatCount(seatCount);
		requestDto.setAge(age);
		requestDto.setCompanyCode("5000");
		Map map = (Map) redisUtils.get(Constants.SYS_SERVICE_INFO);
		SysServiceInfo sysServiceInfo = (SysServiceInfo) map.get(CoreServiceEnum.Cross_Sale_Plan.getUrlKey());
		requestDto.setPartnerAccountCode(sysServiceInfo.getUserName()); // LBISLE
		requestDto.setPassKey(sysServiceInfo.getPassword()); // E0EE5F4991879A8556FB79FA4BB0FE64
		requestDto.setOperatorDate(DateUtil.getStringDate()); // 2018-01-12 10:37:42
		requestDto.setRecordCode("CXPTA2016061401");
		requestDto.setAgreementCode(agreementCode);
		// requestDto.setAgreementNo(sysServiceInfo.getTocken()); // CHQ001697
		requestDto.setPdk("8f250d4de892b51ebaceb5840a69a3f5");
		requestDto.setAccess_token("salePlat-lbi");
		requestDto.setFlowId(sysServiceInfo.getUserName() + DateUtil.getStringDateYyMMdd() + UUID.getShortUuid());
		HttpEntity<QuerySPPlansRequestDTO> requestEntity = new HttpEntity<QuerySPPlansRequestDTO>(requestDto,
				RequestUtils.genEncryptHttpHeaders(sysServiceInfo.getUserName(), sysServiceInfo.getPassword()));
		QueryPlansResponseDTO response = new QueryPlansResponseDTO();
		response = restTemplate.postForObject(sysServiceInfo.getUrl(), requestEntity, QueryPlansResponseDTO.class);
		if (response.getStatus() != null && response.getStatus()) {
			sr.setSuccess();
			sr.setResult(response.getPlanLists());
		} else {
			sr.setFail();
			sr.setResult(response.getResultMessage());
		}
		return sr;
	}

	@Override
	public ServiceResult CrossSalePlanSave(QuerySPPlansRequestDTO requestDto) {
		ServiceResult sr = new ServiceResult();
		requestDto.setCompanyCode("5000");
		Map map = (Map) redisUtils.get(Constants.SYS_SERVICE_INFO);
		SysServiceInfo sysServiceInfo = (SysServiceInfo) map.get(CoreServiceEnum.Cross_Sale_Plan_Save.getUrlKey());
		requestDto.setPartnerAccountCode(sysServiceInfo.getUserName()); // LBISLE
		requestDto.setPassKey(sysServiceInfo.getPassword()); // E0EE5F4991879A8556FB79FA4BB0FE64
		requestDto.setOperatorDate(DateUtil.getStringDate()); // 2018-01-12 10:37:42
		requestDto.setRecordCode("CXPTA2016061401");
		// requestDto.setAgreementCode(sysServiceInfo.getTocken());
		// requestDto.setAgreementNo(sysServiceInfo.getTocken()); // CHQ001697
		requestDto.setPdk("8f250d4de892b51ebaceb5840a69a3f5");
		requestDto.setAccess_token("salePlat-lbi");
		requestDto.setFlowId(sysServiceInfo.getUserName() + DateUtil.getStringDateYyMMdd() + UUID.getShortUuid());
		TbSpSaleLog saleLog = new TbSpSaleLog();
		saleLog.setOperation(sysServiceInfo.getDescription());
		// saleLog.setMark(sysServiceInfo.getUrl());
		saleLog.setRequestTime(new Date());
		QueryPlansResponseDTO response = new QueryPlansResponseDTO();
		try {
			HttpEntity<QuerySPPlansRequestDTO> requestEntity = new HttpEntity<QuerySPPlansRequestDTO>(requestDto,
					RequestUtils.genEncryptHttpHeaders(sysServiceInfo.getUserName(), sysServiceInfo.getPassword()));
			saleLog.setRequestData(JSON.toJSONString(requestEntity));
			response = restTemplate.postForObject(sysServiceInfo.getUrl(), requestEntity, QueryPlansResponseDTO.class);
		} catch (Exception e) {
			sr.setFail();
			saleLog.setResponseData(e.toString());
			spSaleLogDao.save(saleLog);
			return sr;
		}
		saleLog.setResponseData(JSON.toJSONString(response));
		spSaleLogDao.save(saleLog);
		if (response.getStatus() != null && response.getStatus()) {
			sr.setSuccess();
			sr.setResult(response.getPlanLists());
		} else {
			sr.setFail();
			sr.setResult(response.getResultMessage());
		}
		return sr;
	}

	@Override
	public ServiceResult CrossSalePlanFind(String documentNo, String agreementCode) {
		QuerySPPlansRequestDTO requestDto = new QuerySPPlansRequestDTO();
		ServiceResult sr = new ServiceResult();
		requestDto.setCompanyCode("5000");
		Map map = (Map) redisUtils.get(Constants.SYS_SERVICE_INFO);
		SysServiceInfo sysServiceInfo = (SysServiceInfo) map.get(CoreServiceEnum.Cross_Sale_Plan_Find.getUrlKey());
		requestDto.setPartnerAccountCode(sysServiceInfo.getUserName()); // LBISLE
		requestDto.setPassKey(sysServiceInfo.getPassword()); // E0EE5F4991879A8556FB79FA4BB0FE64
		requestDto.setOperatorDate(DateUtil.getStringDate()); // 2018-01-12 10:37:42
		requestDto.setRecordCode("CXPTA2016061401");
		requestDto.setAgreementCode(sysServiceInfo.getTocken());
		// requestDto.setAgreementNo(sysServiceInfo.getTocken()); // CHQ001697
		requestDto.setPdk("8f250d4de892b51ebaceb5840a69a3f5");
		requestDto.setAccess_token("salePlat-lbi");
		requestDto.setFlowId(sysServiceInfo.getUserName() + DateUtil.getStringDateYyMMdd() + UUID.getShortUuid());
		requestDto.setDocumentNo(documentNo);
		TbSpSaleLog saleLog = new TbSpSaleLog();
		saleLog.setOperation(sysServiceInfo.getDescription());
		// saleLog.setMark(sysServiceInfo.getUrl());
		saleLog.setRequestTime(new Date());
		QueryPlansResponseDTO response = new QueryPlansResponseDTO();
		try {
			HttpEntity<QuerySPPlansRequestDTO> requestEntity = new HttpEntity<QuerySPPlansRequestDTO>(requestDto,
					RequestUtils.genEncryptHttpHeaders(sysServiceInfo.getUserName(), sysServiceInfo.getPassword()));
			saleLog.setRequestData(JSON.toJSONString(requestEntity));
			response = restTemplate.postForObject(sysServiceInfo.getUrl(), requestEntity, QueryPlansResponseDTO.class);
		} catch (Exception e) {
			sr.setFail();
			saleLog.setResponseData(e.toString());
			spSaleLogDao.save(saleLog);
			return sr;
		}
		saleLog.setResponseData(JSON.toJSONString(response));
		spSaleLogDao.save(saleLog);
		if (response.getStatus() != null && response.getStatus()) {
			sr.setSuccess();
			sr.setResult(response.getPlanLists());
		} else {
			sr.setFail();
			sr.setResult(response.getResultMessage());
		}
		return sr;
	}

	@Override
	public ServiceResult Verify(TVerifyRequstDTO tVerifyRequstDTO) {
		ServiceResult sr = new ServiceResult();
		// Object obj = new Object();
		Map map = (Map) redisUtils.get(Constants.SYS_SERVICE_INFO);
		SysServiceInfo sysServiceInfo = (SysServiceInfo) map.get(CoreServiceEnum.VERIFY.getUrlKey());
		tVerifyRequstDTO.setPartnerAccountCode(sysServiceInfo.getUserName()); // LBISLE
		tVerifyRequstDTO.setPassKey(sysServiceInfo.getPassword()); // E0EE5F4991879A8556FB79FA4BB0FE64
		// tVerifyRequstDTO.setFlowId(sysServiceInfo.getUserName() +
		// DateUtil.getStringDateYyMMdd() + UUID.getShortUuid()); //
		// LBISLE180112ZyIKAtwM
		tVerifyRequstDTO.setOperatorDate(DateUtil.getStringDate()); // 2018-01-12 10:37:42
		tVerifyRequstDTO.setRecordCode("CXPTA2016061401");
		// tMotorPremiumRequestDTO.setAgreementNo(sysServiceInfo.getTocken()); //
		// CHQ001697
		// tVerifyRequstDTO.setAgreementNo(insureQuery.getAgreementCode());
		tVerifyRequstDTO.setPdk("8f250d4de892b51ebaceb5840a69a3f5");
		tVerifyRequstDTO.setAccess_token("salePlat-lbi");
		// venDto.setVinNo("LDC703N23A0180839");
		HttpEntity<TVerifyRequstDTO> requestEntity = null;
		TbSpSaleLog saleLog = new TbSpSaleLog();
		saleLog.setOperation(sysServiceInfo.getDescription());
		saleLog.setRequestTime(new Date());
		try {
			requestEntity = new HttpEntity<TVerifyRequstDTO>(tVerifyRequstDTO,
					RequestUtils.genEncryptHttpHeaders(sysServiceInfo.getUserName(), sysServiceInfo.getPassword()));
			saleLog.setRequestData(BeanUtilExt.toJsonString(requestEntity));
			sr = restTemplate.postForObject(sysServiceInfo.getUrl(), requestEntity, ServiceResult.class);
		} catch (Exception e) {
			saleLog.setResponseData(e.toString());
			spSaleLogDao.save(saleLog);
			sr.setFail();
			return sr;
		}
		saleLog.setResponseData(BeanUtilExt.toJsonString(sr));
		spSaleLogDao.save(saleLog);
		return sr;
	}

}
