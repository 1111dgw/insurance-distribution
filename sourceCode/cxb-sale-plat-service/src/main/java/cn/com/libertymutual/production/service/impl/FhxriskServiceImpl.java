package cn.com.libertymutual.production.service.impl;

import cn.com.libertymutual.production.dao.nomorcldatasource.FhxriskMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.FhxriskExample;
import cn.com.libertymutual.production.model.nomorcldatasource.FhxriskKey;
import cn.com.libertymutual.production.service.api.FhxriskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by steven.li on 2017/12/18.
 */
@Service
public class FhxriskServiceImpl implements FhxriskService {

    @Autowired
    private FhxriskMapper fhxriskMapper;

    @Override
    public void insert(FhxriskKey record) {
        fhxriskMapper.insertSelective(record);
    }

    @Override
    public List<FhxriskKey> findAll() {
        FhxriskExample criteria = new FhxriskExample();
        return fhxriskMapper.selectByExample(criteria);
    }

    @Override
    public List<FhxriskKey> findByRisk(String riskcode) {
        FhxriskExample criteria = new FhxriskExample();
        criteria.createCriteria().andRiskcodeEqualTo(riskcode);
        return fhxriskMapper.selectByExample(criteria);
    }
}
