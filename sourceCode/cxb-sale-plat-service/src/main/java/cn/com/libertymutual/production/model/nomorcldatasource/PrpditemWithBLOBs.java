package cn.com.libertymutual.production.model.nomorcldatasource;

public class PrpditemWithBLOBs extends Prpditem {
    private String itemcname;

    private String itemename;

    public String getItemcname() {
        return itemcname;
    }

    public void setItemcname(String itemcname) {
        this.itemcname = itemcname == null ? null : itemcname.trim();
    }

    public String getItemename() {
        return itemename;
    }

    public void setItemename(String itemename) {
        this.itemename = itemename == null ? null : itemename.trim();
    }
}