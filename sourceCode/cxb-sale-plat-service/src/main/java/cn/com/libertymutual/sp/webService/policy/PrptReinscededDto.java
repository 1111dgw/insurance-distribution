
package cn.com.libertymutual.sp.webService.policy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for prptReinscededDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prptReinscededDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="businessType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="coSumClaim" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="coinsName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="commSignedLine" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="DICDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="exchangeFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="inDisPremium" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="oriAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="oriCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="oriPremium" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="originalPolicyNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="reinsCededType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="reinsCiCharges" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="reinsFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="reinsMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="signedLine" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="slipNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tax" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="taxRate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prptReinscededDto", propOrder = {
    "businessType",
    "coSumClaim",
    "coinsName",
    "commSignedLine",
    "dicDesc",
    "exchangeFlag",
    "inAmount",
    "inDisPremium",
    "oriAmount",
    "oriCurrency",
    "oriPremium",
    "originalPolicyNo",
    "reinsCededType",
    "reinsCiCharges",
    "reinsFlag",
    "reinsMode",
    "signedLine",
    "slipNo",
    "tax",
    "taxRate"
})
public class PrptReinscededDto implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 8112136396068187902L;
	protected String businessType;
    protected double coSumClaim;
    protected String coinsName;
    protected double commSignedLine;
    @XmlElement(name = "DICDesc")
    protected String dicDesc;
    protected String exchangeFlag;
    protected double inAmount;
    protected double inDisPremium;
    protected double oriAmount;
    protected String oriCurrency;
    protected double oriPremium;
    protected String originalPolicyNo;
    protected String reinsCededType;
    protected double reinsCiCharges;
    protected String reinsFlag;
    protected String reinsMode;
    protected String signedLine;
    protected String slipNo;
    protected double tax;
    protected String taxRate;

    /**
     * Gets the value of the businessType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessType() {
        return businessType;
    }

    /**
     * Sets the value of the businessType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessType(String value) {
        this.businessType = value;
    }

    /**
     * Gets the value of the coSumClaim property.
     * 
     */
    public double getCoSumClaim() {
        return coSumClaim;
    }

    /**
     * Sets the value of the coSumClaim property.
     * 
     */
    public void setCoSumClaim(double value) {
        this.coSumClaim = value;
    }

    /**
     * Gets the value of the coinsName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoinsName() {
        return coinsName;
    }

    /**
     * Sets the value of the coinsName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoinsName(String value) {
        this.coinsName = value;
    }

    /**
     * Gets the value of the commSignedLine property.
     * 
     */
    public double getCommSignedLine() {
        return commSignedLine;
    }

    /**
     * Sets the value of the commSignedLine property.
     * 
     */
    public void setCommSignedLine(double value) {
        this.commSignedLine = value;
    }

    /**
     * Gets the value of the dicDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDICDesc() {
        return dicDesc;
    }

    /**
     * Sets the value of the dicDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDICDesc(String value) {
        this.dicDesc = value;
    }

    /**
     * Gets the value of the exchangeFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExchangeFlag() {
        return exchangeFlag;
    }

    /**
     * Sets the value of the exchangeFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExchangeFlag(String value) {
        this.exchangeFlag = value;
    }

    /**
     * Gets the value of the inAmount property.
     * 
     */
    public double getInAmount() {
        return inAmount;
    }

    /**
     * Sets the value of the inAmount property.
     * 
     */
    public void setInAmount(double value) {
        this.inAmount = value;
    }

    /**
     * Gets the value of the inDisPremium property.
     * 
     */
    public double getInDisPremium() {
        return inDisPremium;
    }

    /**
     * Sets the value of the inDisPremium property.
     * 
     */
    public void setInDisPremium(double value) {
        this.inDisPremium = value;
    }

    /**
     * Gets the value of the oriAmount property.
     * 
     */
    public double getOriAmount() {
        return oriAmount;
    }

    /**
     * Sets the value of the oriAmount property.
     * 
     */
    public void setOriAmount(double value) {
        this.oriAmount = value;
    }

    /**
     * Gets the value of the oriCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriCurrency() {
        return oriCurrency;
    }

    /**
     * Sets the value of the oriCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriCurrency(String value) {
        this.oriCurrency = value;
    }

    /**
     * Gets the value of the oriPremium property.
     * 
     */
    public double getOriPremium() {
        return oriPremium;
    }

    /**
     * Sets the value of the oriPremium property.
     * 
     */
    public void setOriPremium(double value) {
        this.oriPremium = value;
    }

    /**
     * Gets the value of the originalPolicyNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginalPolicyNo() {
        return originalPolicyNo;
    }

    /**
     * Sets the value of the originalPolicyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginalPolicyNo(String value) {
        this.originalPolicyNo = value;
    }

    /**
     * Gets the value of the reinsCededType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReinsCededType() {
        return reinsCededType;
    }

    /**
     * Sets the value of the reinsCededType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReinsCededType(String value) {
        this.reinsCededType = value;
    }

    /**
     * Gets the value of the reinsCiCharges property.
     * 
     */
    public double getReinsCiCharges() {
        return reinsCiCharges;
    }

    /**
     * Sets the value of the reinsCiCharges property.
     * 
     */
    public void setReinsCiCharges(double value) {
        this.reinsCiCharges = value;
    }

    /**
     * Gets the value of the reinsFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReinsFlag() {
        return reinsFlag;
    }

    /**
     * Sets the value of the reinsFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReinsFlag(String value) {
        this.reinsFlag = value;
    }

    /**
     * Gets the value of the reinsMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReinsMode() {
        return reinsMode;
    }

    /**
     * Sets the value of the reinsMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReinsMode(String value) {
        this.reinsMode = value;
    }

    /**
     * Gets the value of the signedLine property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSignedLine() {
        return signedLine;
    }

    /**
     * Sets the value of the signedLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSignedLine(String value) {
        this.signedLine = value;
    }

    /**
     * Gets the value of the slipNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSlipNo() {
        return slipNo;
    }

    /**
     * Sets the value of the slipNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSlipNo(String value) {
        this.slipNo = value;
    }

    /**
     * Gets the value of the tax property.
     * 
     */
    public double getTax() {
        return tax;
    }

    /**
     * Sets the value of the tax property.
     * 
     */
    public void setTax(double value) {
        this.tax = value;
    }

    /**
     * Gets the value of the taxRate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxRate() {
        return taxRate;
    }

    /**
     * Sets the value of the taxRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxRate(String value) {
        this.taxRate = value;
    }

}
