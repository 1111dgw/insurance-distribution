package cn.com.libertymutual.sp.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpInputConfig;
import cn.com.libertymutual.sp.bean.TbSpQuestionnaire;
import cn.com.libertymutual.sp.service.api.QuestionnaireService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/admin/question")
public class QuestionnaireControllerWeb {

	@Autowired
	private QuestionnaireService questionnaireService;
	
	
	@ApiOperation(value = "问卷列表", notes = "问卷列表")
	@PostMapping(value = "/questionList")
	@ApiImplicitParams(value = { 
		@ApiImplicitParam(name = "title", value = "标题", required = false, paramType = "query", dataType = "String"),
		@ApiImplicitParam(name = "type", value = "类型", required = false, paramType = "query", dataType = "String"), 
	})
	public ServiceResult questionList(String title, String type) {
		ServiceResult sr = new ServiceResult();

		sr = questionnaireService.questionList(title, type);
		return sr;

	}

	
	
	@ApiOperation(value = "新增问卷", notes = "新增问卷")
	@PostMapping(value = "/addQuestionnaire")
	public ServiceResult addQuestionnaire(@RequestBody TbSpQuestionnaire questionnaire) {
		ServiceResult sr = new ServiceResult();

		try {
			sr = questionnaireService.addOrUpdateQuestionnaire(questionnaire);
		} catch (Exception e) {
			sr.setFail();
			sr.setResult("创建问卷失败！");
			e.printStackTrace();
		}
		return sr;

	}

	
	@ApiOperation(value = "自定义选项列表", notes = "自定义选项列表")
	@PostMapping(value = "/petConfigList")
	public ServiceResult petConfigList() {
		ServiceResult sr = new ServiceResult();

		sr = questionnaireService.petConfigList();
		return sr;

	}

	
	
	@ApiOperation(value = "新增自定义选项", notes = "新增自定义选项")
	@PostMapping(value = "/addPetConfig")
	public ServiceResult addPetConfig(@RequestBody TbSpInputConfig inputConfig) {
		ServiceResult sr = new ServiceResult();

		try {
			sr = questionnaireService.addPetConfig(inputConfig);
		} catch (Exception e) {
			sr.setFail();
			sr.setResult("新增自定义选项失败！");
			e.printStackTrace();
		}
		return sr;

	}
	
	
	
	
}
