package cn.com.libertymutual.sp.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;

import cn.com.libertymutual.core.banks.BankCardBin;
import cn.com.libertymutual.core.redis.util.RedisUtils;
import cn.com.libertymutual.core.util.Constants;
import cn.com.libertymutual.core.util.PinYinUtils;
import cn.com.libertymutual.sp.bean.TbSpBankCard;
import cn.com.libertymutual.sp.dao.BankCardDao;
import cn.com.libertymutual.sp.service.api.BankCardService;

@Service()
public class BankCardServiceImpl implements BankCardService {

	private Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	private BankCardDao bankCardDao;
	@Resource
	private RedisUtils redis;

	@Override
	public TbSpBankCard getBankCard(String charNo) {
		if (StringUtils.isNotBlank(charNo) && charNo.length() > 6) {
			// 全部银行列表
			List<TbSpBankCard> cards = findAllBank();
			// 尾号
			String lastNo = charNo.substring(0, 6);
			for (TbSpBankCard card : cards) {
				// 匹配
				if (lastNo.equals(card.getCardLastNo())) {
					return card;
				}
			}
		}
		log.warn("号码不足6位");
		return null;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<TbSpBankCard> findAllBank() {

		List<TbSpBankCard> cards = null;

		Object obj = redis.get(Constants.BANK_LIST_DBMS);
		if (obj != null) {
			cards = (List<TbSpBankCard>) obj;
			return cards;
		}

		cards = bankCardDao.findAll();
		if (CollectionUtils.isNotEmpty(cards)) {
			// 缓存到redis一个星期
			redis.setWithExpireTime(Constants.BANK_LIST_DBMS, cards, 7 * 24 * 60 * 60);
			return cards;
		} else {
			log.warn("查询银行卡列表为空");
		}
		return cards;
	}

	@Override
	public List<String> findAllBankNameByNotRepeat() {
		// set去重
		Set<String> set = new HashSet<String>();
		List<String> newLists = new ArrayList<>();
		for (TbSpBankCard card : this.findAllBank()) {
			if (StringUtils.isNotBlank(card.getBankName()) && set.add(card.getBankName())) {// add到set时返回是否已存在该字符串,true=不存在
				newLists.add(card.getBankName());
			}
		}
		return newLists;
	}

	@Override
	public void saveBanks() {
		TbSpBankCard bankCard = null;
		int[] bankBins = BankCardBin.bankBin;
		String[] bankNames = BankCardBin.bankName;
		for (int i = 0; i < bankBins.length; i++) {
			bankCard = new TbSpBankCard();
			bankCard.setCardLastNo(bankBins[i] + "");

			String cardName = bankNames[i];
			if (StringUtils.isNotBlank(cardName)) {
				if (cardName.indexOf(".") >= 0) {
					if (cardName.indexOf("发现卡") >= 0) {
						// 截取银行名称和卡种名称
						String sname = cardName.substring(0, cardName.lastIndexOf("."));
						bankCard.setBankName(sname);
						bankCard.setBankNamePinYin(PinYinUtils.ToPinyinByBank(sname));

						String ename = cardName.substring(cardName.lastIndexOf(".") + 1, cardName.length());
						bankCard.setCardTypeName(ename);
						bankCard.setCardTypeNamePinYin(PinYinUtils.ToPinyinByBank(ename));
					} else {
						// 截取银行名称和卡种名称
						String sname = cardName.substring(0, cardName.indexOf("."));
						bankCard.setBankName(sname);
						bankCard.setBankNamePinYin(PinYinUtils.ToPinyinByBank(sname));

						String ename = cardName.substring(cardName.indexOf(".") + 1, cardName.length());
						bankCard.setCardTypeName(ename);
						bankCard.setCardTypeNamePinYin(PinYinUtils.ToPinyinByBank(ename));
					}
				} else {
					bankCard.setBankName(cardName);
					bankCard.setBankNamePinYin(PinYinUtils.ToPinyinByBank(cardName));
					System.out.println("-------------------没有分割的有[i=" + i + "]，cardName=" + cardName);
				}
			}
			int repeatNo = bankCardDao.countByFiveField(bankCard.getCardLastNo(), bankCard.getBankName(), bankCard.getBankNamePinYin(),
					bankCard.getCardTypeName(), bankCard.getCardTypeNamePinYin());
			if (repeatNo <= 0 && StringUtils.isNotBlank(bankCard.getBankName())) {
				bankCardDao.save(bankCard);
			} else {
				System.out.println("@@@@@@@@@@@@@@已有该记录的重复数据：" + JSON.toJSONString(bankCard));
			}
			System.out.println("以保存[" + (i + 1) + "]条");
		}
	}

}
