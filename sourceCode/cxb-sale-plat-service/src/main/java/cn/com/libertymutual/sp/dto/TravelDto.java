package cn.com.libertymutual.sp.dto;

public class TravelDto {
	private String cname;
	private String code;
	private String ename;
	private Boolean schengen;

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	public Boolean getSchengen() {
		return schengen;
	}

	public void setSchengen(Boolean schengen) {
		this.schengen = schengen;
	}

}
