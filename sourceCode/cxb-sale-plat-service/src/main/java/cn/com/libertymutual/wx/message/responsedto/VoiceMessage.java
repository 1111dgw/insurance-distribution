package cn.com.libertymutual.wx.message.responsedto;

import java.util.Calendar;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import cn.com.libertymutual.wx.common.MessageType;

public class VoiceMessage extends ResponseBaseMessage {
	@XStreamAlias("MediaId")
	private String mediaId;
	@XStreamAlias("Format")
	private String format;

	public VoiceMessage() {
		setMsgType(MessageType.REQ_MESSAGE_TYPE_VOICE);
	}

	public VoiceMessage(ResponseBaseMessage rbm) {
		long ct = Calendar.getInstance().getTimeInMillis();
		setCreateTime(ct);
		setFromUserName(rbm.getFromUserName());
		setToUserName(rbm.getToUserName());
		setMsgType(MessageType.REQ_MESSAGE_TYPE_VOICE);
	}

	public String getMediaId() {
		return this.mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getFormat() {
		return this.format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	@Override
	public String toString() {
		return "VoiceMessage [mediaId=" + mediaId + ", format=" + format + ", getMediaId()=" + getMediaId() + ", getFormat()=" + getFormat()
				+ ", getToUserName()=" + getToUserName() + ", getFromUserName()=" + getFromUserName() + ", getCreateTime()=" + getCreateTime()
				+ ", getMsgType()=" + getMsgType() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}
}
