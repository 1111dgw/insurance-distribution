package cn.com.libertymutual.sp.dto.epayment;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.FIELD)
public class WithPayOrder implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5542161146317978671L;
	
	
	private String merchantId;
    private String externalRefNumber;
    private String orderType;
    private String entryChannel;
    private String amount;
    private String pointAmt;
    private String phoneNO;
    private String cardHolderName;
    private String cardHolderId;
    private String bankName;
    private String openBankName;
    private String openCardProvince;
    private String openCardCity;
    private String voucherNo;
    private String mamMembercode;
    private String subStatus;
    private String mainStatus;
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getExternalRefNumber() {
		return externalRefNumber;
	}
	public void setExternalRefNumber(String externalRefNumber) {
		this.externalRefNumber = externalRefNumber;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getEntryChannel() {
		return entryChannel;
	}
	public void setEntryChannel(String entryChannel) {
		this.entryChannel = entryChannel;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getPointAmt() {
		return pointAmt;
	}
	public void setPointAmt(String pointAmt) {
		this.pointAmt = pointAmt;
	}
	public String getPhoneNO() {
		return phoneNO;
	}
	public void setPhoneNO(String phoneNO) {
		this.phoneNO = phoneNO;
	}
	public String getCardHolderName() {
		return cardHolderName;
	}
	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}
	public String getCardHolderId() {
		return cardHolderId;
	}
	public void setCardHolderId(String cardHolderId) {
		this.cardHolderId = cardHolderId;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getOpenBankName() {
		return openBankName;
	}
	public void setOpenBankName(String openBankName) {
		this.openBankName = openBankName;
	}
	public String getOpenCardProvince() {
		return openCardProvince;
	}
	public void setOpenCardProvince(String openCardProvince) {
		this.openCardProvince = openCardProvince;
	}
	public String getOpenCardCity() {
		return openCardCity;
	}
	public void setOpenCardCity(String openCardCity) {
		this.openCardCity = openCardCity;
	}
	public String getVoucherNo() {
		return voucherNo;
	}
	public void setVoucherNo(String voucherNo) {
		this.voucherNo = voucherNo;
	}
	public String getMamMembercode() {
		return mamMembercode;
	}
	public void setMamMembercode(String mamMembercode) {
		this.mamMembercode = mamMembercode;
	}
	public String getSubStatus() {
		return subStatus;
	}
	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}
	public String getMainStatus() {
		return mainStatus;
	}
	public void setMainStatus(String mainStatus) {
		this.mainStatus = mainStatus;
	}
    
    
    
}
