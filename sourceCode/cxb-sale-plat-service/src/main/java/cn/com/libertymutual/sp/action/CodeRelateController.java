package cn.com.libertymutual.sp.action;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.annotation.SystemValidate;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.biz.IdentifierMarkBiz;
import cn.com.libertymutual.sp.service.api.CodeRelateService;

@RestController
@RequestMapping(value = "/nol/codeRelate")
public class CodeRelateController {
	@Autowired
	private CodeRelateService codeRelateService;
	@Resource
	private IdentifierMarkBiz identifierMarkBiz;

	@RequestMapping(value = "/getCodeType")
	public ServiceResult getCodeType(HttpServletRequest request, HttpServletResponse response, String identCode, String codeType) {

		return codeRelateService.getSaleInitData(codeType);
	}

	@RequestMapping(value = "/getSeleCountry")
	@SystemValidate(validate = false, description = "无需校验接口")
	public ServiceResult getSeleCountry(HttpServletRequest request, HttpServletResponse response, String identCode, String planId) {

		return codeRelateService.getSeleCountry(planId);
	}

	@RequestMapping(value = "/getCliaimsType")
	public ServiceResult getCliaimsType(HttpServletRequest request, HttpServletResponse response, String identCode, String code, String language) {

		return codeRelateService.getCliaimsType(code, language);
	}

	@RequestMapping(value = "/getCarlimitNo")
	@SystemValidate(validate = false, description = "无需校验接口")
	public ServiceResult getCarlimitNo(HttpServletRequest request, HttpServletResponse response, String identCode, String code, String language) {
		return codeRelateService.getCarlimitNo();
	}
}
