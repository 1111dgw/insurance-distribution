package cn.com.libertymutual.production.pojo.request;

public class Request {

	/** 当前页 */
	private int currentPage;

	/** 页面条数 */
	private int pageSize;

	/** 排序字段名称 */
	private String orderBy;

	/** 排序规则 */
	private String order;

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

}
