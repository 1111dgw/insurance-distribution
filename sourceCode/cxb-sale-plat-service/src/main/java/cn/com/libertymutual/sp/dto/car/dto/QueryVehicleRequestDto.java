package cn.com.libertymutual.sp.dto.car.dto;

import java.io.Serializable;

import cn.com.libertymutual.core.base.dto.RequestBaseDto;
/**
 * 车型查询请求对象
 * @author Ryan
 *
 */
public class QueryVehicleRequestDto extends RequestBaseDto implements
		Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6781217966566758918L;
	private String	 modelName;	//厂牌型号
	private String pageNum;	//页码
	private String	intPageCount;	//每页查询条数
	private String vinNo;
	private String subQuotationId;
	
	
	
	public String getSubQuotationId() {
		return subQuotationId;
	}
	public void setSubQuotationId(String subQuotationId) {
		this.subQuotationId = subQuotationId;
	}
	public String getVinNo() {
		return vinNo;
	}
	public void setVinNo(String vinNo) {
		this.vinNo = vinNo;
	}
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	public String getPageNum() {
		return pageNum;
	}
	public void setPageNum(String pageNum) {
		this.pageNum = pageNum;
	}
	public String getIntPageCount() {
		return intPageCount;
	}
	public void setIntPageCount(String intPageCount) {
		this.intPageCount = intPageCount;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((intPageCount == null) ? 0 : intPageCount.hashCode());
		result = prime * result
				+ ((modelName == null) ? 0 : modelName.hashCode());
		result = prime * result + ((pageNum == null) ? 0 : pageNum.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		QueryVehicleRequestDto other = (QueryVehicleRequestDto) obj;
		if (intPageCount == null) {
			if (other.intPageCount != null)
				return false;
		} else if (!intPageCount.equals(other.intPageCount))
			return false;
		if (modelName == null) {
			if (other.modelName != null)
				return false;
		} else if (!modelName.equals(other.modelName))
			return false;
		if (pageNum == null) {
			if (other.pageNum != null)
				return false;
		} else if (!pageNum.equals(other.pageNum))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "QueryVehicleRequestDto [modelName=" + modelName + ", pageNum="
				+ pageNum + ", intPageCount=" + intPageCount + "]";
	}
	
}