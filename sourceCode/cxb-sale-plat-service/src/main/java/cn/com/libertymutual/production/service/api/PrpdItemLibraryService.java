package cn.com.libertymutual.production.service.api;


import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.PrpdItemRiskLibrary;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpditemlibrary;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpditemlibraryWithBLOBs;
import cn.com.libertymutual.production.pojo.request.LinkItemRequest;
import cn.com.libertymutual.production.pojo.request.PrpdItemLibraryRequest;
import cn.com.libertymutual.production.pojo.request.PrpdItemRequest;
import cn.com.libertymutual.production.pojo.response.ItemKind;

import com.github.pagehelper.PageInfo;

public interface PrpdItemLibraryService {
	
	/**
	 * 获取已关联险种下的标的责任
	 * @param prpdItemLibraryRequest
	 * @return
	 */
	public PageInfo<PrpdItemRiskLibrary> findPrpdItemWithLinked(PrpdItemLibraryRequest prpdItemLibraryRequest);
	
	public void insert(PrpditemlibraryWithBLOBs prpditemlibrary);
	
	public PageInfo<PrpditemlibraryWithBLOBs> selectByPage(List<String> itemCodes, LinkItemRequest linkItemRequest);
	
	public PageInfo<PrpditemlibraryWithBLOBs> select(PrpdItemRequest prpdItemRequest);
	
	public List<ItemKind> findItemByRiskKind(String riskCode, String riskVersion, String kindCode, String kindVersion);
	
	/**
	 * 责任管理页面查询接口
	 * @param prpdItemRequest
	 * @return
	 */
	public PageInfo<PrpditemlibraryWithBLOBs> selectItem(PrpdItemLibraryRequest request);
	
	/**
	 * 修改责任
	 * @param record
	 */
	public void update(PrpditemlibraryWithBLOBs record);

	public List<PrpditemlibraryWithBLOBs> selectTopItems();

	public PageInfo<ItemKind> selectWithItemKind(LinkItemRequest linkItemRequest);
}
