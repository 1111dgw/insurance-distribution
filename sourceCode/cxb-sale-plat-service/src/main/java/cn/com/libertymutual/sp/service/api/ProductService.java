package cn.com.libertymutual.sp.service.api;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpHotproduct;
import cn.com.libertymutual.sp.bean.TbSpProduct;
import cn.com.libertymutual.sp.dto.PlanDto;
import cn.com.libertymutual.sp.dto.queryplans.QueryPlansRequestDTO;
import cn.com.libertymutual.sp.dto.queryplans.QueryPlansResponseDTO;
import cn.com.libertymutual.sp.dto.savePlan.PropsalSaveRequestDto;

public interface ProductService {

	ServiceResult productFindAll(String region, String type, int pageNumber, int pageSize, String sorttype);

	//ServiceResult queryPlans(String productId, Integer age, Integer deadLine);

	ServiceResult queryPlan(String planId, Integer age, Integer deadLine);

	ServiceResult addProduct(TbSpProduct tbSpProduct);

	ServiceResult channelProductList(String userCode,Integer pageNumber, Integer pageSize);
	
	ServiceResult ProductList(String riskCode, String status, String branchCode, String productEname, String startDate, String endDate,
			Integer pageNumber, Integer pageSize);

	ServiceResult updateProduct(TbSpProduct tbSpProduct);

	ServiceResult removeProduct(String[] list);

	PropsalSaveRequestDto findRate(PropsalSaveRequestDto requestDto, boolean isAxtx);

	ServiceResult savePlan(PropsalSaveRequestDto request);

	ServiceResult querySalePlan(String productId, String userCode);

	List<String> riskCodeDistinct();

	ServiceResult setHot(TbSpHotproduct tbSpHotproduct);

	ServiceResult getHotProduct(Integer productId, String branchCode);

	ServiceResult hotProductList(String branchCode, Integer pageNumber, Integer pageSize);

	ServiceResult removeHotProduct(String branchCode, String[] list);

	ServiceResult codeNodeList(String type);

	ServiceResult addToAreaHot(String branchCode, String[] listProductId);

	ServiceResult branchHotProductList(String branchCode, Integer pageNumber, Integer pageSize);

	ServiceResult updateProductSerialNo(Integer id, Integer serialNo);

	ServiceResult findAreaInfoByName(String region);

	ServiceResult findById(Integer id, String refereeId, HttpServletRequest request, String userCode);

	ServiceResult addPlans(TbSpProduct tbSpProduct);

	ServiceResult planList(Integer productId);

	ServiceResult rebackProduct(String[] list);

	ServiceResult productByRisk(String riskCode);

	ServiceResult getExclusive(Integer productId, String userCode, String userName, String branchCode, String agrementNo, String isIn, int pageNumber,
			int pageSize);

	ServiceResult setExclusive(Integer productId, String isExclusive, String[] codes);
	public QueryPlansResponseDTO queryPlansByProd(QueryPlansRequestDTO dto);
	public QueryPlansResponseDTO queryPlansByPlanCode(PlanDto planDto);
}
