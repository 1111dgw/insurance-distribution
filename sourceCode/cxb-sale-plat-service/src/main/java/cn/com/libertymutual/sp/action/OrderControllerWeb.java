package cn.com.libertymutual.sp.action;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.util.Constants;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpApplicant;
import cn.com.libertymutual.sp.bean.TbSpArticle;
import cn.com.libertymutual.sp.bean.TbSpOrder;
import cn.com.libertymutual.sp.service.api.IOrderService;
import cn.com.libertymutual.sp.service.api.PayService;
import cn.com.libertymutual.sp.service.api.ShopService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = "/admin/orderWeb")
public class OrderControllerWeb {
	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired IOrderService orderService;
	@Autowired
	private PayService payService;
	@Autowired
	private ShopService shopService;
	
	@ApiOperation(value = "后台分类查询保单列表", notes = "后台分类查询保单列表")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "comCode", value = "归属", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "insuranceDateStart", value = "投保开始日期", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "insuranceDateEnd", value = "投保结束日期", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "payDateStart", value = "支付开始日期", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "payDateEnd", value = "支付结束日期", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "createTimeStart", value = "创建开始日期", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "createTimeEnd", value = "创建结束日期", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "policyNo", value = "保单号", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "proposalNo", value = "投保单号", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "productName", value = "产品名称", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "planName", value = "计划名称", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "applicantName", value = "投保人姓名", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "idNo", value = "投保人身份证号", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "insuredPersonName", value = "被保人姓名", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "insuredPersonNo", value = "被保人身份证号", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "paymentNo", value = "支付订单号", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "branchCode", value = "分公司", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "agreementNo", value = "业务关系代码", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "status", value = "状态", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "refereeId", value = "推荐人", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "refereeMobile", value = "推荐人手机号", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "userName", value = "用户名", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobile", value = "手机号", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "scoreUserName", value = "积分用户", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "scoreUserMobile", value = "积分用户手机号", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "successfulUrl", value = "成功返回路径", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "shareUid", value = "分享标识", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "callBackUrl", value = "返回路径", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "orderNo", value = "订单号", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "pageNumber", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "pageSize", required = true, paramType = "query", dataType = "Long"),
	})
	@PostMapping("/orderList")	
    public ServiceResult orderList(String comCode,String insuranceDateStart,String insuranceDateEnd,String payDateStart,String payDateEnd,String createTimeStart,String createTimeEnd,String policyNo,
    		String proposalNo,String productName,String planName,
    		String applicantName,String idNo,String insuredPersonName,String insuredPersonNo,String paymentNo,String branchCode,String agreementNo,String status,
    		String refereeId,String refereeMobile,String userName,String mobile,String scoreUserName,String scoreUserMobile,String successfulUrl,String shareUid,String callBackUrl,String orderNo,String dealUuid,int pageNumber,int pageSize){
		ServiceResult sr = new ServiceResult();
		sr = orderService.orderList(comCode,insuranceDateStart,insuranceDateEnd,payDateStart,payDateEnd,createTimeStart,createTimeEnd,policyNo,
	    		proposalNo,productName,planName,applicantName,idNo,insuredPersonName,insuredPersonNo,paymentNo,branchCode,agreementNo,status,refereeId,refereeMobile,userName,mobile,scoreUserName,scoreUserMobile,successfulUrl,shareUid,callBackUrl,orderNo,dealUuid,pageNumber,pageSize);
		return sr;		
	}
	
	
	/*
	 * 统一sql查询
	 */
	@ApiOperation(value = "统一sql查询", notes = "统一sql查询")
	@ApiImplicitParams(value = {
		@ApiImplicitParam(name = "sql", value = "投保开始日期", required = true, paramType = "query", dataType = "String"),
	})
	@PostMapping("/findBySql")	
    public ServiceResult findBySql(String sql){
		ServiceResult sr = new ServiceResult();
		sr = orderService.findBySql(sql);
		return sr;		
	}
	
	
	@ApiOperation(value = "", notes = "")
	@PostMapping("/upOrder")	
    public ServiceResult upOrder(@RequestBody @ApiParam(name = "tbSpOrder", value = "tbSpOrder", required = true)TbSpOrder tbSpOrder){
		ServiceResult sr = new ServiceResult();
		sr = orderService.upOrder(tbSpOrder);
		return sr;		
	}
	
	
	/*
	 * 保单信息查询
	 */
	@ApiOperation(value = "保单信息查询", notes = "保单信息查询")
	@ApiImplicitParams(value = {
		@ApiImplicitParam(name = "orderNo", value = "订单号", required = false, paramType = "query", dataType = "String"),
		@ApiImplicitParam(name = "name", value = "名称", required = false, paramType = "query", dataType = "String"),
		@ApiImplicitParam(name = "mobile", value = "电话号码", required = false, paramType = "query", dataType = "String"),
		@ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "String"),
		@ApiImplicitParam(name = "pageSize", value = "每页条数", required = true, paramType = "query", dataType = "String"),
	})
	@PostMapping("/policyInfo")	
    public ServiceResult findPolicyInfo(String orderNo,String name,String mobile,int pageNumber,int pageSize){
		ServiceResult sr = new ServiceResult();
		sr = orderService.findPolicyInfo(orderNo,name,mobile,pageNumber,pageSize);
		return sr;		
	}
	
	/*
	 * 新增保单
	 */
	@ApiOperation(value = "新增保单", notes = "新增保单")
	@PostMapping("/addPolicy")	
    public ServiceResult findPolicyInfo(@RequestBody TbSpApplicant applicant){
		ServiceResult sr = new ServiceResult();
		sr = orderService.addPolicy(applicant);
		return sr;		
	}
	
	@RequestMapping(value = "/callBackUrl" , method = RequestMethod.POST)
	public ServiceResult callbackUrl(String documentNo,Boolean isJump,HttpServletResponse response) {
		return payService.callbackUrl(documentNo,isJump,response);
	}
	
	
	@ApiOperation(value = "查询所有常驻地区", notes = "测试")
	@RequestMapping(value = "/findAllHotArea")
	public ServiceResult findAllHotArea() {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_EXCEPTION);
		try {
			return shopService.findAllHotArea();
		} catch (Exception e) {
			log.warn("查询所有地区异常:" + e.toString());
		}
		return sr;
	}
	
}
