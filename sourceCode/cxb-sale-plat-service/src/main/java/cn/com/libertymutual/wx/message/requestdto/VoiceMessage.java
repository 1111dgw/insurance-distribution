package cn.com.libertymutual.wx.message.requestdto;

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class VoiceMessage extends RequestBaseMessage {
	@XStreamAlias("MediaId")
	private String mediaId;
	@XStreamAlias("Format")
	private String format;

	public String getMediaId() {
		return this.mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getFormat() {
		return this.format;
	}

	public void setFormat(String format) {
		this.format = format;
	}
}
