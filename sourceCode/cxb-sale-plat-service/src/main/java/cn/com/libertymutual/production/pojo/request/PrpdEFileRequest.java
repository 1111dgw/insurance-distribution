package cn.com.libertymutual.production.pojo.request;

import java.util.Date;

public class PrpdEFileRequest extends Request {
	private String matchlevel;

	private String efilecname;

	private String efileename;

	private String plancode;

	private String planname;

	private Date createdate;

	private String riskname;

	private String kindcname;

	private Date startdate;

	private Date enddate;

	private String flag;

	private String validatestatus;

	private String attribute1;

	private String attribute2;

	private String attribute3;

	private String attribute4;

	private String attribute5;

	private String attribute6;

	private String channel;

	private String submitcom;

	private String productname;

	private String submittype;

	private String devtype;

	private String producttype;

	private String managetype;

	private String risktype;

	private String kindtype;

	private Date opendate;

	private Date approvaldate;

	private String busscope;

	private String busarea;

	private String docnum;

	private Date year;

	private String productnature;

	private String rate;

	private String raterange;

	private String ratecoefficient;

	private String hasepolicy;

	private String period;

	private String deductible;

	private String reldeductibe;

	private String insliab;

	private String remark1;

	private String remark2;

	private String linkname;

	private String linknum;

	private String productattr;

	private String signname;

	private String legalname;

	private String actuarialname;

	private String salepromname;

	private String salearea;

	private String salechannel;

	private String otherinfo;

	private String remark3;

	private String efilecode;

	private String riskcode;

	private String riskversion;

	private String kindcode;

	private String kindversion;

	/** 险别代码 */
	private String classcode;

	public String getMatchlevel() {
		return matchlevel;
	}

	public void setMatchlevel(String matchlevel) {
		this.matchlevel = matchlevel;
	}

	public String getEfilecname() {
		return efilecname;
	}

	public void setEfilecname(String efilecname) {
		this.efilecname = efilecname;
	}

	public String getEfileename() {
		return efileename;
	}

	public void setEfileename(String efileename) {
		this.efileename = efileename;
	}

	public String getPlancode() {
		return plancode;
	}

	public void setPlancode(String plancode) {
		this.plancode = plancode;
	}

	public String getPlanname() {
		return planname;
	}

	public void setPlanname(String planname) {
		this.planname = planname;
	}

	public String getRiskname() {
		return riskname;
	}

	public void setRiskname(String riskname) {
		this.riskname = riskname;
	}

	public String getKindcname() {
		return kindcname;
	}

	public void setKindcname(String kindcname) {
		this.kindcname = kindcname;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getValidatestatus() {
		return validatestatus;
	}

	public void setValidatestatus(String validatestatus) {
		this.validatestatus = validatestatus;
	}

	public String getAttribute1() {
		return attribute1;
	}

	public void setAttribute1(String attribute1) {
		this.attribute1 = attribute1;
	}

	public String getAttribute2() {
		return attribute2;
	}

	public void setAttribute2(String attribute2) {
		this.attribute2 = attribute2;
	}

	public String getAttribute3() {
		return attribute3;
	}

	public void setAttribute3(String attribute3) {
		this.attribute3 = attribute3;
	}

	public String getAttribute4() {
		return attribute4;
	}

	public void setAttribute4(String attribute4) {
		this.attribute4 = attribute4;
	}

	public String getAttribute5() {
		return attribute5;
	}

	public void setAttribute5(String attribute5) {
		this.attribute5 = attribute5;
	}

	public String getAttribute6() {
		return attribute6;
	}

	public void setAttribute6(String attribute6) {
		this.attribute6 = attribute6;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getSubmitcom() {
		return submitcom;
	}

	public void setSubmitcom(String submitcom) {
		this.submitcom = submitcom;
	}

	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public String getSubmittype() {
		return submittype;
	}

	public void setSubmittype(String submittype) {
		this.submittype = submittype;
	}

	public String getDevtype() {
		return devtype;
	}

	public void setDevtype(String devtype) {
		this.devtype = devtype;
	}

	public String getProducttype() {
		return producttype;
	}

	public void setProducttype(String producttype) {
		this.producttype = producttype;
	}

	public String getManagetype() {
		return managetype;
	}

	public void setManagetype(String managetype) {
		this.managetype = managetype;
	}

	public String getRisktype() {
		return risktype;
	}

	public void setRisktype(String risktype) {
		this.risktype = risktype;
	}

	public String getKindtype() {
		return kindtype;
	}

	public void setKindtype(String kindtype) {
		this.kindtype = kindtype;
	}

	public Date getOpendate() {
		return opendate;
	}

	public void setOpendate(Date opendate) {
		this.opendate = opendate;
	}

	public Date getApprovaldate() {
		return approvaldate;
	}

	public void setApprovaldate(Date approvaldate) {
		this.approvaldate = approvaldate;
	}

	public String getBusscope() {
		return busscope;
	}

	public void setBusscope(String busscope) {
		this.busscope = busscope;
	}

	public String getBusarea() {
		return busarea;
	}

	public void setBusarea(String busarea) {
		this.busarea = busarea;
	}

	public String getDocnum() {
		return docnum;
	}

	public void setDocnum(String docnum) {
		this.docnum = docnum;
	}

	public Date getYear() {
		return year;
	}

	public void setYear(Date year) {
		this.year = year;
	}

	public String getProductnature() {
		return productnature;
	}

	public void setProductnature(String productnature) {
		this.productnature = productnature;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getRaterange() {
		return raterange;
	}

	public void setRaterange(String raterange) {
		this.raterange = raterange;
	}

	public String getRatecoefficient() {
		return ratecoefficient;
	}

	public void setRatecoefficient(String ratecoefficient) {
		this.ratecoefficient = ratecoefficient;
	}

	public String getHasepolicy() {
		return hasepolicy;
	}

	public void setHasepolicy(String hasepolicy) {
		this.hasepolicy = hasepolicy;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getDeductible() {
		return deductible;
	}

	public void setDeductible(String deductible) {
		this.deductible = deductible;
	}

	public String getReldeductibe() {
		return reldeductibe;
	}

	public void setReldeductibe(String reldeductibe) {
		this.reldeductibe = reldeductibe;
	}

	public String getInsliab() {
		return insliab;
	}

	public void setInsliab(String insliab) {
		this.insliab = insliab;
	}

	public String getRemark1() {
		return remark1;
	}

	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}

	public String getRemark2() {
		return remark2;
	}

	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}

	public String getLinkname() {
		return linkname;
	}

	public void setLinkname(String linkname) {
		this.linkname = linkname;
	}

	public String getLinknum() {
		return linknum;
	}

	public void setLinknum(String linknum) {
		this.linknum = linknum;
	}

	public String getProductattr() {
		return productattr;
	}

	public void setProductattr(String productattr) {
		this.productattr = productattr;
	}

	public String getSignname() {
		return signname;
	}

	public void setSignname(String signname) {
		this.signname = signname;
	}

	public String getLegalname() {
		return legalname;
	}

	public void setLegalname(String legalname) {
		this.legalname = legalname;
	}

	public String getActuarialname() {
		return actuarialname;
	}

	public void setActuarialname(String actuarialname) {
		this.actuarialname = actuarialname;
	}

	public String getSalepromname() {
		return salepromname;
	}

	public void setSalepromname(String salepromname) {
		this.salepromname = salepromname;
	}

	public String getSalearea() {
		return salearea;
	}

	public void setSalearea(String salearea) {
		this.salearea = salearea;
	}

	public String getSalechannel() {
		return salechannel;
	}

	public void setSalechannel(String salechannel) {
		this.salechannel = salechannel;
	}

	public String getOtherinfo() {
		return otherinfo;
	}

	public void setOtherinfo(String otherinfo) {
		this.otherinfo = otherinfo;
	}

	public String getRemark3() {
		return remark3;
	}

	public void setRemark3(String remark3) {
		this.remark3 = remark3;
	}

	public String getEfilecode() {
		return efilecode;
	}

	public void setEfilecode(String efilecode) {
		this.efilecode = efilecode;
	}

	public String getRiskcode() {
		return riskcode;
	}

	public void setRiskcode(String riskcode) {
		this.riskcode = riskcode;
	}

	public String getRiskversion() {
		return riskversion;
	}

	public void setRiskversion(String riskversion) {
		this.riskversion = riskversion;
	}

	public String getKindcode() {
		return kindcode;
	}

	public void setKindcode(String kindcode) {
		this.kindcode = kindcode;
	}

	public String getKindversion() {
		return kindversion;
	}

	public void setKindversion(String kindversion) {
		this.kindversion = kindversion;
	}

	public String getClasscode() {
		return classcode;
	}

	public void setClasscode(String classcode) {
		this.classcode = classcode;
	}

	public Date getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

}
