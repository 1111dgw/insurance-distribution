package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdclause;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdclauseExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdclauseKey;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdclauseWithBLOBs;
@Mapper
public interface PrpdclauseMapper {
    int countByExample(PrpdclauseExample example);

    int deleteByExample(PrpdclauseExample example);

    int deleteByPrimaryKey(PrpdclauseKey key);

    int insert(PrpdclauseWithBLOBs record);

    int insertSelective(PrpdclauseWithBLOBs record);

    List<PrpdclauseWithBLOBs> selectByExampleWithBLOBs(PrpdclauseExample example);

    List<Prpdclause> selectByExample(PrpdclauseExample example);

    PrpdclauseWithBLOBs selectByPrimaryKey(PrpdclauseKey key);

    int updateByExampleSelective(@Param("record") PrpdclauseWithBLOBs record, @Param("example") PrpdclauseExample example);

    int updateByExampleWithBLOBs(@Param("record") PrpdclauseWithBLOBs record, @Param("example") PrpdclauseExample example);

    int updateByExample(@Param("record") Prpdclause record, @Param("example") PrpdclauseExample example);

    int updateByPrimaryKeySelective(PrpdclauseWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(PrpdclauseWithBLOBs record);

    int updateByPrimaryKey(Prpdclause record);
}