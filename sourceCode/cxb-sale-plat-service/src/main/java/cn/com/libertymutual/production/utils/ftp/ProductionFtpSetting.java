package cn.com.libertymutual.production.utils.ftp;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

/**
 * 
 * @title ftp配置
 * @date 2017年7月27日
 */

@Service
@ConfigurationProperties("production.ftp")
public class ProductionFtpSetting {
	
	private String eFilingURL;
	private String indigoHost;
	private int indigoPort;
	private String indigoUser;
	private String indigoPwd;
	private String indigoDir;
	public String geteFilingURL() {
		return eFilingURL;
	}
	public void seteFilingURL(String eFilingURL) {
		this.eFilingURL = eFilingURL;
	}
	public String getIndigoHost() {
		return indigoHost;
	}
	public void setIndigoHost(String indigoHost) {
		this.indigoHost = indigoHost;
	}
	public int getIndigoPort() {
		return indigoPort;
	}
	public void setIndigoPort(int indigoPort) {
		this.indigoPort = indigoPort;
	}
	public String getIndigoUser() {
		return indigoUser;
	}
	public void setIndigoUser(String indigoUser) {
		this.indigoUser = indigoUser;
	}
	public String getIndigoPwd() {
		return indigoPwd;
	}
	public void setIndigoPwd(String indigoPwd) {
		this.indigoPwd = indigoPwd;
	}
	public String getIndigoDir() {
		return indigoDir;
	}
	public void setIndigoDir(String indigoDir) {
		this.indigoDir = indigoDir;
	}
}
