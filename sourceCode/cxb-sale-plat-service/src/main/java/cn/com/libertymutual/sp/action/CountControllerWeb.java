package cn.com.libertymutual.sp.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.service.api.IOrderService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/admin/countWeb")
public class CountControllerWeb {
	
	@Autowired IOrderService orderService;
	
	@ApiOperation(value = "单品排名", notes = "单品排名")
	@ApiImplicitParams(value = { 
			@ApiImplicitParam(name = "pageNumber", value = "分页", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "分页", required = true, paramType = "query", dataType = "Long")})
	@RequestMapping(value = "/countProductList", method = RequestMethod.POST)
    public ServiceResult countProductList() throws Exception {
		ServiceResult sr = new ServiceResult();
		sr = orderService.countProductList();
		return sr;
	}
	
	@ApiOperation(value = "机构排名", notes = "机构排名")
	@ApiImplicitParams(value = { 
			@ApiImplicitParam(name = "pageNumber", value = "分页", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "分页", required = true, paramType = "query", dataType = "Long")})
	@RequestMapping(value = "/branchProductList", method = RequestMethod.POST)
    public ServiceResult branchProductList() throws Exception {
		ServiceResult sr = new ServiceResult();
		sr = orderService.branchProductList();
		return sr;
	}
	
	@ApiOperation(value = "个人看/产品排名", notes = "个人看/产品排名")
	@ApiImplicitParams(value = { 
			@ApiImplicitParam(name = "pageNumber", value = "分页", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "分页", required = true, paramType = "query", dataType = "Long")})
	@RequestMapping(value = "/myProductCountList", method = RequestMethod.POST)
    public ServiceResult myProductCountList(int pageNumber, int pageSize) throws Exception {
		ServiceResult sr = new ServiceResult();
		sr = orderService.myProductCountList(pageNumber, pageSize);
		return sr;
	}
	
}