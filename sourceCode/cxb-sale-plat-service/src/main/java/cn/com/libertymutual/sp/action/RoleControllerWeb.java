package cn.com.libertymutual.sp.action;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.redis.util.RedisUtils;
import cn.com.libertymutual.core.util.Constants;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.req.RoleReq;
import cn.com.libertymutual.sp.service.api.RoleService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = "/admin/role")
public class RoleControllerWeb {

	
	private Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private RedisUtils redisUtils;
	
	
	@ApiOperation(value = "角色列表", notes = "角色列表")
	@ApiImplicitParams(value = {
		  @ApiImplicitParam(name = "type", value = "类型", required = true, paramType = "query", dataType = "String"),
          @ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
          @ApiImplicitParam(name = "pageSize", value = "条数", required = true, paramType = "query", dataType = "Long")
	})
	@PostMapping("/roleList")	
    public ServiceResult roleList(String type,Integer pageNumber,Integer pageSize){
		ServiceResult sr = new ServiceResult();
//		Map<String, List<Integer>> menuMapRel = (Map<String, List<Integer>>)redisUtils.get( Constants.MENU_REL_INFO_KEY ) ;
//		List<Integer> msd = menuMapRel.get("1"); 
//		log.info(msd.toString());
		sr = roleService.roleList(type,pageNumber,pageSize);
		return sr;		
	}
	
	
	
	@ApiOperation(value = "菜单列表", notes = "菜单列表")
	@PostMapping("/allMenuList")	
    public ServiceResult menuList(){
		ServiceResult sr = new ServiceResult();
		sr = roleService.menuList();
		return sr;		
	}
	
	
	//角色对应的菜单权限
	
	@ApiOperation(value = "角色下的菜单列表", notes = "角色下的菜单列表")
	@ApiImplicitParams(value = {
		@ApiImplicitParam(name = "roleId", value = "roleId", required = true, paramType = "query", dataType = "String"),
	})
	@PostMapping("/roleMenuList")	
    public ServiceResult roleMenuList(String roleId){
		ServiceResult sr = new ServiceResult();
		sr = roleService.roleMenuList(roleId);
		return sr;		
	}
	
	
	
	
	@ApiOperation(value = "为角色增加菜单", notes = "为角色增加菜单")
	@ApiImplicitParams(value = {
		@ApiImplicitParam(name = "roleId", value = "roleId", required = true, paramType = "query", dataType = "String"),
		@ApiImplicitParam(name = "menuIds", value = "menuIds", required = true, paramType = "query", dataType = "String"),
	})
	@PostMapping("/addRoleMenu")	
    public ServiceResult addRoleMenu(String roleId,String menuIds){
		ServiceResult sr = new ServiceResult();
		String[] list = menuIds.split(",");
		
		sr = roleService.addRoleMenu(roleId,list);
		return sr;		
	}
	
	
	
	/*
	 * 删除角色
	 */
	
	@ApiOperation(value = "删除角色", notes = "删除角色")
	@ApiImplicitParams(value = {
		@ApiImplicitParam(name = "roleId", value = "roleId", required = true, paramType = "query", dataType = "Long"),
	})
	@PostMapping("/removeRole")	
    public ServiceResult updateStatus(String type,Integer roleId){
		ServiceResult sr = new ServiceResult();
		sr = roleService.updateStatus(type,roleId);
		return sr;		
	}
	
	
	@ApiOperation(value = "设置权限", notes = "设置权限")
	@PostMapping("/setPermission")	
    public ServiceResult setPermission(@RequestBody @ApiParam(name = "RoleReq", value = "RoleReq", required = true)RoleReq roleReq){
		ServiceResult sr = new ServiceResult();
		log.info("控制层roleReq.getFmenuIds()===》："+roleReq.getFmenuIds());
		log.info("控制层roleReq.getResids()===》："+roleReq.getResids());
		sr = roleService.setPermission(roleReq);
		return sr;		
	}
	
	
	@ApiOperation(value = "新增角色", notes = "新增角色")
	@PostMapping("/addRole")	
    public ServiceResult addRole(@RequestBody @ApiParam(name = "RoleReq", value = "RoleReq", required = true)RoleReq roleReq){
		log.info("控制层roleReq===》："+roleReq.toString());
		log.info("控制层roleReq.getFmenuIds()===》："+roleReq.getFmenuIds());
		log.info("控制层roleReq.getResids()===》："+roleReq.getResids());
		ServiceResult sr = new ServiceResult();
		sr = roleService.addRole(roleReq);
		return sr;		
	}
}
