package cn.com.libertymutual.sp.service.api;

import javax.servlet.http.HttpServletRequest;

import cn.com.libertymutual.core.redis.util.RedisUtils;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpUser;
import cn.com.libertymutual.sp.dto.UserDto;
import cn.com.libertymutual.sp.req.StoreRateReq;
import cn.com.libertymutual.wx.message.WeChatUser;

public interface UserService {
	/**
	 * Remarks: 个人账户注册<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年3月5日下午4:15:36<br>
	 * Project：parking<br>
	 * @param request
	 * @param sr
	 * @param userDto
	 * @return
	 * @throws Exception
	 */
	public ServiceResult register(HttpServletRequest request, ServiceResult sr, UserDto userDto) throws Exception;

	/**
	 * Remarks: 渠道账户注册<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年3月5日下午4:27:40<br>
	 * Project：parking<br>
	 * @param request
	 * @param sr
	 * @param userDto
	 * @param type 业务关系代码保存模式,input=录入，select=常住地选择
	 * @return
	 * @throws Exception
	 */
	public ServiceResult registerChannel(HttpServletRequest request, ServiceResult sr, UserDto userDto, String type) throws Exception;

	/**
	 * Remarks: 创建前端渠道用户(synchronized同步线程方式)<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年3月7日上午11:01:05<br>
	 * Project：parking<br>
	 * @param userDto
	 * @return
	 * @throws Exception
	 */
	public TbSpUser createFrontChannelUser(UserDto userDto) throws Exception;

	/**
	 * Remarks: 创建后台渠道用户(synchronized同步线程方式)<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年3月7日上午11:00:51<br>
	 * Project：parking<br>
	 * @param userDto
	 * @param spUser
	 * @return
	 * @throws Exception
	 */
	public ServiceResult createSysChannelUser(UserDto userDto, TbSpUser spUser) throws Exception;

	/**Remarks: 微信授权注册普通账户<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年10月16日下午11:20:14<br>
	 * Project：liberty_sale_plat<br>
	 * @param sr
	 * @param userInfo
	 * @return
	 * @throws Exception
	 */
	public ServiceResult saveWeChatUser(ServiceResult sr, WeChatUser weChatUser, String refereeId, String refereePro) throws Exception;

	/**
	 * 微信授权注册渠道<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年4月9日下午8:53:19<br>
	 * @param sr
	 * @param weChatUser
	 * @param superUserCode
	 * @param oathType
	 * @return
	 * @throws Exception
	 */
	public ServiceResult saveWeChatToBsUser(ServiceResult sr, WeChatUser weChatUser, String superUserCode, String oathType) throws Exception;

	/**
	 * Remarks: 设置密码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年9月27日下午2:25:24<br>
	 * Project：liberty_sale_plat<br>
	 * 
	 * @param sr
	 * @param id 用户id
	 * @param password 新密码
	 * @param confirmPassword 确认密码
	 * @return
	 * @throws Exception
	 */
	public ServiceResult initUserPwd(ServiceResult sr, String userCode, String password, String confirmPassword) throws Exception;

	/**Remarks: 修改密码_短信验证码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年10月15日上午2:36:07<br>
	 * Project：liberty_sale_plat<br>
	 * @param sr
	 * @param userCode
	 * @param mobile
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public ServiceResult updatePwdByMobile(ServiceResult sr, String userCode, String mobile, String password) throws Exception;

	/**Remarks: 实名认证_短信验证码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年10月31日下午4:40:52<br>
	 * Project：liberty_sale_plat<br>
	 * @param sr
	 * @param userCode
	 * @param userName
	 * @param idNumber
	 * @return
	 * @throws Exception
	 */
	public ServiceResult updateIdNumberByMobile(ServiceResult sr, String userCode, String userName, String idNumber) throws Exception;

	/**Remarks: 解绑实名认证_短信验证码 & 图形码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年11月20日下午2:54:57<br>
	 * Project：liberty_sale_plat<br>
	 * @param sr
	 * @param userCode
	 * @param idNumber
	 * @return
	 * @throws Exception
	 */
	public ServiceResult unbindIdNumberByMobile(ServiceResult sr, String userCode, String idNumber) throws Exception;

	/**Remarks: 姓名修改<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年10月14日下午2:02:21<br>
	 * Project：liberty_sale_plat<br>
	 * @param sr
	 * @param id 用户id
	 * @param userName 新姓名
	 * @return
	 * @throws Exception
	 */
	public ServiceResult updateUserName(ServiceResult sr, String userCode, String userName) throws Exception;

	/**Remarks: 手机号修改<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年10月14日下午5:36:39<br>
	 * Project：liberty_sale_plat<br>
	 * @param redis
	 * @param sr
	 * @param id
	 * @param mobile
	 * @return
	 * @throws Exception
	 */
	public ServiceResult updateMobile(RedisUtils redis, ServiceResult sr, String userCode, String mobile) throws Exception;

	/**Remarks: 合并账户<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年11月22日下午2:19:24<br>
	 * Project：liberty_sale_plat<br>
	 * @param redis
	 * @param sr
	 * @param userCode
	 * @param mobile
	 * @return
	 * @throws Exception
	 */
	public ServiceResult updateUserFormerge(RedisUtils redis, ServiceResult sr, String userCode, String mobile) throws Exception;

	/**Remarks: 登录_短信验证码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年9月30日下午6:42:03<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param sr
	 * @param mobile
	 * @return
	 * @throws Exception
	 */
	public ServiceResult loginByMobile(HttpServletRequest request, ServiceResult sr, String mobile, String refereeId, String refereePro)
			throws Exception;

	/**Remarks: 登录_密码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年10月8日下午3:23:09<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param sr
	 * @param mobile
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public ServiceResult loginBypassword(HttpServletRequest request, ServiceResult sr, String mobile, String password) throws Exception;

	/**Remarks: 根据ID获取用户<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年10月16日下午11:26:11<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param sr
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public ServiceResult findUserByIdAndLogin(HttpServletRequest request, ServiceResult sr, Integer id) throws Exception;

	/** 根据userCode获取渠道用户<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年3月28日下午4:52:08<br>
	 * @param request
	 * @param sr
	 * @param userCode
	 * @return
	 * @throws Exception
	 */
	public ServiceResult findUserByUserCode(HttpServletRequest request, ServiceResult sr, String userCode) throws Exception;

	/** 
	 * 查询上级用户<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年4月28日下午3:09:54<br>
	 * @param request
	 * @param sr
	 * @param comCode
	 * @return
	 * @throws Exception
	 */
	public ServiceResult findUserByComcode(HttpServletRequest request, ServiceResult sr, String comCode) throws Exception;

	/**Remarks: 根据微信openID获取用户<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年10月16日下午11:31:13<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param sr
	 * @param weChatUser
	 * @param openId
	 * @return
	 * @throws Exception
	 */
	public ServiceResult updateUserByOpenId(HttpServletRequest request, ServiceResult sr, WeChatUser weChatUser, String openId) throws Exception;

	/**Remarks: 查询某用户所有有效银行卡<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年12月7日下午3:25:22<br>
	 * Project：liberty_sale_plat<br>
	 * @param sr
	 * @param userCode
	 * @return
	 * @throws Exception
	 */
	public ServiceResult findBankListAll(ServiceResult sr, String userCode) throws Exception;

	/**Remarks: 绑定银行卡=>第一步校验卡号<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年12月7日上午12:00:30<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param sr
	 * @param redis
	 * @param userCode
	 * @param userName
	 * @param bankNumber
	 * @param province
	 * @param city
	 * @param bankBranchName
	 * @return
	 * @throws Exception
	 */
	public ServiceResult findBankNoOfBinding(HttpServletRequest request, ServiceResult sr, RedisUtils redis, String userCode, String userName,
			String bankNumber, String province, String city, String bankBranchName) throws Exception;

	/**Remarks: 绑定银行卡=>第二步校验短信验证码并绑定<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年12月8日下午6:16:40<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param sr
	 * @param redis
	 * @param userCode
	 * @param bankName
	 * @param bankNumber
	 * @param mobile
	 * @return
	 * @throws Exception
	 */
	public ServiceResult updateBankOfBinding(HttpServletRequest request, ServiceResult sr, RedisUtils redis, String userCode, String bankName,
			String bankNumber, String mobile) throws Exception;

	/**
	 * Remarks: 解绑银行卡<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年1月19日下午4:03:33<br>
	 * Project：liberty_sale_plat<br>
	 * @param sr
	 * @param userCode
	 * @param bankId
	 * @param number
	 * @return
	 * @throws Exception
	 */
	public ServiceResult updateBankUnbundling(ServiceResult sr, String userCode, Integer bankId, String number) throws Exception;

	/**Remarks: 设置默认银行卡<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年12月8日下午6:15:18<br>
	 * Project：liberty_sale_plat<br>
	 * @param sr
	 * @param userCode
	 * @param bankId
	 * @param number
	 * @return
	 * @throws Exception
	 */
	public ServiceResult updateBankDefault(ServiceResult sr, String userCode, Integer bankId, String number) throws Exception;

	public ServiceResult findAllUser(String storeStartDate,String storeEndDate,String id, String signStartTime, String signEndTime, String userName, String mobile, String nickName,
			String branchCode, String agrementNo, String userType, Integer Maxpoints, Integer Minpoints, String storeFlag, Integer pageNumber,
			Integer pageSize) throws Exception;

	public ServiceResult addressList();

	public ServiceResult updateUser(TbSpUser tbSpUser) throws Exception;

	public ServiceResult findAllShopUser();

	public ServiceResult shopRate(String userCode, int pageNumber, int pageSize) throws Exception;

	public ServiceResult deleteShopRate(Integer id, String type) throws Exception;

	public ServiceResult setShopRate(StoreRateReq storeRate);

	/**
	 * 修改密码
	 * */
	public ServiceResult updatePwdByUserCode(ServiceResult sr, String userCode, String password) throws Exception;

	public ServiceResult addUser(TbSpUser tbSpUser);

	public ServiceResult agreeRate(String agreementNo, int pageNumber, int pageSize);

	String getTopUserCode(String userCode);
}
