package cn.com.libertymutual.production.service.api;

import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdshortratelibrary;

public interface PrpdshortratelibraryService {

	public List<Prpdshortratelibrary> findAll(); 
	
}
