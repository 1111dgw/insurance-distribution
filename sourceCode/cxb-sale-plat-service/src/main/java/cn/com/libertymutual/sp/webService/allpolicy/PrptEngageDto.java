
package cn.com.libertymutual.sp.webService.allpolicy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for prptEngageDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prptEngageDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="clauseCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clauses" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clausesContext" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="engageSerialNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prptEngageDto", propOrder = {
    "clauseCode",
    "clauses",
    "clausesContext",
    "engageSerialNo"
})
public class PrptEngageDto implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    protected String clauseCode;
    protected String clauses;
    protected String clausesContext;
    protected String engageSerialNo;

    /**
     * Gets the value of the clauseCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClauseCode() {
        return clauseCode;
    }

    /**
     * Sets the value of the clauseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClauseCode(String value) {
        this.clauseCode = value;
    }

    /**
     * Gets the value of the clauses property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClauses() {
        return clauses;
    }

    /**
     * Sets the value of the clauses property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClauses(String value) {
        this.clauses = value;
    }

    /**
     * Gets the value of the clausesContext property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClausesContext() {
        return clausesContext;
    }

    /**
     * Sets the value of the clausesContext property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClausesContext(String value) {
        this.clausesContext = value;
    }

    /**
     * Gets the value of the engageSerialNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEngageSerialNo() {
        return engageSerialNo;
    }

    /**
     * Sets the value of the engageSerialNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEngageSerialNo(String value) {
        this.engageSerialNo = value;
    }

}
