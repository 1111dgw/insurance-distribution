package cn.com.libertymutual.production.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import cn.com.libertymutual.production.dao.nomorcldatasource.PrpdclauseMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdclause;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdclauseExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdclauseWithBLOBs;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdclauseExample.Criteria;
import cn.com.libertymutual.production.pojo.request.PrpdClauseRequest;
import cn.com.libertymutual.production.service.api.PrpdClauseService;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

/** 
 * @author Steven.Li
 * @date 2017年7月28日
 *  
 */
@Service
public class PrpdClauseServiceImpl implements PrpdClauseService {

	@Autowired
	private PrpdclauseMapper prpdclauseMapper;

	@Override
	public PageInfo<PrpdclauseWithBLOBs> findPrpdClause(PrpdClauseRequest prpdClauseRequest) {
		PrpdclauseExample example = new PrpdclauseExample();
		Criteria criteria = example.createCriteria();
		String clauseCode = prpdClauseRequest.getClausecode();
		String clauseName = prpdClauseRequest.getClausename();
		Date createDate = prpdClauseRequest.getCreatedate();
		String validStatus = prpdClauseRequest.getValidstatus();
		String orderBy = prpdClauseRequest.getOrderBy();
		String order = prpdClauseRequest.getOrder();
		
		example.setOrderByClause("createdate desc");
		if(!StringUtils.isEmpty(clauseCode)) {
			criteria.andClausecodeLike("%" + clauseCode.toUpperCase() + "%");
		}
		if(!StringUtils.isEmpty(clauseName)) {
			criteria.andClausenameLike("%" + clauseName + "%");
		}
		if (createDate != null) {
			criteria.andCreatedateGreaterThanOrEqualTo(createDate);
			Calendar c = Calendar.getInstance();  
	        c.setTime(createDate);  
	        c.add(Calendar.DAY_OF_MONTH, 1);// 今天+1天  
	        Date tomorrow = c.getTime();
	        criteria.andCreatedateLessThanOrEqualTo(tomorrow);
		}
		if(!StringUtils.isEmpty(validStatus)) {
			criteria.andValidstatusEqualTo(validStatus);
		}
		if (!StringUtils.isEmpty(orderBy) && !"undefined".equals(orderBy)) {
			example.setOrderByClause(orderBy + " " + order);
		}
		//查询标题数据
		criteria.andTitleflagEqualTo("1");
		PageHelper.startPage(prpdClauseRequest.getCurrentPage(), prpdClauseRequest.getPageSize());
		PageInfo<PrpdclauseWithBLOBs> pageInfo = new PageInfo<>(prpdclauseMapper.selectByExampleWithBLOBs(example));
		
		//重新组装内容数据
		List<PrpdclauseWithBLOBs> titleClauses = pageInfo.getList();
		if (!titleClauses.isEmpty()) {
			StringBuffer content = null;
			for (Prpdclause titleClause : titleClauses) {
				example.clear();
//				example.setOrderByClause("cast(lineno as bigint) ASC");
				example.createCriteria().andClausecodeEqualTo(titleClause.getClausecode())
										.andTitleflagEqualTo("0");
				List<PrpdclauseWithBLOBs> contentClauses = prpdclauseMapper.selectByExampleWithBLOBs(example);
				content = new StringBuffer();
				for (PrpdclauseWithBLOBs contentClause : contentClauses) {
					content.append(contentClause.getContext());
				}
				titleClause.setContext(content.toString());
			}
		}
		pageInfo.setList(titleClauses);
		
		return pageInfo;
	}

	@Override
	public void insert(PrpdclauseWithBLOBs record) {
		prpdclauseMapper.insertSelective(record);
	}

	@Override
	public void update(PrpdclauseWithBLOBs record) {
		PrpdclauseExample example = new PrpdclauseExample();
		Criteria criteria = example.createCriteria();
		criteria.andClausecodeEqualTo(record.getClausecode());
		prpdclauseMapper.updateByExampleSelective(record, example);
	}

	@Override
	public void delete(PrpdclauseWithBLOBs record) {
		PrpdclauseExample example = new PrpdclauseExample();
		Criteria criteria = example.createCriteria();
		criteria.andClausecodeEqualTo(record.getClausecode());
		prpdclauseMapper.deleteByExample(example);
	}

	@Override
	public List<PrpdclauseWithBLOBs> findClauseByCondition(String clauseCode, String titleFlag) {
		PrpdclauseExample example = new PrpdclauseExample();
		Criteria criteria = example.createCriteria();
		criteria.andClausecodeEqualTo(clauseCode);
		criteria.andTitleflagEqualTo(titleFlag);
		return prpdclauseMapper.selectByExampleWithBLOBs(example);
	}
	
}
