package cn.com.libertymutual.sp.service.api;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpClause;

public interface ClauseService {

	ServiceResult clauseList(String clauseTitle,String kindCode,int pageNumber, int pageSize);

	ServiceResult addClause(TbSpClause tbSpClause);

	ServiceResult removeClause(Integer id);

	ServiceResult queryClause(String planId, String clauseTitle);

	ServiceResult queryClauseTitle(String planId);

	ServiceResult clauseTitleList();

}
