
package cn.com.libertymutual.sp.webService.allpolicy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for prptmaincargoDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prptmaincargoDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bargainNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="basePriceConfirm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="certificateType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="checkAgentCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="claimSite" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="conveyance" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="endDetailName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="endSiteCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="endSitename" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="invoiceAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="invoiceCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="invoiceNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ladingNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="originalCopyCount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="originalCount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="plusRate" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="startSiteCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="startSiteName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="viaSiteName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prptmaincargoDto", propOrder = {
    "bargainNo",
    "basePriceConfirm",
    "certificateType",
    "checkAgentCode",
    "claimSite",
    "conveyance",
    "endDetailName",
    "endSiteCode",
    "endSitename",
    "invoiceAmount",
    "invoiceCurrency",
    "invoiceNo",
    "ladingNo",
    "originalCopyCount",
    "originalCount",
    "plusRate",
    "startSiteCode",
    "startSiteName",
    "viaSiteName"
})
public class PrptmaincargoDto implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected String bargainNo;
    protected String basePriceConfirm;
    protected String certificateType;
    protected String checkAgentCode;
    protected String claimSite;
    protected String conveyance;
    protected String endDetailName;
    protected String endSiteCode;
    protected String endSitename;
    protected double invoiceAmount;
    protected String invoiceCurrency;
    protected String invoiceNo;
    protected String ladingNo;
    protected double originalCopyCount;
    protected double originalCount;
    protected double plusRate;
    protected String startSiteCode;
    protected String startSiteName;
    protected String viaSiteName;

    /**
     * Gets the value of the bargainNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBargainNo() {
        return bargainNo;
    }

    /**
     * Sets the value of the bargainNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBargainNo(String value) {
        this.bargainNo = value;
    }

    /**
     * Gets the value of the basePriceConfirm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBasePriceConfirm() {
        return basePriceConfirm;
    }

    /**
     * Sets the value of the basePriceConfirm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBasePriceConfirm(String value) {
        this.basePriceConfirm = value;
    }

    /**
     * Gets the value of the certificateType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertificateType() {
        return certificateType;
    }

    /**
     * Sets the value of the certificateType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertificateType(String value) {
        this.certificateType = value;
    }

    /**
     * Gets the value of the checkAgentCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckAgentCode() {
        return checkAgentCode;
    }

    /**
     * Sets the value of the checkAgentCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckAgentCode(String value) {
        this.checkAgentCode = value;
    }

    /**
     * Gets the value of the claimSite property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaimSite() {
        return claimSite;
    }

    /**
     * Sets the value of the claimSite property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaimSite(String value) {
        this.claimSite = value;
    }

    /**
     * Gets the value of the conveyance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConveyance() {
        return conveyance;
    }

    /**
     * Sets the value of the conveyance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConveyance(String value) {
        this.conveyance = value;
    }

    /**
     * Gets the value of the endDetailName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndDetailName() {
        return endDetailName;
    }

    /**
     * Sets the value of the endDetailName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndDetailName(String value) {
        this.endDetailName = value;
    }

    /**
     * Gets the value of the endSiteCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndSiteCode() {
        return endSiteCode;
    }

    /**
     * Sets the value of the endSiteCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndSiteCode(String value) {
        this.endSiteCode = value;
    }

    /**
     * Gets the value of the endSitename property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndSitename() {
        return endSitename;
    }

    /**
     * Sets the value of the endSitename property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndSitename(String value) {
        this.endSitename = value;
    }

    /**
     * Gets the value of the invoiceAmount property.
     * 
     */
    public double getInvoiceAmount() {
        return invoiceAmount;
    }

    /**
     * Sets the value of the invoiceAmount property.
     * 
     */
    public void setInvoiceAmount(double value) {
        this.invoiceAmount = value;
    }

    /**
     * Gets the value of the invoiceCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceCurrency() {
        return invoiceCurrency;
    }

    /**
     * Sets the value of the invoiceCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceCurrency(String value) {
        this.invoiceCurrency = value;
    }

    /**
     * Gets the value of the invoiceNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceNo() {
        return invoiceNo;
    }

    /**
     * Sets the value of the invoiceNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceNo(String value) {
        this.invoiceNo = value;
    }

    /**
     * Gets the value of the ladingNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLadingNo() {
        return ladingNo;
    }

    /**
     * Sets the value of the ladingNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLadingNo(String value) {
        this.ladingNo = value;
    }

    /**
     * Gets the value of the originalCopyCount property.
     * 
     */
    public double getOriginalCopyCount() {
        return originalCopyCount;
    }

    /**
     * Sets the value of the originalCopyCount property.
     * 
     */
    public void setOriginalCopyCount(double value) {
        this.originalCopyCount = value;
    }

    /**
     * Gets the value of the originalCount property.
     * 
     */
    public double getOriginalCount() {
        return originalCount;
    }

    /**
     * Sets the value of the originalCount property.
     * 
     */
    public void setOriginalCount(double value) {
        this.originalCount = value;
    }

    /**
     * Gets the value of the plusRate property.
     * 
     */
    public double getPlusRate() {
        return plusRate;
    }

    /**
     * Sets the value of the plusRate property.
     * 
     */
    public void setPlusRate(double value) {
        this.plusRate = value;
    }

    /**
     * Gets the value of the startSiteCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartSiteCode() {
        return startSiteCode;
    }

    /**
     * Sets the value of the startSiteCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartSiteCode(String value) {
        this.startSiteCode = value;
    }

    /**
     * Gets the value of the startSiteName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartSiteName() {
        return startSiteName;
    }

    /**
     * Sets the value of the startSiteName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartSiteName(String value) {
        this.startSiteName = value;
    }

    /**
     * Gets the value of the viaSiteName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getViaSiteName() {
        return viaSiteName;
    }

    /**
     * Sets the value of the viaSiteName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setViaSiteName(String value) {
        this.viaSiteName = value;
    }

}
