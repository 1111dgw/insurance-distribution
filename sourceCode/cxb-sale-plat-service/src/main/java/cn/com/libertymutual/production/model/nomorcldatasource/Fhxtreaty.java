package cn.com.libertymutual.production.model.nomorcldatasource;

import java.util.Date;

public class Fhxtreaty {
    private String treatyno;

    private String extreatyno;

    private String refno;

    private String treatyname;

    private String treatyename;

    private String treatytype;

    private String uwyear;

    private Date startdate;

    private Date enddate;

    private Date closedate;

    private String currency;

    private Date extenddate;

    private String extendflag;

    private String stateflag;

    private String flag;

    private String updatercode;

    private Date updatertime;

    private String remarks;

    private Date createdate;

    private String creatercode;

    private String fhxtreatyremarks;

    public String getTreatyno() {
        return treatyno;
    }

    public void setTreatyno(String treatyno) {
        this.treatyno = treatyno == null ? null : treatyno.trim();
    }

    public String getExtreatyno() {
        return extreatyno;
    }

    public void setExtreatyno(String extreatyno) {
        this.extreatyno = extreatyno == null ? null : extreatyno.trim();
    }

    public String getRefno() {
        return refno;
    }

    public void setRefno(String refno) {
        this.refno = refno == null ? null : refno.trim();
    }

    public String getTreatyname() {
        return treatyname;
    }

    public void setTreatyname(String treatyname) {
        this.treatyname = treatyname == null ? null : treatyname.trim();
    }

    public String getTreatyename() {
        return treatyename;
    }

    public void setTreatyename(String treatyename) {
        this.treatyename = treatyename == null ? null : treatyename.trim();
    }

    public String getTreatytype() {
        return treatytype;
    }

    public void setTreatytype(String treatytype) {
        this.treatytype = treatytype == null ? null : treatytype.trim();
    }

    public String getUwyear() {
        return uwyear;
    }

    public void setUwyear(String uwyear) {
        this.uwyear = uwyear == null ? null : uwyear.trim();
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public Date getClosedate() {
        return closedate;
    }

    public void setClosedate(Date closedate) {
        this.closedate = closedate;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency == null ? null : currency.trim();
    }

    public Date getExtenddate() {
        return extenddate;
    }

    public void setExtenddate(Date extenddate) {
        this.extenddate = extenddate;
    }

    public String getExtendflag() {
        return extendflag;
    }

    public void setExtendflag(String extendflag) {
        this.extendflag = extendflag == null ? null : extendflag.trim();
    }

    public String getStateflag() {
        return stateflag;
    }

    public void setStateflag(String stateflag) {
        this.stateflag = stateflag == null ? null : stateflag.trim();
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag == null ? null : flag.trim();
    }

    public String getUpdatercode() {
        return updatercode;
    }

    public void setUpdatercode(String updatercode) {
        this.updatercode = updatercode == null ? null : updatercode.trim();
    }

    public Date getUpdatertime() {
        return updatertime;
    }

    public void setUpdatertime(Date updatertime) {
        this.updatertime = updatertime;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks == null ? null : remarks.trim();
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public String getCreatercode() {
        return creatercode;
    }

    public void setCreatercode(String creatercode) {
        this.creatercode = creatercode == null ? null : creatercode.trim();
    }

    public String getFhxtreatyremarks() {
        return fhxtreatyremarks;
    }

    public void setFhxtreatyremarks(String fhxtreatyremarks) {
        this.fhxtreatyremarks = fhxtreatyremarks == null ? null : fhxtreatyremarks.trim();
    }
}