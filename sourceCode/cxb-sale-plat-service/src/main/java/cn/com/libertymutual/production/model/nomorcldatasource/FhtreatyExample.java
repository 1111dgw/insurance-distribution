package cn.com.libertymutual.production.model.nomorcldatasource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FhtreatyExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public FhtreatyExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andTreatynoIsNull() {
            addCriterion("TREATYNO is null");
            return (Criteria) this;
        }

        public Criteria andTreatynoIsNotNull() {
            addCriterion("TREATYNO is not null");
            return (Criteria) this;
        }

        public Criteria andTreatynoEqualTo(String value) {
            addCriterion("TREATYNO =", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoNotEqualTo(String value) {
            addCriterion("TREATYNO <>", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoGreaterThan(String value) {
            addCriterion("TREATYNO >", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoGreaterThanOrEqualTo(String value) {
            addCriterion("TREATYNO >=", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoLessThan(String value) {
            addCriterion("TREATYNO <", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoLessThanOrEqualTo(String value) {
            addCriterion("TREATYNO <=", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoLike(String value) {
            addCriterion("TREATYNO like", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoNotLike(String value) {
            addCriterion("TREATYNO not like", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoIn(List<String> values) {
            addCriterion("TREATYNO in", values, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoNotIn(List<String> values) {
            addCriterion("TREATYNO not in", values, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoBetween(String value1, String value2) {
            addCriterion("TREATYNO between", value1, value2, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoNotBetween(String value1, String value2) {
            addCriterion("TREATYNO not between", value1, value2, "treatyno");
            return (Criteria) this;
        }

        public Criteria andExtreatynoIsNull() {
            addCriterion("EXTREATYNO is null");
            return (Criteria) this;
        }

        public Criteria andExtreatynoIsNotNull() {
            addCriterion("EXTREATYNO is not null");
            return (Criteria) this;
        }

        public Criteria andExtreatynoEqualTo(String value) {
            addCriterion("EXTREATYNO =", value, "extreatyno");
            return (Criteria) this;
        }

        public Criteria andExtreatynoNotEqualTo(String value) {
            addCriterion("EXTREATYNO <>", value, "extreatyno");
            return (Criteria) this;
        }

        public Criteria andExtreatynoGreaterThan(String value) {
            addCriterion("EXTREATYNO >", value, "extreatyno");
            return (Criteria) this;
        }

        public Criteria andExtreatynoGreaterThanOrEqualTo(String value) {
            addCriterion("EXTREATYNO >=", value, "extreatyno");
            return (Criteria) this;
        }

        public Criteria andExtreatynoLessThan(String value) {
            addCriterion("EXTREATYNO <", value, "extreatyno");
            return (Criteria) this;
        }

        public Criteria andExtreatynoLessThanOrEqualTo(String value) {
            addCriterion("EXTREATYNO <=", value, "extreatyno");
            return (Criteria) this;
        }

        public Criteria andExtreatynoLike(String value) {
            addCriterion("EXTREATYNO like", value, "extreatyno");
            return (Criteria) this;
        }

        public Criteria andExtreatynoNotLike(String value) {
            addCriterion("EXTREATYNO not like", value, "extreatyno");
            return (Criteria) this;
        }

        public Criteria andExtreatynoIn(List<String> values) {
            addCriterion("EXTREATYNO in", values, "extreatyno");
            return (Criteria) this;
        }

        public Criteria andExtreatynoNotIn(List<String> values) {
            addCriterion("EXTREATYNO not in", values, "extreatyno");
            return (Criteria) this;
        }

        public Criteria andExtreatynoBetween(String value1, String value2) {
            addCriterion("EXTREATYNO between", value1, value2, "extreatyno");
            return (Criteria) this;
        }

        public Criteria andExtreatynoNotBetween(String value1, String value2) {
            addCriterion("EXTREATYNO not between", value1, value2, "extreatyno");
            return (Criteria) this;
        }

        public Criteria andConntreatynoIsNull() {
            addCriterion("CONNTREATYNO is null");
            return (Criteria) this;
        }

        public Criteria andConntreatynoIsNotNull() {
            addCriterion("CONNTREATYNO is not null");
            return (Criteria) this;
        }

        public Criteria andConntreatynoEqualTo(String value) {
            addCriterion("CONNTREATYNO =", value, "conntreatyno");
            return (Criteria) this;
        }

        public Criteria andConntreatynoNotEqualTo(String value) {
            addCriterion("CONNTREATYNO <>", value, "conntreatyno");
            return (Criteria) this;
        }

        public Criteria andConntreatynoGreaterThan(String value) {
            addCriterion("CONNTREATYNO >", value, "conntreatyno");
            return (Criteria) this;
        }

        public Criteria andConntreatynoGreaterThanOrEqualTo(String value) {
            addCriterion("CONNTREATYNO >=", value, "conntreatyno");
            return (Criteria) this;
        }

        public Criteria andConntreatynoLessThan(String value) {
            addCriterion("CONNTREATYNO <", value, "conntreatyno");
            return (Criteria) this;
        }

        public Criteria andConntreatynoLessThanOrEqualTo(String value) {
            addCriterion("CONNTREATYNO <=", value, "conntreatyno");
            return (Criteria) this;
        }

        public Criteria andConntreatynoLike(String value) {
            addCriterion("CONNTREATYNO like", value, "conntreatyno");
            return (Criteria) this;
        }

        public Criteria andConntreatynoNotLike(String value) {
            addCriterion("CONNTREATYNO not like", value, "conntreatyno");
            return (Criteria) this;
        }

        public Criteria andConntreatynoIn(List<String> values) {
            addCriterion("CONNTREATYNO in", values, "conntreatyno");
            return (Criteria) this;
        }

        public Criteria andConntreatynoNotIn(List<String> values) {
            addCriterion("CONNTREATYNO not in", values, "conntreatyno");
            return (Criteria) this;
        }

        public Criteria andConntreatynoBetween(String value1, String value2) {
            addCriterion("CONNTREATYNO between", value1, value2, "conntreatyno");
            return (Criteria) this;
        }

        public Criteria andConntreatynoNotBetween(String value1, String value2) {
            addCriterion("CONNTREATYNO not between", value1, value2, "conntreatyno");
            return (Criteria) this;
        }

        public Criteria andOpttypeIsNull() {
            addCriterion("OPTTYPE is null");
            return (Criteria) this;
        }

        public Criteria andOpttypeIsNotNull() {
            addCriterion("OPTTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andOpttypeEqualTo(String value) {
            addCriterion("OPTTYPE =", value, "opttype");
            return (Criteria) this;
        }

        public Criteria andOpttypeNotEqualTo(String value) {
            addCriterion("OPTTYPE <>", value, "opttype");
            return (Criteria) this;
        }

        public Criteria andOpttypeGreaterThan(String value) {
            addCriterion("OPTTYPE >", value, "opttype");
            return (Criteria) this;
        }

        public Criteria andOpttypeGreaterThanOrEqualTo(String value) {
            addCriterion("OPTTYPE >=", value, "opttype");
            return (Criteria) this;
        }

        public Criteria andOpttypeLessThan(String value) {
            addCriterion("OPTTYPE <", value, "opttype");
            return (Criteria) this;
        }

        public Criteria andOpttypeLessThanOrEqualTo(String value) {
            addCriterion("OPTTYPE <=", value, "opttype");
            return (Criteria) this;
        }

        public Criteria andOpttypeLike(String value) {
            addCriterion("OPTTYPE like", value, "opttype");
            return (Criteria) this;
        }

        public Criteria andOpttypeNotLike(String value) {
            addCriterion("OPTTYPE not like", value, "opttype");
            return (Criteria) this;
        }

        public Criteria andOpttypeIn(List<String> values) {
            addCriterion("OPTTYPE in", values, "opttype");
            return (Criteria) this;
        }

        public Criteria andOpttypeNotIn(List<String> values) {
            addCriterion("OPTTYPE not in", values, "opttype");
            return (Criteria) this;
        }

        public Criteria andOpttypeBetween(String value1, String value2) {
            addCriterion("OPTTYPE between", value1, value2, "opttype");
            return (Criteria) this;
        }

        public Criteria andOpttypeNotBetween(String value1, String value2) {
            addCriterion("OPTTYPE not between", value1, value2, "opttype");
            return (Criteria) this;
        }

        public Criteria andRefnoIsNull() {
            addCriterion("REFNO is null");
            return (Criteria) this;
        }

        public Criteria andRefnoIsNotNull() {
            addCriterion("REFNO is not null");
            return (Criteria) this;
        }

        public Criteria andRefnoEqualTo(String value) {
            addCriterion("REFNO =", value, "refno");
            return (Criteria) this;
        }

        public Criteria andRefnoNotEqualTo(String value) {
            addCriterion("REFNO <>", value, "refno");
            return (Criteria) this;
        }

        public Criteria andRefnoGreaterThan(String value) {
            addCriterion("REFNO >", value, "refno");
            return (Criteria) this;
        }

        public Criteria andRefnoGreaterThanOrEqualTo(String value) {
            addCriterion("REFNO >=", value, "refno");
            return (Criteria) this;
        }

        public Criteria andRefnoLessThan(String value) {
            addCriterion("REFNO <", value, "refno");
            return (Criteria) this;
        }

        public Criteria andRefnoLessThanOrEqualTo(String value) {
            addCriterion("REFNO <=", value, "refno");
            return (Criteria) this;
        }

        public Criteria andRefnoLike(String value) {
            addCriterion("REFNO like", value, "refno");
            return (Criteria) this;
        }

        public Criteria andRefnoNotLike(String value) {
            addCriterion("REFNO not like", value, "refno");
            return (Criteria) this;
        }

        public Criteria andRefnoIn(List<String> values) {
            addCriterion("REFNO in", values, "refno");
            return (Criteria) this;
        }

        public Criteria andRefnoNotIn(List<String> values) {
            addCriterion("REFNO not in", values, "refno");
            return (Criteria) this;
        }

        public Criteria andRefnoBetween(String value1, String value2) {
            addCriterion("REFNO between", value1, value2, "refno");
            return (Criteria) this;
        }

        public Criteria andRefnoNotBetween(String value1, String value2) {
            addCriterion("REFNO not between", value1, value2, "refno");
            return (Criteria) this;
        }

        public Criteria andTreatynameIsNull() {
            addCriterion("TREATYNAME is null");
            return (Criteria) this;
        }

        public Criteria andTreatynameIsNotNull() {
            addCriterion("TREATYNAME is not null");
            return (Criteria) this;
        }

        public Criteria andTreatynameEqualTo(String value) {
            addCriterion("TREATYNAME =", value, "treatyname");
            return (Criteria) this;
        }

        public Criteria andTreatynameNotEqualTo(String value) {
            addCriterion("TREATYNAME <>", value, "treatyname");
            return (Criteria) this;
        }

        public Criteria andTreatynameGreaterThan(String value) {
            addCriterion("TREATYNAME >", value, "treatyname");
            return (Criteria) this;
        }

        public Criteria andTreatynameGreaterThanOrEqualTo(String value) {
            addCriterion("TREATYNAME >=", value, "treatyname");
            return (Criteria) this;
        }

        public Criteria andTreatynameLessThan(String value) {
            addCriterion("TREATYNAME <", value, "treatyname");
            return (Criteria) this;
        }

        public Criteria andTreatynameLessThanOrEqualTo(String value) {
            addCriterion("TREATYNAME <=", value, "treatyname");
            return (Criteria) this;
        }

        public Criteria andTreatynameLike(String value) {
            addCriterion("TREATYNAME like", value, "treatyname");
            return (Criteria) this;
        }

        public Criteria andTreatynameNotLike(String value) {
            addCriterion("TREATYNAME not like", value, "treatyname");
            return (Criteria) this;
        }

        public Criteria andTreatynameIn(List<String> values) {
            addCriterion("TREATYNAME in", values, "treatyname");
            return (Criteria) this;
        }

        public Criteria andTreatynameNotIn(List<String> values) {
            addCriterion("TREATYNAME not in", values, "treatyname");
            return (Criteria) this;
        }

        public Criteria andTreatynameBetween(String value1, String value2) {
            addCriterion("TREATYNAME between", value1, value2, "treatyname");
            return (Criteria) this;
        }

        public Criteria andTreatynameNotBetween(String value1, String value2) {
            addCriterion("TREATYNAME not between", value1, value2, "treatyname");
            return (Criteria) this;
        }

        public Criteria andTreatyenameIsNull() {
            addCriterion("TREATYENAME is null");
            return (Criteria) this;
        }

        public Criteria andTreatyenameIsNotNull() {
            addCriterion("TREATYENAME is not null");
            return (Criteria) this;
        }

        public Criteria andTreatyenameEqualTo(String value) {
            addCriterion("TREATYENAME =", value, "treatyename");
            return (Criteria) this;
        }

        public Criteria andTreatyenameNotEqualTo(String value) {
            addCriterion("TREATYENAME <>", value, "treatyename");
            return (Criteria) this;
        }

        public Criteria andTreatyenameGreaterThan(String value) {
            addCriterion("TREATYENAME >", value, "treatyename");
            return (Criteria) this;
        }

        public Criteria andTreatyenameGreaterThanOrEqualTo(String value) {
            addCriterion("TREATYENAME >=", value, "treatyename");
            return (Criteria) this;
        }

        public Criteria andTreatyenameLessThan(String value) {
            addCriterion("TREATYENAME <", value, "treatyename");
            return (Criteria) this;
        }

        public Criteria andTreatyenameLessThanOrEqualTo(String value) {
            addCriterion("TREATYENAME <=", value, "treatyename");
            return (Criteria) this;
        }

        public Criteria andTreatyenameLike(String value) {
            addCriterion("TREATYENAME like", value, "treatyename");
            return (Criteria) this;
        }

        public Criteria andTreatyenameNotLike(String value) {
            addCriterion("TREATYENAME not like", value, "treatyename");
            return (Criteria) this;
        }

        public Criteria andTreatyenameIn(List<String> values) {
            addCriterion("TREATYENAME in", values, "treatyename");
            return (Criteria) this;
        }

        public Criteria andTreatyenameNotIn(List<String> values) {
            addCriterion("TREATYENAME not in", values, "treatyename");
            return (Criteria) this;
        }

        public Criteria andTreatyenameBetween(String value1, String value2) {
            addCriterion("TREATYENAME between", value1, value2, "treatyename");
            return (Criteria) this;
        }

        public Criteria andTreatyenameNotBetween(String value1, String value2) {
            addCriterion("TREATYENAME not between", value1, value2, "treatyename");
            return (Criteria) this;
        }

        public Criteria andClosedateIsNull() {
            addCriterion("CLOSEDATE is null");
            return (Criteria) this;
        }

        public Criteria andClosedateIsNotNull() {
            addCriterion("CLOSEDATE is not null");
            return (Criteria) this;
        }

        public Criteria andClosedateEqualTo(Date value) {
            addCriterion("CLOSEDATE =", value, "closedate");
            return (Criteria) this;
        }

        public Criteria andClosedateNotEqualTo(Date value) {
            addCriterion("CLOSEDATE <>", value, "closedate");
            return (Criteria) this;
        }

        public Criteria andClosedateGreaterThan(Date value) {
            addCriterion("CLOSEDATE >", value, "closedate");
            return (Criteria) this;
        }

        public Criteria andClosedateGreaterThanOrEqualTo(Date value) {
            addCriterion("CLOSEDATE >=", value, "closedate");
            return (Criteria) this;
        }

        public Criteria andClosedateLessThan(Date value) {
            addCriterion("CLOSEDATE <", value, "closedate");
            return (Criteria) this;
        }

        public Criteria andClosedateLessThanOrEqualTo(Date value) {
            addCriterion("CLOSEDATE <=", value, "closedate");
            return (Criteria) this;
        }

        public Criteria andClosedateIn(List<Date> values) {
            addCriterion("CLOSEDATE in", values, "closedate");
            return (Criteria) this;
        }

        public Criteria andClosedateNotIn(List<Date> values) {
            addCriterion("CLOSEDATE not in", values, "closedate");
            return (Criteria) this;
        }

        public Criteria andClosedateBetween(Date value1, Date value2) {
            addCriterion("CLOSEDATE between", value1, value2, "closedate");
            return (Criteria) this;
        }

        public Criteria andClosedateNotBetween(Date value1, Date value2) {
            addCriterion("CLOSEDATE not between", value1, value2, "closedate");
            return (Criteria) this;
        }

        public Criteria andTreatytypeIsNull() {
            addCriterion("TREATYTYPE is null");
            return (Criteria) this;
        }

        public Criteria andTreatytypeIsNotNull() {
            addCriterion("TREATYTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andTreatytypeEqualTo(String value) {
            addCriterion("TREATYTYPE =", value, "treatytype");
            return (Criteria) this;
        }

        public Criteria andTreatytypeNotEqualTo(String value) {
            addCriterion("TREATYTYPE <>", value, "treatytype");
            return (Criteria) this;
        }

        public Criteria andTreatytypeGreaterThan(String value) {
            addCriterion("TREATYTYPE >", value, "treatytype");
            return (Criteria) this;
        }

        public Criteria andTreatytypeGreaterThanOrEqualTo(String value) {
            addCriterion("TREATYTYPE >=", value, "treatytype");
            return (Criteria) this;
        }

        public Criteria andTreatytypeLessThan(String value) {
            addCriterion("TREATYTYPE <", value, "treatytype");
            return (Criteria) this;
        }

        public Criteria andTreatytypeLessThanOrEqualTo(String value) {
            addCriterion("TREATYTYPE <=", value, "treatytype");
            return (Criteria) this;
        }

        public Criteria andTreatytypeLike(String value) {
            addCriterion("TREATYTYPE like", value, "treatytype");
            return (Criteria) this;
        }

        public Criteria andTreatytypeNotLike(String value) {
            addCriterion("TREATYTYPE not like", value, "treatytype");
            return (Criteria) this;
        }

        public Criteria andTreatytypeIn(List<String> values) {
            addCriterion("TREATYTYPE in", values, "treatytype");
            return (Criteria) this;
        }

        public Criteria andTreatytypeNotIn(List<String> values) {
            addCriterion("TREATYTYPE not in", values, "treatytype");
            return (Criteria) this;
        }

        public Criteria andTreatytypeBetween(String value1, String value2) {
            addCriterion("TREATYTYPE between", value1, value2, "treatytype");
            return (Criteria) this;
        }

        public Criteria andTreatytypeNotBetween(String value1, String value2) {
            addCriterion("TREATYTYPE not between", value1, value2, "treatytype");
            return (Criteria) this;
        }

        public Criteria andUwyearIsNull() {
            addCriterion("UWYEAR is null");
            return (Criteria) this;
        }

        public Criteria andUwyearIsNotNull() {
            addCriterion("UWYEAR is not null");
            return (Criteria) this;
        }

        public Criteria andUwyearEqualTo(String value) {
            addCriterion("UWYEAR =", value, "uwyear");
            return (Criteria) this;
        }

        public Criteria andUwyearNotEqualTo(String value) {
            addCriterion("UWYEAR <>", value, "uwyear");
            return (Criteria) this;
        }

        public Criteria andUwyearGreaterThan(String value) {
            addCriterion("UWYEAR >", value, "uwyear");
            return (Criteria) this;
        }

        public Criteria andUwyearGreaterThanOrEqualTo(String value) {
            addCriterion("UWYEAR >=", value, "uwyear");
            return (Criteria) this;
        }

        public Criteria andUwyearLessThan(String value) {
            addCriterion("UWYEAR <", value, "uwyear");
            return (Criteria) this;
        }

        public Criteria andUwyearLessThanOrEqualTo(String value) {
            addCriterion("UWYEAR <=", value, "uwyear");
            return (Criteria) this;
        }

        public Criteria andUwyearLike(String value) {
            addCriterion("UWYEAR like", value, "uwyear");
            return (Criteria) this;
        }

        public Criteria andUwyearNotLike(String value) {
            addCriterion("UWYEAR not like", value, "uwyear");
            return (Criteria) this;
        }

        public Criteria andUwyearIn(List<String> values) {
            addCriterion("UWYEAR in", values, "uwyear");
            return (Criteria) this;
        }

        public Criteria andUwyearNotIn(List<String> values) {
            addCriterion("UWYEAR not in", values, "uwyear");
            return (Criteria) this;
        }

        public Criteria andUwyearBetween(String value1, String value2) {
            addCriterion("UWYEAR between", value1, value2, "uwyear");
            return (Criteria) this;
        }

        public Criteria andUwyearNotBetween(String value1, String value2) {
            addCriterion("UWYEAR not between", value1, value2, "uwyear");
            return (Criteria) this;
        }

        public Criteria andStartdateIsNull() {
            addCriterion("STARTDATE is null");
            return (Criteria) this;
        }

        public Criteria andStartdateIsNotNull() {
            addCriterion("STARTDATE is not null");
            return (Criteria) this;
        }

        public Criteria andStartdateEqualTo(Date value) {
            addCriterion("STARTDATE =", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateNotEqualTo(Date value) {
            addCriterion("STARTDATE <>", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateGreaterThan(Date value) {
            addCriterion("STARTDATE >", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateGreaterThanOrEqualTo(Date value) {
            addCriterion("STARTDATE >=", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateLessThan(Date value) {
            addCriterion("STARTDATE <", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateLessThanOrEqualTo(Date value) {
            addCriterion("STARTDATE <=", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateIn(List<Date> values) {
            addCriterion("STARTDATE in", values, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateNotIn(List<Date> values) {
            addCriterion("STARTDATE not in", values, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateBetween(Date value1, Date value2) {
            addCriterion("STARTDATE between", value1, value2, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateNotBetween(Date value1, Date value2) {
            addCriterion("STARTDATE not between", value1, value2, "startdate");
            return (Criteria) this;
        }

        public Criteria andEnddateIsNull() {
            addCriterion("ENDDATE is null");
            return (Criteria) this;
        }

        public Criteria andEnddateIsNotNull() {
            addCriterion("ENDDATE is not null");
            return (Criteria) this;
        }

        public Criteria andEnddateEqualTo(Date value) {
            addCriterion("ENDDATE =", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateNotEqualTo(Date value) {
            addCriterion("ENDDATE <>", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateGreaterThan(Date value) {
            addCriterion("ENDDATE >", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateGreaterThanOrEqualTo(Date value) {
            addCriterion("ENDDATE >=", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateLessThan(Date value) {
            addCriterion("ENDDATE <", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateLessThanOrEqualTo(Date value) {
            addCriterion("ENDDATE <=", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateIn(List<Date> values) {
            addCriterion("ENDDATE in", values, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateNotIn(List<Date> values) {
            addCriterion("ENDDATE not in", values, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateBetween(Date value1, Date value2) {
            addCriterion("ENDDATE between", value1, value2, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateNotBetween(Date value1, Date value2) {
            addCriterion("ENDDATE not between", value1, value2, "enddate");
            return (Criteria) this;
        }

        public Criteria andExtenddateIsNull() {
            addCriterion("EXTENDDATE is null");
            return (Criteria) this;
        }

        public Criteria andExtenddateIsNotNull() {
            addCriterion("EXTENDDATE is not null");
            return (Criteria) this;
        }

        public Criteria andExtenddateEqualTo(Date value) {
            addCriterion("EXTENDDATE =", value, "extenddate");
            return (Criteria) this;
        }

        public Criteria andExtenddateNotEqualTo(Date value) {
            addCriterion("EXTENDDATE <>", value, "extenddate");
            return (Criteria) this;
        }

        public Criteria andExtenddateGreaterThan(Date value) {
            addCriterion("EXTENDDATE >", value, "extenddate");
            return (Criteria) this;
        }

        public Criteria andExtenddateGreaterThanOrEqualTo(Date value) {
            addCriterion("EXTENDDATE >=", value, "extenddate");
            return (Criteria) this;
        }

        public Criteria andExtenddateLessThan(Date value) {
            addCriterion("EXTENDDATE <", value, "extenddate");
            return (Criteria) this;
        }

        public Criteria andExtenddateLessThanOrEqualTo(Date value) {
            addCriterion("EXTENDDATE <=", value, "extenddate");
            return (Criteria) this;
        }

        public Criteria andExtenddateIn(List<Date> values) {
            addCriterion("EXTENDDATE in", values, "extenddate");
            return (Criteria) this;
        }

        public Criteria andExtenddateNotIn(List<Date> values) {
            addCriterion("EXTENDDATE not in", values, "extenddate");
            return (Criteria) this;
        }

        public Criteria andExtenddateBetween(Date value1, Date value2) {
            addCriterion("EXTENDDATE between", value1, value2, "extenddate");
            return (Criteria) this;
        }

        public Criteria andExtenddateNotBetween(Date value1, Date value2) {
            addCriterion("EXTENDDATE not between", value1, value2, "extenddate");
            return (Criteria) this;
        }

        public Criteria andLogoutdateIsNull() {
            addCriterion("LOGOUTDATE is null");
            return (Criteria) this;
        }

        public Criteria andLogoutdateIsNotNull() {
            addCriterion("LOGOUTDATE is not null");
            return (Criteria) this;
        }

        public Criteria andLogoutdateEqualTo(Date value) {
            addCriterion("LOGOUTDATE =", value, "logoutdate");
            return (Criteria) this;
        }

        public Criteria andLogoutdateNotEqualTo(Date value) {
            addCriterion("LOGOUTDATE <>", value, "logoutdate");
            return (Criteria) this;
        }

        public Criteria andLogoutdateGreaterThan(Date value) {
            addCriterion("LOGOUTDATE >", value, "logoutdate");
            return (Criteria) this;
        }

        public Criteria andLogoutdateGreaterThanOrEqualTo(Date value) {
            addCriterion("LOGOUTDATE >=", value, "logoutdate");
            return (Criteria) this;
        }

        public Criteria andLogoutdateLessThan(Date value) {
            addCriterion("LOGOUTDATE <", value, "logoutdate");
            return (Criteria) this;
        }

        public Criteria andLogoutdateLessThanOrEqualTo(Date value) {
            addCriterion("LOGOUTDATE <=", value, "logoutdate");
            return (Criteria) this;
        }

        public Criteria andLogoutdateIn(List<Date> values) {
            addCriterion("LOGOUTDATE in", values, "logoutdate");
            return (Criteria) this;
        }

        public Criteria andLogoutdateNotIn(List<Date> values) {
            addCriterion("LOGOUTDATE not in", values, "logoutdate");
            return (Criteria) this;
        }

        public Criteria andLogoutdateBetween(Date value1, Date value2) {
            addCriterion("LOGOUTDATE between", value1, value2, "logoutdate");
            return (Criteria) this;
        }

        public Criteria andLogoutdateNotBetween(Date value1, Date value2) {
            addCriterion("LOGOUTDATE not between", value1, value2, "logoutdate");
            return (Criteria) this;
        }

        public Criteria andRepremiumbaseIsNull() {
            addCriterion("REPREMIUMBASE is null");
            return (Criteria) this;
        }

        public Criteria andRepremiumbaseIsNotNull() {
            addCriterion("REPREMIUMBASE is not null");
            return (Criteria) this;
        }

        public Criteria andRepremiumbaseEqualTo(String value) {
            addCriterion("REPREMIUMBASE =", value, "repremiumbase");
            return (Criteria) this;
        }

        public Criteria andRepremiumbaseNotEqualTo(String value) {
            addCriterion("REPREMIUMBASE <>", value, "repremiumbase");
            return (Criteria) this;
        }

        public Criteria andRepremiumbaseGreaterThan(String value) {
            addCriterion("REPREMIUMBASE >", value, "repremiumbase");
            return (Criteria) this;
        }

        public Criteria andRepremiumbaseGreaterThanOrEqualTo(String value) {
            addCriterion("REPREMIUMBASE >=", value, "repremiumbase");
            return (Criteria) this;
        }

        public Criteria andRepremiumbaseLessThan(String value) {
            addCriterion("REPREMIUMBASE <", value, "repremiumbase");
            return (Criteria) this;
        }

        public Criteria andRepremiumbaseLessThanOrEqualTo(String value) {
            addCriterion("REPREMIUMBASE <=", value, "repremiumbase");
            return (Criteria) this;
        }

        public Criteria andRepremiumbaseLike(String value) {
            addCriterion("REPREMIUMBASE like", value, "repremiumbase");
            return (Criteria) this;
        }

        public Criteria andRepremiumbaseNotLike(String value) {
            addCriterion("REPREMIUMBASE not like", value, "repremiumbase");
            return (Criteria) this;
        }

        public Criteria andRepremiumbaseIn(List<String> values) {
            addCriterion("REPREMIUMBASE in", values, "repremiumbase");
            return (Criteria) this;
        }

        public Criteria andRepremiumbaseNotIn(List<String> values) {
            addCriterion("REPREMIUMBASE not in", values, "repremiumbase");
            return (Criteria) this;
        }

        public Criteria andRepremiumbaseBetween(String value1, String value2) {
            addCriterion("REPREMIUMBASE between", value1, value2, "repremiumbase");
            return (Criteria) this;
        }

        public Criteria andRepremiumbaseNotBetween(String value1, String value2) {
            addCriterion("REPREMIUMBASE not between", value1, value2, "repremiumbase");
            return (Criteria) this;
        }

        public Criteria andCalculatebaseIsNull() {
            addCriterion("CALCULATEBASE is null");
            return (Criteria) this;
        }

        public Criteria andCalculatebaseIsNotNull() {
            addCriterion("CALCULATEBASE is not null");
            return (Criteria) this;
        }

        public Criteria andCalculatebaseEqualTo(String value) {
            addCriterion("CALCULATEBASE =", value, "calculatebase");
            return (Criteria) this;
        }

        public Criteria andCalculatebaseNotEqualTo(String value) {
            addCriterion("CALCULATEBASE <>", value, "calculatebase");
            return (Criteria) this;
        }

        public Criteria andCalculatebaseGreaterThan(String value) {
            addCriterion("CALCULATEBASE >", value, "calculatebase");
            return (Criteria) this;
        }

        public Criteria andCalculatebaseGreaterThanOrEqualTo(String value) {
            addCriterion("CALCULATEBASE >=", value, "calculatebase");
            return (Criteria) this;
        }

        public Criteria andCalculatebaseLessThan(String value) {
            addCriterion("CALCULATEBASE <", value, "calculatebase");
            return (Criteria) this;
        }

        public Criteria andCalculatebaseLessThanOrEqualTo(String value) {
            addCriterion("CALCULATEBASE <=", value, "calculatebase");
            return (Criteria) this;
        }

        public Criteria andCalculatebaseLike(String value) {
            addCriterion("CALCULATEBASE like", value, "calculatebase");
            return (Criteria) this;
        }

        public Criteria andCalculatebaseNotLike(String value) {
            addCriterion("CALCULATEBASE not like", value, "calculatebase");
            return (Criteria) this;
        }

        public Criteria andCalculatebaseIn(List<String> values) {
            addCriterion("CALCULATEBASE in", values, "calculatebase");
            return (Criteria) this;
        }

        public Criteria andCalculatebaseNotIn(List<String> values) {
            addCriterion("CALCULATEBASE not in", values, "calculatebase");
            return (Criteria) this;
        }

        public Criteria andCalculatebaseBetween(String value1, String value2) {
            addCriterion("CALCULATEBASE between", value1, value2, "calculatebase");
            return (Criteria) this;
        }

        public Criteria andCalculatebaseNotBetween(String value1, String value2) {
            addCriterion("CALCULATEBASE not between", value1, value2, "calculatebase");
            return (Criteria) this;
        }

        public Criteria andCurrencyIsNull() {
            addCriterion("CURRENCY is null");
            return (Criteria) this;
        }

        public Criteria andCurrencyIsNotNull() {
            addCriterion("CURRENCY is not null");
            return (Criteria) this;
        }

        public Criteria andCurrencyEqualTo(String value) {
            addCriterion("CURRENCY =", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyNotEqualTo(String value) {
            addCriterion("CURRENCY <>", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyGreaterThan(String value) {
            addCriterion("CURRENCY >", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyGreaterThanOrEqualTo(String value) {
            addCriterion("CURRENCY >=", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyLessThan(String value) {
            addCriterion("CURRENCY <", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyLessThanOrEqualTo(String value) {
            addCriterion("CURRENCY <=", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyLike(String value) {
            addCriterion("CURRENCY like", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyNotLike(String value) {
            addCriterion("CURRENCY not like", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyIn(List<String> values) {
            addCriterion("CURRENCY in", values, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyNotIn(List<String> values) {
            addCriterion("CURRENCY not in", values, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyBetween(String value1, String value2) {
            addCriterion("CURRENCY between", value1, value2, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyNotBetween(String value1, String value2) {
            addCriterion("CURRENCY not between", value1, value2, "currency");
            return (Criteria) this;
        }

        public Criteria andAccdateIsNull() {
            addCriterion("ACCDATE is null");
            return (Criteria) this;
        }

        public Criteria andAccdateIsNotNull() {
            addCriterion("ACCDATE is not null");
            return (Criteria) this;
        }

        public Criteria andAccdateEqualTo(Integer value) {
            addCriterion("ACCDATE =", value, "accdate");
            return (Criteria) this;
        }

        public Criteria andAccdateNotEqualTo(Integer value) {
            addCriterion("ACCDATE <>", value, "accdate");
            return (Criteria) this;
        }

        public Criteria andAccdateGreaterThan(Integer value) {
            addCriterion("ACCDATE >", value, "accdate");
            return (Criteria) this;
        }

        public Criteria andAccdateGreaterThanOrEqualTo(Integer value) {
            addCriterion("ACCDATE >=", value, "accdate");
            return (Criteria) this;
        }

        public Criteria andAccdateLessThan(Integer value) {
            addCriterion("ACCDATE <", value, "accdate");
            return (Criteria) this;
        }

        public Criteria andAccdateLessThanOrEqualTo(Integer value) {
            addCriterion("ACCDATE <=", value, "accdate");
            return (Criteria) this;
        }

        public Criteria andAccdateIn(List<Integer> values) {
            addCriterion("ACCDATE in", values, "accdate");
            return (Criteria) this;
        }

        public Criteria andAccdateNotIn(List<Integer> values) {
            addCriterion("ACCDATE not in", values, "accdate");
            return (Criteria) this;
        }

        public Criteria andAccdateBetween(Integer value1, Integer value2) {
            addCriterion("ACCDATE between", value1, value2, "accdate");
            return (Criteria) this;
        }

        public Criteria andAccdateNotBetween(Integer value1, Integer value2) {
            addCriterion("ACCDATE not between", value1, value2, "accdate");
            return (Criteria) this;
        }

        public Criteria andDuedateIsNull() {
            addCriterion("DUEDATE is null");
            return (Criteria) this;
        }

        public Criteria andDuedateIsNotNull() {
            addCriterion("DUEDATE is not null");
            return (Criteria) this;
        }

        public Criteria andDuedateEqualTo(Integer value) {
            addCriterion("DUEDATE =", value, "duedate");
            return (Criteria) this;
        }

        public Criteria andDuedateNotEqualTo(Integer value) {
            addCriterion("DUEDATE <>", value, "duedate");
            return (Criteria) this;
        }

        public Criteria andDuedateGreaterThan(Integer value) {
            addCriterion("DUEDATE >", value, "duedate");
            return (Criteria) this;
        }

        public Criteria andDuedateGreaterThanOrEqualTo(Integer value) {
            addCriterion("DUEDATE >=", value, "duedate");
            return (Criteria) this;
        }

        public Criteria andDuedateLessThan(Integer value) {
            addCriterion("DUEDATE <", value, "duedate");
            return (Criteria) this;
        }

        public Criteria andDuedateLessThanOrEqualTo(Integer value) {
            addCriterion("DUEDATE <=", value, "duedate");
            return (Criteria) this;
        }

        public Criteria andDuedateIn(List<Integer> values) {
            addCriterion("DUEDATE in", values, "duedate");
            return (Criteria) this;
        }

        public Criteria andDuedateNotIn(List<Integer> values) {
            addCriterion("DUEDATE not in", values, "duedate");
            return (Criteria) this;
        }

        public Criteria andDuedateBetween(Integer value1, Integer value2) {
            addCriterion("DUEDATE between", value1, value2, "duedate");
            return (Criteria) this;
        }

        public Criteria andDuedateNotBetween(Integer value1, Integer value2) {
            addCriterion("DUEDATE not between", value1, value2, "duedate");
            return (Criteria) this;
        }

        public Criteria andAccperiodIsNull() {
            addCriterion("ACCPERIOD is null");
            return (Criteria) this;
        }

        public Criteria andAccperiodIsNotNull() {
            addCriterion("ACCPERIOD is not null");
            return (Criteria) this;
        }

        public Criteria andAccperiodEqualTo(String value) {
            addCriterion("ACCPERIOD =", value, "accperiod");
            return (Criteria) this;
        }

        public Criteria andAccperiodNotEqualTo(String value) {
            addCriterion("ACCPERIOD <>", value, "accperiod");
            return (Criteria) this;
        }

        public Criteria andAccperiodGreaterThan(String value) {
            addCriterion("ACCPERIOD >", value, "accperiod");
            return (Criteria) this;
        }

        public Criteria andAccperiodGreaterThanOrEqualTo(String value) {
            addCriterion("ACCPERIOD >=", value, "accperiod");
            return (Criteria) this;
        }

        public Criteria andAccperiodLessThan(String value) {
            addCriterion("ACCPERIOD <", value, "accperiod");
            return (Criteria) this;
        }

        public Criteria andAccperiodLessThanOrEqualTo(String value) {
            addCriterion("ACCPERIOD <=", value, "accperiod");
            return (Criteria) this;
        }

        public Criteria andAccperiodLike(String value) {
            addCriterion("ACCPERIOD like", value, "accperiod");
            return (Criteria) this;
        }

        public Criteria andAccperiodNotLike(String value) {
            addCriterion("ACCPERIOD not like", value, "accperiod");
            return (Criteria) this;
        }

        public Criteria andAccperiodIn(List<String> values) {
            addCriterion("ACCPERIOD in", values, "accperiod");
            return (Criteria) this;
        }

        public Criteria andAccperiodNotIn(List<String> values) {
            addCriterion("ACCPERIOD not in", values, "accperiod");
            return (Criteria) this;
        }

        public Criteria andAccperiodBetween(String value1, String value2) {
            addCriterion("ACCPERIOD between", value1, value2, "accperiod");
            return (Criteria) this;
        }

        public Criteria andAccperiodNotBetween(String value1, String value2) {
            addCriterion("ACCPERIOD not between", value1, value2, "accperiod");
            return (Criteria) this;
        }

        public Criteria andExtendflagIsNull() {
            addCriterion("EXTENDFLAG is null");
            return (Criteria) this;
        }

        public Criteria andExtendflagIsNotNull() {
            addCriterion("EXTENDFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andExtendflagEqualTo(String value) {
            addCriterion("EXTENDFLAG =", value, "extendflag");
            return (Criteria) this;
        }

        public Criteria andExtendflagNotEqualTo(String value) {
            addCriterion("EXTENDFLAG <>", value, "extendflag");
            return (Criteria) this;
        }

        public Criteria andExtendflagGreaterThan(String value) {
            addCriterion("EXTENDFLAG >", value, "extendflag");
            return (Criteria) this;
        }

        public Criteria andExtendflagGreaterThanOrEqualTo(String value) {
            addCriterion("EXTENDFLAG >=", value, "extendflag");
            return (Criteria) this;
        }

        public Criteria andExtendflagLessThan(String value) {
            addCriterion("EXTENDFLAG <", value, "extendflag");
            return (Criteria) this;
        }

        public Criteria andExtendflagLessThanOrEqualTo(String value) {
            addCriterion("EXTENDFLAG <=", value, "extendflag");
            return (Criteria) this;
        }

        public Criteria andExtendflagLike(String value) {
            addCriterion("EXTENDFLAG like", value, "extendflag");
            return (Criteria) this;
        }

        public Criteria andExtendflagNotLike(String value) {
            addCriterion("EXTENDFLAG not like", value, "extendflag");
            return (Criteria) this;
        }

        public Criteria andExtendflagIn(List<String> values) {
            addCriterion("EXTENDFLAG in", values, "extendflag");
            return (Criteria) this;
        }

        public Criteria andExtendflagNotIn(List<String> values) {
            addCriterion("EXTENDFLAG not in", values, "extendflag");
            return (Criteria) this;
        }

        public Criteria andExtendflagBetween(String value1, String value2) {
            addCriterion("EXTENDFLAG between", value1, value2, "extendflag");
            return (Criteria) this;
        }

        public Criteria andExtendflagNotBetween(String value1, String value2) {
            addCriterion("EXTENDFLAG not between", value1, value2, "extendflag");
            return (Criteria) this;
        }

        public Criteria andInreinscodeIsNull() {
            addCriterion("INREINSCODE is null");
            return (Criteria) this;
        }

        public Criteria andInreinscodeIsNotNull() {
            addCriterion("INREINSCODE is not null");
            return (Criteria) this;
        }

        public Criteria andInreinscodeEqualTo(String value) {
            addCriterion("INREINSCODE =", value, "inreinscode");
            return (Criteria) this;
        }

        public Criteria andInreinscodeNotEqualTo(String value) {
            addCriterion("INREINSCODE <>", value, "inreinscode");
            return (Criteria) this;
        }

        public Criteria andInreinscodeGreaterThan(String value) {
            addCriterion("INREINSCODE >", value, "inreinscode");
            return (Criteria) this;
        }

        public Criteria andInreinscodeGreaterThanOrEqualTo(String value) {
            addCriterion("INREINSCODE >=", value, "inreinscode");
            return (Criteria) this;
        }

        public Criteria andInreinscodeLessThan(String value) {
            addCriterion("INREINSCODE <", value, "inreinscode");
            return (Criteria) this;
        }

        public Criteria andInreinscodeLessThanOrEqualTo(String value) {
            addCriterion("INREINSCODE <=", value, "inreinscode");
            return (Criteria) this;
        }

        public Criteria andInreinscodeLike(String value) {
            addCriterion("INREINSCODE like", value, "inreinscode");
            return (Criteria) this;
        }

        public Criteria andInreinscodeNotLike(String value) {
            addCriterion("INREINSCODE not like", value, "inreinscode");
            return (Criteria) this;
        }

        public Criteria andInreinscodeIn(List<String> values) {
            addCriterion("INREINSCODE in", values, "inreinscode");
            return (Criteria) this;
        }

        public Criteria andInreinscodeNotIn(List<String> values) {
            addCriterion("INREINSCODE not in", values, "inreinscode");
            return (Criteria) this;
        }

        public Criteria andInreinscodeBetween(String value1, String value2) {
            addCriterion("INREINSCODE between", value1, value2, "inreinscode");
            return (Criteria) this;
        }

        public Criteria andInreinscodeNotBetween(String value1, String value2) {
            addCriterion("INREINSCODE not between", value1, value2, "inreinscode");
            return (Criteria) this;
        }

        public Criteria andBrokercodeIsNull() {
            addCriterion("BROKERCODE is null");
            return (Criteria) this;
        }

        public Criteria andBrokercodeIsNotNull() {
            addCriterion("BROKERCODE is not null");
            return (Criteria) this;
        }

        public Criteria andBrokercodeEqualTo(String value) {
            addCriterion("BROKERCODE =", value, "brokercode");
            return (Criteria) this;
        }

        public Criteria andBrokercodeNotEqualTo(String value) {
            addCriterion("BROKERCODE <>", value, "brokercode");
            return (Criteria) this;
        }

        public Criteria andBrokercodeGreaterThan(String value) {
            addCriterion("BROKERCODE >", value, "brokercode");
            return (Criteria) this;
        }

        public Criteria andBrokercodeGreaterThanOrEqualTo(String value) {
            addCriterion("BROKERCODE >=", value, "brokercode");
            return (Criteria) this;
        }

        public Criteria andBrokercodeLessThan(String value) {
            addCriterion("BROKERCODE <", value, "brokercode");
            return (Criteria) this;
        }

        public Criteria andBrokercodeLessThanOrEqualTo(String value) {
            addCriterion("BROKERCODE <=", value, "brokercode");
            return (Criteria) this;
        }

        public Criteria andBrokercodeLike(String value) {
            addCriterion("BROKERCODE like", value, "brokercode");
            return (Criteria) this;
        }

        public Criteria andBrokercodeNotLike(String value) {
            addCriterion("BROKERCODE not like", value, "brokercode");
            return (Criteria) this;
        }

        public Criteria andBrokercodeIn(List<String> values) {
            addCriterion("BROKERCODE in", values, "brokercode");
            return (Criteria) this;
        }

        public Criteria andBrokercodeNotIn(List<String> values) {
            addCriterion("BROKERCODE not in", values, "brokercode");
            return (Criteria) this;
        }

        public Criteria andBrokercodeBetween(String value1, String value2) {
            addCriterion("BROKERCODE between", value1, value2, "brokercode");
            return (Criteria) this;
        }

        public Criteria andBrokercodeNotBetween(String value1, String value2) {
            addCriterion("BROKERCODE not between", value1, value2, "brokercode");
            return (Criteria) this;
        }

        public Criteria andPaycodeIsNull() {
            addCriterion("PAYCODE is null");
            return (Criteria) this;
        }

        public Criteria andPaycodeIsNotNull() {
            addCriterion("PAYCODE is not null");
            return (Criteria) this;
        }

        public Criteria andPaycodeEqualTo(String value) {
            addCriterion("PAYCODE =", value, "paycode");
            return (Criteria) this;
        }

        public Criteria andPaycodeNotEqualTo(String value) {
            addCriterion("PAYCODE <>", value, "paycode");
            return (Criteria) this;
        }

        public Criteria andPaycodeGreaterThan(String value) {
            addCriterion("PAYCODE >", value, "paycode");
            return (Criteria) this;
        }

        public Criteria andPaycodeGreaterThanOrEqualTo(String value) {
            addCriterion("PAYCODE >=", value, "paycode");
            return (Criteria) this;
        }

        public Criteria andPaycodeLessThan(String value) {
            addCriterion("PAYCODE <", value, "paycode");
            return (Criteria) this;
        }

        public Criteria andPaycodeLessThanOrEqualTo(String value) {
            addCriterion("PAYCODE <=", value, "paycode");
            return (Criteria) this;
        }

        public Criteria andPaycodeLike(String value) {
            addCriterion("PAYCODE like", value, "paycode");
            return (Criteria) this;
        }

        public Criteria andPaycodeNotLike(String value) {
            addCriterion("PAYCODE not like", value, "paycode");
            return (Criteria) this;
        }

        public Criteria andPaycodeIn(List<String> values) {
            addCriterion("PAYCODE in", values, "paycode");
            return (Criteria) this;
        }

        public Criteria andPaycodeNotIn(List<String> values) {
            addCriterion("PAYCODE not in", values, "paycode");
            return (Criteria) this;
        }

        public Criteria andPaycodeBetween(String value1, String value2) {
            addCriterion("PAYCODE between", value1, value2, "paycode");
            return (Criteria) this;
        }

        public Criteria andPaycodeNotBetween(String value1, String value2) {
            addCriterion("PAYCODE not between", value1, value2, "paycode");
            return (Criteria) this;
        }

        public Criteria andInreinsnameIsNull() {
            addCriterion("INREINSNAME is null");
            return (Criteria) this;
        }

        public Criteria andInreinsnameIsNotNull() {
            addCriterion("INREINSNAME is not null");
            return (Criteria) this;
        }

        public Criteria andInreinsnameEqualTo(String value) {
            addCriterion("INREINSNAME =", value, "inreinsname");
            return (Criteria) this;
        }

        public Criteria andInreinsnameNotEqualTo(String value) {
            addCriterion("INREINSNAME <>", value, "inreinsname");
            return (Criteria) this;
        }

        public Criteria andInreinsnameGreaterThan(String value) {
            addCriterion("INREINSNAME >", value, "inreinsname");
            return (Criteria) this;
        }

        public Criteria andInreinsnameGreaterThanOrEqualTo(String value) {
            addCriterion("INREINSNAME >=", value, "inreinsname");
            return (Criteria) this;
        }

        public Criteria andInreinsnameLessThan(String value) {
            addCriterion("INREINSNAME <", value, "inreinsname");
            return (Criteria) this;
        }

        public Criteria andInreinsnameLessThanOrEqualTo(String value) {
            addCriterion("INREINSNAME <=", value, "inreinsname");
            return (Criteria) this;
        }

        public Criteria andInreinsnameLike(String value) {
            addCriterion("INREINSNAME like", value, "inreinsname");
            return (Criteria) this;
        }

        public Criteria andInreinsnameNotLike(String value) {
            addCriterion("INREINSNAME not like", value, "inreinsname");
            return (Criteria) this;
        }

        public Criteria andInreinsnameIn(List<String> values) {
            addCriterion("INREINSNAME in", values, "inreinsname");
            return (Criteria) this;
        }

        public Criteria andInreinsnameNotIn(List<String> values) {
            addCriterion("INREINSNAME not in", values, "inreinsname");
            return (Criteria) this;
        }

        public Criteria andInreinsnameBetween(String value1, String value2) {
            addCriterion("INREINSNAME between", value1, value2, "inreinsname");
            return (Criteria) this;
        }

        public Criteria andInreinsnameNotBetween(String value1, String value2) {
            addCriterion("INREINSNAME not between", value1, value2, "inreinsname");
            return (Criteria) this;
        }

        public Criteria andBrokernameIsNull() {
            addCriterion("BROKERNAME is null");
            return (Criteria) this;
        }

        public Criteria andBrokernameIsNotNull() {
            addCriterion("BROKERNAME is not null");
            return (Criteria) this;
        }

        public Criteria andBrokernameEqualTo(String value) {
            addCriterion("BROKERNAME =", value, "brokername");
            return (Criteria) this;
        }

        public Criteria andBrokernameNotEqualTo(String value) {
            addCriterion("BROKERNAME <>", value, "brokername");
            return (Criteria) this;
        }

        public Criteria andBrokernameGreaterThan(String value) {
            addCriterion("BROKERNAME >", value, "brokername");
            return (Criteria) this;
        }

        public Criteria andBrokernameGreaterThanOrEqualTo(String value) {
            addCriterion("BROKERNAME >=", value, "brokername");
            return (Criteria) this;
        }

        public Criteria andBrokernameLessThan(String value) {
            addCriterion("BROKERNAME <", value, "brokername");
            return (Criteria) this;
        }

        public Criteria andBrokernameLessThanOrEqualTo(String value) {
            addCriterion("BROKERNAME <=", value, "brokername");
            return (Criteria) this;
        }

        public Criteria andBrokernameLike(String value) {
            addCriterion("BROKERNAME like", value, "brokername");
            return (Criteria) this;
        }

        public Criteria andBrokernameNotLike(String value) {
            addCriterion("BROKERNAME not like", value, "brokername");
            return (Criteria) this;
        }

        public Criteria andBrokernameIn(List<String> values) {
            addCriterion("BROKERNAME in", values, "brokername");
            return (Criteria) this;
        }

        public Criteria andBrokernameNotIn(List<String> values) {
            addCriterion("BROKERNAME not in", values, "brokername");
            return (Criteria) this;
        }

        public Criteria andBrokernameBetween(String value1, String value2) {
            addCriterion("BROKERNAME between", value1, value2, "brokername");
            return (Criteria) this;
        }

        public Criteria andBrokernameNotBetween(String value1, String value2) {
            addCriterion("BROKERNAME not between", value1, value2, "brokername");
            return (Criteria) this;
        }

        public Criteria andPaynameIsNull() {
            addCriterion("PAYNAME is null");
            return (Criteria) this;
        }

        public Criteria andPaynameIsNotNull() {
            addCriterion("PAYNAME is not null");
            return (Criteria) this;
        }

        public Criteria andPaynameEqualTo(String value) {
            addCriterion("PAYNAME =", value, "payname");
            return (Criteria) this;
        }

        public Criteria andPaynameNotEqualTo(String value) {
            addCriterion("PAYNAME <>", value, "payname");
            return (Criteria) this;
        }

        public Criteria andPaynameGreaterThan(String value) {
            addCriterion("PAYNAME >", value, "payname");
            return (Criteria) this;
        }

        public Criteria andPaynameGreaterThanOrEqualTo(String value) {
            addCriterion("PAYNAME >=", value, "payname");
            return (Criteria) this;
        }

        public Criteria andPaynameLessThan(String value) {
            addCriterion("PAYNAME <", value, "payname");
            return (Criteria) this;
        }

        public Criteria andPaynameLessThanOrEqualTo(String value) {
            addCriterion("PAYNAME <=", value, "payname");
            return (Criteria) this;
        }

        public Criteria andPaynameLike(String value) {
            addCriterion("PAYNAME like", value, "payname");
            return (Criteria) this;
        }

        public Criteria andPaynameNotLike(String value) {
            addCriterion("PAYNAME not like", value, "payname");
            return (Criteria) this;
        }

        public Criteria andPaynameIn(List<String> values) {
            addCriterion("PAYNAME in", values, "payname");
            return (Criteria) this;
        }

        public Criteria andPaynameNotIn(List<String> values) {
            addCriterion("PAYNAME not in", values, "payname");
            return (Criteria) this;
        }

        public Criteria andPaynameBetween(String value1, String value2) {
            addCriterion("PAYNAME between", value1, value2, "payname");
            return (Criteria) this;
        }

        public Criteria andPaynameNotBetween(String value1, String value2) {
            addCriterion("PAYNAME not between", value1, value2, "payname");
            return (Criteria) this;
        }

        public Criteria andExchangeflagIsNull() {
            addCriterion("EXCHANGEFLAG is null");
            return (Criteria) this;
        }

        public Criteria andExchangeflagIsNotNull() {
            addCriterion("EXCHANGEFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andExchangeflagEqualTo(String value) {
            addCriterion("EXCHANGEFLAG =", value, "exchangeflag");
            return (Criteria) this;
        }

        public Criteria andExchangeflagNotEqualTo(String value) {
            addCriterion("EXCHANGEFLAG <>", value, "exchangeflag");
            return (Criteria) this;
        }

        public Criteria andExchangeflagGreaterThan(String value) {
            addCriterion("EXCHANGEFLAG >", value, "exchangeflag");
            return (Criteria) this;
        }

        public Criteria andExchangeflagGreaterThanOrEqualTo(String value) {
            addCriterion("EXCHANGEFLAG >=", value, "exchangeflag");
            return (Criteria) this;
        }

        public Criteria andExchangeflagLessThan(String value) {
            addCriterion("EXCHANGEFLAG <", value, "exchangeflag");
            return (Criteria) this;
        }

        public Criteria andExchangeflagLessThanOrEqualTo(String value) {
            addCriterion("EXCHANGEFLAG <=", value, "exchangeflag");
            return (Criteria) this;
        }

        public Criteria andExchangeflagLike(String value) {
            addCriterion("EXCHANGEFLAG like", value, "exchangeflag");
            return (Criteria) this;
        }

        public Criteria andExchangeflagNotLike(String value) {
            addCriterion("EXCHANGEFLAG not like", value, "exchangeflag");
            return (Criteria) this;
        }

        public Criteria andExchangeflagIn(List<String> values) {
            addCriterion("EXCHANGEFLAG in", values, "exchangeflag");
            return (Criteria) this;
        }

        public Criteria andExchangeflagNotIn(List<String> values) {
            addCriterion("EXCHANGEFLAG not in", values, "exchangeflag");
            return (Criteria) this;
        }

        public Criteria andExchangeflagBetween(String value1, String value2) {
            addCriterion("EXCHANGEFLAG between", value1, value2, "exchangeflag");
            return (Criteria) this;
        }

        public Criteria andExchangeflagNotBetween(String value1, String value2) {
            addCriterion("EXCHANGEFLAG not between", value1, value2, "exchangeflag");
            return (Criteria) this;
        }

        public Criteria andStateflagIsNull() {
            addCriterion("STATEFLAG is null");
            return (Criteria) this;
        }

        public Criteria andStateflagIsNotNull() {
            addCriterion("STATEFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andStateflagEqualTo(String value) {
            addCriterion("STATEFLAG =", value, "stateflag");
            return (Criteria) this;
        }

        public Criteria andStateflagNotEqualTo(String value) {
            addCriterion("STATEFLAG <>", value, "stateflag");
            return (Criteria) this;
        }

        public Criteria andStateflagGreaterThan(String value) {
            addCriterion("STATEFLAG >", value, "stateflag");
            return (Criteria) this;
        }

        public Criteria andStateflagGreaterThanOrEqualTo(String value) {
            addCriterion("STATEFLAG >=", value, "stateflag");
            return (Criteria) this;
        }

        public Criteria andStateflagLessThan(String value) {
            addCriterion("STATEFLAG <", value, "stateflag");
            return (Criteria) this;
        }

        public Criteria andStateflagLessThanOrEqualTo(String value) {
            addCriterion("STATEFLAG <=", value, "stateflag");
            return (Criteria) this;
        }

        public Criteria andStateflagLike(String value) {
            addCriterion("STATEFLAG like", value, "stateflag");
            return (Criteria) this;
        }

        public Criteria andStateflagNotLike(String value) {
            addCriterion("STATEFLAG not like", value, "stateflag");
            return (Criteria) this;
        }

        public Criteria andStateflagIn(List<String> values) {
            addCriterion("STATEFLAG in", values, "stateflag");
            return (Criteria) this;
        }

        public Criteria andStateflagNotIn(List<String> values) {
            addCriterion("STATEFLAG not in", values, "stateflag");
            return (Criteria) this;
        }

        public Criteria andStateflagBetween(String value1, String value2) {
            addCriterion("STATEFLAG between", value1, value2, "stateflag");
            return (Criteria) this;
        }

        public Criteria andStateflagNotBetween(String value1, String value2) {
            addCriterion("STATEFLAG not between", value1, value2, "stateflag");
            return (Criteria) this;
        }

        public Criteria andRemarksIsNull() {
            addCriterion("REMARKS is null");
            return (Criteria) this;
        }

        public Criteria andRemarksIsNotNull() {
            addCriterion("REMARKS is not null");
            return (Criteria) this;
        }

        public Criteria andRemarksEqualTo(String value) {
            addCriterion("REMARKS =", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotEqualTo(String value) {
            addCriterion("REMARKS <>", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksGreaterThan(String value) {
            addCriterion("REMARKS >", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksGreaterThanOrEqualTo(String value) {
            addCriterion("REMARKS >=", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksLessThan(String value) {
            addCriterion("REMARKS <", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksLessThanOrEqualTo(String value) {
            addCriterion("REMARKS <=", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksLike(String value) {
            addCriterion("REMARKS like", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotLike(String value) {
            addCriterion("REMARKS not like", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksIn(List<String> values) {
            addCriterion("REMARKS in", values, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotIn(List<String> values) {
            addCriterion("REMARKS not in", values, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksBetween(String value1, String value2) {
            addCriterion("REMARKS between", value1, value2, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotBetween(String value1, String value2) {
            addCriterion("REMARKS not between", value1, value2, "remarks");
            return (Criteria) this;
        }

        public Criteria andCreatercodeIsNull() {
            addCriterion("CREATERCODE is null");
            return (Criteria) this;
        }

        public Criteria andCreatercodeIsNotNull() {
            addCriterion("CREATERCODE is not null");
            return (Criteria) this;
        }

        public Criteria andCreatercodeEqualTo(String value) {
            addCriterion("CREATERCODE =", value, "creatercode");
            return (Criteria) this;
        }

        public Criteria andCreatercodeNotEqualTo(String value) {
            addCriterion("CREATERCODE <>", value, "creatercode");
            return (Criteria) this;
        }

        public Criteria andCreatercodeGreaterThan(String value) {
            addCriterion("CREATERCODE >", value, "creatercode");
            return (Criteria) this;
        }

        public Criteria andCreatercodeGreaterThanOrEqualTo(String value) {
            addCriterion("CREATERCODE >=", value, "creatercode");
            return (Criteria) this;
        }

        public Criteria andCreatercodeLessThan(String value) {
            addCriterion("CREATERCODE <", value, "creatercode");
            return (Criteria) this;
        }

        public Criteria andCreatercodeLessThanOrEqualTo(String value) {
            addCriterion("CREATERCODE <=", value, "creatercode");
            return (Criteria) this;
        }

        public Criteria andCreatercodeLike(String value) {
            addCriterion("CREATERCODE like", value, "creatercode");
            return (Criteria) this;
        }

        public Criteria andCreatercodeNotLike(String value) {
            addCriterion("CREATERCODE not like", value, "creatercode");
            return (Criteria) this;
        }

        public Criteria andCreatercodeIn(List<String> values) {
            addCriterion("CREATERCODE in", values, "creatercode");
            return (Criteria) this;
        }

        public Criteria andCreatercodeNotIn(List<String> values) {
            addCriterion("CREATERCODE not in", values, "creatercode");
            return (Criteria) this;
        }

        public Criteria andCreatercodeBetween(String value1, String value2) {
            addCriterion("CREATERCODE between", value1, value2, "creatercode");
            return (Criteria) this;
        }

        public Criteria andCreatercodeNotBetween(String value1, String value2) {
            addCriterion("CREATERCODE not between", value1, value2, "creatercode");
            return (Criteria) this;
        }

        public Criteria andCreatedateIsNull() {
            addCriterion("CREATEDATE is null");
            return (Criteria) this;
        }

        public Criteria andCreatedateIsNotNull() {
            addCriterion("CREATEDATE is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedateEqualTo(Date value) {
            addCriterion("CREATEDATE =", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateNotEqualTo(Date value) {
            addCriterion("CREATEDATE <>", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateGreaterThan(Date value) {
            addCriterion("CREATEDATE >", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("CREATEDATE >=", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateLessThan(Date value) {
            addCriterion("CREATEDATE <", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateLessThanOrEqualTo(Date value) {
            addCriterion("CREATEDATE <=", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateIn(List<Date> values) {
            addCriterion("CREATEDATE in", values, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateNotIn(List<Date> values) {
            addCriterion("CREATEDATE not in", values, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateBetween(Date value1, Date value2) {
            addCriterion("CREATEDATE between", value1, value2, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateNotBetween(Date value1, Date value2) {
            addCriterion("CREATEDATE not between", value1, value2, "createdate");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeIsNull() {
            addCriterion("UPDATERCODE is null");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeIsNotNull() {
            addCriterion("UPDATERCODE is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeEqualTo(String value) {
            addCriterion("UPDATERCODE =", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeNotEqualTo(String value) {
            addCriterion("UPDATERCODE <>", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeGreaterThan(String value) {
            addCriterion("UPDATERCODE >", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeGreaterThanOrEqualTo(String value) {
            addCriterion("UPDATERCODE >=", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeLessThan(String value) {
            addCriterion("UPDATERCODE <", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeLessThanOrEqualTo(String value) {
            addCriterion("UPDATERCODE <=", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeLike(String value) {
            addCriterion("UPDATERCODE like", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeNotLike(String value) {
            addCriterion("UPDATERCODE not like", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeIn(List<String> values) {
            addCriterion("UPDATERCODE in", values, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeNotIn(List<String> values) {
            addCriterion("UPDATERCODE not in", values, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeBetween(String value1, String value2) {
            addCriterion("UPDATERCODE between", value1, value2, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeNotBetween(String value1, String value2) {
            addCriterion("UPDATERCODE not between", value1, value2, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNull() {
            addCriterion("UPDATEDATE is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNotNull() {
            addCriterion("UPDATEDATE is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateEqualTo(Date value) {
            addCriterion("UPDATEDATE =", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotEqualTo(Date value) {
            addCriterion("UPDATEDATE <>", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThan(Date value) {
            addCriterion("UPDATEDATE >", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("UPDATEDATE >=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThan(Date value) {
            addCriterion("UPDATEDATE <", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThanOrEqualTo(Date value) {
            addCriterion("UPDATEDATE <=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIn(List<Date> values) {
            addCriterion("UPDATEDATE in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotIn(List<Date> values) {
            addCriterion("UPDATEDATE not in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateBetween(Date value1, Date value2) {
            addCriterion("UPDATEDATE between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotBetween(Date value1, Date value2) {
            addCriterion("UPDATEDATE not between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUnderwritecodeIsNull() {
            addCriterion("UNDERWRITECODE is null");
            return (Criteria) this;
        }

        public Criteria andUnderwritecodeIsNotNull() {
            addCriterion("UNDERWRITECODE is not null");
            return (Criteria) this;
        }

        public Criteria andUnderwritecodeEqualTo(String value) {
            addCriterion("UNDERWRITECODE =", value, "underwritecode");
            return (Criteria) this;
        }

        public Criteria andUnderwritecodeNotEqualTo(String value) {
            addCriterion("UNDERWRITECODE <>", value, "underwritecode");
            return (Criteria) this;
        }

        public Criteria andUnderwritecodeGreaterThan(String value) {
            addCriterion("UNDERWRITECODE >", value, "underwritecode");
            return (Criteria) this;
        }

        public Criteria andUnderwritecodeGreaterThanOrEqualTo(String value) {
            addCriterion("UNDERWRITECODE >=", value, "underwritecode");
            return (Criteria) this;
        }

        public Criteria andUnderwritecodeLessThan(String value) {
            addCriterion("UNDERWRITECODE <", value, "underwritecode");
            return (Criteria) this;
        }

        public Criteria andUnderwritecodeLessThanOrEqualTo(String value) {
            addCriterion("UNDERWRITECODE <=", value, "underwritecode");
            return (Criteria) this;
        }

        public Criteria andUnderwritecodeLike(String value) {
            addCriterion("UNDERWRITECODE like", value, "underwritecode");
            return (Criteria) this;
        }

        public Criteria andUnderwritecodeNotLike(String value) {
            addCriterion("UNDERWRITECODE not like", value, "underwritecode");
            return (Criteria) this;
        }

        public Criteria andUnderwritecodeIn(List<String> values) {
            addCriterion("UNDERWRITECODE in", values, "underwritecode");
            return (Criteria) this;
        }

        public Criteria andUnderwritecodeNotIn(List<String> values) {
            addCriterion("UNDERWRITECODE not in", values, "underwritecode");
            return (Criteria) this;
        }

        public Criteria andUnderwritecodeBetween(String value1, String value2) {
            addCriterion("UNDERWRITECODE between", value1, value2, "underwritecode");
            return (Criteria) this;
        }

        public Criteria andUnderwritecodeNotBetween(String value1, String value2) {
            addCriterion("UNDERWRITECODE not between", value1, value2, "underwritecode");
            return (Criteria) this;
        }

        public Criteria andUnderwriteenddateIsNull() {
            addCriterion("UNDERWRITEENDDATE is null");
            return (Criteria) this;
        }

        public Criteria andUnderwriteenddateIsNotNull() {
            addCriterion("UNDERWRITEENDDATE is not null");
            return (Criteria) this;
        }

        public Criteria andUnderwriteenddateEqualTo(Date value) {
            addCriterion("UNDERWRITEENDDATE =", value, "underwriteenddate");
            return (Criteria) this;
        }

        public Criteria andUnderwriteenddateNotEqualTo(Date value) {
            addCriterion("UNDERWRITEENDDATE <>", value, "underwriteenddate");
            return (Criteria) this;
        }

        public Criteria andUnderwriteenddateGreaterThan(Date value) {
            addCriterion("UNDERWRITEENDDATE >", value, "underwriteenddate");
            return (Criteria) this;
        }

        public Criteria andUnderwriteenddateGreaterThanOrEqualTo(Date value) {
            addCriterion("UNDERWRITEENDDATE >=", value, "underwriteenddate");
            return (Criteria) this;
        }

        public Criteria andUnderwriteenddateLessThan(Date value) {
            addCriterion("UNDERWRITEENDDATE <", value, "underwriteenddate");
            return (Criteria) this;
        }

        public Criteria andUnderwriteenddateLessThanOrEqualTo(Date value) {
            addCriterion("UNDERWRITEENDDATE <=", value, "underwriteenddate");
            return (Criteria) this;
        }

        public Criteria andUnderwriteenddateIn(List<Date> values) {
            addCriterion("UNDERWRITEENDDATE in", values, "underwriteenddate");
            return (Criteria) this;
        }

        public Criteria andUnderwriteenddateNotIn(List<Date> values) {
            addCriterion("UNDERWRITEENDDATE not in", values, "underwriteenddate");
            return (Criteria) this;
        }

        public Criteria andUnderwriteenddateBetween(Date value1, Date value2) {
            addCriterion("UNDERWRITEENDDATE between", value1, value2, "underwriteenddate");
            return (Criteria) this;
        }

        public Criteria andUnderwriteenddateNotBetween(Date value1, Date value2) {
            addCriterion("UNDERWRITEENDDATE not between", value1, value2, "underwriteenddate");
            return (Criteria) this;
        }

        public Criteria andFlagIsNull() {
            addCriterion("FLAG is null");
            return (Criteria) this;
        }

        public Criteria andFlagIsNotNull() {
            addCriterion("FLAG is not null");
            return (Criteria) this;
        }

        public Criteria andFlagEqualTo(String value) {
            addCriterion("FLAG =", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotEqualTo(String value) {
            addCriterion("FLAG <>", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThan(String value) {
            addCriterion("FLAG >", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThanOrEqualTo(String value) {
            addCriterion("FLAG >=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThan(String value) {
            addCriterion("FLAG <", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThanOrEqualTo(String value) {
            addCriterion("FLAG <=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLike(String value) {
            addCriterion("FLAG like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotLike(String value) {
            addCriterion("FLAG not like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagIn(List<String> values) {
            addCriterion("FLAG in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotIn(List<String> values) {
            addCriterion("FLAG not in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagBetween(String value1, String value2) {
            addCriterion("FLAG between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotBetween(String value1, String value2) {
            addCriterion("FLAG not between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andFhtreatyremarksIsNull() {
            addCriterion("FHTREATYREMARKS is null");
            return (Criteria) this;
        }

        public Criteria andFhtreatyremarksIsNotNull() {
            addCriterion("FHTREATYREMARKS is not null");
            return (Criteria) this;
        }

        public Criteria andFhtreatyremarksEqualTo(String value) {
            addCriterion("FHTREATYREMARKS =", value, "fhtreatyremarks");
            return (Criteria) this;
        }

        public Criteria andFhtreatyremarksNotEqualTo(String value) {
            addCriterion("FHTREATYREMARKS <>", value, "fhtreatyremarks");
            return (Criteria) this;
        }

        public Criteria andFhtreatyremarksGreaterThan(String value) {
            addCriterion("FHTREATYREMARKS >", value, "fhtreatyremarks");
            return (Criteria) this;
        }

        public Criteria andFhtreatyremarksGreaterThanOrEqualTo(String value) {
            addCriterion("FHTREATYREMARKS >=", value, "fhtreatyremarks");
            return (Criteria) this;
        }

        public Criteria andFhtreatyremarksLessThan(String value) {
            addCriterion("FHTREATYREMARKS <", value, "fhtreatyremarks");
            return (Criteria) this;
        }

        public Criteria andFhtreatyremarksLessThanOrEqualTo(String value) {
            addCriterion("FHTREATYREMARKS <=", value, "fhtreatyremarks");
            return (Criteria) this;
        }

        public Criteria andFhtreatyremarksLike(String value) {
            addCriterion("FHTREATYREMARKS like", value, "fhtreatyremarks");
            return (Criteria) this;
        }

        public Criteria andFhtreatyremarksNotLike(String value) {
            addCriterion("FHTREATYREMARKS not like", value, "fhtreatyremarks");
            return (Criteria) this;
        }

        public Criteria andFhtreatyremarksIn(List<String> values) {
            addCriterion("FHTREATYREMARKS in", values, "fhtreatyremarks");
            return (Criteria) this;
        }

        public Criteria andFhtreatyremarksNotIn(List<String> values) {
            addCriterion("FHTREATYREMARKS not in", values, "fhtreatyremarks");
            return (Criteria) this;
        }

        public Criteria andFhtreatyremarksBetween(String value1, String value2) {
            addCriterion("FHTREATYREMARKS between", value1, value2, "fhtreatyremarks");
            return (Criteria) this;
        }

        public Criteria andFhtreatyremarksNotBetween(String value1, String value2) {
            addCriterion("FHTREATYREMARKS not between", value1, value2, "fhtreatyremarks");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}