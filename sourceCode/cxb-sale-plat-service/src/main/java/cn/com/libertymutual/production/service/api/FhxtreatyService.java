package cn.com.libertymutual.production.service.api;

import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Fhxtreaty;

/**
 * @author steven.li
 * @create 2017/12/20
 */
public interface FhxtreatyService {

    void insert(Fhxtreaty record);

    List<Fhxtreaty> findAll(Fhxtreaty where);
}
