package cn.com.libertymutual.sp.biz;

import java.text.ParseException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Component;

import cn.com.libertymutual.core.redis.util.RedisUtils;
import cn.com.libertymutual.core.util.Constants;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.dto.UserDto;
import cn.com.libertymutual.sp.service.api.CaptchaService;
import cn.com.libertymutual.sp.service.api.SmsService;
import cn.com.libertymutual.sp.service.api.UserService;

/**
 * @author AoYi
 * 前端用户业务逻辑类
 */
/**
 * @author AoYi
 *
 */
@Component
public class UserBiz {
	private Logger log = LoggerFactory.getLogger(getClass());

	// 注入原子性业务逻辑
	@Resource
	private UserService userService;
	@Resource
	private SmsService smsService;// 短信
	@Resource
	private RedisUtils redis;
	@Resource
	private CaptchaService captchaService;// 图形验证码
	@Resource
	private IdentifierMarkBiz identifierMarkBiz;

	public ServiceResult checkImgCode(HttpServletRequest request, String imgCode) {
		ServiceResult sr = new ServiceResult("图形码错误", ServiceResult.STATE_APP_EXCEPTION);
		// 校验-图形码
		try {
			if (captchaService.validationCaptcha(request, sr, redis, imgCode)) {
				sr.setSuccess();
				sr.setResult("图形码正确");
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return sr;// 图形码错误
	}

	/**
	 * Remarks: 个人账户验证码注册 & 短信验证码 | 图形码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年3月5日下午4:13:35<br>
	 * Project：parking<br>
	 * @param device
	 * @param request
	 * @param response
	 * @param userDto
	 * @param haveImgCode
	 * @return
	 */
	public ServiceResult register(Device device, HttpServletRequest rq, HttpServletResponse rs, UserDto userDto, boolean haveImgCode) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_EXCEPTION);
		try {
			if (haveImgCode) {
				// 校验-图形码
				if (!captchaService.validationCaptcha(rq, sr, redis, userDto.getImgCode())) {
					return sr;
				}
			}
			// 校验-短信验证码
			if (!smsService.verifyCodeBool(rq, sr, redis, userDto.getMobileCode(), userDto.getMobile())) {
				return sr;
			}
			// 注册
			sr = userService.register(rq, sr, userDto);

			identifierMarkBiz.setHeadersToken(device, rs, sr);// 根据用户信息设置token

		} catch (Exception e) {
			log.warn("验证码注册异常:{}", e.toString());
			e.printStackTrace();
		}
		return sr;
	}

	/**
	 * 渠道账户验证码注册 & 短信验证码 | 图形码	 <br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年3月27日下午4:32:16<br>
	 * @param device
	 * @param rq
	 * @param rs
	 * @param userDto
	 * @param haveImgCode
	 * @param type 业务关系代码保存模式,input=录入，select=常住地选择
	 * @return
	 */
	public ServiceResult registerChannel(Device device, HttpServletRequest rq, HttpServletResponse rs, UserDto userDto, boolean haveImgCode,
			String type) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_EXCEPTION);
		try {
			if (haveImgCode) {
				// 校验-图形码
				if (!captchaService.validationCaptcha(rq, sr, redis, userDto.getImgCode())) {
					return sr;
				}
			}
			// 校验-短信验证码
			if (!smsService.verifyCodeBool(rq, sr, redis, userDto.getMobileCode(), userDto.getMobile())) {
				return sr;
			}
			// 注册
			sr = userService.registerChannel(rq, sr, userDto, type);

			identifierMarkBiz.setHeadersToken(device, rs, sr);// 根据用户信息设置token

		} catch (Exception e) {
			log.warn("验证码注册异常:{}", e.toString());
			e.printStackTrace();
		}
		return sr;
	}

	/**Remarks: 设置密码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年10月14日下午4:55:07<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param response
	 * @param identCode
	 * @param id
	 * @param password
	 * @param confirmPassword
	 * @return
	 */
	public ServiceResult initUserPwd(HttpServletRequest request, HttpServletResponse response, String identCode, String id, String password,
			String confirmPassword) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_EXCEPTION);
		try {
			sr = userService.initUserPwd(sr, id, password, confirmPassword);
		} catch (Exception e) {
			log.warn("密码设置异常:{}", e.toString());
			e.printStackTrace();
		}
		return sr;
	}

	/**Remarks: 密码修改_短信验证码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年10月15日上午2:32:06<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param response
	 * @param identCode
	 * @param mobileCode
	 * @param id
	 * @param mobile
	 * @param password
	 * @return
	 */
	public ServiceResult updatePwdByMobile(HttpServletRequest request, HttpServletResponse response, String identCode, String mobileCode, String id,
			String mobile, String password) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_EXCEPTION);
		try {
			// 校验-短信验证码
			if (!smsService.verifyCodeBool(request, sr, redis, mobileCode, mobile)) {
				return sr;
			}
			sr = userService.updatePwdByMobile(sr, id, mobile, password);
		} catch (Exception e) {
			log.warn("密码修改异常:{}", e.toString());
			e.printStackTrace();
		}
		return sr;
	}

	/**Remarks: 密码修改_短信验证码 & 图形码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年10月15日上午2:45:15<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param response
	 * @param identCode
	 * @param imgCode
	 * @param mobileCode
	 * @param id
	 * @param mobile
	 * @param password
	 * @return
	 */
	public ServiceResult updatePwdByMobile(HttpServletRequest request, HttpServletResponse response, String identCode, String imgCode,
			String mobileCode, String id, String mobile, String password) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_EXCEPTION);
		try {
			// 校验-图形码
			if (!captchaService.validationCaptcha(request, sr, redis, imgCode)) {
				return sr;
			}
			// 校验-短信验证码
			if (!smsService.verifyCodeBool(request, sr, redis, mobileCode, mobile)) {
				return sr;
			}
			sr = userService.updatePwdByMobile(sr, id, mobile, password);
		} catch (Exception e) {
			log.warn("密码修改异常:{}", e.toString());
			e.printStackTrace();
		}
		return sr;
	}

	/**Remarks: 实名认证_短信验证码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年10月31日下午4:40:12<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param response
	 * @param identCode
	 * @param mobileCode
	 * @param userCode
	 * @param userName
	 * @param mobile
	 * @param idNumber
	 * @return
	 */
	public ServiceResult updateIdNumberByMobile(HttpServletRequest request, HttpServletResponse response, String identCode, String mobileCode,
			String userCode, String userName, String mobile, String idNumber) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_EXCEPTION);
		try {
			// 校验-短信验证码
			if (!smsService.verifyCodeBool(request, sr, redis, mobileCode, mobile)) {
				return sr;
			}
			sr = userService.updateIdNumberByMobile(sr, userCode, userName, idNumber);
		} catch (Exception e) {
			log.warn("实名认证异常:{}", e.toString());
			e.printStackTrace();
		}
		return sr;
	}

	/**Remarks: 实名认证_短信验证码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年10月31日下午4:40:12<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param response
	 * @param identCode
	 * @param mobileCode
	 * @param userCode
	 * @param userName
	 * @param mobile
	 * @param idNumber
	 * @return
	 */
	public ServiceResult updateIdNumberByMobile(HttpServletRequest request, HttpServletResponse response, String identCode, String imgCode,
			String mobileCode, String userCode, String userName, String mobile, String idNumber) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_EXCEPTION);
		try {
			// 校验-图形码
			if (!captchaService.validationCaptcha(request, sr, redis, imgCode)) {
				return sr;
			}
			// 校验-短信验证码
			if (!smsService.verifyCodeBool(request, sr, redis, mobileCode, mobile)) {
				return sr;
			}
			sr = userService.updateIdNumberByMobile(sr, userCode, userName, idNumber);
		} catch (Exception e) {
			log.warn("实名认证异常:{}", e.toString());
			e.printStackTrace();
		}
		return sr;
	}

	/**Remarks: 解绑实名认证_短信验证码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年11月20日下午2:53:53<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param response
	 * @param identCode
	 * @param mobileCode
	 * @param userCode
	 * @param mobile
	 * @param idNumber
	 * @return
	 */
	public ServiceResult unbindIdNumberByMobile(HttpServletRequest request, HttpServletResponse response, String identCode, String mobileCode,
			String id, String mobile, String idNumber) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_EXCEPTION);
		try {
			// 校验-短信验证码
			if (!smsService.verifyCodeBool(request, sr, redis, mobileCode, mobile)) {
				return sr;
			}
			sr = userService.unbindIdNumberByMobile(sr, id, idNumber);
		} catch (Exception e) {
			log.warn("解绑实名认证异常:{}", e.toString());
			e.printStackTrace();
		}
		return sr;
	}

	/**Remarks: 解绑实名认证_短信验证码 & 图形码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年11月20日下午2:53:53<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param response
	 * @param identCode
	 * @param imgCode
	 * @param mobileCode
	 * @param userCode
	 * @param mobile
	 * @param idNumber
	 * @return
	 */
	public ServiceResult unbindIdNumberByMobile(HttpServletRequest request, HttpServletResponse response, String identCode, String imgCode,
			String mobileCode, String id, String mobile, String idNumber) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_EXCEPTION);
		try {
			// 校验-图形码
			if (!captchaService.validationCaptcha(request, sr, redis, imgCode)) {
				return sr;
			}
			// 校验-短信验证码
			if (!smsService.verifyCodeBool(request, sr, redis, mobileCode, mobile)) {
				return sr;
			}
			sr = userService.unbindIdNumberByMobile(sr, id, idNumber);
		} catch (Exception e) {
			log.warn("解绑实名认证异常:{}", e.toString());
			e.printStackTrace();
		}
		return sr;
	}

	/**Remarks: 姓名修改<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年10月14日下午4:54:54<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param response
	 * @param identCode
	 * @param imgCode
	 * @param id
	 * @param userName
	 * @return
	 */
	public ServiceResult updateUserName(HttpServletRequest request, HttpServletResponse response, String identCode, String imgCode, String id,
			String userName) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_EXCEPTION);
		try {
			// 校验-图形码
			if (!captchaService.validationCaptcha(request, sr, redis, imgCode)) {
				return sr;
			}
			sr = userService.updateUserName(sr, id, userName);
		} catch (Exception e) {
			log.warn("姓名修改异常:{}", e.toString());
			e.printStackTrace();
		}
		return sr;
	}

	/**Remarks: 手机号修改_短信验证码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年10月14日下午4:54:38<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param response
	 * @param identCode
	 * @param mobileCode
	 * @param userCode
	 * @param mobile
	 * @return
	 */
	public ServiceResult updateMobile(HttpServletRequest request, HttpServletResponse response, String identCode, String mobileCode, String userCode,
			String mobile) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_EXCEPTION);
		try {
			// 校验-短信验证码
			if (!smsService.verifyCodeBool(request, sr, redis, mobileCode, mobile)) {
				return sr;
			}
			sr = userService.updateMobile(redis, sr, userCode, mobile);
		} catch (Exception e) {
			log.warn("手机号修改_短信验证码异常:{}", e.toString());
			e.printStackTrace();
		}
		return sr;
	}

	/**Remarks: 手机号修改_短信验证码 & 图形码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年10月14日下午5:35:55<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param response
	 * @param identCode
	 * @param imgCode
	 * @param mobileCode
	 * @param userCode
	 * @param mobile
	 * @return
	 */
	public ServiceResult updateMobile(HttpServletRequest request, HttpServletResponse response, String identCode, String imgCode, String mobileCode,
			String userCode, String mobile) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_EXCEPTION);
		try {
			// 校验-图形码
			if (!captchaService.validationCaptcha(request, sr, redis, imgCode)) {
				return sr;
			}
			// 校验-短信验证码
			if (!smsService.verifyCodeBool(request, sr, redis, mobileCode, mobile)) {
				return sr;
			}
			sr = userService.updateMobile(redis, sr, userCode, mobile);
		} catch (Exception e) {
			log.warn("手机号修改_短信验证码 & 图形码异常:{}", e.toString());
			e.printStackTrace();
		}
		return sr;
	}

	/**Remarks: 合并账户<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年11月22日下午2:22:05<br>
	 * Project：liberty_sale_plat<br>
	 * @param identCode
	 * @param userCode
	 * @param mobile
	 * @return
	 */
	public ServiceResult updateUserFormerge(String userCode, String mobile) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_EXCEPTION);
		try {
			sr = userService.updateUserFormerge(redis, sr, userCode, mobile);
		} catch (Exception e) {
			log.warn("合并账户异常:{}", e.toString());
			e.printStackTrace();
		}
		return sr;
	}

	/**Remarks:登录_短信验证码 <br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年10月14日下午4:53:16<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param response
	 * @param identCode
	 * @param mobileCode
	 * @param mobile
	 * @return
	 */
	public ServiceResult loginByMobile(HttpServletRequest request, HttpServletResponse response, String identCode, String mobileCode, String mobile,
			String refereeId, String refereePro) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_EXCEPTION);
		try {
			// 校验-短信验证码
			if (!smsService.verifyCodeBool(request, sr, redis, mobileCode, mobile)) {
				return sr;
			}
			sr = userService.loginByMobile(request, sr, mobile, refereeId, refereePro);
		} catch (Exception e) {
			log.warn("登录[短信验证码]异常:{}", e.toString());
			e.printStackTrace();
		}
		return sr;
	}

	/**Remarks: 登录_短信验证码 & 图形码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年10月14日下午4:53:56<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param response
	 * @param identCode
	 * @param imgCode
	 * @param mobileCode
	 * @param mobile
	 * @return
	 */
	public ServiceResult loginByMobile(HttpServletRequest request, HttpServletResponse response, String identCode, String imgCode, String mobileCode,
			String mobile, String refereeId, String refereePro) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_EXCEPTION);
		try {
			// 校验-图形码
			if (!captchaService.validationCaptcha(request, sr, redis, imgCode)) {
				return sr;
			}
			// 校验-短信验证码
			if (!smsService.verifyCodeBool(request, sr, redis, mobileCode, mobile)) {
				return sr;
			}
			sr = userService.loginByMobile(request, sr, mobile, refereeId, refereePro);
		} catch (Exception e) {
			log.warn("登录[短信验证码]异常:{}", e.toString());
			e.printStackTrace();
		}
		return sr;
	}

	/**Remarks: 登录_密码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年10月14日下午4:54:13<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param response
	 * @param identCode
	 * @param imgCode
	 * @param mobile
	 * @param password
	 * @return
	 */
	public ServiceResult loginBypassword(HttpServletRequest request, HttpServletResponse response, String identCode, String imgCode, String mobile,
			String password) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_EXCEPTION);
		try {
			// 校验-图形码
			if (!captchaService.validationCaptcha(request, sr, redis, imgCode)) {
				return sr;
			}
			sr = userService.loginBypassword(request, sr, mobile, password);
		} catch (Exception e) {
			log.warn("登录[密码]异常:{}", e.toString());
			e.printStackTrace();
		}
		return sr;
	}

	/**Remarks: 根据ID获取用户<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年10月16日下午11:27:39<br>
	 * Project：liberty_sale_plat<br>
	 * @param id
	 * @return
	 */
	public ServiceResult findUserByIdAndLogin(HttpServletRequest request, Integer id) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_EXCEPTION);
		try {
			sr = userService.findUserByIdAndLogin(request, sr, id);
		} catch (Exception e) {
			log.warn("获取用户异常:{}", e.toString());
			e.printStackTrace();
		}
		return sr;
	}

	/** 根据userCode获取用户<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年3月28日下午4:51:14<br>
	 * @param request
	 * @param userCode
	 * @return
	 */
	public ServiceResult findUserByUserCode(HttpServletRequest request, String userCode) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_EXCEPTION);
		try {
			sr = userService.findUserByUserCode(request, sr, userCode);
		} catch (Exception e) {
			log.warn("获取用户异常:{}", e.toString());
			e.printStackTrace();
		}
		return sr;
	}

	/** 
	 * 查询上级用户<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年4月28日下午3:09:27<br>
	 * @param request
	 * @param comCode
	 * @return
	 */
	public ServiceResult findUserByComcode(HttpServletRequest request, String comCode) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_EXCEPTION);
		try {
			sr = userService.findUserByComcode(request, sr, comCode);
		} catch (Exception e) {
			log.warn("获取用户异常:{}", e.toString());
			e.printStackTrace();
		}
		return sr;
	}

	/**Remarks: 根据微信openID获取用户<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年10月16日下午11:28:15<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param response
	 * @param identCode
	 * @param openId
	 * @return
	 */
	public ServiceResult updateUserByOpenId(HttpServletRequest request, HttpServletResponse response, /* String identCode, */
			String openId) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_EXCEPTION);
		try {
			sr = userService.updateUserByOpenId(request, sr, null, openId);
		} catch (Exception e) {
			log.warn("获取微信用户异常:{}", e.toString());
			e.printStackTrace();
		}
		return sr;
	}

	/**Remarks: 查询某用户所有有效银行卡<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年12月7日下午3:24:11<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param userCode
	 * @return
	 */
	public ServiceResult findBankListAll(HttpServletRequest request, String userCode) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_EXCEPTION);
		try {
			sr = userService.findBankListAll(sr, userCode);
		} catch (Exception e) {
			log.warn("查询某用户所有有效银行卡异常:{}", e.toString());
			e.printStackTrace();
		}
		return sr;
	}

	/**Remarks: 绑定银行卡=>第一步校验卡号<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年12月6日下午11:56:19<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param userCode
	 * @param userName
	 * @param bankNumber
	 * @param province
	 * @param city
	 * @param bankBranchName
	 * @return
	 */
	public ServiceResult findBankNoOfBinding(HttpServletRequest request, String userCode, String userName, String bankNumber, String province,
			String city, String bankBranchName) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_EXCEPTION);
		try {
			sr = userService.findBankNoOfBinding(request, sr, redis, userCode, userName, bankNumber, province, city, bankBranchName);
		} catch (Exception e) {
			log.warn("绑定银行卡=>第一步校验卡号异常:{}", e.toString());
			e.printStackTrace();
		}
		return sr;
	}

	/**Remarks: 绑定银行卡=>第二步校验短信验证码并绑定<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年12月7日上午10:47:50<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param userCode
	 * @param bankName
	 * @param bankNumber
	 * @param mobileCode
	 * @param mobile
	 * @return
	 */
	public ServiceResult updateBankOfBinding(HttpServletRequest request, String userCode, String bankName, String bankNumber, String mobileCode,
			String mobile) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_EXCEPTION);
		try {

			// 校验-短信验证码
			if (!smsService.verifyCodeBool(request, sr, redis, mobileCode, mobile)) {
				return sr;
			}

			sr = userService.updateBankOfBinding(request, sr, redis, userCode, bankName, bankNumber, mobile);
		} catch (Exception e) {
			log.warn("绑定银行卡=>第二步校验短信验证码并绑定异常:{}", e.toString());
			e.printStackTrace();
		}
		return sr;
	}

	/**
	 * Remarks: 解绑银行卡<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年1月19日下午4:02:59<br>
	 * Project：liberty_sale_plat<br>
	 * @param userCode
	 * @param bankId
	 * @param number
	 * @return
	 */
	public ServiceResult updateBankUnbundling(String userCode, Integer bankId, String number) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_EXCEPTION);
		try {
			sr = userService.updateBankUnbundling(sr, userCode, bankId, number);
		} catch (Exception e) {
			log.warn("解绑银行卡异常:{}", e.toString());
			e.printStackTrace();
		}
		return sr;
	}

	/**Remarks: 设置默认银行卡<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年12月8日下午6:14:24<br>
	 * Project：liberty_sale_plat<br>
	 * @param userCode
	 * @param bankId
	 * @param number
	 * @return
	 */
	public ServiceResult updateBankDefault(String userCode, Integer bankId, String number) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_EXCEPTION);
		try {
			sr = userService.updateBankDefault(sr, userCode, bankId, number);
		} catch (Exception e) {
			log.warn("设置默认银行卡异常:{}", e.toString());
			e.printStackTrace();
		}
		return sr;
	}
}
