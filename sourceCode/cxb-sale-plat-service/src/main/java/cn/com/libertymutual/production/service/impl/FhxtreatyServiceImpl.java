package cn.com.libertymutual.production.service.impl;

import cn.com.libertymutual.production.dao.nomorcldatasource.FhxtreatyMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Fhxtreaty;
import cn.com.libertymutual.production.model.nomorcldatasource.FhxtreatyExample;
import cn.com.libertymutual.production.service.api.FhxtreatyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @author steven.li
 * @create 2017/12/20
 */
@Service
public class FhxtreatyServiceImpl implements FhxtreatyService {

    @Autowired
    private FhxtreatyMapper fhxtreatyMapper;

    @Override
    public void insert(Fhxtreaty record) {
        fhxtreatyMapper.insertSelective(record);
    }

    @Override
    public List<Fhxtreaty> findAll(Fhxtreaty where) {
        return fhxtreatyMapper.findAllBySectionExisted(where.getUwyear());
    }
}
