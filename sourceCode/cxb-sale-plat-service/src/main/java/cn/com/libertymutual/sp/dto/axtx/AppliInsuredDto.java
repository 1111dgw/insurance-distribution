package cn.com.libertymutual.sp.dto.axtx;

import java.io.Serializable;

public class AppliInsuredDto implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -8503022884798274684L;
	 
	 
	     private String paymentMethord;
	     private String insuredType	;//	投保人类型
	     private String insuredName	;//	投保人名称
	     private String identifyType	;//投保人证件类型
	     private String identifyNumber;//	投保人证件号码
	     private String linkMobile	;//	投保人手机
	     private String linkName		;//投保联系人
	     private String linkTel		;//	投保人电话
	     private String zipCode		;//	邮编
	     private String gender		;//	性别
	     private String birth			;//出生日期
	     private String age			;//	年龄
	     private String email			;//投保人邮箱地址
	     private String insdaddr		;//	投保人 地址
		public String getPaymentMethord() {
			return paymentMethord;
		}
		public void setPaymentMethord(String paymentMethord) {
			this.paymentMethord = paymentMethord;
		}
		public String getInsuredType() {
			return insuredType;
		}
		public void setInsuredType(String insuredType) {
			this.insuredType = insuredType;
		}
		public String getInsuredName() {
			return insuredName;
		}
		public void setInsuredName(String insuredName) {
			this.insuredName = insuredName;
		}
		public String getIdentifyType() {
			return identifyType;
		}
		public void setIdentifyType(String identifyType) {
			this.identifyType = identifyType;
		}
		public String getIdentifyNumber() {
			return identifyNumber;
		}
		public void setIdentifyNumber(String identifyNumber) {
			this.identifyNumber = identifyNumber;
		}
		public String getLinkMobile() {
			return linkMobile;
		}
		public void setLinkMobile(String linkMobile) {
			this.linkMobile = linkMobile;
		}
		public String getLinkName() {
			return linkName;
		}
		public void setLinkName(String linkName) {
			this.linkName = linkName;
		}
		public String getLinkTel() {
			return linkTel;
		}
		public void setLinkTel(String linkTel) {
			this.linkTel = linkTel;
		}
		public String getZipCode() {
			return zipCode;
		}
		public void setZipCode(String zipCode) {
			this.zipCode = zipCode;
		}
		public String getGender() {
			return gender;
		}
		public void setGender(String gender) {
			this.gender = gender;
		}
		public String getBirth() {
			return birth;
		}
		public void setBirth(String birth) {
			this.birth = birth;
		}
		public String getAge() {
			return age;
		}
		public void setAge(String age) {
			this.age = age;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getInsdaddr() {
			return insdaddr;
		}
		public void setInsdaddr(String insdaddr) {
			this.insdaddr = insdaddr;
		}
		@Override
		public String toString() {
			return "AppliInsuredDto [paymentMethord=" + paymentMethord + ", insuredType=" + insuredType + ", insuredName="
					+ insuredName + ", identifyType=" + identifyType + ", identifyNumber=" + identifyNumber
					+ ", linkMobile=" + linkMobile + ", linkName=" + linkName + ", linkTel=" + linkTel + ", zipCode="
					+ zipCode + ", gender=" + gender + ", birth=" + birth + ", age=" + age + ", email=" + email
					+ ", insdaddr=" + insdaddr + "]";
		}
	     
	     
}
