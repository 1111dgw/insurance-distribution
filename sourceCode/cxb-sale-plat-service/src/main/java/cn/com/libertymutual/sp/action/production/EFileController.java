package cn.com.libertymutual.sp.action.production;

import java.beans.PropertyEditorSupport;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import cn.com.libertymutual.production.pojo.request.PrpdEFileRequest;
import cn.com.libertymutual.production.pojo.response.Response;
import cn.com.libertymutual.production.service.api.business.EFileBusinessService;

/** 
 * @author Steven.Li
 * @date 2017年7月28日
 *  
 */
@RestController
@RequestMapping("efile")
public class EFileController {
	
	@Autowired
	protected EFileBusinessService eFileBusinessService;
	
	/**
	 * 获取备案号信息
	 * @param PrpdEFileRequest
	 * @return
	 */
	@RequestMapping("queryPrpdEFile")
	public Response queryPrpdEFile(PrpdEFileRequest prpdEFileRequest) {
		Response result = eFileBusinessService.findPrpdEFile(prpdEFileRequest);
		return result;
	}
	
	/**
	 * 新增备案号
	 * @param request
	 * @return
	 */
	@RequestMapping("insertEFile")
	public Response addEFile(MultipartHttpServletRequest fileRequest, PrpdEFileRequest prpdEFileRequest) {
		Response result = new Response();
		try {
			result = eFileBusinessService.insertEfile(fileRequest, prpdEFileRequest);
			result.setSuccess();
		} catch (Exception e) {
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}
	
	/**
	 * 修改备案号
	 * @param request
	 * @return
	 */
	@RequestMapping("updateEFile")
	public Response updateEFile(MultipartHttpServletRequest fileRequest, PrpdEFileRequest prpdEFileRequest) {
		Response result = new Response();
		try {
			result = eFileBusinessService.updateEfile(fileRequest, prpdEFileRequest);
			result.setSuccess();
		} catch (Exception e) {
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}
	
	@RequestMapping("download")
	public Response download(String fileNm,HttpServletResponse response) {
		return eFileBusinessService.download(fileNm, response);
	}
	
	@InitBinder
	public void InitBinder(WebDataBinder dataBinder) {
		dataBinder.registerCustomEditor(Date.class, new PropertyEditorSupport() {
			
		    @SuppressWarnings("deprecation")
			public void setAsText(String value) {
		        try {
		        	if(value.indexOf("GMT") == -1 && value.length() > 4) {	//如果不是格林威治时间且传递的值不是四位的年份，即传递的为Long类型的毫秒数
		        		setValue(new Date(new Long(value)));
		        	} else {
		        		setValue(new Date(value));							//如果是格林威治时间
		        	}
		        } catch(Exception e) {
		            setValue(null);
		        }
		    }

		    public String getAsText() {
		        return new SimpleDateFormat("yyyy-MM-dd").format((Date) getValue());
		    }        

		});
		
		dataBinder.registerCustomEditor(String.class, new PropertyEditorSupport() {
			
			public void setAsText(String value) {
	            if("null".equals(value) || "".equals(value)) {
	            	setValue(null);
	            } else {
	            	setValue(value);
	            }
		    }

		    public String getAsText() {
		        return (String) getValue();
		    }        

		});
	}
	
}
