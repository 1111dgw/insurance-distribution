package cn.com.libertymutual.sp.dto.axtx;

import java.io.Serializable;


public class RequestBaseDto implements Serializable {
	/**
	 *  
	 */
	private static final long serialVersionUID = -1445069485598902073L;
	private	String partnerAccountCode;// 合作方编号 宝提供
	private	String passKey; // 提供的密钥 宝提供
	private String flowId; // 业务流程ID 次业务请求,必须上传 约定结构(partnerAccountCode+yyMMdd+8流水
					// )/如果使用方已有规则,在现有规则前必须将合作方编码作为前缀
	private String operatorDate; // 操作时间 yyyy-MM-dd hh:mm:ss
	private String recordCode; // 备案号 案号,平台备案
	private String agreementNo; // 业务关系代码 利宝提供,作为查询条件进行查询操作
	private String pdk ;   //加密后验证码
	private String access_token;
	public String getPartnerAccountCode() {
		return partnerAccountCode;
	}
	public void setPartnerAccountCode(String partnerAccountCode) {
		this.partnerAccountCode = partnerAccountCode;
	}
	public String getPassKey() {
		return passKey;
	}
	public void setPassKey(String passKey) {
		this.passKey = passKey;
	}
	public String getFlowId() {
		return flowId;
	}
	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}
	public String getOperatorDate() {
		return operatorDate;
	}
	public void setOperatorDate(String operatorDate) {
		this.operatorDate = operatorDate;
	}
	public String getRecordCode() {
		return recordCode;
	}
	public void setRecordCode(String recordCode) {
		this.recordCode = recordCode;
	}
	public String getAgreementNo() {
		return agreementNo;
	}
	public void setAgreementNo(String agreementNo) {
		this.agreementNo = agreementNo;
	}
	public String getPdk() {
		return pdk;
	}
	public void setPdk(String pdk) {
		this.pdk = pdk;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((access_token == null) ? 0 : access_token.hashCode());
		result = prime * result
				+ ((agreementNo == null) ? 0 : agreementNo.hashCode());
		result = prime * result + ((flowId == null) ? 0 : flowId.hashCode());
		result = prime * result
				+ ((operatorDate == null) ? 0 : operatorDate.hashCode());
		result = prime
				* result
				+ ((partnerAccountCode == null) ? 0 : partnerAccountCode
						.hashCode());
		result = prime * result + ((passKey == null) ? 0 : passKey.hashCode());
		result = prime * result + ((pdk == null) ? 0 : pdk.hashCode());
		result = prime * result
				+ ((recordCode == null) ? 0 : recordCode.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RequestBaseDto other = (RequestBaseDto) obj;
		if (access_token == null) {
			if (other.access_token != null)
				return false;
		} else if (!access_token.equals(other.access_token))
			return false;
		if (agreementNo == null) {
			if (other.agreementNo != null)
				return false;
		} else if (!agreementNo.equals(other.agreementNo))
			return false;
		if (flowId == null) {
			if (other.flowId != null)
				return false;
		} else if (!flowId.equals(other.flowId))
			return false;
		if (operatorDate == null) {
			if (other.operatorDate != null)
				return false;
		} else if (!operatorDate.equals(other.operatorDate))
			return false;
		if (partnerAccountCode == null) {
			if (other.partnerAccountCode != null)
				return false;
		} else if (!partnerAccountCode.equals(other.partnerAccountCode))
			return false;
		if (passKey == null) {
			if (other.passKey != null)
				return false;
		} else if (!passKey.equals(other.passKey))
			return false;
		if (pdk == null) {
			if (other.pdk != null)
				return false;
		} else if (!pdk.equals(other.pdk))
			return false;
		if (recordCode == null) {
			if (other.recordCode != null)
				return false;
		} else if (!recordCode.equals(other.recordCode))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "RequestBaseDto [partnerAccountCode=" + partnerAccountCode
				+ ", passKey=" + passKey + ", flowId=" + flowId
				+ ", operatorDate=" + operatorDate + ", recordCode="
				+ recordCode + ", agreementNo=" + agreementNo + ", pdk=" + pdk
				+ ", access_token=" + access_token + "]";
	}
	public String getAccess_token() {
		return access_token;
	}
	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}
}
