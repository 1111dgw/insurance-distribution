package cn.com.libertymutual.production.service.api;

import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdkindlibrary;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdration;


public interface PrpdrationService {

	public Prpdkindlibrary checkKindDeletable(String kindCode, String kindVersion);
	
	public void insert(Prpdration record);

	public void deleteByPlan(Prpdration criteria);

	public List<Prpdration> findByPlanCode(String planCode, Boolean isValid);

	public void update(Prpdration record);
	
	public void updateByCriteria(Prpdration record, Prpdration criteria);

	public int getMaxSerialNum(Prpdration criteria);

	public void inActiveRation(String kindCode, String kindVersion);
}
