package cn.com.libertymutual.sp.dto.epayment;

import java.io.Serializable;

public class MasMessage implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6657466564360798752L;
	private String version;
	
	private BatchQryWithPayOrderMsgContent BatchQryWithPayOrderMsgContent;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public BatchQryWithPayOrderMsgContent getBatchQryWithPayOrderMsgContent() {
		return BatchQryWithPayOrderMsgContent;
	}

	public void setBatchQryWithPayOrderMsgContent(
			BatchQryWithPayOrderMsgContent batchQryWithPayOrderMsgContent) {
		BatchQryWithPayOrderMsgContent = batchQryWithPayOrderMsgContent;
	}
	
	
}
