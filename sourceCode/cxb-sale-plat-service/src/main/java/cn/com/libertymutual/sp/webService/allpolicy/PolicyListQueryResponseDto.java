
package cn.com.libertymutual.sp.webService.allpolicy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for policyListQueryResponseDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="policyListQueryResponseDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="motorPolicyDtoList" type="{http://service.liberty.com/common/bean}motorPolicyDto" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="nonMotorPolicyDtoList" type="{http://service.liberty.com/common/bean}nonMotorPolicyDto" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="responseHeadDto" type="{http://service.liberty.com/common/bean}responseHeadDto" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "policyListQueryResponseDto", namespace = "http://prpall.liberty.com/all/cb/policyListQuery/bean", propOrder = {
    "motorPolicyDtoList",
    "nonMotorPolicyDtoList",
    "responseHeadDto"
})
public class PolicyListQueryResponseDto implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@XmlElement(nillable = true)
    protected List<MotorPolicyDto> motorPolicyDtoList;
    @XmlElement(nillable = true)
    protected List<NonMotorPolicyDto> nonMotorPolicyDtoList;
    protected ResponseHeadDto responseHeadDto;

    /**
     * Gets the value of the motorPolicyDtoList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the motorPolicyDtoList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMotorPolicyDtoList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MotorPolicyDto }
     * 
     * 
     */
    public List<MotorPolicyDto> getMotorPolicyDtoList() {
        if (motorPolicyDtoList == null) {
            motorPolicyDtoList = new ArrayList<MotorPolicyDto>();
        }
        return this.motorPolicyDtoList;
    }

    /**
     * Gets the value of the nonMotorPolicyDtoList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the nonMotorPolicyDtoList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNonMotorPolicyDtoList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NonMotorPolicyDto }
     * 
     * 
     */
    public List<NonMotorPolicyDto> getNonMotorPolicyDtoList() {
        if (nonMotorPolicyDtoList == null) {
            nonMotorPolicyDtoList = new ArrayList<NonMotorPolicyDto>();
        }
        return this.nonMotorPolicyDtoList;
    }

    /**
     * Gets the value of the responseHeadDto property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseHeadDto }
     *     
     */
    public ResponseHeadDto getResponseHeadDto() {
        return responseHeadDto;
    }

    /**
     * Sets the value of the responseHeadDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseHeadDto }
     *     
     */
    public void setResponseHeadDto(ResponseHeadDto value) {
        this.responseHeadDto = value;
    }

}
