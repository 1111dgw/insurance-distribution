package cn.com.libertymutual.sp.service.api;

import java.util.List;

import cn.com.libertymutual.sp.bean.TbSpBankCard;

public interface BankCardService {
	/**
	 * 获取银行卡列表<br>
	 * Version：2.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年5月18日上午10:37:25<br>
	 * @return
	 */
	public List<TbSpBankCard> findAllBank();

	/**
	 * 获取银行卡名称列表（已排除重复名称）<br>
	 * Version：2.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年5月18日上午10:37:25<br>
	 * @return
	 */
	public List<String> findAllBankNameByNotRepeat();

	/**
	 * 获取银行卡详细对象<br>
	 * Version：2.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年5月18日上午10:37:22<br>
	 * @param charNo
	 * @return
	 */
	public TbSpBankCard getBankCard(String charNo);

	public void saveBanks();

}
