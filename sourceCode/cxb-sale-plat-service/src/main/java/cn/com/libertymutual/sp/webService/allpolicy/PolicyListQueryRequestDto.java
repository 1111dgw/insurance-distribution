
package cn.com.libertymutual.sp.webService.allpolicy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for policyListQueryRequestDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="policyListQueryRequestDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="insuredIdentityNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="insuredIdentityType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="insuredName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="phoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="requestHeadDto" type="{http://service.liberty.com/common/bean}requestHeadDto" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "policyListQueryRequestDto", namespace = "http://prpall.liberty.com/all/cb/policyListQuery/bean", propOrder = {
    "insuredIdentityNumber",
    "insuredIdentityType",
    "insuredName",
    "phoneNumber",
    "requestHeadDto"
})
public class PolicyListQueryRequestDto implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected String insuredIdentityNumber;
    protected String insuredIdentityType;
    protected String insuredName;
    protected String phoneNumber;
    protected RequestHeadDto requestHeadDto;

    /**
     * Gets the value of the insuredIdentityNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuredIdentityNumber() {
        return insuredIdentityNumber;
    }

    /**
     * Sets the value of the insuredIdentityNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuredIdentityNumber(String value) {
        this.insuredIdentityNumber = value;
    }

    /**
     * Gets the value of the insuredIdentityType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuredIdentityType() {
        return insuredIdentityType;
    }

    /**
     * Sets the value of the insuredIdentityType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuredIdentityType(String value) {
        this.insuredIdentityType = value;
    }

    /**
     * Gets the value of the insuredName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuredName() {
        return insuredName;
    }

    /**
     * Sets the value of the insuredName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuredName(String value) {
        this.insuredName = value;
    }

    /**
     * Gets the value of the phoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Sets the value of the phoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneNumber(String value) {
        this.phoneNumber = value;
    }

    /**
     * Gets the value of the requestHeadDto property.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeadDto }
     *     
     */
    public RequestHeadDto getRequestHeadDto() {
        return requestHeadDto;
    }

    /**
     * Sets the value of the requestHeadDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeadDto }
     *     
     */
    public void setRequestHeadDto(RequestHeadDto value) {
        this.requestHeadDto = value;
    }

}
