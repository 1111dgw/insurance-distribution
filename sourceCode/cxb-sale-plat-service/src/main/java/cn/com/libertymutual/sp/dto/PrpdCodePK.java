package cn.com.libertymutual.sp.dto;


import java.io.Serializable;

public class PrpdCodePK implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -370867870965343109L;
	private String codeType;
	private String codeCode;
	public String getCodeType() {
		return codeType;
	}
	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}
	public String getCodeCode() {
		return codeCode;
	}
	public void setCodeCode(String codeCode) {
		this.codeCode = codeCode;
	}
	
}

