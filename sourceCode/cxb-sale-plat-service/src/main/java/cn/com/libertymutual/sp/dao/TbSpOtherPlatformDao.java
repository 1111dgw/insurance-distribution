package cn.com.libertymutual.sp.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.com.libertymutual.sp.bean.TbSpOtherPlatform;

/**
 * @author AoYi
 * 
 * 注意@Modifying注解需要使用clearAutomatically=true;
 * 同一接口更新后立即查询获得更新后的数据,默认false查询还是更新前的数据
 *
 */
@Repository
public interface TbSpOtherPlatformDao extends PagingAndSortingRepository<TbSpOtherPlatform, Integer>, JpaSpecificationExecutor<TbSpOtherPlatform> {
	@Query("select t from TbSpOtherPlatform t where t.applyName = '智通' "
			+ "and (t.email = :email or t.mobile = :mobile) and t.createTime >= :createTime")
	public List<TbSpOtherPlatform> findByTime(@Param("email") String email, @Param("mobile") String mobile, @Param("createTime") Date createTime);
}
