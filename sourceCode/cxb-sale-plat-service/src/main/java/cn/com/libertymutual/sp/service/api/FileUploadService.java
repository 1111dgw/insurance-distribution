package cn.com.libertymutual.sp.service.api;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.dto.ImageBase64Dto;

/**
 * 文件上传
 */
public interface FileUploadService {
	/**Remarks: 单文件上传<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年11月7日下午6:26:08<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param userCode
	 * @param file
	 * @param absolutePath 绝对路径
	 * @param relativePath 相对路径
	 * @return
	 * @throws IOException
	 */
	public ServiceResult uploadFile(HttpServletRequest request, String userCode, MultipartFile file, String absolutePath, String relativePath)
			throws IOException;

	/**Remarks: 多图片上传[file]<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年11月8日下午6:43:53<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param imgFiles
	 * @param absolutePath
	 * @return
	 * @throws IOException
	 */
	public ServiceResult uploadImages(HttpServletRequest request, MultipartFile[] imgFiles, String absolutePath) throws IOException;

	/**
	 * 多图片上传到SFTP文件服务器[file]<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年4月23日上午11:31:07<br>
	 * @param request
	 * @param imgFiles 图片文件数组
	 * @param rootPath 上传根目录
	 * @param folderName 分类文件夹目录
	 * @return 返回文件标准（去除../这样的符号）的绝地路径列表(list)
	 * @throws IOException
	 */
	public ServiceResult uploadImagesToSFTP(HttpServletRequest request, MultipartFile[] imgFiles, String rootPath, String folderName)
			throws IOException;

	/**Remarks: base64字符串转化成图片,对字节数组字符串进行Base64解码并生成图片<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年11月7日下午6:25:46<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param userCode
	 * @param imgStrCode
	 * @param absolutePath 绝对路径
	 * @param relativePath 相对路径
	 * @return
	 * @throws IOException
	 */
	public ServiceResult GenerateImage(HttpServletRequest request, String userCode, String imgStrCode, String absolutePath, String relativePath)
			throws IOException;

	/**Remarks: 多图片编码上传<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年11月7日下午6:14:17<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param imageBase64Dto
	 * @param absolutePath 绝对路径
	 * @param relativePath 相对路径
	 * @return
	 * @throws IOException
	 */
	public ServiceResult uploadImagesByCode(HttpServletRequest request, ImageBase64Dto imageBase64Dto, String absolutePath, String relativePath)
			throws IOException;

	/** Remarks : 保存文件 <br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年9月27日上午11:26:23<br>
	 * Project：liberty_sale_plat<br>
	 * @param uploadHeadimgPathUsers 相对路径
	 */
	public String saveFile(MultipartFile file, String uploadHeadimgPathUsers, HttpServletRequest request);

	/** Remarks : 根据路径获取文件 <br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年9月27日上午11:26:30<br>
	 * Project：liberty_sale_plat<br>
	 */
	public File getFile(String path, String fileName, HttpServletRequest request);

	/** 
	  * 将文件转成base64 字符串 
	  * @param path文件路径 
	  * @return  *  
	  * @throws Exception 
	  */

	public String encodeBase64File(String path) throws Exception;
}
