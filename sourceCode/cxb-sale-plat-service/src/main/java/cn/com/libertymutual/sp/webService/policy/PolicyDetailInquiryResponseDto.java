
package cn.com.libertymutual.sp.webService.policy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for policyDetailInquiryResponseDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="policyDetailInquiryResponseDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="motorPolicyDto" type="{http://service.liberty.com/common/bean}motorPolicyDto" minOccurs="0"/>
 *         &lt;element name="nonMotorPolicyDto" type="{http://service.liberty.com/common/bean}nonMotorPolicyDto" minOccurs="0"/>
 *         &lt;element name="responseHeadDto" type="{http://service.liberty.com/common/bean}responseHeadDto" minOccurs="0"/>
 *         &lt;element name="mTPLPolicyDto" type="{http://service.liberty.com/common/bean}mtplPolicyDto" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "policyDetailInquiryResponseDto", namespace = "http://prpall.liberty.com/all/cb/policyDetailInquiry/bean", propOrder = {
    "motorPolicyDto",
    "nonMotorPolicyDto",
    "responseHeadDto",
    "mtplPolicyDto"
})
public class PolicyDetailInquiryResponseDto implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    protected MotorPolicyDto motorPolicyDto;
    protected NonMotorPolicyDto nonMotorPolicyDto;
    protected ResponseHeadDto responseHeadDto;
    @XmlElement(name = "mTPLPolicyDto")
    protected MtplPolicyDto mtplPolicyDto;

    /**
     * Gets the value of the motorPolicyDto property.
     * 
     * @return
     *     possible object is
     *     {@link MotorPolicyDto }
     *     
     */
    public MotorPolicyDto getMotorPolicyDto() {
        return motorPolicyDto;
    }

    /**
     * Sets the value of the motorPolicyDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link MotorPolicyDto }
     *     
     */
    public void setMotorPolicyDto(MotorPolicyDto value) {
        this.motorPolicyDto = value;
    }

    /**
     * Gets the value of the nonMotorPolicyDto property.
     * 
     * @return
     *     possible object is
     *     {@link NonMotorPolicyDto }
     *     
     */
    public NonMotorPolicyDto getNonMotorPolicyDto() {
        return nonMotorPolicyDto;
    }

    /**
     * Sets the value of the nonMotorPolicyDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link NonMotorPolicyDto }
     *     
     */
    public void setNonMotorPolicyDto(NonMotorPolicyDto value) {
        this.nonMotorPolicyDto = value;
    }

    /**
     * Gets the value of the responseHeadDto property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseHeadDto }
     *     
     */
    public ResponseHeadDto getResponseHeadDto() {
        return responseHeadDto;
    }

    /**
     * Sets the value of the responseHeadDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseHeadDto }
     *     
     */
    public void setResponseHeadDto(ResponseHeadDto value) {
        this.responseHeadDto = value;
    }

    /**
     * Gets the value of the mtplPolicyDto property.
     * 
     * @return
     *     possible object is
     *     {@link MtplPolicyDto }
     *     
     */
    public MtplPolicyDto getMTPLPolicyDto() {
        return mtplPolicyDto;
    }

    /**
     * Sets the value of the mtplPolicyDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link MtplPolicyDto }
     *     
     */
    public void setMTPLPolicyDto(MtplPolicyDto value) {
        this.mtplPolicyDto = value;
    }

}
