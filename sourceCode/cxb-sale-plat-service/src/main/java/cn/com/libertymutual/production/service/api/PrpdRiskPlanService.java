package cn.com.libertymutual.production.service.api;


import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdriskplan;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdriskplanWithBLOBs;
import cn.com.libertymutual.production.pojo.request.PrpdRiskPlanRequest;

import com.github.pagehelper.PageInfo;

public interface PrpdRiskPlanService {

	/**
	 * 查询方案信息
	 * @param prpdkind
	 * @param currentPage
	 * @param pageSize
	 * @return
	 */
	public PageInfo<PrpdriskplanWithBLOBs> findPrpdRiskPlan(PrpdRiskPlanRequest prpdRiskPlanRequest);
	
	public void insert(PrpdriskplanWithBLOBs record);

	public void update(PrpdriskplanWithBLOBs riskPlan);

	public void inActiveRiskPlan(String kindCode, String kindVersion);

	public List<Prpdriskplan> checkRiskLinkedDeletable(String kindCode,
			String riskcode, String riskversion);

	public List<Prpdriskplan> checkItemLinkedDeletable(String kindCode,
			String kindVersion,
			String itemcode);
	
	/**
	 * 通过险种代码查询关联的方案
	 * @param riskCodes
	 * @return
	 */
	public List<PrpdriskplanWithBLOBs> findPlansByRisks(List<String> riskCodes);

	/**
	 * 获取当前条款已关联的方案
	 * @param key
	 * @param value
	 * @return
	 */
	public List<String> fetchPlancodesKindUsed(String key, String value);
}
