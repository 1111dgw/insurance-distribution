package cn.com.libertymutual.sp.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "tb_sp_scoreconfig")
public class TbSpScoreConfig implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6759242577054856985L;
	private Integer id;
	private String ruleSwitch;
	private Integer period;
	private Double rate;
	private Double feeRate;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date updateTime;
	private String tbreback;
	private String updateCode;// 更新人员

	private Double referRate;// 直接上级比例
	private Double superRate;// 间级上级比例

	public TbSpScoreConfig() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TbSpScoreConfig(Integer period, Double rate) {
		super();
		this.period = period;
		this.rate = rate;
	}

	public TbSpScoreConfig(String ruleSwitch, Integer period, Double rate, Double feeRate, Date updateTime, String tbreback, String updateCode) {
		super();
		this.ruleSwitch = ruleSwitch;
		this.period = period;
		this.rate = rate;
		this.feeRate = feeRate;
		this.updateTime = updateTime;
		this.tbreback = tbreback;
		this.updateCode = updateCode;
	}

	public TbSpScoreConfig(Integer id, String ruleSwitch, Integer period, Double rate, Double feeRate, Date updateTime, String tbreback,
			String updateCode) {
		super();
		this.id = id;
		this.ruleSwitch = ruleSwitch;
		this.period = period;
		this.rate = rate;
		this.feeRate = feeRate;
		this.updateTime = updateTime;
		this.tbreback = tbreback;
		this.updateCode = updateCode;
	}

	public TbSpScoreConfig(String ruleSwitch, Integer period, Double rate, Double feeRate, Date updateTime, String tbreback, String updateCode,
			double referRate, double superRate) {
		super();
		this.ruleSwitch = ruleSwitch;
		this.period = period;
		this.rate = rate;
		this.feeRate = feeRate;
		this.updateTime = updateTime;
		this.tbreback = tbreback;
		this.updateCode = updateCode;
		this.referRate = referRate;
		this.superRate = superRate;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "ruleSwitch", length = 1)
	public String getRuleSwitch() {
		return ruleSwitch;
	}

	public void setRuleSwitch(String ruleSwitch) {
		this.ruleSwitch = ruleSwitch;
	}

	@Column(name = "period", length = 11)
	public Integer getPeriod() {
		return period;
	}

	public void setPeriod(Integer period) {
		this.period = period;
	}

	@Column(name = "rate")
	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	@Column(name = "update_time")
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Column(name = "tbreback", length = 1)
	public String getTbreback() {
		return tbreback;
	}

	public void setTbreback(String tbreback) {
		this.tbreback = tbreback;
	}

	@Column(name = "update_code", length = 32)
	public String getUpdateCode() {
		return updateCode;
	}

	public void setUpdateCode(String updateCode) {
		this.updateCode = updateCode;
	}

	@Column(name = "feeRate", length = 10)
	public Double getFeeRate() {
		return feeRate;
	}

	public void setFeeRate(Double feeRate) {
		this.feeRate = feeRate;
	}

	@Column(name = "REFER_RATE", length = 10)
	public double getReferRate() {
		return referRate;
	}

	public void setReferRate(double referRate) {
		this.referRate = referRate;
	}

	@Column(name = "SUPER_RATE", length = 10)
	public double getSuperRate() {
		return superRate;
	}

	public void setSuperRate(double superRate) {
		this.superRate = superRate;
	}

}
