package cn.com.libertymutual.sp.bean;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_sp_police")
public class TbSpPolice implements Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 4790237962934920896L;
	private Integer id;
	private String userId;
	private String proposalNo;
	private String policeNo;
	private String productName;
	private String planId;
	private String planName;
	private Double amount;
	private Timestamp proposalTime;
	private Timestamp startTime;
	private Timestamp endTime;
	private Timestamp createTime;
	private String applicant;
	private String applicantCarid;
	private String orderNo;
	private String remark;
	private String status;

	// Constructors

	/** default constructor */
	public TbSpPolice() {
	}

	/** full constructor */
	public TbSpPolice(String userId, String proposalNo, String policeNo,
			String productName, String planId, String planName, Double amount,
			Timestamp proposalTime, Timestamp startTime, Timestamp endTime,
			Timestamp createTime, String applicant, String applicantCarid,
			String orderNo, String remark, String status) {
		this.userId = userId;
		this.proposalNo = proposalNo;
		this.policeNo = policeNo;
		this.productName = productName;
		this.planId = planId;
		this.planName = planName;
		this.amount = amount;
		this.proposalTime = proposalTime;
		this.startTime = startTime;
		this.endTime = endTime;
		this.createTime = createTime;
		this.applicant = applicant;
		this.applicantCarid = applicantCarid;
		this.orderNo = orderNo;
		this.remark = remark;
		this.status = status;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "USER_ID", length = 10)
	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "PROPOSAL_NO", length = 50)
	public String getProposalNo() {
		return this.proposalNo;
	}

	public void setProposalNo(String proposalNo) {
		this.proposalNo = proposalNo;
	}

	@Column(name = "POLICE_NO", length = 50)
	public String getPoliceNo() {
		return this.policeNo;
	}

	public void setPoliceNo(String policeNo) {
		this.policeNo = policeNo;
	}

	@Column(name = "PRODUCT_NAME", length = 50)
	public String getProductName() {
		return this.productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "PLAN_ID", length = 50)
	public String getPlanId() {
		return this.planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	@Column(name = "PLAN_NAME", length = 50)
	public String getPlanName() {
		return this.planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	@Column(name = "AMOUNT", precision = 22, scale = 0)
	public Double getAmount() {
		return this.amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@Column(name = "PROPOSAL_TIME", length = 19)
	public Timestamp getProposalTime() {
		return this.proposalTime;
	}

	public void setProposalTime(Timestamp proposalTime) {
		this.proposalTime = proposalTime;
	}

	@Column(name = "START_TIME", length = 19)
	public Timestamp getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	@Column(name = "END_TIME", length = 19)
	public Timestamp getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	@Column(name = "CREATE_TIME", length = 19)
	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	@Column(name = "APPLICANT", length = 100)
	public String getApplicant() {
		return this.applicant;
	}

	public void setApplicant(String applicant) {
		this.applicant = applicant;
	}

	@Column(name = "APPLICANT_CARID", length = 50)
	public String getApplicantCarid() {
		return this.applicantCarid;
	}

	public void setApplicantCarid(String applicantCarid) {
		this.applicantCarid = applicantCarid;
	}

	@Column(name = "ORDER_NO", length = 50)
	public String getOrderNo() {
		return this.orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	@Column(name = "REMARK", length = 50)
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "STATUS", length = 2)
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
	
	
}