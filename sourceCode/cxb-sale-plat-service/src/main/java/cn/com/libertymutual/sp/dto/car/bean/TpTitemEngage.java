package cn.com.libertymutual.sp.dto.car.bean;

import java.io.Serializable;

/**
 * Created by Ryan on 2016-09-05.
 */
public class TpTitemEngage implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -845059372403880403L;
	private int id;
	private String flowId;
	private String specialCode;
	private String specialName;
	private String specialContent;
	private String riskCode;
	private String engageSerialNo;
	private String mustFlag;
	private String updateFlag;
	private String clauseCode;
	private String clausesName;
	private String clausesContext;

	public String getClauseCode() {
		return clauseCode;
	}

	public void setClauseCode(String clauseCode) {
		this.clauseCode = clauseCode;
	}

	public String getClausesName() {
		return clausesName;
	}

	public void setClausesName(String clausesName) {
		this.clausesName = clausesName;
	}

	public String getClausesContext() {
		return clausesContext;
	}

	public void setClausesContext(String clausesContext) {
		this.clausesContext = clausesContext;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFlowId() {
		return flowId;
	}

	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}

	public String getSpecialCode() {
		return specialCode;
	}

	public void setSpecialCode(String specialCode) {
		this.specialCode = specialCode;
	}

	public String getSpecialName() {
		return specialName;
	}

	public void setSpecialName(String specialName) {
		this.specialName = specialName;
	}

	public String getSpecialContent() {
		return specialContent;
	}

	public void setSpecialContent(String specialContent) {
		this.specialContent = specialContent;
	}

	public String getRiskCode() {
		return riskCode;
	}

	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}

	public String getEngageSerialNo() {
		return engageSerialNo;
	}

	public void setEngageSerialNo(String engageSerialNo) {
		this.engageSerialNo = engageSerialNo;
	}

	/**
	 * 是否必传;0必传;1 非必传
	 * @param mustFlag
	 */
	public String getMustFlag() {
		return mustFlag;
	}

	public String getUpdateFlag() {
		return updateFlag;
	}

	public void setMustFlag(String mustFlag) {
		this.mustFlag = mustFlag;
	}

	/**
	 * 是否可编辑0:允许; 1不允许
	 * @param updateFlag
	 */
	public void setUpdateFlag(String updateFlag) {
		this.updateFlag = updateFlag;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TpTitemEngage other = (TpTitemEngage) obj;
		if (engageSerialNo == null) {
			if (other.engageSerialNo != null)
				return false;
		} else if (!engageSerialNo.equals(other.engageSerialNo))
			return false;
		if (flowId == null) {
			if (other.flowId != null)
				return false;
		} else if (!flowId.equals(other.flowId))
			return false;
		if (id != other.id)
			return false;
		if (mustFlag == null) {
			if (other.mustFlag != null)
				return false;
		} else if (!mustFlag.equals(other.mustFlag))
			return false;
		if (riskCode == null) {
			if (other.riskCode != null)
				return false;
		} else if (!riskCode.equals(other.riskCode))
			return false;
		if (specialCode == null) {
			if (other.specialCode != null)
				return false;
		} else if (!specialCode.equals(other.specialCode))
			return false;
		if (specialContent == null) {
			if (other.specialContent != null)
				return false;
		} else if (!specialContent.equals(other.specialContent))
			return false;
		if (specialName == null) {
			if (other.specialName != null)
				return false;
		} else if (!specialName.equals(other.specialName))
			return false;
		if (updateFlag == null) {
			if (other.updateFlag != null)
				return false;
		} else if (!updateFlag.equals(other.updateFlag))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((engageSerialNo == null) ? 0 : engageSerialNo.hashCode());
		result = prime * result + ((flowId == null) ? 0 : flowId.hashCode());
		result = prime * result + id;
		result = prime * result + ((mustFlag == null) ? 0 : mustFlag.hashCode());
		result = prime * result + ((riskCode == null) ? 0 : riskCode.hashCode());
		result = prime * result + ((specialCode == null) ? 0 : specialCode.hashCode());
		result = prime * result + ((specialContent == null) ? 0 : specialContent.hashCode());
		result = prime * result + ((specialName == null) ? 0 : specialName.hashCode());
		result = prime * result + ((updateFlag == null) ? 0 : updateFlag.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "TpTitemEngage [id=" + id + ", flowId=" + flowId + ", specialCode=" + specialCode + ", specialName=" + specialName
				+ ", specialContent=" + specialContent + ", riskCode=" + riskCode + ", engageSerialNo=" + engageSerialNo + ", mustFlag=" + mustFlag
				+ ", updateFlag=" + updateFlag + "]";
	}

}