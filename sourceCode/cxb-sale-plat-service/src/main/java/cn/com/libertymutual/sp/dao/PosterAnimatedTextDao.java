package cn.com.libertymutual.sp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cn.com.libertymutual.sp.bean.PosterAnimatedText;

@Repository
public interface PosterAnimatedTextDao extends PagingAndSortingRepository<PosterAnimatedText, Integer>, JpaSpecificationExecutor<PosterAnimatedText> {

	
	List<PosterAnimatedText> findByPosterId(Integer posterId);

	
	@Transactional
	@Modifying
	@Query(value="delete from t_sp_poster_animated_text  where POSTER_ID = ?1",nativeQuery=true)
	int deleteByPosterId(Integer posterId);

}
