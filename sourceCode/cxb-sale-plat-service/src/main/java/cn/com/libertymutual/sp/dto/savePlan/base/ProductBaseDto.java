package cn.com.libertymutual.sp.dto.savePlan.base;

public class ProductBaseDto {

	
	private String productCode;//计划代码
	private String productName;//计划名称
	private String productId;//计划名称
	private String productDescription;//产品描述
	private String comm1Rate;

	
	
	public String getComm1Rate() {
		return comm1Rate;
	}
	public void setComm1Rate(String comm1Rate) {
		this.comm1Rate = comm1Rate;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	@Override
	public String toString() {
		return "ProductBaseDto [productCode=" + productCode + ", productName=" + productName + ", productDescription="
				+ productDescription + "]";
	}
	public ProductBaseDto(String productCode, String productName, String productDescription) {
		super();
		this.productCode = productCode;
		this.productName = productName;
		this.productDescription = productDescription;
	}
	public ProductBaseDto() {
		super();
	}
	
	
}
