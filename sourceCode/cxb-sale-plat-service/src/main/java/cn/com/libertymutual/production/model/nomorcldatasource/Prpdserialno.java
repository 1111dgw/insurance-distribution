package cn.com.libertymutual.production.model.nomorcldatasource;

public class Prpdserialno {
    private String businesstype;

    private String classcode;

    private String serialtype;

    private Integer serialno;

    public String getBusinesstype() {
        return businesstype;
    }

    public void setBusinesstype(String businesstype) {
        this.businesstype = businesstype == null ? null : businesstype.trim();
    }

    public String getClasscode() {
        return classcode;
    }

    public void setClasscode(String classcode) {
        this.classcode = classcode == null ? null : classcode.trim();
    }

    public String getSerialtype() {
        return serialtype;
    }

    public void setSerialtype(String serialtype) {
        this.serialtype = serialtype == null ? null : serialtype.trim();
    }

    public Integer getSerialno() {
        return serialno;
    }

    public void setSerialno(Integer serialno) {
        this.serialno = serialno;
    }
}