package cn.com.libertymutual.core.database.druid.aop;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import cn.com.libertymutual.core.database.druid.DataSourceContextHolder;

/**
 * 在DAO层决定数据源(注：如果用这方式，service层不能使用事务，否则出问题，因为打开事务打开时，就会决定数据库源了）
 * @author AoYi
 *
 */
@Aspect
@Component
public class DataSourceAop {
	Logger log = LoggerFactory.getLogger(getClass());

	@Before("execution(* cn.com.libertymutual.*.dao..*.find*(..)) or execution(* cn.com.libertymutual.*.dao..*.query*(..)) "
			+ "or execution(* cn.com.libertymutual.*.dao..*.get*(..))")
	public void setReadDataSourceType() {
		DataSourceContextHolder.read();
		// log.info("dataSource切换到：Read");
	}

	@Before("execution(* cn.com.libertymutual.*.dao..*.insert*(..)) "
			+ "or execution(* cn.com.libertymutual.*.dao..*.save*(..)) or execution(* cn.com.libertymutual.*.dao..*.update*(..)) "
			+ "or execution(* cn.com.libertymutual.*.dao..*.delete*(..))")
	public void setWriteDataSourceType() {
		DataSourceContextHolder.write();
		// log.info("dataSource切换到：write");
	}
}
