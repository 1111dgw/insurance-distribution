/*****************************************************************************************************
 * DESC			:	API函数定义
 * Author		:	Steven.Li
 * Datetime		:	2017-07-06
 * ***************************************************************************************************
 * 函数组			函数名称			函数作用
 *
 * 接口函数
 * 			           queryKindData		           按条件获取条款信息
 *                                             initRiskSelect                            初始化险种下拉框信息
 * ***************************************************************************************************
 */

import Constant from "../constant";
import Validations from "../validations";

const KindAPI = {

            initRiskSelect(_this) {
                    $.ajax({
                    type: 'POST',
                    url: Constant.urls.queryClassNotComposite,
                    dataType: 'json',
                    success: function(data) {
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                             _this.classOptions = data.result;
                        } else {
                            _this.$message({type: 'error',message: '调用接口错误！'});
                        }
                    }
                });
            },

            openAddDialog(_this) {
                    _this.showAddDialog = true;
            },

            openEditDialog(_this) {
                    if (_this.selectedRows.length > 1) {
                        _this.$message({type: 'error',message: '只能选择一行数据进行编辑'});
                        return;
                    } else if (_this.selectedRows.length < 1) {
                        _this.$message({type: 'error',message: '请选择条款！'});
                        return;
                    }
                    let vm = _this;

                    $.ajax({
                        type: 'POST',
                        url: Constant.urls.isLastKindVersion,
                        data: JSON.stringify(vm.selectedRows[0]),
                        dataType: 'json',
                        contentType : 'application/json',
                        success: function(data) {
                            if(data.state === 3) {
                                _this.$router.push('/login');
                                _this.$message({type: 'error',message: data.result});
                                return false;
                            }
                            if(data.state == 0) {
                                 if(!data.result.isLastKindVersion) {
                                    vm.$message({type: 'error',message: '只能对最高版本进行编辑！'});
                                 } else {
                                    vm.showEditDialog = true;
                                }
                            } else {
                                vm.$message({type: 'error',message: '调用接口错误！'});
                            }
                        }
                });
            },

            openItemConfigDialog(_this) {
                    if (_this.selectedRows.length > 1) {
                            _this.$message({type: 'error',message: '只能选择一行数据进行关联'});
                            return;
                        } else if (_this.selectedRows.length < 1) {
                            _this.$message({type: 'error',message: '请选择条款！'});
                            return;
                        } else if (_this.selectedRows[0].validstatus == '0') {
                            _this.$message({type: 'error',message: '只能对有效条款进行关联！'});
                            return;
                        }

                        let vm = _this;
                        let api = this;
                        $.ajax({
                        type: 'POST',
                        url: Constant.urls.isLastKindVersion,
                        data: JSON.stringify(vm.selectedRows[0]),
                        dataType: 'json',
                        contentType : 'application/json',
                        success: function(data) {
                            if(data.state === 3) {
                                vm.$router.push('/login');
                                vm.$message({type: 'error',message: data.result});
                                return false;
                            }
                            if(data.state == 0) {
                                 if(!data.result.isLastKindVersion) {
                                    vm.$message({type: 'error',message: '请选择最高版本条款！'});
                                 } else {
                                    if(api.checkRiskLinked(vm)) {            //验证当前条款是否关联险种
                                        if(api.checkItemLinked(vm)) {        //验证当前条款关联的险种下是否关联标的/责任
                                            vm.showItemConfigDialog = true;
                                        }  else {
                                            vm.$message({type: 'error',message: '当前条款已关联的险种下无标的/责任，请先给已关联险种配置标的/责任！'});
                                        }
                                    } else {
                                        vm.$message({type: 'error',message: '当前条款未关联险种，请先关联险种！'});
                                    }
                                }
                            } else {
                                vm.$message({type: 'error',message: '调用接口错误！'});
                            }
                        }
                });
            },

            openRiskConfigDialog(_this) {
                 if (_this.selectedRows.length > 1) {
                        _this.$message({type: 'error',message: '只能选择一行数据进行关联'});
                        return;
                    } else if (_this.selectedRows.length < 1) {
                        _this.$message({type: 'error',message: '请选择条款！'});
                        return;
                    } else if (_this.selectedRows[0].validstatus == '0') {
                        _this.$message({type: 'error',message: '只能对有效条款进行关联！'});
                        return;
                    }
                    let vm = _this;

                    $.ajax({
                            type: 'POST',
                            url: Constant.urls.isLastKindVersion,
                            data: JSON.stringify(vm.selectedRows[0]),
                            dataType: 'json',
                            contentType : 'application/json',
                            success: function(data) {
                                if(data.state === 3) {
                                    vm.$router.push('/login');
                                    vm.$message({type: 'error',message: data.result});
                                    return false;
                                }
                                if(data.state == 0) {
                                     if(!data.result.isLastKindVersion) {
                                        vm.$message({type: 'error',message: '请选择最高版本条款！'});
                                     } else {
                                         vm.showRiskConfigDialog = true;
                                    }
                                } else {
                                    vm.$message({type: 'error',message: '调用接口错误！'});
                                }
                            }
                    });
            },

            openRiskCodeSelectDialog(_this) {
                    _this.showRiskCodeSelectDialog = true;
            },

            kindFormValidation(_this) {
                //添加条款，表单非空校验
                let requiredFields = [];
                requiredFields.push( _this.formData.ownerriskcode);
                requiredFields.push( _this.formData.kindcname);
                requiredFields.push( _this.formData.kindename);
                // requiredFields.push( _this.formData.kindcode);
                requiredFields.push( _this.formData.startdate);
                requiredFields.push( _this.formData.enddate);
                requiredFields.push( _this.formData.shortratetype);
                requiredFields.push( _this.formData.kindNature);
                if(!Validations.required(requiredFields,_this)) {
                        return false;
                }

                //加成比例对应条款代码不为空时，加成比例必填
                if (_this.formData.pluskindcode !== '' && _this.formData.plusrate === '') {
                    _this.$message({type: 'error',message: '请录入加成比例！'});
                    return false;
                }

                return true;
            },

            add(_this) {
                 _this.loading = true;
                 let api = this;

                 if (!api.kindFormValidation(_this)) {
                    _this.loading = false;
                    return ;
                 }

                 _this.formData.calculateflag = _this.formData.calculateflag + '1' + _this.formData.kindType + '00' + _this.formData.kindNature+ _this.formData.cas;

                 $.ajax({
                    type: 'POST',
                    url: Constant.urls.addKind,
                    data: _this.formData,
                    dataType: 'json',
                    success: function(data) {
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                            _this.$message({type: 'success',message: '新增条款成功！'});
                            api.query(_this.$parent);
                             _this.off();
                        } else {
                          alert(data.result);
                        }
                    },
                    complete:function(XMLHttpRequest,textStatus){  
                        if(textStatus=='timeout'){
                            _this.$message({type: 'error',message: '服务器连接超时!'});
                        }
                    },  
                    error:function(XMLHttpRequest, textStatus){  
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                       _this.$message({type: 'error',message: '服务器错误!'});
                    }
                });
                 _this.loading = false;
            },

            delete(_this) {
                    if (_this.selectedRows.length < 1) {
                        _this.$message({
                            type: 'error',
                            message: '请选择条款！'
                        });
                        _this.loading = false;
                        return;
                    }

                    let api = this;
                    let beginInactive = false;

                    _this.$confirm('此操作将注销所选条款, 是否继续?', '提示', {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        type: 'warning'
                    }).then(() => {
                        _this.loading = true;
                        let deletable = api.checkKindDeletable(_this.selectedRows);

                        if (deletable.flag === false) {
                                if(confirm(deletable.errorMsg)) {
                                    beginInactive = true;
                                } else {
                                     _this.loading = false;
                                }
                        } else {
                            beginInactive = true;
                        }

                        let requestData = {selectedRows : _this.selectedRows};
                        if (beginInactive) {
                                    let requestData = {selectedRows : _this.selectedRows};
                                    $.ajax({
                                        type: 'POST',
                                        url: Constant.urls.inActiveKindVersion,
                                        data: JSON.stringify(requestData),
                                        dataType: 'json',
                                        contentType: 'application/json',
                                        success: function(data) {
                                            if(data.state === 3) {
                                                _this.$router.push('/login');
                                                _this.$message({type: 'error',message: data.result});
                                                return false;
                                            }
                                            if(data.state == 0) {
                                                _this.$message({type: 'success', message: '注销成功!'});
                                                _this.queryData(); //重新加载表格数据
                                            } else {
                                                if (data.result.length > 40) {
                                                    alert(data.result);
                                                } else {
                                                    _this.$message({type: 'error',message: data.result});
                                                }
                                            }
                                        },
                                        complete:function(XMLHttpRequest,textStatus){  
                                            if(textStatus=='timeout'){
                                                _this.$message({type: 'error',message: '服务器连接超时!'});
                                            }
                                        },  
                                        error:function(XMLHttpRequest, textStatus){  
                                            console.log(XMLHttpRequest);
                                            console.log(textStatus);
                                           _this.$message({type: 'error',message: '服务器错误!'});
                                        }
                                    });
                                    _this.loading = false;
                        }

                    }).catch(() => {
                        _this.$message({
                            type: 'info',
                            message: '已取消注销'
                        });
                    });
            },

            edit(_this) {
                    let api = this;

                    if (!api.kindFormValidation(_this)) {
                        return ;
                    }

                    _this.$confirm('此操作将修改该条款, 是否继续?', '提示', {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        type: 'warning'
                    }).then(() => {
                        _this.loading = true;
                        let beginEdit = false;
                        if (_this.formData.validstatus === '0' ||  _this.tempNewVersion !== '') {
                                    let kinds = [];
                                    kinds.push(_this.formData)
                                    let deletable = api.checkKindDeletable(kinds);
                                    if (deletable.flag === false) {
                                        if(confirm(deletable.errorMsg)) {
                                            beginEdit = true;
                                        } else {
                                            _this.loading = false;
                                        }
                                    } else {
                                        beginEdit = true;
                                    }
                                    if(_this.tempNewVersion !== '') {
                                            _this.formData.kindversion = _this.tempNewVersion;
                                    }
                        } else {
                              beginEdit = true;
                        }

                        if (beginEdit) {
                                _this.formData.calculateflag = _this.formData.calculateflag + '1' + _this.formData.kindType + '00' + _this.formData.kindNature + _this.formData.cas;
                                $.ajax({
                                    type: 'POST',
                                    url: Constant.urls.updateKind,
                                    data: _this.formData,
                                    dataType: 'json',
                                    success: function(data) {
                                        if(data.state === 3) {
                                            _this.$router.push('/login');
                                            _this.$message({type: 'error',message: data.result});
                                            return false;
                                        }
                                        if(data.state == 0) {
                                            _this.$message({type: 'success',message: '修改成功!'});
                                            _this.$parent.queryData(); //重新加载表格数据
                                            _this.off();
                                        } else {
                                            if (data.result.length > 40) {
                                                alert(data.result);
                                            } else {
                                                _this.$message({type: 'error',message: data.result});
                                            }
                                        }
                                    },
                                    complete:function(XMLHttpRequest,textStatus){  
                                        if(textStatus=='timeout'){
                                            _this.$message({type: 'error',message: '服务器连接超时!'});
                                        }
                                    },  
                                    error:function(XMLHttpRequest, textStatus){  
                                        console.log(XMLHttpRequest);
                                        console.log(textStatus);
                                       _this.$message({type: 'error',message: '服务器错误!'});
                                    }
                                });
                                _this.loading = false;
                        }

                    }).catch(() => {
                        _this.$message({type: 'info',message: '已取消修改'});
                    });
            },

            query(_this) {
                _this.loading = true;
                _this.formData.riskcodes = Object.assign({}, _this.arrayRiskcodes);
                $.ajax({
                    type: 'POST',
                    url: Constant.urls.queryPrpdKindLibrary,
                    data: _this.formData,
                    dataType: 'json',
                    success: function(data) {
                        _this.loading = false;
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                            _this.tableData = data.result;
                            _this.total = data.total;
                        } else {
                           _this.$message({type: 'error',message: '调用接口错误！'});
                        }
                    },
                    complete:function(XMLHttpRequest,textStatus){  
                        if(textStatus=='timeout'){
                            _this.$message({type: 'error',message: '服务器连接超时!'});
                        }
                        _this.loading = false;
                    },  
                    error:function(XMLHttpRequest, textStatus){  
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                       _this.$message({type: 'error',message: '服务器错误!'});
                       _this.loading = false;
                    }
                });
                 setTimeout(function(){
                    _this.isSortRequest = true;
                },500);
            },
            newKindCode(_this) {
                if(_this.formData.ownerriskcode == '') {
                    _this.$message({type: 'error',message: '请先选择统计归属险种！'});
                    return;
                }
                $.ajax({
                    type: 'POST',
                    url: Constant.urls.generateKindCode,
                    data: {
                        ownerriskcode : _this.formData.ownerriskcode,
                        kindType : _this.formData.kindType
                    },
                    dataType: 'json',
                    success: function(data) {
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                            _this.formData.kindcode = data.result;
                        } else {
                           _this.$message({type: 'error',message: '调用接口错误！'});
                        }
                    }
                });
            },
            queryPrpdCompany(_this) {
                $.ajax({
                    type: 'POST',
                    url: Constant.urls.queryPrpdCompany,
                    data: _this.formData,
                    dataType: 'json',
                    success: function(data) {
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                            _this.tableData = data.result;
                            _this.total = data.total;
                        } else {
                           _this.$message({type: 'error',message: '调用接口错误！'});
                        }
                    }
                });
            },
            openItemLinkedDialog(_this) {
                    if (_this.selectedRows.length > 1) {
                            _this.$message({type: 'error',message: '只能选择一行数据进行查看'});
                            return;
                        } else if (_this.selectedRows.length < 1) {
                            _this.$message({type: 'error',message: '请选择条款！'});
                            return;
                        }
                        let vm = _this;
                        $.ajax({
                        type: 'POST',
                        url: Constant.urls.isLastKindVersion,
                        data: JSON.stringify(vm.selectedRows[0]),
                        dataType: 'json',
                        contentType : 'application/json',
                        success: function(data) {
                            if(data.state === 3) {
                                vm.$router.push('/login');
                                vm.$message({type: 'error',message: data.result});
                                return false;
                            }
                            if(data.state == 0) {
                                 if(!data.result.isLastKindVersion) {
                                    vm.$message({type: 'error',message: '请选择最高版本条款！'});
                                 } else {
                                     _this.showItemLinkedDialog = true;
                                }
                            } else {
                                vm.$message({type: 'error',message: '调用接口错误！'});
                            }
                        }
                });

            },
            openRiskLinkedDialog(_this) {
                    if (_this.selectedRows.length > 1) {
                            _this.$message({type: 'error',message: '只能选择一行数据进行查看'});
                            return;
                        } else if (_this.selectedRows.length < 1) {
                            _this.$message({type: 'error',message: '请选择条款！'});
                            return;
                        }
                        let vm = _this;
                        $.ajax({
                        type: 'POST',
                        url: Constant.urls.isLastKindVersion,
                        data: JSON.stringify(vm.selectedRows[0]),
                        dataType: 'json',
                        contentType : 'application/json',
                        success: function(data) {
                            if(data.state === 3) {
                                vm.$router.push('/login');
                                vm.$message({type: 'error',message: data.result});
                                return false;
                            }
                            if(data.state == 0) {
                                 if(!data.result.isLastKindVersion) {
                                    vm.$message({type: 'error',message: '请选择最高版本条款！'});
                                 } else {
                                    _this.showRiskLinkedDialog = true;
                                }
                            } else {
                                vm.$message({type: 'error',message: '调用接口错误！'});
                            }
                        }
                });
            },
            openLogDialog(_this) {
                    if (_this.selectedRows.length > 1) {
                            _this.$message({type: 'error',message: '只能选择一行数据进行查看'});
                            return;
                        } else if (_this.selectedRows.length < 1) {
                            _this.$message({type: 'error',message: '请选择条款！'});
                            return;
                        }
                        let vm = _this;
                        $.ajax({
                        type: 'POST',
                        url: Constant.urls.isLastKindVersion,
                        data: JSON.stringify(vm.selectedRows[0]),
                        dataType: 'json',
                        contentType : 'application/json',
                        success: function(data) {
                            if(data.state === 3) {
                                vm.$router.push('/login');
                                vm.$message({type: 'error',message: data.result});
                                return false;
                            }
                            if(data.state == 0) {
                                 if(!data.result.isLastKindVersion) {
                                    vm.$message({type: 'error',message: '请选择最高版本条款！'});
                                 } else {
                                    _this.logBusinessKey = _this.selectedRows[0].kindcname;
                                    _this.logBusinessKeyValue = _this.selectedRows[0].kindcode;
                                    _this.showLogDialog = true;
                                }
                            } else {
                                vm.$message({type: 'error',message: '调用接口错误！'});
                            }
                        }
                });
            },
            checkRiskLinked(vm) {
                let isLinked = true;
                $.ajax({
                        type: 'POST',
                        url: Constant.urls.checkRiskLinked,
                        data: JSON.stringify(vm.selectedRows[0]),
                        dataType: 'json',
                        contentType : 'application/json',
                        async : false,
                        success: function(data) {
                            if(data.state === 3) {
                                vm.$router.push('/login');
                                vm.$message({type: 'error',message: data.result});
                                return false;
                            }
                            if(data.state == 0) {
                                isLinked = data.result;
                            } else {
                                vm.$message({type: 'error',message: '调用接口错误！'});
                            }
                        }
                });
                return isLinked;
            },
            checkItemLinked(vm) {
                let isLinked = true;
                $.ajax({
                        type: 'POST',
                        url: Constant.urls.checkItemLinked,
                        data: JSON.stringify(vm.selectedRows[0]),
                        dataType: 'json',
                        contentType : 'application/json',
                        async : false,
                        success: function(data) {
                            if(data.state === 3) {
                                vm.$router.push('/login');
                                vm.$message({type: 'error',message: data.result});
                                return false;
                            }
                            if(data.state == 0) {
                                isLinked = data.result;
                            } else {
                                vm.$message({type: 'error',message: '调用接口错误！'});
                            }
                        }
                });
                return isLinked;
            },
            checkKindDeletable(kinds) {
                let requestData = {selectedRows : kinds};
                let deletable = {
                    flag: true,
                    errorMsg: '',
                };
                $.ajax({
                        type: 'POST',
                        url: Constant.urls.checkKindDeletable,
                        data: JSON.stringify(requestData),
                        dataType: 'json',
                        contentType : 'application/json',
                        async : false,
                        success: function(data) {
                            if(data.state == 1) {
                                deletable.flag = false;
                                deletable.errorMsg = data.result
                            }
                        }
                });
                return deletable;
            },
            openRiskLinkItemDialog(_this) {
                    _this.showRiskLinkItemDialog = true;
            },
            queryByOwnerRisk(_this) {
                _this.loading = true;
                $.ajax({
                    type: 'POST',
                    url: Constant.urls.queryByOwnerRisk,
                    data: JSON.stringify(_this.formData),
                    dataType: 'json',
                    contentType : 'application/json',
                    success: function(data) {
                        _this.loading = false;
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                            _this.tableData = data.result;
                            _this.total = data.total;
                        } else {
                           _this.$message({type: 'error',message: '调用接口错误！'});
                        }
                    },
                    complete:function(XMLHttpRequest,textStatus){  
                        if(textStatus=='timeout'){
                            _this.$message({type: 'error',message: '服务器连接超时!'});
                        }
                        _this.loading = false;
                    },  
                    error:function(XMLHttpRequest, textStatus){  
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                       _this.$message({type: 'error',message: '服务器错误!'});
                       _this.loading = false;
                    }
                });
                 setTimeout(function(){
                    _this.isSortRequest = true;
                },500);
            }
}

export default KindAPI
