const Constant = {
       urls: {

        //****************************************************条款管理****************************************************
          queryPrpdKindLibrary : "/cxb/kind/queryPrpdKindLibrary",
          queryPrpdItemLibrary : "/cxb/kind/queryPrpdItemLibrary",
          queryShortRate : "/cxb/kind/queryShortRate",
          generateKindCode : "/cxb/kind/generateKindCode",
          generateItemCode : "/cxb/kind/generateItemCode",
          addKind : "/cxb/kind/addKind",
          addItem : "/cxb/kind/addItem",
          delItemLinked : "/cxb/kind/delItemLinked",
          delRiskLinked : "/cxb/kind/delRiskLinked",
          updateKind : "/cxb/kind/updateKind",
          getKindItem : "/cxb/kind/getKindItem",
          getPrpdKind : "/cxb/kind/getPrpdKind",
          findItemNotRiskLinked : "/cxb/kind/findItemNotRiskLinked",
          linkItem : "/cxb/kind/linkItem",
          linkRisk : "/cxb/kind/linkRisk",
          linkItem2Risk : "/cxb/kind/linkItem2Risk",
          checkRiskLinked : "/cxb/kind/checkRiskLinked",
          checkItemLinked : "/cxb/kind/checkItemLinked",
          checkKindDeletable : "/cxb/kind/checkKindDeletable",
          checkRiskLinkedDeletable : "/cxb/kind/checkRiskLinkedDeletable",
          checkItemLinkedDeletable : "/cxb/kind/checkItemLinkedDeletable",
          inActiveKindVersion : "/cxb/kind/inActiveKindVersion",
          isLastKindVersion : "/cxb/kind/isLastKindVersion",
          queryByOwnerRisk : "/cxb/kind/queryByOwnerRisk",
        //****************************************************************************************************************

        //****************************************************方案管理****************************************************
          queryPrpdRiskPlan : "/cxb/plan/queryPrpdRiskPlan",
          queryRiskKind : "/cxb/plan/queryRiskKind",
          queryKindItems : "/cxb/plan/queryKindItems",
          queryKindItems2 : "/cxb/plan/queryKindItems2",
          queryPlansub : "/cxb/plan/queryPlansub",
          generatePlanCode : "/cxb/plan/generatePlanCode",
          addPlan : "/cxb/plan/addPlan",
          addPlan2 : "/cxb/plan/addPlan2",
          editPlan : "/cxb/plan/editPlan",
          editPlan2 : "/cxb/plan/editPlan2",
          getItemLinked : "/cxb/plan/getItemLinked",
        //****************************************************************************************************************

        //****************************************************因子管理**************************************************
          findProps : "/cxb/props/findProps",
          findPlanProps : "/cxb/props/findPlanProps",
          addProps : "/cxb/props/addProps",
        //****************************************************************************************************************

        //****************************************************备案号管理**************************************************
          queryPrpdEFile : "/cxb/efile/queryPrpdEFile",
          insertEFile : "/cxb/efile/insertEFile",
          updateEFile : "/cxb/efile/updateEFile",
          download : "/cxb/efile/download",
        //****************************************************************************************************************

        //****************************************************特约管理**************************************************
          queryPrpdClause : "/cxb/clause/queryPrpdClause",
          queryPrpdClauseCodeNo : "/cxb/clause/queryPrpdClauseCodeNo",
          addClause : "/cxb/clause/addClause",
          queryCodeRiskByClause : "/cxb/clause/queryCodeRiskByClause",
          queryRiskClauseByClause : "/cxb/clause/queryRiskClauseByClause",
          updatePrpdClause : "/cxb/clause/updatePrpdClause",
        //****************************************************************************************************************

        //****************************************************险类管理**************************************************
          queryPrpdClass : "/cxb/class/queryPrpdClass",
          checkClassCodeUsed : "/cxb/class/checkClassCodeUsed",
          insertPrpdClass : "/cxb/class/insertPrpdClass",
          updatePrpdClass : "/cxb/class/updatePrpdClass",
	  //****************************************************************************************************************

        //****************************************************险种管理**************************************************
          checkRiskCodeUsed : "/cxb/risk/checkRiskCodeUsed",
          addPrpdRisk : "/cxb/risk/addPrpdRisk",
          updatePrpdRisk : "/cxb/risk/updatePrpdRisk",
          linkKind : "/cxb/risk/linkKind",
          queryAllFhtreaty : "/cxb/risk/queryAllFhtreaty",
          queryAllFhxtreaty : "/cxb/risk/queryAllFhxtreaty",
          queryFhsections : "/cxb/risk/queryFhsections",
          queryFhexitemkinds : "/cxb/risk/queryFhexitemkinds",
          queryFhxlayers : "/cxb/risk/queryFhxlayers",
          queryFhxsections : "/cxb/risk/queryFhxsections",
          queryFdriskconfigByRisk : "/cxb/risk/queryFdriskconfigByRisk",
          queryFhriskByRisk : "/cxb/risk/queryFhriskByRisk",
          queryFhexitemkindByRisk : "/cxb/risk/queryFhexitemkindByRisk",
          queryFhxriskByRisk : "/cxb/risk/queryFhxriskByRisk",
          queryRiskByCodes : "/cxb/risk/queryRiskByCodes",
        //****************************************************************************************************************

        //****************************************************责任管理**************************************************
          queryItem : "/cxb/item/queryItem",
          queryParentItems : "/cxb/item/queryParentItems",
          updateItem : "/cxb/item/updateItem",
        //****************************************************************************************************************

        //****************************************************其它*******************************************************
          queryAllPrpdRisk : "/cxb/common/queryAllPrpdRisk",
          queryRisks : "/cxb/common/queryRisks",
          queryAllPrpdClass : "/cxb/common/queryAllPrpdClass",
          queryClassNotComposite : "/cxb/common/queryClassNotComposite",
          queryPrpdCompany : "/cxb/common/queryPrpdCompany",
          findLogs : "/cxb/common/findLogs",
          findLogDetail : "/cxb/common/findLogDetail",
          queryAgentByComCode : "/cxb/common/queryAgentByComCode",
          queryPlansByRisks : "/cxb/common/queryPlansByRisks",
          queryCompanys : "/cxb/common/queryCompanys",
          queryCompanysBycodes : "/cxb/common/queryCompanysBycodes",
          queryByClassAndComposite : "/cxb/common/queryByClassAndComposite",
          queryRiskByClass : "/cxb/common/queryRiskByClass",
        //****************************************************************************************************************

        //****************************************************见费出单管理**************************************************
         queryPolicyPays : "/cxb/policypay/queryPolicyPays",
         insertPrpdPolicyPay : "/cxb/policypay/insertPrpdPolicyPay",
         updatePrpdPolicyPay : "/cxb/policypay/updatePrpdPolicyPay",
         deletePrpdPolicyPay : "/cxb/policypay/deletePrpdPolicyPay",
        //****************************************************************************************************************
       },
}

export default Constant
