/***********************************************************************************************************************
 * DESC			:	常用功能函数定义
 * Author		:	Steven.Li
 * Datetime		:	2017-07-06
 * *********************************************************************************************************************
 * 函数组				函数名称					函数作用
 * 
 * 日期处理
 * 						bindDatePicker			      给输入框[class='bindDate']绑定日期插件
 * 						getNowFormatDate		返回当前日期YYYY-MM-DD
 * 						getAnyDate				返回N天后日期
 * 						getNextYearToday		      返回下一年今天的日期
 *
 * 其他
 *                                    getScreenHeight                 返回屏幕高度
 * *********************************************************************************************************************
 */	

const Utils = {
          /**
           * 给输入框[class='bindDate']绑定日期插件
           */
          bindDatePicker() {
            	$("section.content .bindDate").datepicker({  
            		format: 'yyyy-mm-dd',  
            		language: 'zh-CN',
            		autoclose: true,
            		clearBtn: true,
            		pickDate: true,  
            		pickTime: false,  
            		minView: 'month',
            		todayBtn: 'linked',
            		todayHighlight: true  
            	});
          },

          /**
           * 获取当前时间
           */
          getNowFormatDate () {
              var date = new Date();
              var seperator1 = "-";
              var year = date.getFullYear();
              var month = date.getMonth() + 1;
              var strDate = date.getDate();
              if (month >= 1 && month <= 9) {
                  month = "0" + month;
              }
              if (strDate >= 0 && strDate <= 9) {
                  strDate = "0" + strDate;
              }
              var currentdate = year + seperator1 + month + seperator1 + strDate;
              return currentdate;
          },

          /**
           * 获取N天后日期
           */
          getAnyDate (afterDay) {
          	
              	var today = new Date();
              	today.setTime(today.getTime()+24*60*60*1000*afterDay); //明天
              	
              	var m = today.getMonth()+1;
              	var d = today.getDate();
              	
              	var mStr = m;
              	var dStr = d;
              	if (m < 10) {
              		mStr = '0' + m;
              	}
              	if (d < 10) {
              		dStr = '0' + d;
              	}
              	
              	var tomorrow = today.getFullYear()+"-" + mStr + "-" + dStr;
              	
              	return tomorrow
          },

          /**
           * 获取下一年今天的日期
           */
          getNextYearToday () {
          	
            	var today = new Date();
            	
            	var y = today.getFullYear();
            	var m = today.getMonth()+1;
            	var d = today.getDate();
            	
            	var yStr = y + 1; //下一年
            	var mStr = m;
            	var dStr = d;
            	if (m < 10) {
            		mStr = '0' + m;
            	}
            	if (d < 10) {
            		dStr = '0' + d;
            	}
            	
            	var nextYearToday = yStr +"-" + mStr + "-" + dStr;
            	
            	return nextYearToday;
          },
          getScreenHeight() {
              return $(window).height();
          },
          add0(m){
              return m<10?'0'+m:m 
          },
           /**
             * 时间 / 时间戳 转yyyy-MM-dd 
             */
            getDateTime(val) {
                let d = new Date(val);
                let curr_date = d.getDate();
                let curr_month = d.getMonth() + 1;
                let curr_year = d.getFullYear();
                let curr_hour = d.getHours();
                let curr_minutes = d.getMinutes();  
                let curr_seconds = d.getSeconds();  
                String(curr_month).length < 2 ? (curr_month = "0" + curr_month) : curr_month;
                String(curr_date).length < 2 ? (curr_date = "0" + curr_date) : curr_date;
                let yMdHms = curr_year + "-" + curr_month + "-" + curr_date + ' '+ this.add0(curr_hour)+':'+ this.add0(curr_minutes)+':'+ this.add0(curr_seconds);
                return yMdHms;
            },

            printErrorMsg(errorMsg,_this) {
                  if (errorMsg.length > 40) {
                      alert(errorMsg);
                  } else {
                      _this.$message({type: 'error',message: errorMsg});
                  }
            },

}

export default Utils