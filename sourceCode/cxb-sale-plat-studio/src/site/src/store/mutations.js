const INITIALIZE_DATA = 'INITIALIZE_DATA'
const REMBER_ANSWER = 'REMBER_ANSWER'
const ISFIRST_ROUTER = 'ISFIRST_ROUTER'
const OBTAINWIDTH_HEIGHT = 'OBTAINWIDTH_HEIGHT'
const GET_ALLPRODUCT = 'GET_ALLPRODUCT'
const GET_USER = 'GET_USER'
const EDITORPICTURE_VALUE = 'EDITORPICTURE_VALUE'
const GET_ECAHRTOBJ = 'GET_ECAHRTOBJ'
const GET_HOT_AREA_LIST = 'GET_HOT_AREA_LIST'
const GET_PRODUCT_COMPANY = 'GET_PRODUCT_COMPANY'
export default {
  /*
  初始化险种信息，
   */
  [INITIALIZE_DATA](state, value) {
    if (value.code == "RiskCode") {
      state.riskCodelist = value.data;
    }
    else if (value.code == "BranchCode") {
      state.branchCodelist = value.data;
    }

  },
  //保存路径
  [REMBER_ANSWER](state, url) {
    state.skipUrl = url;
    // state.skipUrl= url;
    // state.IsFirstURl = false;
  },
  //是否第一次进入页面
  [ISFIRST_ROUTER](state) {
    state.IsFirstURl = false;
  },
  //获取浏览器窗口大小
  [OBTAINWIDTH_HEIGHT](state, value) {
    state.winWidth = value.winWidth;
    state.winHeight = value.winHeight;
  },
  //获取全部产品
  [GET_ALLPRODUCT](state, value) {
    state.productList = value;
  },
  //用户登陆后信息
  [GET_USER](state, value) {
    state.user = value;
  },
  //编辑器上传图片传参
  [EDITORPICTURE_VALUE](state, str) {
    state.editorPicture = str;
  },
  //获取图表对象
  [GET_ECAHRTOBJ](state, value) {
    state.echartObj = value;
  },
  //获取常驻地区列表
  [GET_HOT_AREA_LIST](state, data) {
    state.hotAreaList = data;
  },
  //获取产品所属公司
  [GET_PRODUCT_COMPANY](state, data) {
    state.productCompanylist = data;
  }
}