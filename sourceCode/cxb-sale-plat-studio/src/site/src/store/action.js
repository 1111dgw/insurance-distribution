import { codeNodeList, productByRisk, findAllHotArea, prodeucCompanyList } from 'src/api/api';
export default {
  CodeList({ commit, state }, code) {
    let para = new FormData();
    para.append("type", code);
    codeNodeList(para).then((res) => {
      commit('INITIALIZE_DATA', { data: res.data, code: code });
    });
  },
  skipUrlData({ commit, state }, url) {
    commit('REMBER_ANSWER', url);
  },
  editIsfirst({ commit, state }) {
    commit('ISFIRST_ROUTER');
  },
  gainWH({ commit, state }) {
    let winWidth = 0;
    let winHeight = 0;
    if (window.innerWidth)
      winWidth = window.innerWidth;
    else if ((document.body) && (document.body.clientWidth))
      winWidth = document.body.clientWidth;

    if (window.innerHeight)
      winHeight = window.innerHeight;
    else if ((document.body) && (document.body.clientHeight))
      winHeight = document.body.clientHeight;
    commit('OBTAINWIDTH_HEIGHT', { winWidth: winWidth, winHeight: winHeight });
  },
  ProductList({ commit, state }, code) {
    let para = new FormData();
    para.append("riskCode", code);
    productByRisk(para).then((res) => {
      if (res.data.success) {
        commit('GET_ALLPRODUCT', res.data.result);
      }
    }).catch(() => {

    });
  },
  findHotAreaList({ commit, state }) {
    findAllHotArea().then((res) => {
      if (res.data.success) {
        commit('GET_HOT_AREA_LIST', res.data.result);
      }
    }).catch(() => {

    });
  },
  findProductCompanylist({ commit, state }) {
    let para = new FormData();
    para.append("pageNumber", 1);
    para.append("pageSize", 100);
    prodeucCompanyList(para).then((res) => {
      if (res.data.success) {
        commit('GET_PRODUCT_COMPANY', res.data.result.list);
      }
    }).catch(() => {

    });
  },
}