package cn.com.libertymutual.sys.action;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.sys.service.api.ISysCodeNodeService;

@RefreshScope
@RestController
@RequestMapping("/systemCfg/")
public class SysCodeNodeController {
	
	@Autowired
	private ISysCodeNodeService  sysCodeNodeService;
	
	@RequestMapping(value="findSysCode")
    public List findSysCodeNode(String codetype,String sorttype) { 
		return sysCodeNodeService.findSysCodeNode(codetype, sorttype);
    }

}
