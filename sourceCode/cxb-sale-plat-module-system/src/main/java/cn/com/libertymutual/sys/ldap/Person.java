package cn.com.libertymutual.sys.ldap;

import java.io.Serializable;

public class Person implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9179182839067221129L;
	private String manager;
	private String cn;
	private String mobile;
	private String homephone;		//电话
	private String telephoneNumber;//手机号
	private String department;
	private String directReports;
	private String distinguishedName;
	private String l;
	private String postalCode;
	private String pwdLastSet;
	private String badPwdCount;
	private String userAccountControl;
	private String accountExpires;
	private String userPrincipalName;
	private String physicalDeliveryOfficeName;
	private String streetAddress;	//家庭地址
	private String title;
	private String sAMAccountName;  //AD 账号
	private String description;		//描述信息
	private String displayName;		//显示名称
	private String mail;			//邮件
	private String company;			//所属公司
	private String homeMTA;
	public String getManager() {
		return manager;
	}
	public void setManager(String manager) {
		this.manager = manager;
	}
	public String getCn() {
		return cn;
	}
	public void setCn(String cn) {
		this.cn = cn;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPwdLastSet() {
		return pwdLastSet;
	}
	public void setPwdLastSet(String pwdLastSet) {
		this.pwdLastSet = pwdLastSet;
	}
	public String getBadPwdCount() {
		return badPwdCount;
	}
	public void setBadPwdCount(String badPwdCount) {
		this.badPwdCount = badPwdCount;
	}
	public String getUserAccountControl() {
		return userAccountControl;
	}
	public void setUserAccountControl(String userAccountControl) {
		this.userAccountControl = userAccountControl;
	}
	public String getAccountExpires() {
		return accountExpires;
	}
	public void setAccountExpires(String accountExpires) {
		this.accountExpires = accountExpires;
	}
	public String getUserPrincipalName() {
		return userPrincipalName;
	}
	public void setUserPrincipalName(String userPrincipalName) {
		this.userPrincipalName = userPrincipalName;
	}
	public String getPhysicalDeliveryOfficeName() {
		return physicalDeliveryOfficeName;
	}
	public void setPhysicalDeliveryOfficeName(String physicalDeliveryOfficeName) {
		this.physicalDeliveryOfficeName = physicalDeliveryOfficeName;
	}
	public String getStreetAddress() {
		return streetAddress;
	}
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getsAMAccountName() {
		return sAMAccountName;
	}
	public void setsAMAccountName(String sAMAccountName) {
		this.sAMAccountName = sAMAccountName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getHomeMTA() {
		return homeMTA;
	}
	public void setHomeMTA(String homeMTA) {
		this.homeMTA = homeMTA;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getTelephoneNumber() {
		return telephoneNumber;
	}
	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}
	public String getDirectReports() {
		return directReports;
	}
	public void setDirectReports(String directReports) {
		this.directReports = directReports;
	}
	public String getDistinguishedName() {
		return distinguishedName;
	}
	public void setDistinguishedName(String distinguishedName) {
		this.distinguishedName = distinguishedName;
	}
	public String getL() {
		return l;
	}
	public void setL(String l) {
		this.l = l;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getHomephone() {
		return homephone;
	}
	public void setHomephone(String homephone) {
		this.homephone = homephone;
	}
	@Override
	public String toString() {
		return "Person [manager=" + manager + ", cn=" + cn + ", mobile="
				+ mobile + ", homephone=" + homephone + ", telephoneNumber="
				+ telephoneNumber + ", department=" + department
				+ ", directReports=" + directReports + ", distinguishedName="
				+ distinguishedName + ", l=" + l + ", postalCode=" + postalCode
				+ ", pwdLastSet=" + pwdLastSet + ", badPwdCount=" + badPwdCount
				+ ", userAccountControl=" + userAccountControl
				+ ", accountExpires=" + accountExpires + ", userPrincipalName="
				+ userPrincipalName + ", physicalDeliveryOfficeName="
				+ physicalDeliveryOfficeName + ", streetAddress="
				+ streetAddress + ", title=" + title + ", sAMAccountName="
				+ sAMAccountName + ", description=" + description
				+ ", displayName=" + displayName + ", mail=" + mail
				+ ", company=" + company + ", homeMTA=" + homeMTA + "]";
	}


}
