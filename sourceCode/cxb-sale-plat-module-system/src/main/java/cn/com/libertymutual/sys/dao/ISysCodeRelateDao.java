package cn.com.libertymutual.sys.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import cn.com.libertymutual.sys.bean.SysCodeRelate;

public interface ISysCodeRelateDao extends PagingAndSortingRepository<SysCodeRelate, Integer>, JpaSpecificationExecutor<SysCodeRelate> {

	@Query(" from SysCodeRelate  where codeType =:codeType   " + " and code =:code " + " and codeType1 =:codeType1 " + " and code1 =:code1 "
			+ " and riskCode =:riskCode " + " and codeType2 =:codeType2 " + " and validStatus ='1' order by  serialNo asc")
	public Iterable<SysCodeRelate> findByCodeTypeAndvalidStatusOrderBySerialNoAsc(@Param("codeType") String codeType, @Param("code") String code,
			@Param("codeType1") String codeType1, @Param("code1") String code1, @Param("riskCode") String riskCode,
			@Param("codeType2") String codeType2

	);

	/**
	 * 根据车主类型.车辆类型,险种,来查询车辆使用性质
	 * @param insuredNature
	 * @param carKind
	 * @param riskCode
	 * @param useNature
	 * @return
	 */
	@Query(" from SysCodeRelate  where   " + "  code =:code " + " and code1 =:code1 " + " and riskCode =:riskCode "
			+ " and validStatus ='1' order by  serialNo asc")
	public List<SysCodeRelate> findUseNatureCode(@Param("code") String code, @Param("code1") String code1, @Param("riskCode") String riskCode

	);

	/**
	 * code:3
	codeType:InsuredNature
	codeType1:CarKind
	code1:A0
	riskCode:0502
	codeType2:UseNature
	 */

	/**
	 * 根据车主类型.车辆类型,险种,来判断车辆使用性质是否存在
	 * @param insuredNature
	 * @param carKind
	 * @param riskCode
	 * @param useNature
	 * @return
	 */
	@Query(" from SysCodeRelate  where    " + " code =:code " + " and code1 =:code1 " + " and riskCode =:riskCode " + " and code2 =:code2 "
			+ " and validStatus ='1' order by  serialNo asc")
	public Iterable<SysCodeRelate> checkUseNatureCode(@Param("code") String insuredNature, @Param("code1") String carKind,
			@Param("riskCode") String riskCode, @Param("code2") String useNature

	);

	@Query(" from SysCodeRelate  where codeType =?1 and code = ?2  ORDER BY code asc")
	public List<SysCodeRelate> getOccupatioCategory(String codeType, String code);

	@Query(" from SysCodeRelate  where codeType = :code  ORDER BY code asc")
	public List<SysCodeRelate> getSaleInitData(@Param("code") String code);

	@Query(" from SysCodeRelate  where code like %:code% and  codeEname1 = :language  ORDER BY code asc")
	public List<SysCodeRelate> getCliaimsType(@Param("code") String code, @Param("language") String language);

	@Query("select t from SysCodeRelate t where t.codeType = :code  ORDER BY t.code asc")
	public Page<SysCodeRelate> riskListPage(@Param("code") String code, Pageable pageable);
}