package cn.com.libertymutual.sys.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import cn.com.libertymutual.core.redis.util.RedisUtils;
import cn.com.libertymutual.core.util.Constants;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sys.bean.SysCodeNode;
import cn.com.libertymutual.sys.bean.SysCodeRisk;
import cn.com.libertymutual.sys.dao.ISysCodeNodeDao;
import cn.com.libertymutual.sys.dao.ISysCodeRiskDao;
import cn.com.libertymutual.sys.service.api.ISysCodeNodeService;

import com.google.common.collect.Lists;

@Service("sysCodeNodeService")
public class SysCodeNodeService implements ISysCodeNodeService {

	
 
	@Autowired ISysCodeNodeDao sysCodeNodeDao;
	@Autowired ISysCodeRiskDao sysCodeRiskDao;
	@Resource RedisUtils redisUtils;
	@Override
	public List<SysCodeNode> findCodeInfoForValid(String codeType) {
		List<SysCodeNode> rlist= Lists.newArrayList();
		Iterable<SysCodeNode> its = sysCodeNodeDao.findByCodeTypeAndvalidStatusOrderBySerialNoAsc(codeType, "1");
		for(SysCodeNode  it: its ){
			rlist.add(it);
		}
		return rlist;
	
	}

	@Override
	public List<SysCodeNode> findCodeInfoByRiskCode(String codeType,
			String riskCode ,Map<String, SysCodeNode> nodeMap) {
		List<SysCodeNode> rlist= Lists.newArrayList();
		Iterable<SysCodeRisk> codeRisks = sysCodeRiskDao.findAll(); 
		
		for(SysCodeRisk sr:  codeRisks){
			if(sr.getCodeType().equals(codeType) && sr.getRiskCode().equals(riskCode)){
			   SysCodeNode node = nodeMap.get(sr.getCode());
			   if(node!=null && "1".equals(node.getValidStatus())){
				rlist.add(nodeMap.get(sr.getCode()));
			   }
			}
		}
	 
		return rlist;
	}

	@Override 
	public List<SysCodeNode> findAreaSortList() {
		List<SysCodeNode> rlist= Lists.newArrayList();
		Iterable<SysCodeNode> its = sysCodeNodeDao.findByCodeTypeAndCodeLike(Constants.CODE_AREASORT,"__" );
		for(SysCodeNode  it: its ){
			rlist.add(it);
		}
		return rlist;
	}

	@Override
	public List<SysCodeNode> findAreaSortList(String code) {
		List<SysCodeNode> rlist= Lists.newArrayList();
		Iterable<SysCodeNode> its = sysCodeNodeDao.findByCodeTypeAndCodeLike(Constants.CODE_AREASORT,code+"__" );
		for(SysCodeNode  it: its ){
			rlist.add(it);
		}
		return rlist;
	}

	@Override
	public List<SysCodeNode> findSysCodeNode(String codetype, String sorttype) {
		Sort sort=null;
		if("ASC".equals(sorttype) ){
			sort =new Sort(Direction.ASC,"id");
		}else{
			sort =new Sort(Direction.DESC,"id");
		}
		
		List<SysCodeNode> list =sysCodeNodeDao.findAll(new Specification<SysCodeNode>(){  
			@Override
			public Predicate toPredicate(Root<SysCodeNode> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {			     			        
				List<Predicate> list = new ArrayList<>();
				list.add(cb.like(root.get("validStatus").as(String.class),"1"));
				list.add(cb.like(root.get("codeType").as(String.class),codetype));
				Predicate[] p = new Predicate[list.size()];
	
				return cb.and(list.toArray(p));
			}  
        }, sort);
		
		
		
		return list;
	}

	public ServiceResult addProductCompany(SysCodeNode sysCodeNode) {
		ServiceResult sr = new ServiceResult();
		sysCodeNode.setCodeType("ProductCompany");
		sysCodeNode.setCode("ProductCompany");
		sysCodeNode.setCodeEname(sysCodeNode.getCodeCname());
		sysCodeNode.setCodeCname(sysCodeNode.getCodeCname());
		sysCodeNode.setValidStatus("1");
		sysCodeNode.setSerialNo(1);
		SysCodeNode dbsysCodeNode = sysCodeNodeDao.save(sysCodeNode);
		sr.setResult(dbsysCodeNode);
		return sr;
	}
 

}
