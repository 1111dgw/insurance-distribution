package cn.com.libertymutual.sys.dao;

import org.springframework.data.repository.CrudRepository;

import cn.com.libertymutual.sys.bean.PrpRisk;

public interface IRiskDao extends CrudRepository<PrpRisk, Integer> {


}
