package cn.com.libertymutual.sys.bean;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by Ryan on 2016-09-09.
 */
public class SysMenuResPK implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -452113995059153914L;
	private int menuid;
    private String resid;

    @Column(name = "MENUID", nullable = false)
    @Id   
//    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getMenuid() {
        return menuid;
    }

    public void setMenuid(int menuid) {
        this.menuid = menuid;
    }

    @Column(name = "RESID", nullable = false, length = 32)
    @Id
    public String getResid() {
        return resid;
    }

    public void setResid(String resid) {
        this.resid = resid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysMenuResPK that = (SysMenuResPK) o;

        if (menuid != that.menuid) return false;
        if (resid != null ? !resid.equals(that.resid) : that.resid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = menuid;
        result = 31 * result + (resid != null ? resid.hashCode() : 0);
        return result;
    }

	@Override
	public String toString() {
		return "SysMenuResPK [menuid=" + menuid + ", resid=" + resid + "]";
	}
}
