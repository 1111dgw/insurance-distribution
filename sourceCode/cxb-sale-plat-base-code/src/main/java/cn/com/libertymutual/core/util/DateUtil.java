package cn.com.libertymutual.core.util;

import java.sql.Timestamp;
import java.text.Format;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.base.Strings;

/**
 * 
 * <p>
 * 日期工具类
 * </p>
 * 
 * <p>
 * Copyright: 版权所有 (c) 2002 - 2008<br>
 * Company: mymost
 * </p>
 * 
 * @author yl
 * @version 2011-6-29
 */
public class DateUtil {

	private final static String EMPTY = "";
	public final static String DEFAULT_PATTERN = "yyyy-MM-dd";

	public final static String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm";

	public final static String DATE_TIME_PATTERN2 = "yyyy-MM-dd HH:mm:ss";

	public final static String DATE_TIME_PATTERN2LANG = "yyyy-MM-dd HH:mm:ss.SSS";

	public final static String DATE_TIME_PATTERN3 = "yyyyMMdd";

	public final static String DATE_TIME_PATTERN4 = "yyyyMMddHHmmss";
	public final static String DATE_TIME_PATTERN4LANG = "yyyyMMddHHmmssSSS";

	public final static String DATE_TIME_PATTERN5 = "HH:mm:ss";

	public final static String DATE_TIME_PATTERNHH = "yyyyMMddHH";

	public final static String PATTERN_YYYY_CH = "yyyy年";
	public final static String PATTERN_YYYY_MM_CH = "yyyy年MM月";
	public final static String PATTERN_YYYY_MM_DD_CH = "yyyy年MM月dd日";
	public final static String PATTERN_M_D_CH = "M月d日";
	public final static String PATTERN_MM_DD_CH = "MM月dd日";
	public final static String PATTERN_YY_MM_DD = "yyMMdd";

	/**
	 * 添加日期
	 * 
	 * @param part
	 *            加减日期常量对应的数字
	 * @param n
	 *            要添加的数字
	 * @param date
	 *            要添加的日期,yyyy-MM-dd HH:mm:ss或者yyyy-MM-dd
	 * @return 添加后的日期
	 * @param 1=YEAR=年
	 * @param 2=MONTH月,从
	 *            0 开始，也就是全年 12 个月由 0 ~ 11 进行表示
	 * @param 3=WEEK_OF_YEAR,当前年中的星期数
	 * @param 4=WEEK_OF_MONTH,当前月中的星期数
	 * @param 5=DAY_OF_MONTH,一个月中的某天。
	 * @param 6=DAY_OF_YEAR,一年中的某天
	 * @param 7=DAY_OF_WEEK,一周中的一天,SUNDAY
	 *            = 1,MONDAY = 2,TUESDAY = 3,WEDNESDAY = 4,THURSDAY = 5,FRIDAY =
	 *            6,SATURDAY = 7
	 * @param 8=DAY_OF_WEEK_IN_MONTH,当前月中的第几个星期
	 * @param 10=HOUR,上午或下午的小时
	 * @param 11=HOUR_OF_DAY,一天中的小时
	 * @param 12=MINUTE,一小时中的分钟
	 * @param 13=SECOND,一分钟中的秒
	 * @param 14=MILLISECOND,一秒中的毫秒
	 */
	public static String dateAdd(int part, int n, String date) {
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_TIME_PATTERN2);
		GregorianCalendar gc = new GregorianCalendar();
		try {
			gc.setTime(sdf.parse(date));
			gc.add(part, n);
		} catch (ParseException e) {
			sdf = new SimpleDateFormat(DEFAULT_PATTERN);
			try {
				gc.setTime(sdf.parse(date));
				gc.add(part, n);
			} catch (ParseException e2) {
				return null;
			}
		}
		return sdf.format(gc.getTime());
	}

	/**
	   * 将短时间格式字符串转换为时间 yyyy-MM-dd 
	   * 
	   * @param strDate
	   * @return
	   */
	public static Date strToDate(String strDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		ParsePosition pos = new ParsePosition(0);
		Date strtodate = formatter.parse(strDate, pos);
		return strtodate;
	}

	/**
	 * 获取指定时间字符串的Date时间
	 * 
	 * @return返回Date
	 */
	public static Date getDateByString(String strDate) {
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_TIME_PATTERN2);
		ParsePosition pos = new ParsePosition(0);
		Date strtodate = formatter.parse(strDate, pos);
		return strtodate;
	}

	/**
	 * 一天的毫秒数
	 */
	public final static long ONE_DATE_TIME = 24 * 60 * 60 * 1000l;

	/**
	 * 获取当前时间的字符串 yyMMdd
	 * 
	 * @return返回字符串格式 yyMMdd
	 */
	public static String getStringDateYyMMdd() {
		Date currentTime = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat(PATTERN_YY_MM_DD);
		String dateString = formatter.format(currentTime);
		return dateString;
	}

	/**
	 * 获取现在时间
	 * 
	 * @return返回字符串格式 yyyy-MM-dd HH:mm:ss
	 */
	public static String getStringDate() {
		Date currentTime = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_TIME_PATTERN2);
		String dateString = formatter.format(currentTime);
		return dateString;
	}

	/**
	 * 获取当前时间的字符串2
	 * 
	 * @return返回字符串格式 yyyy-MM-dd HH:mm:ss.SSS
	 */
	public static String getStringDate2() {
		Date currentTime = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_TIME_PATTERN2LANG);
		String dateString = formatter.format(currentTime);
		return dateString;
	}

	/**
	 * 把long类型的日期转成指定格式pattern的字符串返回
	 * 
	 * @param time
	 * @param pattern
	 * @return
	 */
	public static String dateFromat(long time, String pattern) {
		if (time > 0) {
			Date thisDate = new Date(time);
			String p = DEFAULT_PATTERN;
			if (CheckIsNull.isNotEmpty(pattern)) {
				p = pattern;
			}
			SimpleDateFormat sdf = new SimpleDateFormat(p);
			return sdf.format(thisDate);
		}
		return EMPTY;
	}

	/**
	 * 把long类型的日期转成"yyyy-MM-dd"的字符串返回
	 * 
	 * @param time
	 * @param pattern
	 * @return
	 */
	public static String dateFromat(long time) {
		if (time > 0) {
			Date thisDate = new Date(time);
			String p = DEFAULT_PATTERN;
			SimpleDateFormat sdf = new SimpleDateFormat(p);
			return sdf.format(thisDate);
		}
		return EMPTY;
	}

	/**
	 * 把Date类型"yyyy-MM-dd HH:mm:ss"的日期转成"yyyy-MM-dd"的字符串返回
	 * 
	 * @param time
	 * @param pattern
	 * @return
	 */
	public static Date dateFormat(Date time) {
		if (null != time) {
			String p = DEFAULT_PATTERN;
			SimpleDateFormat format = new SimpleDateFormat(p);
			try {
				return format.parse(format.format(time));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * 将时间转换转成"yyyy-MM-dd"的字符串返回<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年4月16日上午10:02:33<br>
	 * @param thisDate
	 * @return
	 */
	public static String dateFromat(Date thisDate) {
		if (null != thisDate) {
			String p = DEFAULT_PATTERN;
			SimpleDateFormat sdf = new SimpleDateFormat(p);
			return sdf.format(thisDate);
		}
		return EMPTY;
	}

	/**
	 * 把long类型的日期转成"yyyy-MM-dd"的字符串返回
	 * 
	 * @param time
	 * @param pattern
	 * @return
	 */
	public static String dateFromat(Date thisDate, String pattern) {
		if (thisDate != null) {
			String p = DEFAULT_PATTERN;
			if (CheckIsNull.isNotEmpty(pattern)) {
				p = pattern;
			}
			SimpleDateFormat sdf = new SimpleDateFormat(p);
			return sdf.format(thisDate);
		}
		return EMPTY;
	}

	/**
	 * 新历转成农历日期
	 * 
	 * @return
	 */
	public static long new2OldDate(long newDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(newDate);
		Date time = ChineseCalendar.sCalendarSolarToLundar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH));
		return time.getTime();
	}

	/**
	 * 农历转新历
	 * 
	 * @param oldDate
	 * @return
	 */
	public static long old2NewDate(long oldDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(oldDate);
		Date time = ChineseCalendar.sCalendarLundarToSolar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH));
		return time.getTime();
	}

	/**
	 * 得到当前日志的timestamp串 格式：yyyy-MM-dd HH:mm:ss
	 * 
	 * @update 2016-05-19 16:05 zhaoyu
	 * 
	 * @param oldDate
	 * @return
	 * @throws Exception
	 */
	public static Timestamp stringToTimestamp(String oldDate) throws Exception {
		Format f = new SimpleDateFormat(DATE_TIME_PATTERN2);
		Date d = new Date();

		d = (Date) f.parseObject(oldDate);
		Timestamp ts = new Timestamp(d.getTime());

		return ts;
	}

	/**
	 * 得到当前日志的timestamp串 格式：yyyy-MM-dd HH:mm:ss
	 * 
	 * @param oldDate
	 * @return
	 */
	public static Timestamp dateToTimestamp(Date oldDate) {
		Timestamp ts = new Timestamp(oldDate.getTime());
		return ts;
	}

	/**
	 * 得到当前日志的timestamp串 格式：yyyy-MM-dd HH:mm:ss
	 * 
	 * @update 2016-05-19 16:05 zhaoyu
	 * @param oldDate
	 * @return
	 * @throws Exception
	 */
	public static Timestamp stringToTimestamp(String oldDate, String pattern) throws Exception {
		Format f = new SimpleDateFormat(pattern);
		Date d = new Date();

		d = (Date) f.parseObject(oldDate);
		Timestamp ts = new Timestamp(d.getTime());

		return ts;
	}

	/**
	 * 将形如2003-2-2的时间字符串转换为 Date对象
	 * 
	 * @update 2016-05-19 16:06 zhaoyu
	 * @param dateStr
	 * @return long
	 * @throws Exception
	 */
	public static Date convertStringToDate(String dateStr) throws Exception {
		if (StringUtil.isEmpty(dateStr)) {
			return null;
		}
		// SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
		return sdf2.parse(dateStr);
	}

	public static Date convertStringToDateYMDhm(String dateStr) throws Exception {
		if (StringUtil.isEmpty(dateStr)) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		// SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.parse(dateStr);
	}

	/**
	 * 将形如2003-2-2的时间字符串转换为 Date对象，如果字符串格式不正确，返回当前时间对象
	 * 
	 * @update 2016-05-19 16:07 zhaoyu
	 * @param dateStr
	 * @return long
	 * @throws Exception
	 */
	public static Date convertStringToDate(String dateStr, String pattern) throws Exception {
		if (StringUtil.isEmpty(dateStr) || StringUtil.isEmpty(pattern)) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.parse(dateStr);
	}

	/**
	 * bob
	 * 
	 * @param dateStr
	 * @param pattern
	 * @return
	 * @throws Exception
	 */
	public static Calendar convertStringToCalendar(String dateStr, String pattern) throws Exception {
		if (StringUtil.isEmpty(dateStr) || StringUtil.isEmpty(pattern)) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);

		Calendar cal = Calendar.getInstance();
		cal.setTime(sdf.parse(dateStr));
		return cal;
	}

	public static int convertStringToCalendarNum(String dateStr, String pattern, int field) throws Exception {

		Calendar cal = convertStringToCalendar(dateStr, pattern);

		if (field == Calendar.YEAR) {
			return cal.get(field) - 1900;
		}

		return cal.get(field);
	}

	public static int getCalendarNum(Calendar cal, int field) throws Exception {

		if (field == Calendar.YEAR) {
			return cal.get(field) - 1900;
		}

		return cal.get(field);
	}

	/**
	 * 日期取整函数<Br>
	 * 将指定日期从指定的时间单位处开始取整，如将2011-11-1 15:30 抹去时间 获得 2011-11-1 00:00
	 * 
	 * @update 2016-05-19 16:09 zhaoyu
	 * @param time
	 * @param field
	 *            接受值为java.util.Calendar的常量 Calendar.DATE 从天数处开始取整 Calendar.MONTH
	 *            从月份处开始取整 2011-5-3 返回 2011-5-1 Calendar.YEAR 从年份处开始取整 Calendar.HOUR
	 *            从小时取整
	 * @return long
	 * @throws Exception
	 */
	public static long trunc(long time, int field) throws Exception {
		String format = "yyyyMMdd 00:00";
		switch (field) {
		case Calendar.DATE:
			break;
		case Calendar.MONTH:
			format = "yyyyMM";
			break;
		case Calendar.YEAR:
			format = "yyyy";
			break;
		case Calendar.HOUR:
			format = "yyyyMMdd hh:00";
			break;
		default:
			throw new IllegalArgumentException("日期取整时参数错误：没有" + field + "这种类型。常量值请参考java.util.Calendar");
		}
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.parse(sdf.format(time)).getTime();
	}

	/**
	 * 将指定时间对象的小时分钟信息归零
	 * 
	 * @param time
	 * @return long
	 * @throws Exception
	 */
	public static long truncDay(long time) throws Exception {
		return DateUtil.trunc(time, Calendar.DATE);
	}

	/**
	 * 得到指定日的起始时间（0时0分0秒）
	 * 
	 * @param date
	 * @return
	 */
	public static long getDayStartTime(long date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.clear(Calendar.MINUTE);
		calendar.clear(Calendar.SECOND);
		calendar.clear(Calendar.MILLISECOND);
		return calendar.getTimeInMillis();
	}

	/**
	 * 得到指定日的结束时间（23时59分59秒...）
	 * 
	 * @param date
	 * @return
	 */
	public static long getDayEndTime(long date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.clear(Calendar.MINUTE);
		calendar.clear(Calendar.SECOND);
		calendar.clear(Calendar.MILLISECOND);
		return calendar.getTimeInMillis() + 24 * 60 * 60 * 1000 - 1;
	}

	/**
	 * 获取本月第一天日期
	 * 
	 * @return
	 */
	public static Date getThisMonth() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DATE, 1);
		return cal.getTime();
	}

	/**
	 * 获取本月最后一天日期
	 * 
	 * @return
	 */
	public static Date getLastDayOfThisMonth() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DATE, 1);
		cal.roll(Calendar.DATE, -1);
		return cal.getTime();
	}

	/**
	 * 获取今天开始的时间
	 * 
	 * @return
	 * @throws Exception
	 */
	public static long getToday() throws Exception {
		return truncDay(System.currentTimeMillis());
	}

	/**
	 * 判断两个日期是否跨年
	 * 
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	public static boolean checkIsNewYear(long beginDate, long endDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(beginDate);
		int beginYear = cal.get(Calendar.YEAR);
		cal.setTimeInMillis(endDate);
		int endYear = cal.get(Calendar.YEAR);
		return beginYear != endYear;
	}

	/**
	 * 传入当前日期，格式是yyyy-mm-dd
	 * 
	 * @param date
	 * @return 返回传入日期是当前的今年某个月的某个星期 格式为:yyyymmww
	 * @throws Exception
	 */
	public static String getDayofWeekofMonth(String time) throws Exception {
		// String time = "2011-12-02";

		Date date = new SimpleDateFormat("yyyy-MM-dd").parse(time);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		if (calendar.get(Calendar.DAY_OF_WEEK) == 1) {
			/*
			 * System.out.println("本周期末时间： <=" + new
			 * SimpleDateFormat("yyyyMMdd").format(calendar .getTime()));
			 */
			/*
			 * calendar.set(Calendar.DAY_OF_YEAR, calendar .get(Calendar.DAY_OF_YEAR) - 7);
			 * System.out.println("本周期初时间：<=" + new
			 * SimpleDateFormat("yyyyMMdd").format(calendar .getTime()));
			 */
			return new SimpleDateFormat("yyyyMMdd").format(calendar.getTime());
		} else {
			while (calendar.get(Calendar.DAY_OF_WEEK) < 7) {
				calendar.set(Calendar.DAY_OF_WEEK, calendar.get(Calendar.DAY_OF_WEEK) + 1);
			}
			calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) + 1);
			/*
			 * System.out.println("本周期末时间： <=" + new
			 * SimpleDateFormat("yyyyMMdd").format(calendar .getTime()));
			 * calendar.set(Calendar.DAY_OF_YEAR, calendar .get(Calendar.DAY_OF_YEAR) - 7);
			 * System.out.println("本周期初时间：<=" + new
			 * SimpleDateFormat("yyyyMMdd").format(calendar .getTime()));
			 */
			return new SimpleDateFormat("yyyyMMdd").format(calendar.getTime());
		}
		/*
		 * Calendar c_now = new GregorianCalendar(); Calendar c_begin = new
		 * GregorianCalendar(); Calendar c_end = new GregorianCalendar();
		 * DateFormatSymbols dfs = new DateFormatSymbols(); SimpleDateFormat df = new
		 * SimpleDateFormat("yyyy-M-dd"); java.util.Date cDate = null; try { cDate =
		 * df.parse(date); } catch (ParseException e) { e.printStackTrace(); }
		 * c_now.setTime(cDate); int year = c_now.get(Calendar.YEAR); int month =
		 * c_now.get(Calendar.MONTH) + 1; int today = c_now.get(Calendar.DAY_OF_MONTH);
		 * System.out.println(today); int[] days = { 0, 31, 28, 31, 30, 31, 30, 31, 31,
		 * 30, 31, 30, 31 }; if (year % 4 == 0) { days[2] = 29;// 大年 } c_begin.set(2010,
		 * month - 1, 1); // 月 0-11 天 0- c_end.set(2010, month - 1, days[month]);
		 * 
		 * int count = 1; c_end.add(Calendar.DAY_OF_YEAR, 1); // 结束日期下滚一天是为了包含最后一天 int
		 * day = c_now.get(Calendar.DAY_OF_WEEK) - 1; while (c_begin.before(c_end)) { if
		 * (day == 8) { count++; day = 1; }
		 * 
		 * if (today == c_begin.get(Calendar.DAY_OF_MONTH)) {
		 * //System.out.println(df.format(c_begin.getTime()) + "是" + month //+ "月第" +
		 * count + "周的第" + day + "天"); String returnmonth =
		 * (c_begin.get(Calendar.YEAR)+1) + "" + (c_begin.get(Calendar.MONTH)+1); Date
		 * date2; String returnvalue = ""; try { date2 = new
		 * SimpleDateFormat("yyyyMM").parse(returnmonth); returnvalue = new
		 * SimpleDateFormat("yyyyMM").format(date2)+ "0" + count; } catch
		 * (ParseException e) { e.printStackTrace(); } return returnvalue ; } day++; //
		 * System.out.println("第"+count+"周 日期："+new // java.sql.Date(c_begin.
		 * getTime().getTime())+", "+weeks[c_begin.get(Calendar.DAY_OF_WEEK)]);
		 * 
		 * c_begin.add(Calendar.DAY_OF_YEAR, 1); } //System.out.println("共计（跨越）：" +
		 * (count) + "周"); return null;
		 */
	}

	/**
	 * 
	 * @param date
	 *            当前日期
	 * @return 得到当前月日期所属季度
	 */
	public static int getCurrQuarter(String str) {
		String s = "";
		if (null != str) {
			String da = str.substring(4, 6);
			String yy = str.substring(0, 4);
			int m = Integer.valueOf(da);
			int cs = (m - 1) / 3 + 1;
			s = yy + String.valueOf(cs);

		}
		return Integer.parseInt(s);
	}

	/**
	 * 得到当前月份
	 * 
	 * @param str
	 * @return
	 */
	public static int getMonth(String str) {
		int reValue = 0;
		if (null != str) {
			String s = str.substring(0, 6);
			reValue = Integer.parseInt(s);
		}
		return reValue;
	}

	/**
	 * 得到上一个星期的日期
	 * 
	 * @param dateStr
	 * @return
	 * @throws Exception
	 */
	public static Calendar getBeforeWeekDay(String dateStr) throws ParseException {
		Date date = null;

		date = new SimpleDateFormat("yyyyMMdd").parse(dateStr);

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - 7);
		return calendar;
	}

	/**
	 * 得到两个日期的间隔天数
	 */
	public static long getDaysBetween(Date startDate, Date endDate) {
		long days = 0;
		Calendar fromCalendar = Calendar.getInstance();
		fromCalendar.setTime(startDate);
		fromCalendar.set(Calendar.HOUR_OF_DAY, 0);
		fromCalendar.set(Calendar.MINUTE, 0);
		fromCalendar.set(Calendar.SECOND, 0);
		fromCalendar.set(Calendar.MILLISECOND, 0);

		Calendar toCalendar = Calendar.getInstance();
		toCalendar.setTime(endDate);
		toCalendar.set(Calendar.HOUR_OF_DAY, 0);
		toCalendar.set(Calendar.MINUTE, 0);
		toCalendar.set(Calendar.SECOND, 0);
		toCalendar.set(Calendar.MILLISECOND, 0);
		days = ((toCalendar.getTime().getTime() - fromCalendar.getTime().getTime()) / (1000 * 60 * 60 * 24));
		return days;
	}

	/**
	 * 得到两个时间的间隔小时数
	 */
	public static long getHoursBetween(Date startDate, Date endDate) {
		long times = 0;

		times = getTimesBetween(startDate, endDate) / (1000 * 60 * 60);
		return times;
	}

	/**
	 * 得到两个时间的间隔毫秒数, times>0 则endDate大于startDate ; times<0 , 则endDate小于startDate
	 */
	public static long getTimesBetween(Date startDate, Date endDate) {
		long times = 0;
		Calendar fromCalendar = Calendar.getInstance();
		fromCalendar.setTime(startDate);

		Calendar toCalendar = Calendar.getInstance();
		toCalendar.setTime(endDate);
		times = toCalendar.getTime().getTime() - fromCalendar.getTime().getTime();
		return times;
	}

	/**
	 * 判断指定日期是否为当天
	 */
	public static boolean isCurrentDay(long date) {
		return getDayStartTime(new Date().getTime()) <= date && getDayEndTime(new Date().getTime()) >= date;
	}

	/**
	 * 获取时分秒
	 * 
	 * @param date
	 * @return
	 */
	public static String getHHMMSS(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		String hh = c.get(Calendar.HOUR_OF_DAY) < 10 ? ("0" + c.get(Calendar.HOUR_OF_DAY)) : (c.get(Calendar.HOUR_OF_DAY) + "");
		String mm = c.get(Calendar.MINUTE) < 10 ? ("0" + c.get(Calendar.MINUTE)) : (c.get(Calendar.MINUTE) + "");
		String ss = c.get(Calendar.SECOND) < 10 ? ("0" + c.get(Calendar.SECOND)) : (c.get(Calendar.SECOND) + "");
		return hh + ":" + mm + ":" + ss;

	}

	/**
	 * 获取上月的今天
	 */
	public static Date getTodayOfLastMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, -1);
		return calendar.getTime();
	}

	/**
	 * 获取明天
	 */
	public static String getTomorrow(Date date) {
		if (CheckIsNull.isEmpty(date))
			date = new Date(System.currentTimeMillis());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.HOUR, +24);
		return dateFromat(calendar.getTime());
	}

	/**
	 * 获取明天
	 */
	public static Date tomorrow(Date date) {
		if (CheckIsNull.isEmpty(date))
			date = new Date(System.currentTimeMillis());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.HOUR, +24);
		return calendar.getTime();
	}

	/**
	 * 获取明年
	 */
	public static Date nextYear(Date date) {
		if (CheckIsNull.isEmpty(date))
			date = new Date(System.currentTimeMillis());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.YEAR, 1);
		return calendar.getTime();
	}

	/**
	 * 获取隔一年的时间,
	 * 
	 * @param date
	 *            if date==null date= today; eg. 20160929
	 * @return eg. 20170928
	 */
	public static Date getWithInOneYear(Date date) {
		if (CheckIsNull.isEmpty(date))
			date = new Date(System.currentTimeMillis());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.YEAR, 1);
		calendar.add(Calendar.DAY_OF_YEAR, -1);
		return calendar.getTime();
	}

	/**
	 * 获取后天
	 */
	public static String getTheDayAfterTomorrow(Date date) {
		if (CheckIsNull.isEmpty(date))
			date = new Date(System.currentTimeMillis());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.HOUR, +48);
		return dateFromat(calendar.getTime());
	}

	/**
	 * 获取24小时前时间
	 */
	public static long get24HoursAgo(Date date) {
		if (CheckIsNull.isEmpty(date))
			date = new Date(System.currentTimeMillis());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.SECOND, -(60 * 60 * 24));
		return calendar.getTimeInMillis();
	}

	/**
	 * Remarks: 计算时间差<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年2月16日下午8:49:56<br>
	 * Project：parking<br>
	 * @param begin 起始时间
	 * @param end 结束时间
	 * @return  相差的天数,当小于1天则为0
	 */
	public static int dateDiff(String begin, String end) {
		int days = 0;

		Date s = getDateByString(begin);
		Date e = getDateByString(end);
		days = (int) Math.abs(e.getTime() - s.getTime()) / (24 * 60 * 60 * 1000);
		return days;
	}

	/**Remarks: 将一个表示时间段的数转换为毫秒数 <br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年2月16日下午8:50:20<br>
	 * Project：parking<br>
	 * @param num 时间差数值,支持小数 
	 * @param type 时间类型：1->秒,2->分钟,3->小时,4->天 
	 * @return long类型时间差毫秒数，当为-1时表示参数有错 
	 */
	public static long formatToTimeMillis(double num, int type) {
		if (num <= 0)
			return 0;
		switch (type) {
		case 0:
			return (long) (num * 1000);
		case 1:
			return (long) (num * 60 * 1000);
		case 2:
			return (long) (num * 60 * 60 * 1000);
		case 3:
			return (long) (num * 24 * 60 * 60 * 1000);
		default:
			return -1;
		}

	}

	/**
	 * 获取某一指定时间的前一段时间
	 * 
	 * @param num
	 *            时间差数值
	 * @param type
	 *            时间差类型：1->秒,2->分钟,3->小时,4->天
	 * @param date
	 *            参考时间
	 * @return 返回Date对象
	 */
	public static Date getPreTime(double num, int type, Date date) {
		long nowLong = date.getTime();// 将参考日期转换为毫秒时间
		Date time = new Date(nowLong - formatToTimeMillis(num, type));// 减去时间差毫秒数
		return time;
	}

	/**
	 * 判断某个时间为星期几
	 * 
	 * @param ptime
	 */
	public static int dayForWeek(String pTime) throws Exception {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Calendar c = Calendar.getInstance();
		c.setTime(format.parse(pTime));
		int dayForWeek = 0;
		if (c.get(Calendar.DAY_OF_WEEK) == 1) {
			dayForWeek = 7;
		} else {
			dayForWeek = c.get(Calendar.DAY_OF_WEEK) - 1;
		}
		return dayForWeek;
	}

	/**
	 * 返回指定天数位移后的日期
	 */
	public static Date dayOffset(Date date, int offset) {
		return offsetDate(date, Calendar.DATE, offset);
	}

	/**
	 * 返回指定日期相应位移后的日期
	 * 
	 * @param date
	 *            参考日期
	 * @param field
	 *            位移单位，见 {@link Calendar}
	 * @param offset
	 *            位移数量，正数表示之后的时间，负数表示之前的时间
	 * @return 位移后的日期
	 */
	public static Date offsetDate(Date date, int field, int offset) {
		Calendar calendar = convert(date);
		calendar.add(field, offset);
		return calendar.getTime();
	}

	private static Calendar convert(Date date) {
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		return calendar;
	}

	/**
	 * 试用参数Format格式化Calendar成字符串
	 * 
	 * @param cal
	 * @param pattern
	 * @return String
	 */
	public static String format(Calendar cal, String pattern) {
		return cal == null ? "" : new SimpleDateFormat(pattern).format(cal.getTime());
	}

	/**
	 * 求几分钟之后的时间
	 */
	public static Date getLastDate(int lastDate) {
		java.util.Calendar Cal = java.util.Calendar.getInstance();
		Cal.setTime(new Date());
		Cal.add(java.util.Calendar.MINUTE, lastDate);
		return Cal.getTime();
	}

	public static boolean isInDate(Date date, List<String> times) {
		if (times == null)
			return false;
		boolean flag = false;
		for (int i = 0; i < times.size(); i++) {
			flag |= isInDate(new Date(), times.get(i).split("-")[0], times.get(i).split("-")[1]);
		}
		if (flag) {// 可以下单
			return true;
		} else {// 不能下单
			return false;
		}
	}

	public static boolean isInDeliDate(String deliData, List<String> times) {
		if (times == null)
			return false;
		// boolean flag = false;
		String[] deliDateArray = deliData.split("-");
		// 兼容老格式 8:00-12:00
		if (deliDateArray.length < 2)
			return false;
		boolean startFlag = false;
		boolean endFlag = false;
		long startDeliDate = formatTime(deliDateArray[0], 10, 0);
		long endDeliDate = formatTime(deliDateArray[1], -1, 0);
		if (startDeliDate > endDeliDate) {
			endDeliDate = formatTime(deliDateArray[1], -1, 1);
		}
		// 11:00-11:20 11:01-11:21
		// 11:02-11:18
		for (int i = 0; i < times.size(); i++) {
			// flag|= isInDate(new Date(), times.get(i).split("-")[0],
			// times.get(i).split("-")[1]);
			long timesStart = formatTime(times.get(i).split("-")[0], 0, 0);
			long timesEnd = formatTime(times.get(i).split("-")[1], 0, 0);
			if (timesStart > timesEnd) {
				timesEnd = formatTime(times.get(i).split("-")[1], 0, 1);
			}
			// 如果开始时间在范围内，结束时间也在范围内，则认为可下单
			if (startDeliDate >= timesStart && startDeliDate <= timesEnd)
				startFlag = true;
			if (endDeliDate >= timesStart && endDeliDate <= timesEnd)
				endFlag = true;
			if (startFlag && endFlag)
				break;
		}

		if (startFlag && endFlag) {// 可以下单
			return true;
		} else {// 不能下单
			return false;
		}
	}

	private static long formatTime(String time, int minNum, int isTomorrow) {
		// time格式：8:00.12:00，8:1,8:12
		String times[] = time.split(":");
		int hour;
		int min;
		if (times[0].length() == 1) {
			hour = Integer.parseInt(times[0]);
		} else {
			hour = Integer.parseInt(times[0]);
		}

		if (times[1].length() == 1) {
			min = Integer.parseInt(times[1]);
		} else {
			min = Integer.parseInt(times[1]);
		}

		Calendar calendar = Calendar.getInstance();

		calendar.set(Calendar.MINUTE, min);
		calendar.set(Calendar.HOUR_OF_DAY, hour);
		calendar.clear(Calendar.SECOND);
		calendar.clear(Calendar.MILLISECOND);
		if (isTomorrow == 1) {
			calendar.add(Calendar.DATE, 1);
		}
		if (minNum > 0)
			calendar.add(Calendar.MINUTE, minNum);

		return calendar.getTimeInMillis();

	}

	/**
	 * 判断date在不在某个时间段内 6:00~12:00
	 * 
	 * @param date
	 *            要判断的时间
	 * @param strDateBegin
	 *            开始时间：6:00
	 * @param strDateEnd
	 *            结束时间 12:00
	 * @return
	 */
	public static boolean isInDate(Date date, String strDateBegin, String strDateEnd) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String strDate = sdf.format(date); // 截取当前时间时分秒
		int strDateH = Integer.parseInt(strDate.substring(11, 13));// 小时
		int strDateM = Integer.parseInt(strDate.substring(14, 16));// 分钟
		int strDateBeginH = Integer.parseInt(strDateBegin.split(":")[0]);
		int strDateBeginM = Integer.parseInt(strDateBegin.split(":")[1]);

		int strDateEndH = Integer.parseInt(strDateEnd.split(":")[0]);
		int strDateEndM = Integer.parseInt(strDateEnd.split(":")[1]);
		if ((strDateH >= strDateBeginH && strDateH <= strDateEndH)) { // 当前时间小时数在开始时间和结束时间小时数之间
			if (strDateM > strDateBeginM && ((strDateM < strDateEndM) || strDateEndM == 0)) {
				return true; // 当前时间小时数等于开始时间小时数，分钟数在开始和结束之间
			} else if (strDateH == strDateBeginH && strDateM >= strDateBeginM && strDateM <= strDateEndM) {
				return true; // 当前时间小时数等于开始时间小时数，分钟数等于开始时间分钟数，
			} else if (strDateH == strDateBeginH && strDateM == strDateBeginM) {
				return true;
			} // 当前时间小时数大等于开始时间小时数，等于结束时间小时数，分钟数小等于结束时间分钟数
			else if (strDateH >= strDateBeginH && strDateH == strDateEndH && strDateM <= strDateEndM) {
				return true; // 当前时间小时数大等于开始时间小时数，等于结束时间小时数，分钟数等于结束时间分钟数，秒数小等于结束时间秒数
			} else if (strDateH >= strDateBeginH && strDateH == strDateEndH && strDateM == strDateEndM) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/** 把整数秒数转换成时分秒格式显示 */
	public static String secToTime(int time) {
		String timeStr = null;
		int hour = 0;
		int minute = 0;
		int second = 0;
		if (time <= 0)
			return "00:00:00";
		else {
			minute = time / 60;
			if (minute < 60) {
				second = time % 60;
				timeStr = "00:" + unitFormat(minute) + ":" + unitFormat(second);
			} else {
				hour = minute / 60;
				if (hour > 99)
					return "99:59:59";
				minute = minute % 60;
				second = time - hour * 3600 - minute * 60;
				timeStr = unitFormat(hour) + ":" + unitFormat(minute) + ":" + unitFormat(second);
			}
		}
		return timeStr;
	}

	public static String unitFormat(int i) {
		String retStr = null;
		if (i >= 0 && i < 10)
			retStr = "0" + Integer.toString(i);
		else
			retStr = "" + i;
		return retStr;
	}

	/**
	 * 时间字符串1---》时间字符串2
	 * 
	 * @throws Exception
	 */
	public static String transferDateStr(String dateStr, String soureFromat, String objectFromat) throws Exception {
		if (!StringUtil.isEmpty(dateStr)) {
			Date date = convertStringToDate(dateStr, soureFromat);
			if (null != date) {
				return dateFromat(date, objectFromat);
			}
		}
		return "";
	}

	public static String transferDateStr(String dateStr) throws Exception {

		if (!StringUtil.isEmpty(dateStr)) {
			// 时间修改,如果长度为8位 ,将时间转后在将时间转为yyyy-MM-dd;否则直接返回时间 update ryan.lv 20160817
			if (dateStr.length() == 8) {
				Date date = convertStringToDate(dateStr, "yyyyMMdd");
				if (null != date) {
					return dateFromat(date, "yyyy-MM-dd");
				}
			} else {
				return dateStr;
			}

		}
		return "";
	}

	/**
	 * 判断字符串是否属于标准的日期格式 yyyy-MM-dd 考虑了闰年的情况
	 */
	public static boolean isDefaultDateFormate(String dateStr) {
		boolean flag = false;
		if (StringUtil.isEmpty(dateStr)) {
			return flag;
		}
		try {
			// 优先匹配格式
			String eL = "[0-9]{4}-[0-9]{2}-[0-9]{2}";
			Pattern p = Pattern.compile(eL);
			Matcher m = p.matcher(dateStr);
			if (!m.matches()) {
				return m.matches();
			}
			// 再次匹配 正确的日期数字
			eL = "^((\\d{2}(([02468][048])|([13579][26]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])))))|(\\d{2}(([02468][1235679])|([13579][01345789]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))";
			p = Pattern.compile(eL);
			m = p.matcher(dateStr);
			flag = m.matches();
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}

	/**
	 * 根据出生日期获取年龄
	 * 
	 * @param birthdate
	 * @return
	 */
	public static int getAgeByBrithday(Date birthdate) {
		GregorianCalendar currentDay = new GregorianCalendar();
		currentDay.setTime(birthdate);
		int byear, bmonth, bday;
		byear = currentDay.get(Calendar.YEAR);
		bmonth = currentDay.get(Calendar.MONTH) + 1;
		bday = currentDay.get(Calendar.DAY_OF_MONTH);

		GregorianCalendar newCurrentDay = new GregorianCalendar();
		newCurrentDay.setTime(new Date());
		int month = newCurrentDay.get(Calendar.MONTH) + 1;
		int day = newCurrentDay.get(Calendar.DAY_OF_MONTH);
		int age = newCurrentDay.get(Calendar.YEAR) - byear - 1;
		if (bmonth < month || bmonth == month && bday <= day) {
			age++;
		}

		return age;

	}

	public static String formatCSTOrGMTTime(String time) {
		try {
			// 转换时间，如果转换出错则不进行转换
			time = new SimpleDateFormat("yyyy-MM-dd").format(new Date(time));
		} catch (Exception e) {
			try {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.US);
				simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+8"));
				time = new SimpleDateFormat("yyyy-MM-dd").format(simpleDateFormat.parse(time));
			} catch (Exception d) {
				return time;
			}
		}
		return time;
	}

	/**
	 * 获取本年最后一天
	 * 
	 * @throws Exception
	 */

	public static String getThisYearLastDate() {
		return getThisYearLastDate(dateFromat(new Date()));
	}

	/**
	 * 获取本年最后一天 thisDate : yyyy-mm-dd
	 * 
	 * @throws Exception
	 */

	public static String getThisYearLastDate(String thisDate) {
		if (StringUtil.isEmpty(thisDate)) {
			return "";
		}
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(convertStringToDate(thisDate));
			calendar.set(Calendar.MONTH, 11);
			calendar.set(Calendar.DAY_OF_MONTH, 31);
			return dateFromat(calendar.getTime());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	/**
	 * 判断是不是同 一天
	 */
	public static boolean isOneDay(Date date1, Date date2) {
		if (null == date1 || null == date2) {
			return false;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		Calendar calendar2 = Calendar.getInstance();
		calendar2.setTime(date2);
		calendar2.set(Calendar.HOUR_OF_DAY, 0);

		if (calendar2.getTime().getTime() == calendar.getTime().getTime()) {
			return true;
		}
		return false;
	}

	/**
	 * 增加年数
	 */
	public static Date changeDate(Date date, int filed, int change) {
		if (null == date) {
			return date;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(filed, change);
		return calendar.getTime();
	}

	/*
	 * 获取本周第一天
	 */
	public static Date getWeekStartDate() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		Date date = cal.getTime();
		return date;
	}

	/**
	 * 获取本年第一天
	 * 
	 * @throws Exception
	 */

	public static String getThisYearFirstDate() {
		return getThisYearFirstDate(dateFromat(new Date()));
	}

	/**
	 * 获取本年第一天 thisDate : yyyy-mm-dd
	 * 
	 * @throws Exception
	 */

	public static String getThisYearFirstDate(String thisDate) {
		if (StringUtil.isEmpty(thisDate)) {
			return "";
		}
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(convertStringToDate(thisDate));
			calendar.set(Calendar.MONTH, 0);
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			return dateFromat(calendar.getTime());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	/**
	 * 获取本月第一天
	 * 
	 * @throws Exception
	 */

	public static String getThisMonthFirstDate() {
		return getThisMonthFirstDate(dateFromat(new Date()));
	}

	/**
	 * 获取本月第一天 thisDate : yyyy-mm-dd
	 */

	public static String getThisMonthFirstDate(String thisDate) {
		if (StringUtil.isEmpty(thisDate)) {
			return "";
		}
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(convertStringToDate(thisDate));
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			return dateFromat(calendar.getTime());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	/**
	 * 获得当日开始时间
	 * 
	 * @param addDay
	 * @return
	 */
	public static Date getStartTime(int addDay) {
		Calendar todayStart = Calendar.getInstance();
		todayStart.set(Calendar.HOUR_OF_DAY, 0);
		todayStart.set(Calendar.MINUTE, 0);
		todayStart.set(Calendar.SECOND, 0);
		todayStart.set(Calendar.MILLISECOND, 0);
		todayStart.add(Calendar.DAY_OF_MONTH, addDay);
		return todayStart.getTime();
	}

	/**
	 * 获得当日结束时间
	 * 
	 * @param addDay
	 * @return
	 */
	public static Date getEndTime(int addDay) {
		Calendar todayEnd = Calendar.getInstance();
		todayEnd.set(Calendar.HOUR_OF_DAY, 23);
		todayEnd.set(Calendar.MINUTE, 59);
		todayEnd.set(Calendar.SECOND, 59);
		todayEnd.set(Calendar.MILLISECOND, 999);
		todayEnd.add(Calendar.DAY_OF_MONTH, addDay);
		return todayEnd.getTime();
	}

	/**
	 * 
	 * 获取N年的前一天
	 * 
	 * @throws Exception
	 * @param years
	 * @param startDate
	 *            yyyy-mm-dd
	 * @return
	 */
	public static String getAfterNYearBeforeDate(int years, String startDate) {
		if (StringUtil.isEmpty(startDate)) {
			return "";
		}
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(convertStringToDate(startDate));
			calendar.add(Calendar.YEAR, years);
			calendar.add(Calendar.DAY_OF_MONTH, -1);
			return dateFromat(calendar.getTime());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	public static String getNowDate(String pattern) {

		return java.time.LocalDateTime.now().format(DateTimeFormatter.ofPattern(Strings.isNullOrEmpty(pattern) ? DATE_TIME_PATTERN2 : pattern));

	}

	/**
	 * 获取一年后的前一天:即终保时间 startDate : yyyy-mm-dd
	 * 
	 * @throws Exception
	 */

	public static String getAfterYearBeforeDate(String startDate) {
		if (StringUtil.isEmpty(startDate)) {
			return "";
		}
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(convertStringToDate(startDate));
			calendar.add(Calendar.YEAR, 1);
			calendar.add(Calendar.DAY_OF_MONTH, -1);
			return dateFromat(calendar.getTime());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	/**
	 * 判读是否脱保
	 */

	public static boolean isOverdue(Date endDate, String endhours) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(endDate);
		if (dateFromat(new Date()).equals(dateFromat(endDate)) && Integer.parseInt(endhours) < Calendar.getInstance().get(Calendar.HOUR_OF_DAY)) {// 当天
																																					// 脱保
			return true;
		} else if (!dateFromat(new Date()).equals(dateFromat(endDate)) && endDate.getTime() < new Date().getTime()) {// 非当天
																														// 脱保
			return true;
		}
		return false;
	}

	public static void main(String[] args) {

		System.out.println(dateFromat(new Date(), PATTERN_MM_DD_CH));

		System.out.println(DateUtil.dateAdd(2, -1, DateUtil.getStringDate()));

		System.out.println("当时日期字符串：" + new SimpleDateFormat(DATE_TIME_PATTERN2).format(getStartTime(0)));

		System.out.println("当时日期时间戳：" + getStartTime(0).getTime());

		System.out.println("当时日期Timestamp时间戳：" + new Timestamp(getStartTime(0).getTime()).getTime());

		System.out.println("两时间相差天数：" + dateDiff("2018-01-02 00:00:00", new SimpleDateFormat(DATE_TIME_PATTERN2).format(getStartTime(0))));
	}
}
