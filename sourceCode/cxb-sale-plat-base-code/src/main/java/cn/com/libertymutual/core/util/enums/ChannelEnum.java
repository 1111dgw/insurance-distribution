package cn.com.libertymutual.core.util.enums;

public enum ChannelEnum {
	UI平台("insure"),
	第三方接口平台("autumn"),
	核心系统("core"),
	销售管理系统("sale");

	
	private String channelCode;
	private ChannelEnum( String channelCode ) {
		this.channelCode = channelCode;
	}
	
	public String getChannelCode() {
		return channelCode;
	}
}
