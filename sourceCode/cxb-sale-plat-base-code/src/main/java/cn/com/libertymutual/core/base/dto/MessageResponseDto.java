package cn.com.libertymutual.core.base.dto;

public class MessageResponseDto {
	
	private String result;//	相同内容群发请求处理结果： 	0：成功   	非0：失败
	
	private String msgid;//	平台流水号：非0，64位整型，对应Java和C#的long，不可用int解析。result非0时，msgid为0
	
	//	用户自定义流水号：默认与请求报文中的custid保持一致，若请求报文中没有custid参数或值为空，
	//  则返回由梦网生成的代表本批短信的唯一编号
	//  result非0时，custid为空
	private String custid;
	
	private String resultMsg;// 操作成功与失败的描述

	public String getResultMsg() {
		return resultMsg;
	}

	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getMsgid() {
		return msgid;
	}

	public void setMsgid(String msgid) {
		this.msgid = msgid;
	}

	public String getCustid() {
		return custid;
	}

	public void setCustid(String custid) {
		this.custid = custid;
	}
}
