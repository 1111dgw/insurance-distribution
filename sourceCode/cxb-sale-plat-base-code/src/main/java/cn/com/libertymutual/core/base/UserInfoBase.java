package cn.com.libertymutual.core.base;

import java.io.Serializable;

public class UserInfoBase implements Serializable {
	/**
	 * 
	 */
	private String tokenId;
	private String userId;
	private String userCode;
	private String userName;
	private String userType;	//用户类型
	private static final long serialVersionUID = 1L;
	public String getTokenId() {
		return tokenId;
	}
	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	
}
