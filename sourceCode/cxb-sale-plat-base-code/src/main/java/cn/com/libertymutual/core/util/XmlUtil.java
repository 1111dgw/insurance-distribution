package cn.com.libertymutual.core.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * 操作xml工具类
 * @author xrxianga
 * @date 2014-6-17
 */
public class XmlUtil {
	private static final Logger logger = LoggerFactory.getLogger(XmlUtil.class);
	/**
	 * 把xml格式内容转化为java map数据集合对象
	 * @author xrxianga
	 * @param text xml格式字符串
	 * @return Map<String, Object>数据集对象
	 * @throws Exception
	 * @date 2014-6-16下午02:37:12
	 */
	public static Map<String, Object> doElementMapData(String text)throws Exception{
		Document doc = null;
		Element root = null;
		Map<String, Object> map = null;
		try{
			if(!"".equals(CommonUtil.doEmpty(text))){
				doc = DocumentHelper.parseText(text);
				root = doc.getRootElement();
				map = doGetElementMapData(root, root.getName().trim());
			}	
		}catch(Exception e){
			logger.error("------解析xml时报异常，异常消息："+e.getMessage());
			throw new Exception(e.getMessage(),e);
		}finally{
			if(null!=root){
				root.clone();
			}
			if(null!=root){
				doc.clone();
			}
		}
		return map;
	}
	/**
	 * 获取所有非重复元素下的集合
	 * @author xrxianga
	 * @param root
	 * @param name
	 * @return
	 * @throws Exception
	 * @date 2014-6-16下午02:29:25
	 */
	private static Map<String, Object> doGetElementMapData(Element root,String name)throws Exception{
		Map<String, Object> map = new HashMap<String, Object>();
		Iterator<?> iter = root.elementIterator();
		while(iter.hasNext()){
			Element element = (Element) iter.next();
			String nme = name+"-"+element.getName().trim();
			int size = element.elements().size();
			if(size>0){//有子元素
				boolean flag = isRepeat(element);
				if(flag){//有重复项
					map.put(nme,doGetElementListMapData(element, nme));
				}else{
					map.putAll(doGetElementMapData(element, nme));
				}
			}else{
				map.put(nme, element.getTextTrim());
			}
		}
		return map;
	}
	/**
	 * 获取重复元素下所有集合
	 * @author xrxianga
	 * @param root
	 * @param name
	 * @return
	 * @throws Exception
	 * @date 2014-6-16下午02:29:15
	 */
	private static List<Map<String, Object>> doGetElementListMapData(Element root,String name)throws Exception{
		List<Map<String, Object>> listMap = new ArrayList<Map<String,Object>>();
		Map<String, Object> map = new HashMap<String, Object>();
		Iterator<?> iter = root.elementIterator();
		while(iter.hasNext()){
			Element element = (Element) iter.next();
			String nme = name+"-"+element.getName().trim();
			int size = element.elements().size();
			if(size>0){//有子元素
				boolean flag = isRepeat(element);
				if(flag){//有重复项
					listMap.addAll(doGetElementListMapData(element, nme));
				}else{
					listMap.add(doGetElementMapData(element, nme));
				}
			}else{
				map.put(nme, element.getTextTrim());
			}
		}
		if(!map.isEmpty()){
			listMap.add(map);
		}
		return listMap;
	}
	/**
	 * 判断是否有重复元素
	 * @author xrxianga
	 * @param root
	 * @return
	 * @throws Exception
	 * @date 2014-6-16下午02:28:59
	 */
	private static boolean isRepeat(Element root)throws Exception{
		int k = 0;
		String num[] = new String[root.elements().size()];
		Iterator<?> iter = root.elementIterator();
		while(iter.hasNext()){
			Element element = (Element) iter.next();
			num[k]=element.getName().trim();
			k++;
		}
		for (int i = 0; i < num.length; i++) {
			for (int j = i+1; j < num.length; j++) {
				String one = num[i].trim();
				String two = num[j].trim();
				if(one.equals(two)){
					return true;
				}
			}
		}
		return false;
	}
}
