package cn.com.libertymutual.core.web.util;

import org.springframework.util.PathMatcher;

public class PathMatcherUtil {

	private String[] patterns = new String[0];
	private PathMatcher pathMatcher;

	public PathMatcherUtil() {
		
	}

	public PathMatcherUtil( String[] patterns ) {
		this.patterns = patterns;
	}
	
	public void setPatterns( String[] patterns ) {
		this.patterns = patterns;
	}
	
	public void addPatterns( String pattern ) {
		String[] patterns2 = new String[this.patterns == null ? 0 : this.patterns.length+1];
		
		if( patterns2 == null || patterns2.length < 1 ) return;
		
		System.arraycopy(this.patterns, 0, patterns2, 0, this.patterns.length);
		patterns2[this.patterns.length] = pattern;
		
		this.patterns = patterns2;
	}
	
	/*public void addPatterns( String[] pattern ) {
		String[] patterns2 = new String[patterns == null ? 0 : patterns.length+1];
		
		System.arraycopy(patterns, 0, patterns2, 0, patterns.length);
		System.arraycopy(pattern, 0, patterns2, patterns.length, pattern.length);
	}*/

	public void setPathMatcher( PathMatcher pathMatcher ) {
		this.pathMatcher = pathMatcher;
	}
	
	public boolean matches(String lookupPath, PathMatcher pathMatcher) {
		PathMatcher pathMatcherToUse = (this.pathMatcher != null) ? this.pathMatcher : pathMatcher;
		if (this.patterns != null) {
			for (String pattern : this.patterns) {
				if (pathMatcherToUse.match(pattern, lookupPath)) {
					return true;
				}
			}
		}
		
		return false;
	}
}
