package cn.com.libertymutual.core.util.enums;

public enum CoreServiceEnum {

	/**
	 * 用户登录信息
	 */
	USER_LOGIN("userLoginInfo"),
	/**
	 * 保存方案
	 */
	COMMON_REQUEST("commonRequest"),
	/**
	 * 本地回调url
	 */
	CALLBACK_URL("callbackUrlZypt"),
	/**
	 * 角色信息查询
	 */
	ROLE_QUERY("roleInfo"),
	/**
	 * 保单查询
	 */
	POLICY_QUERY("prpallVisaInfo"),
	/**
	 * 保单打印
	 */
	POLICY_PRINT("MotorPrint"),
	/**
	 * 打印状态查询
	 */
	POLICY_PRINT_STATUS("MotorPrintStauts"),
	/**
	 * VIN查询
	 */
	VIN_QUERY("MotorVinnoQuery"),
	/**
	 * 车型查询
	 */
	MOTOR_MODELCODE_QUERY("MotorModelcodeQuery"),
	/**
	 * 客户信息更新
	 */
	CUSTOMERINFO_UPDATE("CustomerInfoUpdate"),
	/**
	 * 客户信息查询
	 */
	CUSTOMERINFO_QUERY("CustomerInfoQuery"),
	/**
	 * 上传文件到eFiling
	 */
	UPLOAD_DOC_EFILING("eFilingUploadDocument"),
	/**
	 * 保存上传资料信息
	 */
	SAVE_DOC_EFILING("eFilingSaveDocument"),
	/**
	 * 支付完成后的通知地址
	 */
	EPAYMENT_CALLBACK_URL("ePaymentCallbackUrl"),
	/**
	 * 支付完成跳转的URL
	 */
	EPAYMENT_REDIRECT_URL("ePaymentRedirectUrl"),
	/**
	 * 网银支付地址
	 */
	EPAYMENT_SEND_URL("ePaymentSendUrl"),
	/**
	 * 资料查看
	 */
	VIEW_DOCUMENT("eFilingViewDocument"),
	/**
	 * 支付状态查询
	 */
	PAYMENT_STATUS_QUERY("PaymentStatusQuery"),
	/**
	 * 支付方式修改
	 */
	PAYMENT_METHOD_UPDATE("PaymentMethodUpdate"),
	/**
	 * 支付信息查询
	 */
	PAYMENT_INFO_QUERY("PaymentInfoQuery"),
	/**
	 * 补登信息查询
	 */
	EPAYMENT_INFO_QUERY("EPaymentInfoQuery"),

	/**撤销
	 * */
	Proposal_Recall("ProposalRecall"),

	/**平台交互
	 * */
	Platform_Interaction("platformInteraction"),
	/**
	 * 产品查询
	 */
	Production_Query("ProductionQuery"),

	/**
	 * 保单详情查询
	 */
	Policy_Detail_Inquiry("PolicyDetailInquiry"),
	/**
	 * 微门店保单详情查询
	 */
	WX_Policy_Detail("WXPolicyDetail"),

	/**
	 * 续保查询
	 */
	Renewal_Query("RenewalQuery"),
	/**
	 * 车险Token认证
	 */
	Car_Token("CarToken"),
	/**
	 * 投保车辆信息查询
	 */
	Query_Vehicle("QueryVehicle"),
	/**
	 * 商业险转保
	 */
	Proposal_Vertify("ProposalVertify"),
	/***
	 * 交强险转保
	 */
	MtplProposal_Vertify("MtplProposalVertify"),
	/***
	 * 保费计算
	 */
	Premium_CalCar("MotorPremiumCalCar"),
	/***
	 * 转保
	 */
	VERIFY("Verify"),
	/***
	 * 投保提核
	 */
	Proposal_Save("proposalSave"),
	/**
	 * 文件服务器地址
	 */
	FILE_SERVER_URL("FileServerUrl"),
	/**
	 * 发票打印服务器地址
	 */
	INVOICE_SERVER_URL("VatServerUrl"),
	/**
	 * 核保信息URL
	 */
	UNDWRT_INFO_URL("undwrt"),

	/**
	 * 身份信息采集
	 */
	Indentity_Collect_URL("IndentityCollect"),
	/**
	 * 身份信息采集 电销
	 */
	Indentity_Collect_DX_URL("IndentityCollect_DX"),
	/**
	 * 新车备案
	 */
	NewCar_Record_RUL("NewCarRecord"),
	/**
	 * 北京车型查询
	 */
	BJMotorMode_Query_URL("BJMotorModeQuery"),
	/**
	 * 身份信息无线查询
	 */
	CardInfo_Query_URL("CardInfoQuery"),
	/**
	 * 验证码输入
	 */
	VericodeInput_URL("VericodeInput"),
	/**
	 * 配送接口
	 */
	Distribute_URL("Distribute"),

	/**
	 * 短地址服务查询地址
	 */
	SHORT_URL_M("ShortURL_M"),
	/**
	 * 日志推送地址
	 */
	LOG_MONITOR_PUSH_URL("LogMonitorPush"),
	/**
	 * 北京电子保单生成
	 */
	YBPrintUpload_URL("ybPrintUpload"),
	/**
	 * 黑名单
	 */
	QueryBlackList_URL("queryBlackList"),
	/**
	 * 快递查询
	 */
	LbexpressTrackQuery_URL("lbexpressTrackQuery"),

	/**
	 * 手机号校验
	 */
	Valition_Phone_URL("ValitionPhone"), NomSubmitUnderWrite_2724_URL("nomSubmitUnderWrite_2724"),
	/**
	 * 数据预填
	 */
	DataPreFill_URL("dataPreFill"),

	/**
	 * 
	 */
	SmsCallBack_URL("SmsCallBack"),

	/**
	 * 发送短信梦网地址信息
	 */
	Sms_Address("SmsAddress"),
	/**
	 * 电子发票打印
	 */
	Print_Eletronic_Invoice("PrintEletronicInvoice"),
	/**
	 * 电子发票查询
	 */
	Query_Eletronic_Invoice("QueryEletronicInvoice"),
	/**
	 * 北京电子投保相关地址
	 */
	BJProposalinform_Address("Proposalinform"),

	/**
	 * 非车电子保单下载
	 */
	NomEPolicyDown_URL("NomEPolicyDown"),
	/**
	 * 短息发送
	 */
	SendMsg_URL("SendMsg"),
	/**
	 * 查询业务关系代码
	 */
	FindAgentInfoByAgreementCode_URL("FindAgentInfoByAgreementCode"),
	/**
	 * 女生一号计划查询
	 */
	QUERY_SPPLANS_URL("QuerySPPlans"),
	/**
	 * 交叉销售计划查询
	 */
	Cross_Sale_Plan("CrossSalePlan"),
	/**
	 * 交叉销售计划查询保存
	 */
	Cross_Sale_Plan_Save("CrossSalePlanSave"),
	/**
	 * 交叉销售计划查询查找
	 */
	Cross_Sale_Plan_Find("CrossSalePlanFind"),
	/**
	 * 安行天下
	 */
	AXTX_2724_INSURE("Axtx2724Insure"),
	/**
	 * 查询vin
	 */
	FIND_VIN("QueryVin"),
	/**
	 * 报案
	 */
	Report_CLAIMS_URL("ReportClaims"),
	/**
	 * 理赔计划查询
	 */
	QUERY_CLAIMS_URL("QueryClaims"),
	/**
	 * 查询保单
	 */
	QUERY_POLICY_URL("QueryPolicys"),
	/**
	 * 查询旅行目的地
	 */
	TRAVEL_DESTINATION_URL("TravelDestination"),
	/**
	 * 查询单据详情
	 */
	QUERY_DETAIL_URL("QueryDetail"),
	/**
	 * 查询投保单
	 */
	QUERY_Proposal_URL("QueryProposal"),
	/**
	 * 查询保单状态
	 */
	QUERY_POLICYSTATUS_URL("QueryPolicyStatus"),
	/**
	 * 查询投保单状态
	 */
	QUERY_PROPOSALSTATUS_URL("QueryProposalStatus"),
	/**
	 * 理赔报案查询
	 */
	QUERY_Report_URL("QueryReports"),
	/**
	 *待支付列表查询
	 */
	QUERY_PAYLIST_URL("QueryPayList"),
	/**
	 * 回调URL
	 */
	CALL_BACK_URL("CallbackUrl"),
	/**
	 *身份验证
	 */
	IDValidate_URL("IDValidate"),
	/**
	 * 北京电子保单下载
	 */
	Down_BJPolicy_URL("DownBJPolicy"),

	/**
	 * 获取销售人名称
	 */
	QUERY_SALE_NAME("QUERY_SALE_NAME"),

	/**
	 * 获取职业类别
	 */
	QUERY_PRPD_CODE("QueryPrpdCode"),

	/**
	 * 获取微信用户验证信息
	 */
	QUERY_RULE_MSG("QueryRuleMsg"),

	/**
	 * epayment 快钱 提现
	 */
	EPAYMENT_CASH_URL("EpaymentCash"),
	/**
	 * MQ积分提现
	 */
	MQ_CASH_URL("MQ_Cash"),
	/**
	 * 快钱交易查询
	 */
	EPAYMENT_QUERYCNP_URL("EpaymentQueryCnp");

	private String coreServiceUrlKey;

	private CoreServiceEnum(String coreServiceUrlKey) {
		this.coreServiceUrlKey = coreServiceUrlKey;
	}

	public String getUrlKey() {
		return coreServiceUrlKey;
	}
}
